module RedirectFromEmail
  extend ActiveSupport::Concern
  included do    
  end

  def redirect_from_go_to_url
    id_syllabus_program = params[:id_syllabus_program]
    redirect_to "/syllabuses/syllabus_programs/#{id_syllabus_program}"
  end  

  def set_redirect_if_came_from_email
    #if request contain go_to this petition came from email send to app
    if request.original_url.include? ("go_to")
      id_syllabus_program = params[:id_syllabus_program]
      if params[:approver_id].present?
        user = User.find(params[:approver_id])
        # set session value if has role approver
        session[:rol_to_use] = ROLE_APPROVER  if user.is_approver? 
      end  
  
      session["user_return_to"] = syllabus_program_path(id_syllabus_program)
    end  
  end
  
  def original_url
    base_url + original_fullpath
  end

private
  # to redirect after login if exist session user_return_to
  def after_sign_in_path_for(resource)
    session["user_return_to"] || root_path
  end


end 