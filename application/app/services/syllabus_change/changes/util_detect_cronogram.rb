module SyllabusChange
  module Changes
    module UtilDetectCronogram
      def iterate_audit_and_set_hash_values_syllabus()
        cant_state = 0
        @objects_audit.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            puts "KEY encontrado #{key}"
            cant_state = increment_cant_state(cant_state, key, value)
            if has_more_than_one_state # state (to_be_approve)
              return false if find_second_state?(cant_state)
            else
              value_audit = get_last_value_of_audit(value)
              return false if ((key == "state") && (value_audit == TO_BE_APPROVED_STRING))
            end
            set_hash_value(key, value)
          end
        end
      end

      # this method will be overwritten in inherited clases like ScheduleActivityChange example
      def set_hash_value(key, value) end

      def increment_cant_state(cant_state, key, value)
        value_audit = get_last_value_of_audit(value)
        cant_state += 1 if ((key == "state") && (value_audit == TO_BE_APPROVED_STRING))
        cant_state
      end

      def find_second_state?(cant_state)
        true if cant_state == 2
      end

      def count_changes_if_exist_changes_between_values(value_one, value_two, key_name)
        if second_parameter_contain_data?(value_one, value_two)
          @changes_detected += 1 if value_one != value_two["#{key_name}"]
        end
      end

      def second_parameter_contain_data?(value_one, value_two)
        unless value_two.present?
          @changes_detected += 1
          return false
        else
          return true
        end
      end

      def iterate_fields_and_count_changes(options = {})
        index = options[:index]
        object = options[:object]
        last_change_object = options[:last_change_object]
        last_change_object ||= []
        array_fields = options[:array_fields]
        array_fields.each do |name_field|
          count_changes_if_exist_changes_between_values(
            object["#{name_field}"],
            last_change_object[index],
            "#{name_field}"
          )
        end
      end

      def size_of_array_are_diferent?(array_one, array_two)
        size_one_array = get_size_if_is_valid_array(array_one)
        size_array_two = get_size_if_is_valid_array(array_two)
        if (size_one_array != size_array_two)
          return true
          @changes_detected += 1
        end
        false
      end

      def get_size_if_is_valid_array(array_value)
        return 0 if array_value == "{}"
        array_value.to_a.size
      end
    end
  end
end
