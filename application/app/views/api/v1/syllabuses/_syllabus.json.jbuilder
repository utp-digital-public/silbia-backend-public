json.id                       syllabus.id
json.course do
    json.partial! "api/v1/syllabuses/course", course: syllabus.course
end
json.weekly_hours             syllabus.weekly_hours
json.credits                  syllabus.credits
json.cycle                    syllabus.cycle
json.syllabus_content         syllabus.syllabus_content
json.year                     syllabus.year