FactoryBot.define do
  factory :doc_template do
    name { "Plantilla 2019" }
    type_of_template { "silabus" }
    is_active { false }
  end
end
