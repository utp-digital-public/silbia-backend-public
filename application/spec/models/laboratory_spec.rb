require 'rails_helper'

RSpec.describe Laboratory, type: :model do
  it { should validate_presence_of(:name).with_message("es requerido")}
  it { should validate_presence_of(:type_laboratory).with_message("es requerido")}
end
