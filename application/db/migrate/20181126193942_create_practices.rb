class CreatePractices < ActiveRecord::Migration[5.2]
  def change
    create_table :practices do |t|
      t.string :description, null:false , default: ""
      t.string :code, null:false

      t.timestamps
    end
  end
end
