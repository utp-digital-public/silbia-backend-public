class CurriculumCourse < ApplicationRecord
  validates :year, :modality_program, :desc_grade, :faculty_code,:faculty_name,
    :program_academic, :academic_plan_code, :career_code, :career_name, :cycle,
    :course_catalog_code,:course_name,:course_catalog_cgt_code,:credits,
    :weekly_hours,presence: { message: 'es requerido'}
end
