json.extract! doc_template, :id, :name, :type_of_template, :is_active, :created_at, :updated_at
json.url doc_template_url(doc_template, format: :json)
