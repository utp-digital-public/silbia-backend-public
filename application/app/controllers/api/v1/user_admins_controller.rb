class Api::V1::UserAdminsController < Api::ApiV1AdminController
  before_action :set_user_admin, only: [:show, :update, :destroy]
  before_action :set_order_params, only: [:index]

  def index
    @user_admins = UserAdmin
    @user_admins = @user_admins.order(@sort) unless @sort.blank?
    @user_admins = @user_admins.page(params[:page])
  end

  def create
    @user_admin = UserAdmin.new(user_admin_params)
    if @user_admin.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @user_admin.errors) and return
    else
      @user_admin.save!()
    end
  end

  def show
  end

  def update
    @user_admin.assign_attributes(user_admin_params)
    if @user_admin.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @user_admin.errors) and return
    else
      @user_admin.save!()
    end
  end

  def destroy
    total_admins = UserAdmin.count()
    if (@user_admin.id != @current_user.id && total_admins > 1)
      @user_admin.destroy
    elsif (total_admins == 1)
      render_bad_request("No se puede eliminar al único administrador") and return
    else
      render_bad_request("No se puede eliminar al administrador actual") and return
    end
  end

  private

  def user_admin_params
    params.permit(:email, :first_name, :last_name, :password)
  end

  def set_user_admin
    @user_admin = UserAdmin.find(params[:id])
  end

  def set_order_params
    order = if params[:order].present? then params[:order] else "asc" end
    @sort = if params[:sort].present? then params[:sort] + " " + order else :first_name end
  end
end
