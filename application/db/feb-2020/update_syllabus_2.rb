require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'feb_2020_update_aprober_syllabus_2.csv'))
puts "UPDATE APPROBER AND EDITOR TO SYLLABUSES) "
csv = CSV.parse(csv_text.scrub, headers: true)

customPrompt = Import::CustomPrompt.new
cycle_id = customPrompt.choice_cycle

import_obj = Import::ConvertToJson.new
cant_rows = 0
csv.each_with_index do |row, index|
  cant_rows = cant_rows+1
  email_supervisor = row["email_supervisor"]
  email_coordinator = row["email_coordinator"]
  catalog_code = row["catalog_code"]
  type_program = row["type_program"]
  supervisor = import_obj.search_user_by_email(email_supervisor)
  coordinator = import_obj.search_user_by_email(email_coordinator)
  course = import_obj.search_course_by_catalog_code(catalog_code)
  syllabus = Syllabus.where(course_id: course.id).where(cycle_id: cycle_id).first
  syllabus_program_obj = syllabus.syllabus_programs.where(type_program: type_program).first

  syllabus_program_obj.update_attributes!(coordinator_id: coordinator.id, supervisor_id: supervisor.id)

  puts  "NroFilaExcel: #{index+1} | SilabusProgramId: #{syllabus_program_obj.id} | Nombre Super: #{supervisor.name}  | Nombre Coordinador: #{coordinator.name} | Email Coordinato: #{email_coordinator}"
end
puts "Update #{cant_rows} records"