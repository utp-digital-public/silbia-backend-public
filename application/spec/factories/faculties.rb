FactoryBot.define do
  factory :faculty do
    code { "MyString" }
    name { "MyString" }
  end
end
