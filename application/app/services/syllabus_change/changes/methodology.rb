module SyllabusChange
  module Changes
    class Methodology < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::DetectChangeSyllabusContent
      attr_accessor :syllabus, :auditable_objects
      def initialize(**attribute)
        super(**attribute)
        attribute.each do |k,v|
          instance_variable_set("@#{k}", v) unless v.nil?
        end  
        @title = "Metodologia"
        @id_section = "section-metodologia"
        @changes_detected = 0
        @is_edited = false
      end

      def call
        key_to_extract = "metodología"
        current_methodology = SyllabusChange::Changes::ExtractContentText.extract_to(
          @syllabus_program.program_json, 
          key_to_extract
        )
        if @has_more_than_one_state== true
          @hash_values[:cronogram_methodology] = set_hash_value_when_has_more_than_one_state()
        else
          @hash_values[:cronogram_methodology] = 
            set_hash_values_when_have_less_than_one_state("program_json",@objects_audit)
        end  
        @is_edited = detect_change_in_methodology(current_methodology)
      end

      def set_hash_value_when_has_more_than_one_state
        iterate_audit_and_set_hash_values_syllabus()
        @hash_values[:cronogram_methodology]
      end  

      def detect_change_in_methodology(current_methodology)
        return false unless @hash_values[:cronogram_methodology].present?  
        with_changes = false
        last_change_methodology = @hash_values[:cronogram_methodology]["texts"]
        value_methodology = last_change_methodology[0]["content"]
        puts "EN METODOLOGIA"
        puts "CURRENT: . #{current_methodology} y ANTIGUO #{value_methodology}"
        with_changes = true  if  current_methodology!= value_methodology 
        with_changes
      end 
      
      def set_hash_value(key,value) 
        @hash_values[:cronogram_methodology] = get_value_before_the_last_change(value) if key == "program_json"
      end
      
    end 
  end  
end      