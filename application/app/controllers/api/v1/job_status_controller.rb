class Api::V1::JobStatusController < Api::ApiV1AdminController
  before_action :set_job_status, only: [:show]

  def show
  end

  private

  def set_job_status
    @job_status = JobStatus.find(params[:id])
  end
end
