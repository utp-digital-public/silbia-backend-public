module SyllabusChange
  module Changes
    class GeneralChange
      include SyllabusChange::Changes::UtilDetectCronogram
      include SyllabusChange::Util::UtilSyllabusContent
      attr_accessor :title, :is_edited, :id_section, :syllabus_program, :objects_audit, :hash_values,
                    :has_more_than_one_state

      def initialize(**attributes)
        attributes.each do |k, v|
          instance_variable_set("@#{k}", v) unless v.nil?
          @is_edited = false
          @hash_values = Hash.new
        end
      end

      def to_hash
        {
          title: @title,
          is_edited: @is_edited,
          id_section: @id_section,
        }
      end

      # def set_hash_values_when_have_less_than_one_state(text_key_to_compare, objects_audit)
      #   objects_audit.try(:each) do |audit|
      #     audit.audited_changes.each do |key,value|
      #       if key == text_key_to_compare
      #         before_value =  get_value_before_the_last_change(value)
      #         value_audit = get_last_value_of_audit(value)
      #         return before_value
      #       end
      #     end
      #   end
      #   ""
      # end

    end
  end
end
