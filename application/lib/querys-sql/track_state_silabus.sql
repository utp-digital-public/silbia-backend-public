SELECT
   courses.catalog_code,
   courses."name",
   users."name",
   /*tracking_status_syllabuses.created_at,*/
   (tracking_status_syllabuses.created_at - interval '5 hour') as fecha_edicion, 
   CASE
      WHEN
         tracking_status_syllabuses.state = 1 
      THEN
         'Pendiente' 
      WHEN
         tracking_status_syllabuses.state = 2 
      THEN
         'En edición' 
      WHEN
         tracking_status_syllabuses.state = 3 
      THEN
         'Por aprobar' 
      WHEN
         tracking_status_syllabuses.state = 4 
      THEN
         'Aprobado' 
      WHEN
         tracking_status_syllabuses.state = 5 
      THEN
         'Rechazado' 
      ELSE
         'Sin estado' 
   END
   AS ESTADO 
from
   tracking_status_syllabuses 
   inner join
      syllabus_programs 
      on tracking_status_syllabuses.syllabus_program_id = syllabus_programs.id 
   inner join
      syllabuses 
      ON syllabus_programs.syllabus_id = syllabuses.id 
   inner join
      courses 
      ON syllabuses.course_id = courses.id 
   inner join
      users 
      ON tracking_status_syllabuses.user_id_track = users.id
order by catalog_code, fecha_edicion      
