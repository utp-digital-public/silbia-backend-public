require 'swagger_helper'

RSpec.describe 'api/v1/dashboard', type: :request do

  path '/api/v1/dashboard/filter_syllabuses' do
    get(summary: 'filter_syllabuses dashboard') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/dashboard' do
    get(summary: 'list dashboards') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end
end
