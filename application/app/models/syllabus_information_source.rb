class SyllabusInformationSource < ApplicationRecord
  audited associated_with: :syllabus
  belongs_to :syllabus
  belongs_to :book
  has_many :unit_information_sources, :dependent => :destroy, inverse_of: :syllabus_information_source
  has_many :units, :through => :unit_information_sources

  scope :order_by_type_source, -> { order("type_source ASC") }
  scope :with_syllabus_and_type_source, ->(syllabus_id, type_source) {
          where(syllabus_id: syllabus_id)
            .where(type_source: type_source)
        }
  scope :collection_by_ids, ->(arr_ids) {
          where("id IN (?)", arr_ids).includes(:unit_information_sources)
        }

  scope :all_relations_included, -> {
          includes(:book).includes(:syllabus).includes(:unit_information_sources)
        }

  scope :information_sources_by_syllabus, ->(syllabus_id) {
          where(syllabus_id: syllabus_id)
        }
  def self.basic
    where(type_source: "Básica")
  end

  def self.complementary
    where(type_source: "Complementaria")
  end
end
