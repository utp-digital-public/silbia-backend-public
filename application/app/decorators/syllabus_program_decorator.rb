class SyllabusProgramDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def edit_show_button_syllabus
    if ( (h.current_user.is_approver?) and (model.supervisor_id == h.current_user.id) )
       h.link_to "Revisar", h.syllabus_program_path(syllabus_program), class: "btn btn-primary outline", :"data-turbolinks" => false
    elsif ( (h.current_user.is_creator?) && (model.coordinator_id == h.current_user.id ) )
      if model.may_approve_silabus?
        h.link_to "Revisar", h.syllabus_program_path(syllabus_program), class: "btn btn-primary outline", :"data-turbolinks" => false
      else
        h.link_to "Editar", h.edit_syllabus_program_path(syllabus_program), class: "btn btn-primary outline", :"data-turbolinks" => false
      end
    end  
  end

  def state_syllabus_html
    state = model.state.downcase
    label =""  
    case state
      when NO_READ_STATE_STRING
        class_css = "pending_status"
        label = "Pendiente"
      when EDITION_STATE_STRING
        class_css = "in_edition"
        label = "En Edición"     
      when TO_BE_APPROVED_STRING
        class_css = "in_revision"
        label = "En Revisión"
      when APPROVED_STATE_STRING
        class_css = "approved"
        label = "Aprobado"
      when REJECTED_STATE_STRING
        class_css ="observed"
        label ="Observado"     
    end
    class_css ||= "observed"
    "<span class='state #{class_css}'> #{label.upcase} </span>".html_safe
  end
  
  

  def coordinator_html
    "<td class='show_inline'> 
      <div class='user-img'> #{h.avatar_image(model.coordinator)} </div>  
        <span>#{model.coordinator.name} </span> </td> ".html_safe if h.current_user.is_approver?
  end

  def weekly_hours_message 
    if !model.weekly_hours.nil? && model.weekly_hours > 0
      return weekly_hours
    end
    "(Dato Pendiente)"  
  end  
  
end
