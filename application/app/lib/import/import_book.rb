module Import
  class ImportBook < Import::Entity
    def import_book_if_not_exist
      import_obj = Import::ConvertToJson.new
      total_records_create = 0
      @csv.each_with_index do |row, index|
        code = row['code'].delete(' ')
        book_find = import_obj.search_book_by_code(code)
        unless book_find.present?
          book = build_book(row)
          book.save!
          total_records_create = total_records_create+1
          puts "#{book.code}, #{book.title} saved"
        end  
      end
      puts "New records was  #{total_records_create} created"
      puts "There are now #{Book.count} rows in the transactions table"
    end

    def build_book(row)
      book = Book.new
      book.code = row['code']
      book.title = if row['title'].nil?  then "No aplica" else row['title'] end
      book.author = row['autor']
      book.support = row['support']
      book.year = if row['year'].present? then row['year'].to_i else 0 end
      book.number_editorial = row['number_editorial']
      book.editorial = row['editorial']
      is_acquired = row['is_acquired']
      if is_acquired.present?
        book.is_acquired = if is_acquired == "SI" then true else false end
      else
        book.is_acquired = nil    
      end
      book  
    end  
    
  end  
end  