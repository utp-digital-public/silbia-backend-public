class AddFlagsToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :flags, :jsonb, default: {}
  end
end
