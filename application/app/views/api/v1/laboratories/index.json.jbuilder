json.status "success"
json.data do
  json.array! @laboratories do |laboratory|
    json.id laboratory.id
    json.name laboratory.name
    json.type_laboratory laboratory.type_laboratory
  end
end
