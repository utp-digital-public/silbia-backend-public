#TODO: add test with capybara to interaction with software.
class ApplicationController < ActionController::Base
  include RedirectFromEmail
  # Pundi Authorization filtros
  include Pundit
  include SentryConf
  #after_action :verify_authorized, unless: :devise_controller? ,  #except: :index
  #to catch message error Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def append_info_to_payload(payload)
    super
    payload[:host] = request.host
    payload[:remote_ip] = request.remote_ip
    payload[:ip] = request.ip
  end
   
  def set_attr_to_current_user 
    current_user.rol_to_use_in_session = session[:rol_to_use] 
  end  

  # show message not authorized
  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
 
    flash[:error] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: "No tiene permiso para realizar esta acción"
    redirect_to(request.referrer || root_path)
  end

  def setHeaderExportExcel(name_file)
    headers['Content-Type'] = "application/vnd.ms-excel; charset=UTF-8; charset=ISO-8859-1; header=present"
    headers['Content-Disposition'] = 'attachment; filename="' + name_file +'.xls"'
    headers['Cache-Control'] = ''  
  end

protected
  def layout_by_resource
    if devise_controller?
      "login"
    else
      "application"
    end
  end

  def session_expirada
    unless user_signed_in?
      set_redirect_if_came_from_email
      redirect_to "/users/sign_in"   
    end 
  end
end
