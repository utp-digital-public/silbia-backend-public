module Import
  class EvaluationSystem < ComponentSyllabus
    attr_accessor :type, :description, :recoverable, :weight, :observations, :rules

    def as_json(options={})
      {
        type: @type,
        description: @description,
        weight: @weight,
        observations: @observations,
        recoverable: @recoverable,
        section_template_id: @section_template_id
      }
    end

  end
end