require 'rails_helper'

RSpec.describe Book, type: :model do
  describe 'relaciones' do
    it { should have_many(:courses).through(:information_sources)}
  end
  it { should validate_presence_of(:code).with_message("es requerido")}
  it { should validate_presence_of(:title).with_message("es requerido")}
  it { should validate_numericality_of(:year).with_message("debe ser un numero")}

  describe "validate unique code book" do
    subject { Book.new(code:"BOOK23456",title: "mi libro",author:"Juan Perez", year:2018) }
    it { should validate_uniqueness_of(:code).ignoring_case_sensitivity.with_message("ya existe") }
  end  
end
