class AddIndexSupervisorToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_index :syllabus_programs, :supervisor_id
  end
end
