# this class use the gem activerecord-import to insert masive 
module Admin
  class AuditableSyllabusService < Admin::AuditableService
    def initialize(list_objects, auditable_type, comment=nil)
      super(list_objects)
      @auditable_type = auditable_type
      @comment = comment
    end

    def build_array_objects()
      @list_objects.try(:each) do |object|
        auditable = Audited::Audit.new
        auditable.assign_attributes({
          auditable_type: @auditable_type,
          auditable_id: object.id,
          username: "Administrator",
          version: @version,
          action: @action,
          request_uuid: SecureRandom.uuid,
          audited_changes: object.attributes,
          comment: @comment

        })
        @arr_objects << auditable
      end  
    end  

    def save_masive()
      build_array_objects()
      Audited::Audit.import @arr_objects
    end

  end
end  

