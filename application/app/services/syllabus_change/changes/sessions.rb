module SyllabusChange
  module Changes
    class Sessions < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::UtilDetectCronogram

      def initialize(**attribute)
        super(**attribute)
        attribute.each do |k, v|
          instance_variable_set("@#{k}", v) unless v.nil?
        end
        @title = "Cronograma"
        @id_section = "section-sessions-topics"
        @changes_detected = 0
        @is_edited = false
        @syllabus = @syllabus_program.syllabus
      end

      def detect()
        @is_edited = detect_changes_sessions_and_session_topics?(@last_date)
      end

      def detect_changes_sessions_and_session_topics?(date_to_filter)
        changes_detectd_sessions = sessions_have_changes?(date_to_filter)
        return true if changes_detectd_sessions
        changes_detectd_session_topics = session_topics_have_changes?(date_to_filter)
        return true if changes_detectd_session_topics
        return false
      end

      def sessions_have_changes?(date_to_filter)
        #action_update_create
        audits = Audited.audit_class.where(
          auditable_id: @syllabus_program.sessions.pluck(:id),
          auditable_type: "Session",
          action: ["update", "create"],
          user_id: @syllabus_program.coordinator_id,
        )
        audits = audits.where("created_at>=?", date_to_filter).reorder("created_at DESC")
        keys_to_compare = ["laboratory_id", "session_number", "modality_id"]
        audits.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            return true if compare_if_changes_ocurred?(key, value, keys_to_compare)
          end
        end
        #action_deleted
        audits = Audited.audit_class.where(
          auditable_type: "Session",
          action: "destroy",
          associated_type: "SyllabusProgram",
          associated_id: @syllabus_program.id,
          user_id: @syllabus_program.coordinator_id,
        )
        audits = audits.where("created_at>=?", date_to_filter).reorder("created_at DESC")
        return true if audits.size > 0
        false
      end

      def session_topics_have_changes?(date_to_filter)
        #update_create_action
        audits = Audited.audit_class.where(
          auditable_id: @syllabus_program.session_topics.pluck(:id),
          auditable_type: "SessionTopic",
          user_id: @syllabus_program.coordinator_id,
        )
        audits = audits.where("created_at>=?", date_to_filter).reorder("created_at DESC")
        keys_to_compare = ["session_id", "topic_id", "activities"]
        audits.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            return true if compare_if_changes_ocurred?(key, value, keys_to_compare)
          end
        end
        #delete_action
        audits = Audited.audit_class.where(
          auditable_type: "SessionTopic",
          associated_type: "Session",
          associated_id: @syllabus_program.sessions.pluck(:id),
          action: "destroy",
          user_id: @syllabus_program.coordinator_id,
        )
        audits = audits.where("created_at>=?", date_to_filter).reorder("created_at DESC")
        return true if audits.size > 0
        false
      end

      def compare_if_changes_ocurred?(key, value, keys_to_compare)
        if keys_to_compare.include? key
          return true unless value.kind_of?(Array)
          puts "hasta aqui entro comparar"
          value_before_change = get_value_before_the_last_change(value)
          value_after_change = get_last_value_of_audit(value)
          puts " Su key es #{key} El valor anterior es: #{get_value_before_the_last_change(value)} El valor nuevo es : #{get_last_value_of_audit(value)} El ultimo : #{value}"
          if key == "activities"
            value_before_change = value_before_change.map { |i| i.slice("description", "type_activity", "label_description") }
            value_after_change = value_after_change.map { |i| i.slice("description", "type_activity", "label_description") }
            puts "Su key es #{key} El valor anterior es: #{value_before_change} El valor nuevo es : #{value_after_change}"
          end

          if (value_before_change != value_after_change)
            return true
          end
        end
      end

      # This method overrite
      def set_hash_value(key, value)
        @hash_values[:information_source] = get_value_before_the_last_change(value) if key == "program_json"
      end
    end
  end
end
