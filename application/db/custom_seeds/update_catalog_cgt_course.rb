require 'csv'
### Migrate COURSE from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'courses.csv'))
csv = CSV.parse(csv_text.scrub, headers: true)

import_obj = Import::ConvertToJson.new
total_records_updated = 0
csv.each_with_index do |row, index|
  name_equivalent_cgt = row['name_equivalent_cgt']
  row_code_cgt = row['catalog_code_cgt']
  row_code_pregrade = row['catalog_code']
  puts row_code_pregrade
  course_pregrade = import_obj.search_course_by_catalog_code(  row_code_pregrade )
  if course_pregrade.present?
    total_records_updated = import_obj.updated_catalog_cgt_to_course( 
      course_pregrade, 
      name_equivalent_cgt, 
      row_code_cgt,
      total_records_updated ) if row_code_cgt.present?
      puts total_records_updated
    else 
      puts "NO EXISTE EL CURSO CON CODIGO  #{row_code_pregrade} y NOMBRE:  #{row['name']} FILA #{index+1}"
    break    
  end
  
end
puts "records was  #{total_records_updated} updated"
#####################################################################################


