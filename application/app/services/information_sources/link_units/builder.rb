module InformationSources
  module LinkUnits
    module Builder

      @information_sources_basic = []

      def self.call(array_information_sources)
        obj_array_parsed = JSON.parse(array_information_sources.to_s.to_json)
        return true unless obj_array_parsed.present?
        arr_id_information = array_information_sources.map{ |information_source| information_source["id"] }
        @information_sources_basic = InformationSource.collection_by_ids( arr_id_information ).basic
        array_information_sources.try(:each) do |information_source|
          if information_source["type_source"] == BASIC_INFORMATION_SOURCE
            delete_logic_all_links( information_source["id"])
            iterate_and_save_link_units( information_source )
          end   
        end  
      end
      
      def obtain_list_information_sources()

      end  

      def self.update_link(link_unit)
        object_update = LinkUnitInformationSource.where(
          number_unit: link_unit["number_unit"],
          information_source_id: link_unit["information_source_id"]
          ).first
        object_update.update_attributes!( hash_attributes_link(link_unit) )
      end
      
      def self.create_link(link_unit)
        object_new = LinkUnitInformationSource.new(
          information_source_id: link_unit["information_source_id"] 
          )
        object_new.assign_attributes( hash_attributes_link(link_unit) )
        object_new.save!
      end

      def self.hash_attributes_link(link_unit)
        {
          number_unit: link_unit["number_unit"],
          from_to_page: link_unit["from_to_page"],
          page_unit: link_unit["page_unit"],
          is_checked: link_unit["is_checked"],
          delete_logic: false
        }
      end

      def self.iterate_and_save_link_units(information_source_json)
        obj_information_source = InformationSource.where(id: information_source_json["id"]).first
        return false unless obj_information_source.present?
        information_source_json["link_units"].try(:each) do |link_unit|
          if exist_link_unit?(link_unit)
            update_link(link_unit)
          else
            create_link(link_unit) 
          end   
        end
      end  
      
      def self.exist_link_unit?(link_unit)
        @information_sources_basic.try(:each) do |information_source|
          if (link_unit["information_source_id"].to_i == information_source.id)
            information_source.link_units.try(:each) do |link_unit_travel|
              return true if (link_unit["number_unit"] == link_unit_travel.number_unit)
            end
            return  false  
          end
        end 
      end
      
      def self.delete_logic_all_links(information_source_id)
        LinkUnitInformationSource.where(
          information_source_id: information_source_id).update_all(
            delete_logic: true
          )
      end  

    end 
  end  
end  