module Import
  class TextComponent < ComponentSyllabus
    attr_accessor :title,:content, :max_words, :edit_only_pregrade
    # def initialize(title = "", content = "", max_words = 200, edit_only_pregrade = true, section_template_id = 1)
    #   @title = title
    #   @content = content
    #   @max_words = max_words
    #   @edit_only_pregrade = edit_only_pregrade
    #   @section_template_id = section_template_id
    # end

    def as_json(options={})
      {
        type: "text",
        title: @title,
        content: @content,
        max_words: @max_words,
        edit_only_pregrade: @edit_only_pregrade,
        section_template_id: 1
      }
    end
  end
end