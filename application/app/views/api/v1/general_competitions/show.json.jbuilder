json.status "success"
json.data do
  json.id @general_competition.id
  json.name @general_competition.name
  json.description @general_competition.description
  json.code @general_competition.code
  json.created_at @general_competition.created_at
  json.updated_at @general_competition.updated_at
end
