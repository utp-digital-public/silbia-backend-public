require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'feb_2020_update_aprober_syllabus_5.csv'))
puts "UPDATE APPROBER AND EDITOR TO SYLLABUSES) "
csv = CSV.parse(csv_text.scrub, headers: true)

customPrompt = Import::CustomPrompt.new
cycle_id = customPrompt.choice_cycle

import_obj = Import::ConvertToJson.new
cant_rows = 0
csv.each_with_index do |row, index|
  cant_rows = cant_rows+1
  email_coordinator = row["email_coordinator"]
  email_supervisor = row["email_supervisor"]
  catalog_code = row["catalog_code"]
  coordinator = import_obj.search_user_by_email(email_coordinator)
  supervisor = import_obj.search_user_by_email(email_supervisor)
  course = import_obj.search_course_by_catalog_code(catalog_code)
  syllabus = Syllabus.where(course_id: course.id).where(cycle_id: cycle_id).first
  if syllabus.present? 
    syllabus_param_hash = {
      program_json: {"texts": [{"type": "text", "title": "metodología", "content": "", "max_words": 1800, "edit_only_pregrade": false, "section_template_id": 1}], "units": [{"title": "", "weeks": [], "achievement": "", "section_template_id": nil}]},
      type_program: TYPE_PROGRAM_CGT, 
      state: NO_READ_STATE, 
      coordinator_id: coordinator.id,
      supervisor_id: supervisor.id,
      evaluation_system_json:{},
      modality_id: 2
    }
    ####
    cgt_syllabus = Import::SyllabusProgramCgt.new( syllabus_param_hash )
    import_obj.build_new_syllabus_program(syllabus,cgt_syllabus)
    syllabus.save!
    puts "Silabus saved"
  else
    puts "************ Error not found course : #{row['catalog_code']} ********"   
  end  

end
puts "Update #{cant_rows} records"