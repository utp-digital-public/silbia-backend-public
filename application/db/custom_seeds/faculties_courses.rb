require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'syllabus_migrate.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
catalog_code = ""
cant = 0
csv.each do |row|
  if ( (catalog_code!= row["catalog_code"]) && (row["catalog_code"].present?) )
    catalog_code = row["catalog_code"].delete(' ')
    if row["id_facultad"].present?
      split_arr = row["id_facultad"].split(",")
      split_arr.try(:each) do |id_facultad|
        puts "El codigo de curso es " + catalog_code
        course = Course.where("catalog_code =? or  catalog_code_cgt =?", catalog_code, catalog_code).first
        puts "el curso segun catalogo es " +course.name
        faculty = Faculty.find(id_facultad)
        course.faculties << faculty
        puts "#{course.id}, #{faculty.id} saved"
        cant+=1
      end  
    end  
  end  
end
puts "There are now #{cant} rows in the transactions table"