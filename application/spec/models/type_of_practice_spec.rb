require 'rails_helper'

RSpec.describe TypeOfPractice, type: :model do
  describe 'relations' do
    it { should have_many(:practices)}
  end
  it { should validate_presence_of(:name).with_message("es requerido")}
end
