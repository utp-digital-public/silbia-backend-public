module SyllabusChange
  module Changes
    class Units < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::UtilDetectCronogram

      def initialize(**attribute)
        super(**attribute)
        attribute.each do |k, v|
          instance_variable_set("@#{k}", v) unless v.nil?
        end
        @title = "Unidas y temas de aprendizaje"
        @id_section = "section-units-topics"
        @changes_detected = 0
        @is_edited = false
        @syllabus = @syllabus_program.syllabus
      end

      def detect()
        @is_edited = detect_changes_units_and_topics?(@last_date)
      end

      def detect_changes_units_and_topics?(date_to_filter)
        changes_detected_units = units_have_changes?(date_to_filter)
        return true if changes_detected_units
        changes_detected_topics = topics_have_changes?(date_to_filter)
        return true if changes_detected_topics
        return false
      end

      def units_have_changes?(date_to_filter)
        keys_to_compare = ["title", "achievement"]
        #update_create_action
        audits = Audited.audit_class.where(
          associated_type: "Syllabus",
          associated_id: @syllabus.id,
          auditable_type: "Unit",
          action: ["create", "update"],
          user_id: @syllabus_program.coordinator_id,
        ).reorder("created_at DESC")
        audits = audits.where("created_at>=?", date_to_filter)
        audits.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            return true if compare_if_changes_ocurred?(key, value, keys_to_compare)
          end
        end
        #delete_action
        audits = Audited.audit_class.where(
          auditable_type: "Unit",
          associated_type: "Syllabus",
          associated_id: @syllabus.id,
          user_id: @syllabus_program.coordinator_id,
          action: "destroy",
        )
        audits = audits.where("created_at>=?", date_to_filter).reorder("created_at DESC")
        return true if audits.size > 0
        return false
      end

      def topics_have_changes?(date_to_filter)
        #update_create_action
        keys_to_compare = ["topic"]
        audits = Audited.audit_class.where(
          auditable_id: @syllabus.topics.pluck(:id),
          auditable_type: "Topic",
          user_id: @syllabus_program.coordinator_id,
        )
        audits = audits.where("created_at>=?", date_to_filter).reorder("created_at DESC")
        audits.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            return true if compare_if_changes_ocurred?(key, value, keys_to_compare)
          end
        end
        #delete_action
        audits = Audited.audit_class.where(
          auditable_type: "Topic",
          associated_type: "Unit",
          associated_id: @syllabus.units.pluck(:id),
          action: "destroy",
          user_id: @syllabus_program.coordinator_id,
        )
        audits = audits.where("created_at>=?", date_to_filter).reorder("created_at DESC")
        return true if audits.size > 0
        false
      end

      def compare_if_changes_ocurred?(key, value, keys_to_compare)
        if keys_to_compare.include? key
          return true unless value.kind_of?(Array)
          puts "hasta aqui entro comparar unidades"
          value_before_change = get_value_before_the_last_change(value)
          value_after_change = get_last_value_of_audit(value)
          puts " Su key es #{key} El valor anterior es: #{get_value_before_the_last_change(value)} El valor nuevo es : #{get_last_value_of_audit(value)} El ultimo : #{value}"

          if (value_before_change != value_after_change)
            return true
          end
        end
      end

      # This method overrite
      def set_hash_value(key, value)
        @hash_values[:information_source] = get_value_before_the_last_change(value) if key == "program_json"
      end
    end
  end
end
