class Faculty < ApplicationRecord
  has_and_belongs_to_many :courses
  validates :name,:code, presence: { message: "es requerido" }
end
