Trestle.resource(:syllabus_programs) do
  menu do
    item :syllabus_programs, icon: "fa fa-star", label: "Silabos"
  end

  scope :pending, -> { model.where(state: 1) }, label: "Pendiente"
  scope :in_progress, -> { model.where(state: 2) }, label: "En Edición"
  scope :to_be_approved, -> { model.where(state: 3) }, label: "En Revisión", class: "hola"
  scope :approved, -> { model.where(state: 4) }, label: "Aprobado"
  scope :observed, -> { model.where(state: 5) }, label: "Observado"

  collection do
    model.whit_cycle_active.includes(syllabus: { :course => :program_courses }).includes(:coordinator).includes(:supervisor)
  end

  search do |query, params|
    results = collection
    unless query.blank? && params["supervisor_id"].blank? && params["updated_at"].blank?
      results = results.where(id: ProgramCourse.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{query}%", "%#{query}%").map { |ff| ff.get_current_syllabus_program }.compact.map { |s| s.id }) unless query.blank?
      results = results.where(supervisor_id: params[:supervisor_id].to_i) unless params[:supervisor_id].blank?
      unless params[:updated_at].blank?
        from_datetime = Time.zone.strptime(params[:updated_at], "%d/%m/%Y %H:%M %p")
        results = results.where(updated_at: from_datetime..Time.zone.now)
      end
    end
    results
  end

  # Customize the table columns shown on the index view.
  #
  table do
    selectable_column unless: -> { @without_scope }
    column :syllabus_id, header: "Curso" do |syllabus_program|
      content_tag(:div, class: "syllabus-name-header") do
        concat image_tag("combined-shape@3x.png", class: "authorized-icon", alt: syllabus_program.download_ability_message, title: syllabus_program.download_ability_message) if syllabus_program.authorized && !syllabus_program.is_inactive
        concat image_tag("icons/alert-red.svg", class: "unauthorized-icon", alt: syllabus_program.download_ability_message, title: syllabus_program.download_ability_message) unless syllabus_program.authorized && !syllabus_program.is_inactive
        concat content_tag(:div, syllabus_program.name_syllabus, style: "display:inline-block")
      end
    end
    column :updated_at, ->(syllabus_program) { syllabus_program.updated_at.strftime("%d/%m/%Y %I:%M%P") }, header: "Modificado", sort: :updated_at
    column :coordinator, ->(syllabus_program) { syllabus_program.coordinator.name }, header: "Editor", sort: :coordinator_id
    column :supervisor_id, ->(syllabus_program) { syllabus_program.supervisor.name }, header: "Aprobador", sort: :supervisor_id
    column :state, header: "Estado", sort: :state, align: :center do |syllabus_program|
      status_tag(
        syllabus_program.getStateName(SyllabusProgram.getArrStates),
        { 1 => :pending, 2 => :in_progress, 3 => :to_be_approved, 4 => :approved, 5 => :observed }[syllabus_program.state_integer_value] || :success
      )
    end
    actions
    actions do |toolbar, instance, admin|
      toolbar.link "Ver PDF", "syllabus_programs/#{instance.id}/export.pdf", target: "_blank", class: "btn-pdf btn btn-link"
      toolbar.link "Ver PDF Resumido", "syllabus_programs/#{instance.id}/exportSummarized.pdf", target: "_blank", class: "btn-pdf btn btn-link"
    end
  end

  # Customize the form fields shown on the new/edit views.
  #
  form do |syllabus_program|
    text_field :program_json, label: "program_json"
    text_field :type_program, label: "type_program"
    select :state, syllabus_program.permissible_states_for_current_state_plus_current(false)
    static_field :coordinator, syllabus_program.coordinator.name, label: "Editor"
    static_field :supervisor, syllabus_program.supervisor.name, label: "Aprobador"
    static_field :authorized, syllabus_program.authorized ? "Sí" : "No", label: "¿Está homologado?"
    check_box :enable_edit_formulas
    check_box :is_inactive
    check_box :show_modal
    select :modality_id, Modality.all.map { |m| [m.name, m.id] }
    text_field :evaluation_system_json, label: "Sistema de evaluación"
  end

  controller do
    def index
      @able_to_download_count = 0
      if params[:scope].present? && params[:scope] == "approved"
        syllabuses = SyllabusProgram.whit_cycle_active.actives.authorized.includes(syllabus: { :course => :program_courses }).includes(:coordinator).includes(:supervisor).approved
        syllabuses = syllabuses.where(id: ProgramCourse.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{params[:q]}%", "%#{params[:q]}%").map { |ff| ff.get_current_syllabus_program }.compact.map { |s| s.id }) unless params[:q].blank?
        syllabuses = syllabuses.where(supervisor_id: params[:supervisor_id].to_i) unless params[:supervisor_id].blank?
        unless params[:updated_at].blank?
          min_date = Time.zone.strptime(params[:updated_at], "%d/%m/%Y %H:%M %p")
          syllabuses = syllabuses.where(updated_at: min_date..Time.zone.now)
        end
        @able_to_download_count = syllabuses.size
      end

      @without_scope = params[:scope].blank?
      @all_scopes = Hash[SyllabusProgram.aasm.states.map { |s| [s.name, s.human_name] }].stringify_keys
      @scope_states = []
      unless params[:scope].blank?
        s = SyllabusProgram.new
        s.state = params[:scope]
        @scope_states = s.permissible_states_for_current_state_plus_current(true)
        s = nil
      end
      super
    end

    def update_states
      ApplicationRecord.transaction do
        begin
          selected_syllabus = SyllabusProgram.where(id: params[:selected])
          selected_syllabus.update_all(state: params[:selected_state])
          flash[:message] = flash_message("success", message: "Se actualizó el estado de #{selected_syllabus.count} sílabos.", title: "Actualización exitosa")
        rescue AASM::InvalidTransition
          flash[:error_hash] =
            {
              title: "Transición invalida de estado de algunos sílabos.",
              message: "<br/>Revise los silabos seleccionados",
            }
        ensure
          redirect_to syllabus_programs_admin_index_path
        end
      end
    end

    def job_status
      @job_status = JobStatus.last
      if @job_status.progress == @job_status.max_progress
        @job_status.increase_progress
        @job_status.setting_zip
        SyllabusProgram.delay.make_zip(@job_status, Cycle.active.name)
      end
      respond_to do |format|
        format.json do
          render :json => @job_status
        end
      end
    end

    def show_modal_download
      syllabuses = SyllabusProgram.whit_cycle_active.includes(syllabus: { :course => :program_courses }).includes(:coordinator).includes(:supervisor).approved
      syllabuses = syllabuses.where(id: ProgramCourse.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{params[:q]}%", "%#{params[:q]}%").map { |ff| ff.get_current_syllabus_program }.compact.map { |s| s.id }) unless params[:q].blank?
      syllabuses = syllabuses.where(supervisor_id: params[:supervisor_id].to_i) unless params[:supervisor_id].blank?
      unless params[:updated_at].blank?
        min_date = Time.zone.strptime(params[:updated_at], "%d/%m/%Y %H:%M %p")
        syllabuses = syllabuses.where(updated_at: min_date..Time.zone.now)
      end
      @syllabuses_valid, @syllabuses_invalid = syllabuses.partition { |s| s.authorized && !s.is_inactive }
      @syllabuses_ids_valid = @syllabuses_valid.map { |s| s.id }
      @syllabuses_invalid_count = @syllabuses_invalid.size
    end

    def collection_download
      syllabus_program_ids = params[:syllabus_ids]
      status = JobStatus.create!(name: "Creación de PDF - #{Time.zone.now.strftime("%Y-%m-%d-%H-%M-%S")}", max_progress: syllabus_program_ids.size)
      SyllabusProgram.delay.generate_pdf_collection(syllabus_program_ids, status)
      status.start_process
      # flash[:message] = flash_message("success", message: "Se guardó correctamente el PDF del curso #{@syllabus_program.name_syllabus}", title: "PDF creado exitosamente")
    end

    def export
      @syllabus_program = SyllabusProgram.find(params[:id])
      nombre_reporte = helpers.format_name_pdf_sillabus(@syllabus_program)
      orientacion = "Portrait"
      @syllabus = @syllabus_program.syllabus
      @information_sources_basic = @syllabus.course.information_sources.basic.all_relations_included.order_by_type_source
      @information_sources_complementary = @syllabus.course.information_sources.complementary.all_relations_included.order_by_type_source

      @course = @syllabus_program.syllabus.course
      @competitions = Competition.where("course_id =?", @course.id).order("career,competition_description ")
      if @course.for_all_career.present?
        @competitions = @competitions.group("competition_description,criterion, achievement_level").select("'todos' as career, competition_description, criterion, achievement_level")
      end
      @careers_competition = @syllabus_program.get_program_course.careers.pluck("name")

      respond_to do |format|
        format.html { render template: "/syllabuses/html/template", :layout => false }
        format.pdf {
          render template: "/syllabuses/pdf/template",
            page_size: "A4",
            pdf: nombre_reporte,
            locals: { syllabus: @syllabus, syllabus_program: @syllabus_program, course: @course, information_sources_basic: @information_sources_basic, information_sources_complementary: @information_sources_complementary, careers_competition: @careers_competition },
            orientation: orientacion,
            # outline: {
            #   outline: true,
            #   outline_depth: 50 },
            margin: {
              top: 20,
              bottom: 17,
              left: 18,
              right: 20,
            },
            footer: {
              spacing: 30,
            # html: {
            #    template: '/syllabuses/pdf/pdf-footer.html'
            # }
            }
        }
      end
    end

    def exportSummarized
      @syllabus_program = SyllabusProgram.find(params[:id])
      nombre_reporte = helpers.format_name_pdf_sillabus(@syllabus_program)
      orientacion = "Portrait"
      @syllabus = @syllabus_program.syllabus
      @information_sources_basic = @syllabus.course.information_sources.basic.all_relations_included.order_by_type_source
      @information_sources_complementary = @syllabus.course.information_sources.complementary.all_relations_included.order_by_type_source

      @course = @syllabus_program.syllabus.course
      @competitions = Competition.where("course_id =?", @course.id).order("career,competition_description ")
      if @course.for_all_career.present?
        @competitions = @competitions.group("competition_description,criterion, achievement_level").select("'todos' as career, competition_description, criterion, achievement_level")
      end
      @careers_competition = @syllabus_program.get_program_course.careers.pluck("name")

      respond_to do |format|
        format.html { render template: "/syllabuses/html/template", :layout => false }
        format.pdf {
          render template: "/syllabuses/pdf/summarized_template",
            page_size: "A4",
            pdf: nombre_reporte,
            locals: { syllabus: @syllabus, syllabus_program: @syllabus_program, course: @course, information_sources_basic: @information_sources_basic, information_sources_complementary: @information_sources_complementary, careers_competition: @careers_competition },
            orientation: orientacion,
            # outline: {
            #   outline: true,
            #   outline_depth: 50 },
            margin: {
              top: 20,
              bottom: 17,
              left: 18,
              right: 20,
            },
            footer: {
              spacing: 30,
            # html: {
            #    template: '/syllabuses/pdf/pdf-footer.html'
            # }
            }
        }
      end
    end

    def update_general_achievement
      errors = []
      syllabus_count = 0
      csv_text = File.read(params[:file].path)
      cycle_id = Cycle.active.id
      csv = CSV.parse(csv_text.scrub, headers: true)

      import_obj = Import::ConvertToJson.new
      cant_rows = 0
      csv.each_with_index do |row, index|
        if row[0].present?
          catalog_code = row[0]
          cant_rows = cant_rows + 1
          general_achievement = row[1]
          course = ProgramCourse.search_course_by_catalog_code(catalog_code)
          if course.present?
            syllabus = Syllabus.where(course_id: course.course_id).where(cycle_id: cycle_id).first

            if syllabus.present? && general_achievement.present?
              if general_achievement.length.between?(1, 4000)
                syllabus.syllabus_content["texts"][2]["content"] = general_achievement
                syllabus.save
                syllabus_count = syllabus_count + 1
              else
                errors << "Error en la fila #{index + 1}, no cumple con longitud válida"
              end
            else
              errors << "Error en la fila #{index + 1}, No existe silabo"
            end
          else
            errors << "Error en la fila #{index + 1}, Curso existe silabo"
          end
        end
      end
      result = { errors: errors, syllabus_count: syllabus_count }
      if result[:errors].empty?
        flash[:result_hash] = {
          title: "Importación exitosa",
          message: "Se actualizaron #{result[:syllabus_count]} sílabos.",
        }
        redirect_to syllabus_programs_admin_index_path
      else
        flash[:error_hash] =
          {
            title: result[:error_type] == "invalid_document" ? "Error en el documento" : "Error en la importación. #{result[:syllabus_count] == 1 ? "Se actualizó 1 silabo" : "Se actualizaron #{result[:syllabus_count]} silabos"} y #{result[:errors].count == 1 ? "1 fila presenta error" : "#{result[:errors].count} filas presentan errores:"}",
            message: "<br/> #{result[:errors].uniq.join("<br/>")}",
          }
        redirect_to syllabus_programs_admin_index_path
      end
    end
  end

  routes do
    get :export, on: :member
    get :exportSummarized, on: :member
    get :job_status, on: :collection
    get :show_modal_download, on: :collection
    post :store_pdf, on: :member
    post :update_states, on: :collection
    post :collection_download, on: :collection
    post :update_general_achievement, on: :collection
  end

  # By default, all parameters passed to the update and create actions will be
  # permitted. If you do not have full trust in your users, you should explicitly
  # define the list of permitted parameters.
  #
  # For further information, see the Rails documentation on Strong Parameters:
  #   http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters
  #
  params do |params|
    params.require(:syllabus_program).permit(:coordinator_id, :supervisor_id, :state, :enable_edit_formulas, :is_inactive, :modality_id)
  end
end
