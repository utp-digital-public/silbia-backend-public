require 'rails_helper'

RSpec.describe Faculty, type: :model do
  
  it { should validate_presence_of(:code).with_message("es requerido")}
  it { should validate_presence_of(:name).with_message("es requerido")}
end
