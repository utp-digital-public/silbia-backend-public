require 'csv'

user_1 = User.where(code_user: "c18609").first
user_1.update_attributes!(email: "agonzalega@utp.edu.pe")

user_2 = User.where(code_user: "e13243").first
user_2.add_role ROLE_CREATOR.to_sym

### Migrate Users from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'feb_2020_users_inactive.csv'))
puts "UPDATE USERS EMAILS TO INACTIVE"
csv = CSV.parse(csv_text.scrub, headers: true)

cant_rows = 0
csv.each_with_index do |row, index|
	cant_rows = cant_rows+1
	code_user = row["code"]
	email = row["new-email"]
	user = User.find_by_code_user(code_user)
	user.email = email
	if user.save
		puts  "NroFilaExcel: #{index+1} | Email: #{email} | User: #{code_user}"
	end
end
puts "Update #{cant_rows} records"
