class CreateLaboratories < ActiveRecord::Migration[5.2]
  def change
    create_table :laboratories do |t|
      t.string :name, null: false
      t.integer :type_laboratory, null: false

      t.timestamps
    end
  end
end
