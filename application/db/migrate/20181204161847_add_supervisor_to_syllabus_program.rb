class AddSupervisorToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :supervisor_id, :integer
  end
end
