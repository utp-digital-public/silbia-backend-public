json.status "success"
json.data do
  json.syllabus do
    json.partial! "api/v1/syllabus_programs/syllabus", syllabus: @syllabus
  end
end
