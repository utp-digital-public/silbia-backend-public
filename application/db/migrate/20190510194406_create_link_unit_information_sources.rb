class CreateLinkUnitInformationSources < ActiveRecord::Migration[5.2]
  def change
    create_table :link_unit_information_sources do |t|
      t.integer :number_unit
      t.integer :from_to_page
      t.integer :page_unit
      t.boolean :is_checked, default: false
      t.references :information_source, foreign_key: true
      t.timestamps
    end
  end
end
