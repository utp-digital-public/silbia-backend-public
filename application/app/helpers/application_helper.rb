module ApplicationHelper
  include Util::Util

  def format_name_pdf(name)
    string = remove_accents(name).titleize.delete(" ")
    return string
  end

  def format_name_pdf_sillabus(syllabus_program)
    begin
      syllabus_program.get_catalog_code + "_" + format_name_pdf(syllabus_program.name_syllabus)
    rescue Exception => e
      ""
    end
  end

  def show_message_top_header
    if ((@approver_observation_syllabus.present?))
      ruta_partial = "/syllabuses/messages_observation"
      render partial: ruta_partial, formats: [:html]
      # else
      #ruta_partial = "/layouts/return_back" unless @approver_observation_syllabus
    end
  end

  def avatar_image(user)
    "<object 
      data='https://s3.amazonaws.com/silbia-bucket/users/#{user.code_use_down}.jpg' 
      type='image/jpg'
      class='avatar-top'>
      #{image_tag("avatar.png", width: "32", height: "32")}
      <br> <br>
    </object>".html_safe
  end

  def bootstrap_class_for(flash_type)
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)}") do
        concat(content_tag(:button, class: "close", data: { dismiss: "alert" }) do
          concat "×"
        end)
        concat message
      end)
    end
    nil
  end
end
