json.id syllabus_program.id
json.weekly_hours syllabus_program.weekly_hours
json.credits syllabus_program.credits
json.code_catalog_of_syllabus syllabus_program.code_catalog_of_syllabus
json.name_syllabus syllabus_program.name_syllabus
json.enable_edit_formulas syllabus_program.enable_edit_formulas
json.modality syllabus_program.modality
json.state syllabus_program.state
json.is_inactive syllabus_program.is_inactive
json.type_program syllabus_program.type_program
json.show_modal syllabus_program.show_modal
json.authorized syllabus_program.authorized
json.coordinator_id syllabus_program.coordinator_id
json.coordinator_name syllabus_program.coordinator_name
json.supervisor_id syllabus_program.supervisor_id
json.supervisor_name syllabus_program.supervisor_name
json.human_syllabus_state syllabus_program.aasm.human_state
json.permissible_states syllabus_program.permissible_states_for_current_state_plus_current(false)
json.updated_at syllabus_program.updated_at
