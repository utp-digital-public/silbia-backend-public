class Api::V1::PracticesController < Api::ApiV1Controller
  before_action :set_order_params, only: [:index]
  before_action :set_practice, only: [:show, :update, :delete]

  def index
    @practice_list = Practice.all
    @practice_list = @practice_list.order(@sort) unless @sort.blank?
  end

  def create
    @practice = Practice.new(practice_params)
    if @practice.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @practice.errors) and return
    else
      ApplicationRecord.transaction do
        begin
          @practice.save!()
        rescue Exception => e
          render_internal_server_error(e.message)
        end
      end
    end
  end

  def update
    @practice.assign_attributes(practice_params)
    if @practice.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @practice.errors) and return
    end
    ApplicationRecord.transaction do
      begin
        @practice.save!()
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  private

  def set_order_params
    order = if params[:order].present? then params[:order] else "asc" end
    @sort = if params[:sort].present? then params[:sort] + " " + order else :description end
  end

  def practice_params
    params.permit(:description, :code, :type_of_practice_id)
  end

  def set_practice
    @practice = Practice.find(params[:id])
  end
end
