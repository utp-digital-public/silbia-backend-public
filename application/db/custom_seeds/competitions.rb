require 'csv'
### Migrate COMPETITIONS from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'competitions.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  catalog_code = row["Codigo"]
  course = Course.where(catalog_code: catalog_code).first
  if course.present?
    comp = Competition.new
    comp.career = row["Carrera"]
    comp.competition_description = row["Competencia"]
    comp.criterion = row["Criterio"]
    achievement_level_value = row["Nivel"].to_s + ": " + row["Descripción del nivel"].to_s
    comp.achievement_level = achievement_level_value
    comp.course_id = course.id
    comp.save!
    puts "#{comp.competition_description}, #{catalog_code} saved"
  end  

end
puts "There are now #{Competition.count} rows in the transactions table"
#####################################################################################