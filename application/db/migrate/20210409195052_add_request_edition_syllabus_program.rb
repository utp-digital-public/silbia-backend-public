class AddRequestEditionSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :request_edition, :boolean, default: false
  end
end
