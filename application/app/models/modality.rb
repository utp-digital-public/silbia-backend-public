class Modality < ApplicationRecord
  validates :name, presence: { message: "es requerido" }
  has_many :syllabus_programs
  has_many :program_courses
  has_many :modality_cycles, :dependent => :destroy
  has_many :cycles, :through => :modality_cycles
end
