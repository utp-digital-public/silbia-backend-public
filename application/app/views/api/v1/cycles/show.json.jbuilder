json.status "success"
json.data do
  json.partial! "api/v1/cycles/cycle", cycle: @cycle
end
