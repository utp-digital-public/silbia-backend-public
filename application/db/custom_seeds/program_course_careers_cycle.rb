require "csv"
all_careers = Hash[Career.all.map { |x| [x[:code], x[:id]] }]
all_courses = Hash[ProgramCourse.active.all.map { |x| [x[:catalog_code], x[:id]] }]
puts "Hay #{ProgramCourseCareer.count} registros en program_course_careers"

### Migrate cycle from csv file  ################################################
csv_text = File.read(Rails.root.join("lib", "seeds_csv", "program_course_careers_cycle.csv"))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  if all_careers[row["career_code"]] && all_courses[row["catalog_code"]]
    program_course_career = ProgramCourseCareer.find_or_initialize_by(career_id: all_careers[row["career_code"]], program_course_id: all_courses[row["catalog_code"]])
    program_course_career.cycle_number = row["cycle_number"]
    begin
      program_course_career.save!
      puts "Se actualizo el curso #{row["catalog_code"]} con la carrera #{row["career_code"]}"
    rescue Exception => e
      p "Error: #{program_course_career.errors.full_messages.first}"
    end
  else
    puts "No existe el curso #{row["catalog_code"]} o la carrera #{row["career_code"]}"
  end
end
puts "Hay #{ProgramCourseCareer.count} registros en program_course_careers"
