class Api::V1::DashboardController < Api::ApiV1Controller
    require "base64"
    
    before_action :set_params_to_dashboard, only: [:index, :filter_syllabuses]
  
    def index
      begin
        if @state_selected.present? || @query.present?
          load_syllabuses_dashboard(state: @state_selected, query: @query )
        else
          load_syllabuses_dashboard()
        end
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  
    def filter_syllabuses
      begin
        ruta_partial ="/dashboard/table_dashboard"
        load_syllabuses_dashboard(state: @state_selected )
        
        unless request.original_fullpath.include?("page=")
          headers['number_items'] = @quantity_list_syllabus
          headers['item_selected_label'] = @nombre_select
          # return  respond_to do |format|
          #   format.html {render partial: ruta_partial, saluda: "eee" }
          # end  
        #else
          # return respond_to do |format|
          #   format.html {render template: '/dashboard/index' }
          # end  
        end
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
    
    def load_syllabuses_dashboard(filters={})
      syllabus_program_service = SyllabusProgramService.new
      preload_user_to_evit_n_1_query()
      syllabus_programs = syllabus_program_service.list_syllabus_for_roles(current_user, filters)
      
      # without filter and reorder the query, order only for state
      @syllabus_total = syllabus_program_service.list_syllabus_for_roles(current_user, filters.except(:state)).reorder('state')
      # get states groupe to query
      sql = @syllabus_total.group("state").select("state,COUNT(*) AS quantity").to_sql
      @states_grouped = ActiveRecord::Base.connection.execute(sql)
      @quantity_list_syllabus = get_number_item_state(syllabus_programs)
      @arr_states = SyllabusProgram.getArrStates
      
      @syllabus_programs = syllabus_programs.page(params[:page])#.per(10)
      #@pagy, @syllabus_programs = pagy(syllabus_programs)
    end  
  end
  
  def get_name_state(id)
    text = "Todos"
    SyllabusProgram.getArrStates.try(:each) do |element|
      text = element[:label]  if id.to_s == element[:value].to_s
    end
    text    
  end  
  
  def get_number_item_state(list)
    quantity = 0
    list.try(:each_with_index) do |element, index|
      quantity+=1
    end 
    return quantity 
  end  
  
  private
   def preload_user_to_evit_n_1_query
    # preload to evit N+1 roles query
    users = User.with_role(ROLE_CREATOR.to_sym).preload(:roles)
    #users = User.with_role(ROLE_APPROVER.to_sym).preload(:roles)
   end 
  
   def set_params_to_dashboard
    @state_selected =  if  params[:state_selected].present? then params[:state_selected] else "" end
    @query =  if  params[:query].present? then params[:query] else "" end
    @nombre_select = get_name_state(@state_selected)
   end 