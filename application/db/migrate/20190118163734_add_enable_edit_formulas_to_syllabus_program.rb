class AddEnableEditFormulasToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :enable_edit_formulas, :boolean, default: false
  end
end
