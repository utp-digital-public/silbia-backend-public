json.status "success"
json.data do
  json.users do
    json.array! @users, partial: "api/v1/users/user", as: :user
  end
  json.roles do
    json.array! @roles
  end
end
json.partial! "api/v1/shared/pagination", collection: @users
