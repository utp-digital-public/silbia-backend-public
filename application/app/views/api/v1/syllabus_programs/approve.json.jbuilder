json.modalities do
  json.array! @syllabus_program.syllabus.modalities, partial: "api/v1/syllabus_programs/syllabus_modality", as: :modality
end
json.can_edit policy(@syllabus_program).edit?
json.can_approve policy(@syllabus_program).approve?
json.human_syllabus_state @syllabus_program.aasm.human_state
json.state @syllabus_program.state
json.flags @syllabus_program.flags
json.result @result[:message]
