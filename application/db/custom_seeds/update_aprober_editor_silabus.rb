require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'update_aprober_syllabus.csv'))
puts "UPDATE APPROBER AND EDITOR TO SYLLABUSES) "
csv = CSV.parse(csv_text.scrub, headers: true)

import_obj = Import::ConvertToJson.new
cant_rows = 0
csv.each_with_index do |row, index|
  if row["new_update"].present?
    cant_rows = cant_rows+1
    email_supervisor = row["email_supervisor"]
    email_coordinator = row["email_coordinator"]
    catalog_code = row["catalog_code"]
    cycle_id = row["cycle_id"]
    type_program = row["type_program"]
    supervisor = import_obj.search_user_by_email(email_supervisor)
    coordinator = import_obj.search_user_by_email(email_coordinator)
    course = import_obj.search_course_by_catalog_code(catalog_code)
    syllabus = Syllabus.where(course_id: course.id).where(cycle_id: cycle_id).first
    syllabus_program_obj = syllabus.syllabus_programs.where(type_program: type_program).first
    
    syllabus_program_obj.update_attributes!(coordinator_id: coordinator.id, supervisor_id: supervisor.id)
    
    puts  "NroFilaExcel: #{index+1} | SilabusProgramId: #{syllabus_program_obj.id} | Nombre Super: #{supervisor.name}  | Nombre Coordinador: #{coordinator.name} | Email Coordinato: #{email_coordinator}"
  end
end
puts "Update #{cant_rows} records"