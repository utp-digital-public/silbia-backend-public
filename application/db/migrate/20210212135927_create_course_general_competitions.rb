class CreateCourseGeneralCompetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :course_general_competitions do |t|
      t.references :program_course, foreign_key: true
      t.references :general_competition, foreign_key: true
      t.integer :level, null: false
      t.timestamps
    end
  end
end
