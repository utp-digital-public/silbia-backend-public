json.status "success"
json.data do
  json.partial! "api/v1/program_courses/program_course", program_course: @program_course
end
