class RedesignSyllabusMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_messages, :section, :integer, :default => 0, null: false
    add_column :syllabus_messages, :revision_number, :integer, :default => 0, null: false
    add_column :syllabus_programs, :revision_number, :integer, :default => 0, null: false
  end
end
