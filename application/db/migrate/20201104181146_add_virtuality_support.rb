class AddVirtualitySupport < ActiveRecord::Migration[5.2]
  def change
    add_reference :program_courses, :modality, foreign_key: true
    add_column :program_courses, :deleted, :boolean, :default => false
    add_index :program_courses, :catalog_code
    ProgramCourse.all.each do |pc|
      if pc.type == "Blended"
        pc.modality_id = 2
      else
        pc.modality_id = 1
      end
      pc.save
    end
    Modality.create!([
      {id: VIRTUAL_CODE, name: "virtual"},
    ])
  end
end
