class AddIsAcquiredToBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :is_acquired, :boolean
  end
end
