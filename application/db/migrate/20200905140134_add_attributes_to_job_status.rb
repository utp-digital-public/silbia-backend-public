class AddAttributesToJobStatus < ActiveRecord::Migration[5.2]
  def change
    add_column :job_statuses, :file_paths, :text, :default => ""
    add_column :job_statuses, :job_name, :string, :default => ""
  end
end
