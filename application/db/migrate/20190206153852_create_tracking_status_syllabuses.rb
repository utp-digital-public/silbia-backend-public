class CreateTrackingStatusSyllabuses < ActiveRecord::Migration[5.2]
  def change
    create_table :tracking_status_syllabuses do |t|
      t.references :syllabus_program, foreign_key: true, null: false
      t.integer :state, null:false
      t.integer :user_id_track, null:false

      t.timestamps
    end
  end
end
