class AddDefaultValueColumnStateToSyllabusMigrate < ActiveRecord::Migration[5.2]
  def change
    change_column :syllabus_messages, :state, :integer, :default => 0
  end
end
