require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'practices.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each_with_index do |row, index|
  id_practice = row["id"].strip
  practice_find = Practice.where(id:id_practice)
  
  unless practice_find.present?
    puts "FILA #{index+1} TIPO practica es: #{row["code"]}"
    practice = Practice.new
    practice.description = row["description"]
    practice.code = row["code"]
    practice.type_of_practice_id = nil
    practice.save!
    puts "#{practice.code}, #{practice.description} saved"
  end  
end
puts "There are now #{Practice.count} rows in the transactions table"