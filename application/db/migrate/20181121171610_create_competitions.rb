class CreateCompetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :competitions do |t|
      t.string :career, default: ''
      t.string :competition_description, default: ""
      t.string :criterion, default: ""
      t.string :achievement_level, default: ""
      t.references :course, foreign_key: true
      t.timestamps
    end
  end
end
