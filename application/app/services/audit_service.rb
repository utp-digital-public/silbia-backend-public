class AuditService
    attr_accessor :arr_objects  
    def initialize(list_objects,auditable_type, comment=nil)
        @list_objects = list_objects
        @action = "create"
        @version = 1
        @arr_objects = []        
        @auditable_type = auditable_type
        @comment = comment
    end

    def save_masive()
        @list_objects.try(:each) do |object|
            auditable = Audited::Audit.new
            auditable.assign_attributes({
              auditable_type: @auditable_type,
              auditable_id: object.id,
              username: "Administrator",
              version: @version,
              action: @action,
              request_uuid: SecureRandom.uuid,
              audited_changes: object.attributes,
              comment: @comment
    
            })
            @arr_objects << auditable
        end
        Audited::Audit.import @arr_objects
    end  
end