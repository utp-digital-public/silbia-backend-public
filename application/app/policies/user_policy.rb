class UserPolicy < ApplicationPolicy
  def initialize(user, record)
    @user = user
    @record = record
  end

  def is_admin?
    return @user.is_a? UserAdmin
  end
end
