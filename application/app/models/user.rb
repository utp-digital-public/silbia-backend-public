class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable, :timeoutable
  validates :name, :first_name, :code_user, :email,:password, presence: {message: "es requerido"}, on: :create
  validates :email, :code_user, uniqueness: { message: Proc.new{|user, data|
      "ya está siendo usado." 
    }
  }
  validates :email, format: {:with => /\A^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(utp.edu)\.pe$\z/i, message: Proc.new{|prog_course, data|
      "debe tener el formato adecuado: nombre_de_usuario@utp.edu.pe" 
    }
  }
  extend Enumerize
  enumerize :rol, in: [:approver => 1, :creator => 2]
  # asign references to activities user
  has_many :login_activities, as: :user
  enum state: [:active, :inactive]

  attr_accessor :rol_to_use_in_session
  attr_accessor :is_approver
  attr_accessor :is_creator

  def is_approver?
    evaluate_rol(ROLE_APPROVER)
  end
  
  def is_creator?
    evaluate_rol(ROLE_CREATOR)
  end
  
  def evaluate_rol(rol)
    unless self.rol_to_use_in_session.present?
      return true if self.has_cached_role? rol.to_sym
    else 
     return true  if self.rol_to_use_in_session == rol
    end
    false  
  end

  def roles_label
    roles = ""
    unless has_not_role_asigned?
      if is_approver? && is_creator?
        roles = "Editor/Aprobador"
      else
        roles = "Editor" if is_creator?
        roles = "Aprobador" if is_approver?
      end
    end
    roles 
  end

  def code_use_down
    if code_user.present? then self.code_user.downcase else "" end
  end
  
  # custom methods to update or create user if exist or not
  def self.update_or_create(attributes)
    assign_or_new(attributes).save
  end
  
  def self.assign_or_new(attributes)
    obj = first || new
    obj.assign_attributes(attributes)
    obj
  end

  def has_not_role_asigned?
    !self.roles.present?
  end  

  def self.search_user_by_email(email)
    email = email.delete(' ')
    user = User.where("LOWER(email) = LOWER(?)", email).first
    return nil unless user.present?
    user
  end

  def self.import file
    spreadsheet = Roo::Spreadsheet.open(file)
    errors = []
    users_count = 0
    return {errors: ["Documento vacío. Debe ingresar un archivo con la estructura correcta"], users_count: 0, error_type:"invalid_document"} if spreadsheet.first_row.nil?
    all_headers = spreadsheet.row(5)
    unless all_headers.size == 8
      return {errors: ["No se han considerado todas las columnas. Debe ingresar un archivo con la estructura correcta"], users_count: 0, error_type:"invalid_document"}
    end
    (6..spreadsheet.last_row).each do |i|
      user = User.new
      user.code_user = spreadsheet.row(i)[0].to_s
      user.code_user.strip! if user.code_user.present?
      user.first_name = spreadsheet.row(i)[1].to_s
      user.first_name.strip! if user.first_name.present?
      user.name = spreadsheet.row(i)[2].to_s
      user.name.strip! if user.name.present?
      input_password = spreadsheet.row(i)[5].to_s
      input_password.strip! if input_password.present?
      user.password = input_password
      user.password_confirmation = input_password
      user.email = spreadsheet.row(i)[6].to_s
      user.email.strip! if user.email.present?
      begin
        if user.save!
          is_editor = spreadsheet.row(i)[3].present? ? I18n.transliterate(spreadsheet.row(i)[3].to_s).downcase.strip.eql?("si") : false
          is_approver = spreadsheet.row(i)[4].present? ? I18n.transliterate(spreadsheet.row(i)[4].to_s).downcase.strip.eql?("si") : false
          user.add_role ROLE_APPROVER.to_sym if is_approver
          user.add_role ROLE_CREATOR.to_sym if is_editor
          users_count += 1
        end
      rescue ActiveRecord::RecordInvalid => invalid
        errors <<  "Error en la Fila #{i}: #{user.errors.full_messages.first}"
        next
      rescue Exception => e
        errors <<  "Error en la Fila #{i}: #{e.message}"
        next
      end
    end
    {errors: errors, users_count: users_count}
  end

end
