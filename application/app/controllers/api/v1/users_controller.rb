class Api::V1::UsersController < Api::ApiV1SharedController
  before_action :set_filter_params, only: [:index]
  before_action :set_user, only: [:show, :update]

  def index
    query = params[:query]
    @users = User
    @users = @users.where(state: params[:inactive]) if params[:inactive].present?
    @users = @users.where("users.name ILIKE unaccent(?) ", "%#{query}%") unless query.blank?
    count_all = @users.count
    @roles = [
      { label: "Aprobadores", value: ROLE_APPROVER.to_sym },
      { label: "Editores", value: ROLE_CREATOR.to_sym },
    ]
    @roles.try(:each) do |element|
      element[:quantity] = @users.with_role(element[:value]).count || 0 unless element[:value].blank?
      element[:quantity] = count_all if element[:value].blank?
    end
    @users = @users.with_role(params[:role]) unless params[:role].blank?
    @users = @users.with_role(ROLE_APPROVER.to_sym) if params[:approver] == "true"
    @users = @users.with_role(ROLE_CREATOR.to_sym) if params[:creator] == "true"

    if @paginate == "false"
      @users = @users.order(@sort).page(params[:page]).per(count_all)
    else
      @users = @users.order(@sort).page(params[:page])
    end
  end

  def create
    authorize :user, :is_admin?
    @user = User.create!(user_params)
    if @user.save
      @user.add_role ROLE_APPROVER.to_sym if @user.is_approver == true
      @user.add_role ROLE_CREATOR.to_sym if @user.is_creator == true
      render_ok(@user)
    else
      render_bad_request("No se pudo crear el usuario", "Error al guardar", "", @user.errors) and return
    end
  end

  def show
  end

  def update
    authorize :user, :is_admin?
    if @user.update_attributes(user_params)
      if @user.is_approver?
        @user.remove_role(ROLE_APPROVER.to_sym) unless @user.is_approver == true
      else
        @user.add_role(ROLE_APPROVER.to_sym) if @user.is_approver == true
      end
      if @user.is_creator?
        @user.remove_role(ROLE_CREATOR.to_sym) unless @user.is_creator == true
      else
        @user.add_role(ROLE_CREATOR.to_sym) if @user.is_creator == true
      end
    else
      render_bad_request("No se pudo actualizar el usuario", "Error al guardar", "", @user.errors) and return
    end
  end

  def destroy
    authorize :user, :is_admin?
    @user.destroy
  end

  private

  def set_filter_params
    order = if params[:order].present? then params[:order] else "asc" end
    sort = if params[:sort].present? then sort = params[:sort] else "name" end
    @paginate = if params[:paginate].present? then params[:paginate] else true end
    @sort = if sort.present? then sort + " " + order else "" end
  end

  def user_params
    params.permit(:email, :first_name, :name, :password, :is_approver, :is_creator, :code_user, :show_modal_sidebar, :active)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
