json.status "success"
json.data do
  json.program_course do
    json.partial! "api/v1/program_courses/program_course", program_course: @program_course
  end

  json.similar_courses do
    json.array! @similar_courses, partial: "api/v1/program_courses/similar_course", as: :similar_course
  end
  json.basic_books do
    json.array! @book_basic_ids, partial: "api/v1/books/book", as: :book
  end
  json.complementary_books do
    json.array! @book_complementary_ids, partial: "api/v1/books/book", as: :book
  end
  json.course_modalities @course_modalities
end
