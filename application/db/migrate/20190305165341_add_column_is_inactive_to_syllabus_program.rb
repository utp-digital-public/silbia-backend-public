class AddColumnIsInactiveToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :is_inactive, :boolean, default: false
  end
end
