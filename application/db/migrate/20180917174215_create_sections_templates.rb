class CreateSectionsTemplates < ActiveRecord::Migration[5.2]
  def change
    create_table :sections_templates do |t|
      t.references :doc_template, foreign_key: true
      t.references :component, foreign_key: true
      t.string :title, default: ''
      t.integer :sort, null: false

      t.timestamps
    end
  end
end
