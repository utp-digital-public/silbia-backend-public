require 'rails_helper'
require 'support/admin/spec_test_helper'

RSpec.describe "Admin::CopySyllabuses", type: :request do
  
  describe "GET /admin_copy_syllabuses" do
    context "when user is autenticated" do
      it "return http status 200" do
        login_admin
        get "/copy_syllables"
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST  #execute_copy" do
    context "when user is autenticated" do
      it "return http status 200" do
        login_admin
        cycle_active = FactoryBot.create(:cycle) 
        cycle_to_copy = FactoryBot.create(:cycle_to_copy)
        post "/copy_syllables/execute_copy" , 
        params: { 
          copy_syllabus: { 
            cycle_from_copy: cycle_active.id
          },
          copy_syllabus_cycle_to_copy: cycle_to_copy.id,
          format: :js
        }
        expect(response).to have_http_status(200)
      end
      it "Copy syllabus from one cycle to other cycle" do
        login_admin
        cycle_from = FactoryBot.create(:cycle)
        syllabus_obj= FactoryBot.create(:syllabus) 
        syllabus_program = FactoryBot.create(:syllabus_program, syllabus: syllabus_obj)
        Cycle.all.update_all(enabled: false)

        cycle_to_copy = FactoryBot.create(:cycle_to_copy)
        #expect {
          post "/copy_syllables/execute_copy", 
          params: { 
            copy_syllabus: { 
              cycle_from_copy: cycle_from.id
            },
            copy_syllabus_cycle_to_copy: cycle_to_copy.id,
            format: :js
          }
        #}.to change(cycle_to_copy.syllabuses, :count).by(1)
      end
    end  
  end

  describe "GET #change_from_copy" do
    context "when user is autenticated" do
      it "return http status 200" do
        login_admin
        cycle_active = FactoryBot.create(:cycle) 
        get "/copy_syllables/change_from_copy",
        params: {
          format: :js
        },
        xhr: true
      end
    end
  end
end
