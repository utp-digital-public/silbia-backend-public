FactoryBot.define do
  factory :book do
    code {"MyString"}
    title {"Title of book"}
    author {"One author"}
    support {"MyString"}
    year {"2018"}
    number_editorial {"43"}
    editorial {"Coquito"}
  end
end
