class SpecificCompetition < ApplicationRecord
  belongs_to :career
  has_many :course_specific_competitions, :dependent => :destroy, :inverse_of => :specific_competition
  validates :career, :code, :name, :description, presence: { message: "es requerido" }
  validates :code, uniqueness: { message: "ya existe", scope: :career_id }
end
