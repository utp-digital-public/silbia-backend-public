class FixMigrateUnitTopics < ActiveRecord::Migration[5.2]
  def change
=begin
    execute "truncate table session_topics;"
    execute "truncate table sessions CASCADE;"
    execute "truncate table topics CASCADE;"
    execute "truncate table units CASCADE;"
    execute "drop table if exists tmp_migration_units;"
    execute "select sp.id,
                s.id                        syllabus_id,
                sp.modality_id,
                pc.name,
                unit ->> 'title'            title,
                unit ->> 'number_unit'      number_unit,
                unit ->> 'achievement'      achievement,
                week ->> 'number_week'       number_week,
                session ->> 'number_session' number_session,
                session ->> 'subject'       topic,
                session ->> 'laboratory' laboratory,
                session ->> 'type_laboratory' type_laboratory,
                session -> 'activities' activities,
                pc.authorized homologado,
                idx,
                idxw,
                idxs
            INTO TEMP TABLE tmp_migration_units

            from syllabus_programs sp
                  join syllabuses s on sp.syllabus_id = s.id
                  join program_courses pc
                      on s.course_id = pc.course_id and sp.modality_id = pc.modality_id
                ,
              jsonb_array_elements(sp.program_json -> 'units') with ordinality as t(unit, idx),
              jsonb_array_elements(unit -> 'weeks') with ordinality as r(week, idxw),
              jsonb_array_elements(week -> 'sessions') with ordinality as q(session, idxs)

            order by sp.id, idx, cast(week ->> 'number_week' as int)    ,
                  cast(session ->> 'number_session' as int)
            ;"
    execute "insert into units(syllabus_id, unit_number,title, achievement, created_at, updated_at)
            select datos.syllabus_id, idx, max(datos.title) title, max(datos.achievement) achievement, now(), now()
            from tmp_migration_units datos
            left join units on datos.syllabus_id  = units.syllabus_id and datos.idx = units.unit_number
            where units.id is null and datos.modality_id=1
            group by datos.syllabus_id, datos.idx
            order by 1,2;"
    execute "insert
            into topics (unit_id, topic, created_at, updated_at)
            select id, topic, now(), now()
            from (
                    select units.id, tmp_migration_units.topic
                    from units
                              join tmp_migration_units
                                  on units.syllabus_id = tmp_migration_units.syllabus_id and
                                      units.unit_number = tmp_migration_units.idx
                              left join topics t on units.id = t.unit_id and tmp_migration_units.topic = t.topic
                    where t.id is null
                      and tmp_migration_units.topic != ''
                      and tmp_migration_units.modality_id = 1
                    ORDER BY cast(tmp_migration_units.number_session as int)
                ) t
            group by id, topic;"
    execute "insert into sessions(syllabus_program_id, laboratory_id, unit_id, week_number, session_number, modality_id,
      created_at, updated_at)
      select t.id,
      cast(case laboratory when '' then '0' else laboratory end as int) laboratory,
      u.id,
      cast(t.number_week as int)                                        week_number,
      cast(t.number_session as int)                                     session_number,
      cast(type_laboratory as int)                                      modality_id,
      now(),
      now()
      from tmp_migration_units t
      join units u on t.syllabus_id = u.syllabus_id and t.idx = u.unit_number
      left join sessions s on u.id = s.unit_id and cast(t.number_session as int) = s.session_number
      where s.id is null
      and t.topic != '' and t.modality_id=1
      order by t.id,cast(t.number_week as int) ,cast(t.number_session as int);"
    execute "insert into session_topics(session_id,topic_id,activities,created_at,updated_at)
    select s.id, tt.id, t.activities, now(), now()
    from tmp_migration_units t
             join units u on t.syllabus_id = u.syllabus_id and t.idx = u.unit_number
             join topics tt on u.id = tt.unit_id and tt.topic = t.topic
             join sessions s on u.id = s.unit_id and cast(t.number_session as int) = s.session_number
             left join session_topics st on s.id = st.session_id and u.id = st.topic_id
    where st.id is null and t.modality_id=1;"
    execute "drop table if exists tmp_consolidado;"
    execute "select semipresencial.*,
                    presencial.catalog_code                                         catalog_code_p,
                    presencial.cant_unidades                                        cant_unidades_p,
                    presencial.cant_semanas                                         cant_semanas_p,
                    presencial.cant_sesiones                                        cant_sesiones_p,
                    (presencial.cant_unidades = semipresencial.cant_unidades and
                    (presencial.cant_sesiones = semipresencial.cant_sesiones or
                      2 * presencial.cant_sesiones = semipresencial.cant_sesiones)) coincide
              INTO TEMP TABLE tmp_consolidado

              from (
                      select sp.id,
                            s.id                                  syllabus_id,
                            c.name,
                            pc.catalog_code,
                            pc.authorized,
                            count(1)                              cant_sesiones,
                            count(distinct order_unit)            cant_unidades,
                            count(distinct week -> 'number_week') cant_semanas
                      from syllabus_programs sp
                              join syllabuses s on sp.syllabus_id = s.id
                              join program_courses pc
                                    on s.course_id = pc.course_id and sp.modality_id = pc.modality_id
                              join cycles c on s.cycle_id = c.id
                              ,
                          jsonb_array_elements(sp.program_json -> 'units')
                              with ordinality as t(unit, order_unit),
                          jsonb_array_elements(unit -> 'weeks')
                              with ordinality as r(week, order_week),
                          jsonb_array_elements(week -> 'sessions')
                              with ordinality as q(session, order_session)
                      where sp.is_inactive = false
                        and sp.modality_id = 2
                      group by sp.id, s.id, pc.catalog_code, c.name, pc.authorized) semipresencial
                      join (
                select sp.id,
                        s.id                                  syllabus_id,
                        c.name,
                        pc.catalog_code,
                        pc.authorized,
                        count(1)                              cant_sesiones,
                        count(distinct order_unit)            cant_unidades,
                        count(distinct week -> 'number_week') cant_semanas
                from syllabus_programs sp
                          join syllabuses s on sp.syllabus_id = s.id
                          join program_courses pc
                              on s.course_id = pc.course_id and sp.modality_id = pc.modality_id
                          join cycles c on s.cycle_id = c.id
                        ,
                      jsonb_array_elements(sp.program_json -> 'units')
                          with ordinality as t(unit, order_unit),
                      jsonb_array_elements(unit -> 'weeks')
                          with ordinality as r(week, order_week),
                      jsonb_array_elements(week -> 'sessions')
                          with ordinality as q(session, order_session)
                where sp.is_inactive = false
                  and sp.modality_id = 1
                group by sp.id, s.id, pc.catalog_code, c.name, pc.authorized) presencial
                          on semipresencial.syllabus_id = presencial.syllabus_id;"
    execute "insert into sessions(syllabus_program_id, laboratory_id, unit_id, week_number, session_number, modality_id,
                            created_at, updated_at)
       select sp.id,
              cast(case sp.laboratory when '' then '0' else laboratory end as int) laboratory,
              p.id                                                                 unit_id,
              cast(sp.number_week as int),
              cast(sp.number_session as int),
              cast(sp.type_laboratory as int),
              now(),
              now()
       from (select m.*, (cast(number_session as int) + 1) / 2 new_number_session
             from tmp_migration_units m
                      join tmp_consolidado c on m.id = c.id and coincide = true and cant_sesiones = 2 * cant_sesiones_p
             where m.modality_id = 2) sp
                left join (select u.id, u.unit_number, u.syllabus_id, t.id topic_id, t.topic, tm.number_session
                           from units u
                                    join topics t on u.id = t.unit_id
                                    join tmp_migration_units tm
                                         on tm.syllabus_id = u.syllabus_id and tm.idx = u.unit_number and
                                            tm.modality_id = 1 and
                                            t.topic = tm.topic
                           group by u.id, u.unit_number, u.syllabus_id, t.id, t.topic, tm.number_session
       ) p on sp.syllabus_id = p.syllabus_id and
              cast(p.number_session as int) = new_number_session
           and sp.modality_id = 2
                left join sessions
                          on sessions.syllabus_program_id = sp.id and sessions.session_number = cast(sp.number_session as int)
       where sessions.id is null
       order by sp.syllabus_id, cast(sp.number_session as int)"

    execute("insert into sessions(syllabus_program_id, laboratory_id, unit_id, week_number, session_number, modality_id,
                            created_at, updated_at)
       select sp.id,
              cast(case sp.laboratory when '' then '0' else laboratory end as int) laboratory,
              p.id                                                                 unit_id,
              cast(sp.number_week as int),
              cast(sp.number_session as int),
              cast(sp.type_laboratory as int),
              now(),
              now()
       from (select m.*, cast(number_session as int) new_number_session
             from tmp_migration_units m
                      join tmp_consolidado c on m.id = c.id and coincide = true and cant_sesiones = cant_sesiones_p
             where m.modality_id = 2) sp
                join (select u.id, u.unit_number, u.syllabus_id, t.id topic_id, t.topic, tm.number_session
                      from units u
                               join topics t on u.id = t.unit_id
                               join tmp_migration_units tm
                                    on tm.syllabus_id = u.syllabus_id and tm.idx = u.unit_number and
                                       tm.modality_id = 1 and
                                       t.topic = tm.topic
                      group by u.id, u.unit_number, u.syllabus_id, t.id, t.topic, tm.number_session
       ) p on sp.syllabus_id = p.syllabus_id and
              cast(p.number_session as int) = new_number_session
           and sp.modality_id = 2
                left join sessions
                          on sessions.syllabus_program_id = sp.id and sessions.session_number = cast(sp.number_session as int)
       where sessions.id is null
       order by sp.syllabus_id, cast(sp.number_session as int);")

    execute("insert into session_topics(session_id, topic_id, activities, created_at, updated_at)
       select s.id, p.topic_id, sp.activities, now(), now()
       from (select m.*, (cast(number_session as int) + 1) / 2 new_number_session
             from tmp_migration_units m
                      join tmp_consolidado c on m.id = c.id and coincide = true and cant_sesiones = 2 * cant_sesiones_p
             where m.modality_id = 2) sp
                join sessions s on sp.id = s.syllabus_program_id and cast(sp.number_session as int) = s.session_number
                join (select u.id, u.unit_number, u.syllabus_id, t.id topic_id, t.topic, tm.number_session
                      from units u
                               join topics t on u.id = t.unit_id
                               join tmp_migration_units tm
                                    on tm.syllabus_id = u.syllabus_id and tm.idx = u.unit_number and
                                       tm.modality_id = 1 and
                                       t.topic = tm.topic
                      group by u.id, u.unit_number, u.syllabus_id, t.id, t.topic, tm.number_session
       ) p on sp.syllabus_id = p.syllabus_id and
              cast(p.number_session as int) = new_number_session
           and sp.modality_id = 2
                left join session_topics st on s.id = st.session_id and st.topic_id = p.topic_id
       where st.id is null
       order by sp.syllabus_id, cast(sp.number_session as int);")

    execute("insert into session_topics(session_id, topic_id, activities, created_at, updated_at)
       select s.id, p.topic_id, sp.activities, now(), now()
       from (select m.*, cast(number_session as int) new_number_session
             from tmp_migration_units m
                      join tmp_consolidado c on m.id = c.id and coincide = true and cant_sesiones = cant_sesiones_p
             where m.modality_id = 2) sp
                join sessions s on sp.id = s.syllabus_program_id and cast(sp.number_session as int) = s.session_number
                join (select u.id, u.unit_number, u.syllabus_id, t.id topic_id, t.topic, tm.number_session
                      from units u
                               join topics t on u.id = t.unit_id
                               join tmp_migration_units tm
                                    on tm.syllabus_id = u.syllabus_id and tm.idx = u.unit_number and
                                       tm.modality_id = 1 and
                                       t.topic = tm.topic
                      group by u.id, u.unit_number, u.syllabus_id, t.id, t.topic, tm.number_session
       ) p on sp.syllabus_id = p.syllabus_id and
              cast(p.number_session as int) = new_number_session
           and sp.modality_id = 2
                left join session_topics st on s.id = st.session_id and st.topic_id = p.topic_id
       where st.id is null
       order by sp.syllabus_id, cast(sp.number_session as int);")
=end
  end
end
