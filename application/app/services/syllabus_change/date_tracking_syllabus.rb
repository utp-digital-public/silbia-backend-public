module SyllabusChange
  class DateTrackingSyllabus
    # params
    # @syllabus_program
    # @has_more_than_one_state
    ####
    def initialize(**attribute)
      attribute.each do |k, v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end

    def get_date_to_filter()
      if @has_more_than_one_state.present?
        return get_penultimate_date_to_be_approve()
      else
        return get_first_date_create()
      end
    end

    def get_last_date_to_be_approve()
      array_dates = get_last_two_dates_wich_state_was_to_be_approve()
      last_date = array_dates[0] if array_dates.present?
    end

    def get_first_date_create()
      @syllabus_program.created_at
    end

    def get_penultimate_date_to_be_approve()
      array_dates = get_last_two_dates_wich_state_was_to_be_approve()
      penultimate_date = array_dates[1] if array_dates.present?
      puts "ULTIMA ES #{array_dates[0]}"
      puts "PENULTIMA ES #{array_dates[1]}"
      penultimate_date
    end

    def get_last_two_dates_wich_state_was_to_be_approve()
      list_track =
        TrackingStatusSyllabus.where(syllabus_program_id: @syllabus_program.id)
          .where(state: TO_BE_APPROVED)
          .order("created_at DESC").limit(2)
      list_track.map { |track| track.created_at }
    end
  end
end
