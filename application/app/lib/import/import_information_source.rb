module Import
  class ImportInformationSource < Import::Entity
    def import_information_source_if_not_exist
      import_obj = Import::ConvertToJson.new
      total_records_create = 0
      @csv.each_with_index do |row, index|
        code_course = row['catalog_code'].strip
        code_catalog_book = row['item_number'].strip
        course = Course.where(catalog_code: code_course).first
        book = Book.where(code: code_catalog_book).first
        source_information_find = import_obj.exist_information_source?(book, course)
        unless source_information_find.present?
          source_inf = build_information_source(book, course,row)
          if source_inf.present?
            source_inf.save!
          end  
          total_records_create = total_records_create+1
          puts "#{code_course}, #{code_catalog_book} saved"
        else 
          puts "ESTA FUENTE DE INFORMACIÓN SÍ EXISTE fila =#{index} COURSE: #{code_course} book: #{code_catalog_book}"  

        end  
      end
      puts "New records was  #{total_records_create} created"
      puts "There are now #{InformationSource.count} rows in the transactions table"
    end

    def build_information_source(book, course,row)
      if book.present? && course.present?
        source_inf = InformationSource.new
        source_inf.book_id = book.id
        source_inf.course_id = course.id
        source_inf.type_source = row['type_source']
        source_inf.observation = row['observation']
        return source_inf
      else
        puts "No existe el libro con código #{row['item_number'].strip} para crear" unless book.present?
        puts "No existe el curso con código #{row['catalog_code'].strip} para crear" unless course.present?
      end  
    end  
    
  end  
end  