FactoryBot.define do
  factory :competition do
    career { "MyString" }
    competition_description { "MyString" }
    criterion { "MyString" }
    achievement_level { "MyString" }
    course 
  end
end
