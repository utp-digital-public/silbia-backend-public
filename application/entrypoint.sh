#!/bin/bash
set -e

# If the database exists, migrate. Otherwise setup (create and migrate)
bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:create db:migrate
echo "PostgreSQL database has been created & migrated!"

# Remove a potentially pre-existing server.pid for Rails.
if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi
# Precompile assets
bundle exec rake assets:precompile

# bundle exec bin/delayed_job -n 4 run

bundle exec rails server -b 0.0.0.0 -p 3000

