require "roo"
spreadsheet = Roo::Spreadsheet.open("./lib/seeds_csv/in_person_pc_1.xlsx")
(7..spreadsheet.last_row).each do |i|
  begin
    course = Course.new
    course.for_all_career = spreadsheet.row(i)[4].present? ? I18n.transliterate(spreadsheet.row(i)[4].to_s).downcase.strip.eql?("si") : false
    course.for_all_faculties = spreadsheet.row(i)[5].present? ? I18n.transliterate(spreadsheet.row(i)[5].to_s).downcase.strip.eql?("si") : false
    code_in_person = spreadsheet.row(i)[0].to_s
    code_in_person.strip! if code_in_person.present?
    credits_in_person = spreadsheet.row(i)[13]
    w_hours_in_person = spreadsheet.row(i)[15]
    supervisor = User.search_user_by_email(spreadsheet.row(i)[6].to_s)
    coordinator_preg = User.search_user_by_email(spreadsheet.row(i)[7].to_s)
    if supervisor.nil? or coordinator_preg.nil?
      p "No existe el supervisor o coordinador"
    end
    name_in_person = spreadsheet.row(i)[2].to_s
    name_in_person.strip! if name_in_person.present?
    course.program_courses.build(catalog_code: code_in_person, type: "InPerson", modality_id: 1, name: name_in_person, credits: credits_in_person, weekly_hours: w_hours_in_person, coordinator_id: coordinator_preg.id, supervisor_id: supervisor.id)
    s = nil
    begin
      if course.save!
        s = Syllabus.new
        s.cycle_id = Cycle.active.id
        s.cycle_academic = REGULAR_CYCLE
        s.year = 2020
        s.doc_template_id = 1
        s.curriculum_course_id = 1
        s.course = course
        fundamentation = spreadsheet.row(i)[9].to_s[0..1450] unless spreadsheet.row(i)[9].blank?
        summary = spreadsheet.row(i)[10].to_s[0..999] unless spreadsheet.row(i)[10].blank?
        achievement = spreadsheet.row(i)[11].to_s[0..999] unless spreadsheet.row(i)[11].blank?
        methodology = spreadsheet.row(i)[12].to_s[0..1799] unless spreadsheet.row(i)[12].blank?
        s.syllabus_content = Syllabus.set_content_json_silabus(fundamentation, summary, achievement)
        json_units = { units: [], texts: [Syllabus.populate_methodology_json_text(methodology)] }.to_json

        course.program_courses.each do |p_course|
          career = Career.where("modality = ? and unaccent(name) ILIKE unaccent(?)", 0, "Administracion de Negocios Internacionales").first
          unless career.nil?
            p_course.program_course_careers.build(career_id: career.id)
            begin
              p_course.save!
              puts "Se asoció el curso #{p_course.name} con la carrera #{career.name}"
            rescue Exception => e
              p "Error: #{p_course.errors.full_messages.first}"
            end
          end
          syllabus_param_hash = {
            program_json: JSON.parse(json_units),
            type_program: TYPE_PROGRAM_PREGRADO,
            state: NO_READ_STATE,
            coordinator_id: coordinator_preg.id,
            supervisor_id: supervisor.id,
            modality_id: PRESENCIAL_CODE,
            evaluation_system_json: { evaluation_system: [] },
            evaluation_rules_json: { rules: [] },
          }
          s.syllabus_programs.build(syllabus_param_hash)
          s.save!
        end
      end
    rescue Exception => e
      p "Error en la Fila #{i}: #{e.message}"
    end
  rescue Exception => e
    p "Error en la Fila #{i}: #{e.message}"
  end
end
