require 'highline'
module Import
  # ENV["custom_seed"] is a environment 
  ###### To run this excecute: ####
  # rails db:seed:name_of_seed custom_seed=yes
  class CustomPrompt
    def initialize()
      @cli = HighLine.new
    end

    def choice_cycle
      value_choice = nil
      @cli.choose do |menu|
        menu.index_color  = :rgb_999999 # override default color of index
        # you can also use constants like :blue
        menu.prompt = "Selecciona un ciclo"
        cycles = Cycle.all.order("id ASC")
        cycles.each do |cycle|
          menu.choices(cycle.name.to_sym) { value_choice = cycle.id }
        end                                       
      end
      #@cli.ask "question"
      puts "Proceso iniciado"
      value_choice 
    end
  end
end    