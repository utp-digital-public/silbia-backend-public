class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :code
      t.string :title, null: false
      t.string :author, default: "Sin autor"
      t.string :support
      t.integer :year, default: 0
      t.string :number_editorial
      t.string :editorial
      t.timestamps
    end
  end
end
