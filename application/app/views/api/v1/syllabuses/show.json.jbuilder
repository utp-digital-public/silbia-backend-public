json.status "success"
json.data do
    json.syllabus_program do
        json.partial! "api/v1/syllabuses/syllabus_program", syllabus_program: @syllabus_program
    end
    json.permissions                        @permsions_json
    json.laboratories                       @laboratories
    json.practices                          @practice_list
    json.changes_syllabus                   @change_detect_syllabus
    json.show_modal_sidebar                 @show_modal_sidebar
    json.syllabus_observations              @observation_syllabus
    json.syllabus_previews_observations     @observation_preview_syllabus
    json.careers_syllabus                   @careers_competition
    json.rules                              @syllabus_rules
end