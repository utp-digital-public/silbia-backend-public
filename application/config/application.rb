require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Simple
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    
    # Configu time zone  Lima - Peru and set default to active record
    config.time_zone = 'Lima'
    config.active_record.default_timezone = :local

    config.assets.paths << "#{Rails.root}/vendor/assets/javascript"
    config.generators do |g|
      g.test_framework :rspec,
      fixtures: true,
      view_specs: false,
      helper_specs: false,
      routing_specs: false
    end

    config.middleware.insert_before 0, Rack::Cors do
      ENV.fetch('CORS_ORIGIN_API').split(',').each do |n|
        allow do
          origins n
          resource '/api/v1/*', 
            :headers => :any, :methods => [:get, :patch, :put, :delete, :post, :options]
        end
      end
    end
    
    # Config Logger
    Rails.logger = Logger.new(STDOUT)
    config.logger = ActiveSupport::Logger.new("log/#{Rails.env}.log")
    
  end
end
