class AddIndexToCurriculumCourse < ActiveRecord::Migration[5.2]
  def change
    add_index :curriculum_courses, :modality_program
    add_index :curriculum_courses, :course_catalog_code
    add_index :curriculum_courses, :course_catalog_cgt_code
  end
end
