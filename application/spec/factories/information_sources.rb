FactoryBot.define do
  factory :information_source do
    association :course 
    association :book 
    observation {"This is one observation"}
    type_source { "Básica"}
  end

  trait :information_source_basic_with_link_units do
    before(:create) do |instance|
      instance.type_source = "Básica"
    end
    after(:create) do |instance|
      FactoryBot.create_list(:link_unit_information_source, 3, information_source_id: instance.id)
    end  
  end

  trait :information_source_complementary_with_link_units do
    before(:create) do |instance|
      instance.type_source = "Complementaria"
    end
    after(:create) do |instance|
      FactoryBot.create_list(:link_unit_information_source, 3, information_source_id: instance.id)
    end  
  end

  trait :information_sources_basic do
    type {"Básica" }
  end

  trait :information_sources_complementary do
    type { "Complementaria"}
  end
end
