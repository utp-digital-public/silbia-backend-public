class Syllabus < ApplicationRecord
  audited
  belongs_to :doc_template
  belongs_to :curriculum_course
  belongs_to :cycle
  belongs_to :course
  has_many :syllabus_programs, :dependent => :destroy
  has_many :units, :dependent => :destroy
  has_many :topics, :through => :units
  has_many :syllabus_information_sources, :dependent => :destroy, :inverse_of => :syllabus
  has_many :books, :through => :syllabus_information_sources
  extend Enumerize
  enumerize :cycle_academic, in: { regular: 1, verano: 2 }
  validates :year, :cycle, :cycle_academic, presence: { message: "es requerido" }
  validates_numericality_of :year, only_integer: true, message: "debe ser un numero entero"

  scope :active, -> { where(cycle_id: Cycle.active.id).first }
  # def self.modality
  #   modality.camelcase
  # end

  def self.set_content_json_silabus(fundamentation, sumilla, specific_achievement_of_learning)
    obj_fundamentation = Import::TextComponent.new(
      title: "fundamentación",
      content: fundamentation || "",
      max_words: 4000,
      edit_only_pregrade: true,
      section_template_id: 1,
    )
    obj_sumilla = Import::TextComponent.new(
      title: "sumilla",
      content: sumilla || "",
      max_words: 4000,
      edit_only_pregrade: true,
      section_template_id: 2,
    )
    obj_specific_achievement_of_learning = Import::TextComponent.new(
      title: "logro general de aprendizaje",
      content: specific_achievement_of_learning || "",
      max_words: 4000,
      edit_only_pregrade: true,
      section_template_id: 3,
    )
    title = "", content = "", max_words = 200, edit_only_pregrade = true, section_template_id = 1
    result = {
      texts: [
        obj_fundamentation.as_json,
        obj_sumilla.as_json,
        obj_specific_achievement_of_learning.as_json,
      ],
      #evaluation_system: set_evalution_system_silabus(row["formulas_evaluacion"],6),
      rules: [],
    }.to_json

    result = JSON.parse(result)
  end

  def self.populate_methodology_json_text(methodology_content)
    text_methodology = Import::TextComponent.new(
      title: "metodología",
      content: methodology_content,
      max_words: 4000,
      edit_only_pregrade: false,
      section_template_id: 3,
    )
    text_methodology
  end

  def modalities
    course_deleted = course.program_courses.select { |c| c["deleted"] == true && c["modality_id"] == PRESENCIAL_CODE }.first
    modalities = []
    syllabus_programs.each do |syllabus_program|
      order = syllabus_program.modality_id
      if syllabus_program.modality_id == VIRTUAL_CODE && course_deleted.present?
        order = 1
      end

      modalities.push({
        syllabus_program: syllabus_program,
        order: order,
        state: syllabus_program.state,
        observations: syllabus_program.current_observations.group_by { |o| o.type },
        editions: syllabus_program.current_editions.group_by { |o| o.type },
        prev_observations: syllabus_program.last_revision_observations.group_by { |o| o.type },
      })
    end
    return modalities.sort_by { |k| k[:order] }
  end

  def all_user_authorized
    return syllabus_programs.pluck("coordinator_id") + syllabus_programs.pluck("supervisor_id")
  end

  def base_syllabus_program
    course_deleted = course.program_courses.select { |c| c["deleted"] == true && c["modality_id"] == PRESENCIAL_CODE }.first
    if course_deleted
      return syllabus_programs.select { |s| s["modality_id"] == VIRTUAL_CODE }.first
    else
      return syllabus_programs.select { |s| s["modality_id"] == PRESENCIAL_CODE }.first
    end
  end
end
