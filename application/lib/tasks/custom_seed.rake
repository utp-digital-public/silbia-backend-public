# lib/tasks/custom_seed.rake
namespace :db do
  namespace :seed do
    directories = 
    ['custom_seeds','sprint-13','sprint-14',
     'sprint-15','sprint-16','min-features']
    directories.each do |directory|
      Dir[Rails.root.join('db', directory, '*.rb')].each do |filename|
        task_name = File.basename(filename, '.rb')
        desc "Seed " + task_name + ", based on the file with the same name in `db/custom_seeds/*.rb`"
        task task_name.to_sym => :environment do
          load(filename) if File.exist?(filename)
        end
      end
    end  
  end
end

