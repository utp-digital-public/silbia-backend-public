module Import
  class ImportTypeOfPractice < Import::Entity
    def import_type_of_practices_if_not_exist
      total_records_create = 0
      @csv.each_with_index do |row, index|
        id_type_of_practice = row["id"].strip.to_i
        name = row['name']
        type_of_practice = TypeOfPractice.where(id:id_type_of_practice) 
        unless type_of_practice.present?
          type_of_practice = build_type_of_practice(row)
          type_of_practice.save!
          total_records_create = total_records_create+1
          puts "#{type_of_practice.name} saved"
        else
          puts "YA EXISTE Y ES "+  type_of_practice.name 
        end
      end
      puts "New records was  #{total_records_create} created"
      puts "There are Total  #{TypeOfPractice.count} rows in the transactions table"
    end

    def build_type_of_practice(row)
      type_of_practice = TypeOfPractice.new
      type_of_practice.name = row['name']
      type_of_practice 
    end  
    
  end  
end  