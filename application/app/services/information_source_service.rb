class InformationSourceService
  attr_reader :id_course, :type_source, :id_book

  def initialize(**attributes)
    attributes.each do |k,v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
  end
  def get_information_sources_by_type(type_source)
    information_sources = InformationSource.where(course_id: id_course).order("id ASC")
    if type_source == "Básica"
      information_sources = information_sources.basic
    else
      information_sources = information_sources.complementary
    end
    information_sources.includes(:book)
  end  
end  