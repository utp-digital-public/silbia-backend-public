class AddCourseToSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_reference :syllabuses, :course, foreign_key: true
  end
end
