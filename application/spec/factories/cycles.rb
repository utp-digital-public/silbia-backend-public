FactoryBot.define do
  factory :cycle do
    sequence(:name) { |n| "2019 March #{n}" }
    enabled { true }
    cycle_start { DateTime.now }
    cycle_end { DateTime.now}
  end
  
  factory :cycle_to_copy, class: Cycle do
    sequence(:name) { |n| "2020 March #{n}" }
    enabled { true }
    cycle_start { DateTime.now }
    cycle_end { DateTime.now}
  end

  factory :cycle_disabled, class: Cycle do
    sequence(:name) { |n| "2020 March #{n}" }
    enabled { false }
    cycle_start { DateTime.now }
    cycle_end { DateTime.now}
  end
end
