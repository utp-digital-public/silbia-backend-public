class RenameTypeColumnToInformationSources < ActiveRecord::Migration[5.2]
  def change
    rename_column :information_sources, :type, :type_source
  end
end
