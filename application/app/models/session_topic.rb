class SessionTopic < ApplicationRecord
  audited associated_with: :session
  belongs_to :topic
  belongs_to :session
end
