class CreateCourseSpecificCompetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :course_specific_competitions do |t|
      t.references :program_course_career, foreign_key: true
      t.references :specific_competition, foreign_key: true
      t.integer :level, null: false
      t.timestamps
    end
  end
end
