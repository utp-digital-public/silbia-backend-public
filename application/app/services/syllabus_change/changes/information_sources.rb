module SyllabusChange
  module Changes
    class InformationSources < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::UtilDetectCronogram
      def initialize(**attribute)
        super(**attribute)
        @title = "Fuentes de Información"
        @id_section = "section-fuentes"
        @changes_detected = 0
        @is_edited = false
        @course = @syllabus_program.syllabus.course
      end
       
      def detect()
        date_track = SyllabusChange::DateTrackingSyllabus.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: @has_more_than_one_state
        )
        date_to_filter = date_track.get_date_to_filter()
        @is_edited = detect_changes_in_link_and_information_sources?(date_to_filter)
      end 
      
      def get_array_id_information_source_by_course(course)
        list_information = InformationSource.information_sources_by_course(course.id)
        list_information.map{ |information_source| information_source.id }
      end  

      def detect_changes_in_link_and_information_sources?(date_to_filter)
        changes_detected_information = information_source_have_changes?(date_to_filter)
        changes_link_units = link_units_have_changes?(date_to_filter)
        #puts "FLAG INFORMATION ES: #{changes_detected_information} FLAG LINK_UNIT #{changes_link_units}"
        if ((changes_detected_information) || (changes_link_units) )
          return true
        else 
          return false  
        end  
      end

      def information_source_have_changes?(date_to_filter)
        audits = 
          Audited.audit_class.where('audited_changes @> ?', {course_id: @course.id}.to_json)
          .where("auditable_type=?","InformationSource").reorder("created_at DESC")
        audits = audits.where("created_at>=?", date_to_filter) 
        return true if audits.present?
        false
      end
      
      def link_units_have_changes?(date_to_filter)
        array_id_informations = get_array_id_information_source_by_course(@course)
        audits = get_list_audits(array_id_informations, date_to_filter) 
        audits.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
           return true if compare_if_changes_ocurred?(key,value)
          end
        end
        puts "hasta aqui no paso nada"
        return true if is_the_first_time_unit_link_records_created?(audits)   
        false
      end

      def get_list_audits(array_id_informations, date_to_filter)
        audits = Audited.audit_class.where("associated_id IN (?)", array_id_informations)
        audits = audits.where("auditable_type=?","LinkUnitInformationSource").reorder("created_at DESC")
        audits = audits.where("created_at>=?", date_to_filter)
        audits 
      end
      
      def is_the_first_time_unit_link_records_created?(audits)
        puts "TIENE MAS DE UN ESTADO ES: #{@has_more_than_one_state}"
        #if ( (has_not_more_than_one_state?() ) && (audits.present?) )
         if ( (not_have__more_than_one_state?() ) && 
             (verify_if_have_unit_linked_with_check?(audits)) )
          return true 
        else  
         return false
        end 
      end

      def verify_if_have_unit_linked_with_check?(audits)
        number_check_true = true
        audits.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            if ( (key == "is_checked" ) && (get_last_value_of_audit(value)==true) )
              puts "HAY UNO QUE ES  TRUE"
              return true
            end  
           #return true if compare_if_changes_ocurred?(key,value)
          end
        end
        return false
      end
      
     def iterate_audits_for_information_sources_block(audits)
      audits.try(:each) do |audit|
        audit.audited_changes.each do |key, value|
          yield(key,value)
        end
      end
     end  
      
      def not_have__more_than_one_state?
        @has_more_than_one_state ? false : true
      end  

      def compare_if_changes_ocurred?(key,value)
        keys_to_compare= ['page_unit','is_checked','from_to_page']
        if keys_to_compare.include? key
          puts "hasta aqui entro comparar"
          value_before_change = get_value_before_the_last_change(value)
          value_after_change = get_last_value_of_audit(value)
           puts " Su key es #{key} El valor anterior es: #{get_value_before_the_last_change(value)} El valor nuevo es : #{get_last_value_of_audit(value)} El ultimo : #{value}"
           if ( value_before_change != value_after_change )
            return true
           end 
        end  
      end  
  
       # This method overrite
      def set_hash_value(key, value)
        @hash_values[:information_source] = get_value_before_the_last_change(value) if key == "program_json"
      end  
      
    end  
  end
end  