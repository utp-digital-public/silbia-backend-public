require 'rails_helper'
#TODO: complete test 
RSpec.describe DashboardController, type: :controller do
  let!(:user){ FactoryBot.create(:user, :approver) }
  before { sign_in user }
  
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
