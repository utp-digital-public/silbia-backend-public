class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :catalog_code, null:false
      t.string :name, null:false
      t.string :short_name
      t.string :catalog_code_cgt

      t.timestamps
    end
  end
end
