module SyllabusChange
  class DetectChangeSection
    attr_accessor :syllabus_program, :date_change_to_be_approved,
      :array_changes_in_syllabus, :hash_values, :list_auditable_objects_syllabus

    def initialize(syllabus_program, is_base)
      @syllabus_program = syllabus_program
      @is_base = is_base
      @hash_values = Hash.new
      @section_detect_array = [{ section: "fundamentación", section_id: 2, type: "shared" },
                               { section: "Sumilla", section_id: 3, type: "shared" },
                               { section: "Logro general de aprendizaje", section_id: 4, type: "shared" },
                               { section: "Unidades", section_id: 5, type: "shared" },
                               { section: "Metodología", section_id: 6, type: "unique" },
                               { section: "Sistema de evaluación", section_id: 7, type: "unique" },
                               { section: "Fuentes de Información", section_id: 8, type: "shared" },
                               { section: "Cronograma", section_id: 9, type: "unique" }]
      @array_changes_in_syllabus = Array.new
    end

    def calculate
      has_more_than_one_state = true
      object_track_with_last_date_to_be_approved = get_last_date_change_to_be_approved
      @last_date = (object_track_with_last_date_to_be_approved.present?) ? object_track_with_last_date_to_be_approved.created_at : @syllabus_program.created_at
      puts "la ultima fecha fue: #{@last_date}"
      objects_audit =
        @syllabus_program.audits.where(user_id: @syllabus_program.coordinator_id).where("created_at >=?", @last_date).reorder("version DESC")

      if (!@is_base)
        #detect changes in shared sections
        puts "cambios en las secciones unicas"
        @section_detect_array = @section_detect_array.select { |x| x[:type] == "unique" }
      end
      obtain_changes_in_syllabus(@last_date, has_more_than_one_state)
      @section_detect_array.each do |section_detect|
        populate_array_changes(section_detect, objects_audit)
      end
    end

    def obtain_changes_in_syllabus_last_preview(last_date_in_date_time, preview_date_in_date_time, has_more_than_one_state)
      auditable_object = SyllabusChange::AuditSyllableObject.new(syllabus_program: @syllabus_program)
      @list_auditable_objects_syllabus =
        auditable_object.get_audits_sylabus_with_date_previews_and_last(last_date_in_date_time, preview_date_in_date_time)
    end

    def obtain_changes_in_syllabus(last_date_in_date_time, has_more_than_one_state)
      auditable_object = SyllabusChange::AuditSyllableObject.new(syllabus_program: @syllabus_program)
      @list_auditable_objects_syllabus = Audited::Audit.all
        .where(auditable_type: "Syllabus")
        .where(auditable_id: @syllabus_program.syllabus.id)
        .where(user_id: @syllabus_program.coordinator_id)
        .where("created_at >= ?", last_date_in_date_time)
        .order("created_at DESC")

      # if has_more_than_one_state.present?
      #   @list_auditable_objects_syllabus =
      #     auditable_object.get_audits_sylabus_with_date_greather_than(last_date_in_date_time)
      # else
      #   @list_auditable_objects_syllabus =
      #   #auditable_object.get_audits_sylabus_with_date_are_equals(last_date_in_date_time)
      #   auditable_object.get_audits_sylabus_with_date_greather_than(last_date_in_date_time)
      # end
    end

    def get_last_date_change_to_be_approved
      last_date_to_be_approved =
        TrackingStatusSyllabus.where(syllabus_program_id: @syllabus_program.id)
          .where(state: TO_BE_APPROVED).order("created_at DESC").first
    end

    def get_last_two_dates_wich_state_was_to_be_approve()
      list_track =
        TrackingStatusSyllabus.where(syllabus_program_id: @syllabus_program.id)
          .where(state: TO_BE_APPROVED)
          .order("created_at DESC").limit(2)
      list_track.map { |track| track.created_at }
    end

    def populate_array_changes(section_detect, objects_audit)
      has_more_than_one_state = true
      case section_detect[:section_id]
      when 2
        foundation_audit = SyllabusChange::Changes::Foundation.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state,
        )
        foundation_audit.call
        foundation_audit.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(foundation_audit.to_hash) if foundation_audit.is_edited
        #@array_changes_in_syllabus.push({ section_id: section_detect.section_id, revision_number: nil })
      when 3
        sumilla_audit = SyllabusChange::Changes::Sumilla.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state,
        )
        sumilla_audit.call
        sumilla_audit.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(sumilla_audit.to_hash) if sumilla_audit.is_edited
      when 4
        achievement_audit = SyllabusChange::Changes::GeneralAchievement.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state,
        )
        achievement_audit.call

        achievement_audit.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(achievement_audit.to_hash) if achievement_audit.is_edited
      when 5
        units_topics = SyllabusChange::Changes::Units.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: has_more_than_one_state,
          last_date: @last_date,
        )
        units_topics.detect()
        units_topics.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(units_topics.to_hash) if units_topics.is_edited
      when 6
        methodology = SyllabusChange::Changes::Methodology.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit,
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state,
        )
        methodology.call()
        methodology.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(methodology.to_hash) if methodology.is_edited
      when 7
        evaluation_system = SyllabusChange::Changes::EvaluationSystem.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit,
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state,
        )
        evaluation_system.call()

        rules = SyllabusChange::Changes::Ruler.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit,
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state,
          last_date: @last_date,
        )
        rules.call()
        p "RULES ES #{rules.is_edited}"
        p "EVALUATION SYSTEMA ES: #{evaluation_system.is_edited}"
        evaluation_system.is_edited = true if rules.is_edited.present?

        evaluation_system.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(evaluation_system.to_hash) if evaluation_system.is_edited
      when 8
        information_sources = SyllabusChange::Changes::SyllabusInformationSources.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: has_more_than_one_state,
          last_date: @last_date,
        )
        information_sources.detect()
        information_sources.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(information_sources.to_hash) if information_sources.is_edited
      when 9
        sessions_changes = SyllabusChange::Changes::Sessions.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: has_more_than_one_state,
          last_date: @last_date,
        )
        sessions_changes.detect()
        sessions_changes.id_section = section_detect[:section_id]
        @array_changes_in_syllabus.push(sessions_changes.to_hash) if sessions_changes.is_edited
      end
    end

    def population_array_changes(section_detect, objects_audit, has_more_than_one_state, audits_evaluation_system)
      case section_detect
      when "general_data"
        @array_changes_in_syllabus.push({
          title: "Datos Generales",
          is_edited: false,
          id_section: "section-datosgenerales",
        })
      when "fundamentation"
        foundation_audit = SyllabusChange::Changes::Foundation.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state,
        )
        foundation_audit.call
        @array_changes_in_syllabus.push(foundation_audit.to_hash)
      when "sumilla"
        sumilla_audit = SyllabusChange::Changes::Sumilla.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state,
        )
        sumilla_audit.call
        @array_changes_in_syllabus.push(sumilla_audit.to_hash)
      when "general_achievement"
        achievement_audit = SyllabusChange::Changes::GeneralAchievement.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state,
        )
        achievement_audit.call
        @array_changes_in_syllabus.push(achievement_audit.to_hash)
      when "evaluation_system"
        evaluation_system = SyllabusChange::Changes::EvaluationSystem.new(
          syllabus_program: @syllabus_program,
          objects_audit: audits_evaluation_system,
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state,
        )
        evaluation_system.call()

        rules = SyllabusChange::Changes::Ruler.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit,
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state,
        )
        rules.call()
        p "RULES ES #{rules.is_edited}"
        p "EVALUATION SYSTEMA ES: #{evaluation_system.is_edited}"
        evaluation_system.is_edited = true if rules.is_edited.present?
        @array_changes_in_syllabus.push(evaluation_system.to_hash)
      when "methodology"
        methodology = SyllabusChange::Changes::Methodology.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit,
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state,
        )
        methodology.call()
        @array_changes_in_syllabus.push(methodology.to_hash)
      when "cronogram"
        cronogram = SyllabusChange::Changes::ScheduleActivityChange.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit,
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state,
        )
        cronogram.detect()
        @array_changes_in_syllabus.push(cronogram.to_hash)
      when "information_source"
        information_sources = SyllabusChange::Changes::InformationSources.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: has_more_than_one_state,
        )
        information_sources.detect()
        @array_changes_in_syllabus.push(information_sources.to_hash)
      else
      end
    end

    def has_more_than_one_state_by?(state)
      number_of_found_states = 0
      @syllabus_program.audits_desc.try(:each) do |audit|
        audit.audited_changes.try(:each) do |key, value|
          number_of_found_states += 1 if ((key == "state") && (value[1] == state))
          return true if number_of_found_states > 1
        end
      end
      return false
    end
  end
end
