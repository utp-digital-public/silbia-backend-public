class SyllabusProgram < ApplicationRecord
  audited associated_with: :syllabus
  include StateMachineSyllabus
  extend Enumerize
  enumerize :type_program, in: { :pregrado => 1, :cgt => 2 }
  #enumerize :state, in: {:pending=>1,:process=>2,:to_be_approved=>3, :approved=>4,:observed=>5}
  enum state: {
    pending: 1,
    in_progress: 2,
    to_be_approved: 3,
    approved: 4,
    observed: 5,
  }

  scope :actives, -> { where(:is_inactive => false) }
  scope :authorized, -> { where(:authorized => true) }
  scope :inactives, -> { where(:is_inactive => true) }
  scope :whit_cycle_active, -> { joins(:syllabus => :cycle).where("cycles.enabled =?", true) }
  scope :match_course_coordinator, -> {
          includes(syllabus: [{ course: [:program_courses] }, :cycle])
            .includes(:coordinator)
            .includes(:modality)
        }
  # Ex:- scope :active, -> {where(:active => true)}
  scope :number_of_syllabuses, ->(cycle_id) {
          joins(:syllabus => :course).where("syllabuses.cycle_id=?", cycle_id).count
        }
  scope :syllabus_by_cycle, ->(cycle_id) {
          joins(:syllabus).where("syllabuses.cycle_id=?", cycle_id)
        }

  def audits_desc
    self.audits.reorder("created_at DESC")
  end

  belongs_to :syllabus
  has_many :syllabus_messages, :dependent => :destroy
  has_many :sessions, :dependent => :destroy
  has_many :session_topics, :through => :sessions
  has_many :topics, :through => :session_topics
  has_many :syllabus_editions, :dependent => :destroy
  validates :program_json, :modality_id, presence: { message: "es requerido" }
  belongs_to :coordinator, class_name: "User", foreign_key: "coordinator_id"
  belongs_to :supervisor, class_name: "User", foreign_key: "supervisor_id", optional: true
  belongs_to :modality
  accepts_nested_attributes_for :syllabus_messages

  SQL_ORDER_FOR_APPROVER = "
  CASE 
    WHEN state = #{TO_BE_APPROVED} THEN 1
    WHEN state = #{EDITION_STATE} THEN 2
    WHEN state = #{NO_READ_STATE} THEN 3
    WHEN state = #{APPROVED_STATE} THEN 4
    WHEN state = #{REJECTED_STATE} THEN 5
  END
  "

  SQL_ORDER_FOR_EDITOR = "
  CASE 
    WHEN state = #{NO_READ_STATE} THEN 1
    WHEN state = #{EDITION_STATE} THEN 2
    WHEN state = #{TO_BE_APPROVED} THEN 3
    WHEN state = #{APPROVED_STATE} THEN 4
    WHEN state = #{REJECTED_STATE} THEN 5
  END 
  "

  SQL_ORDER_COURSE = "
  (SELECT name FROM program_courses where courses.id = program_courses.course_id AND program_courses.modality_id = syllabus_programs.modality_id ) 

  "

  def state_integer_value
    self.state_before_type_cast
  end

  def self.getArrStates
    [{ label: "Todos", value: "" },
     { label: "Pendiente", value: 1 },
     { label: "En Edición", value: 2 },
     { label: "En Revisión", value: 3 },
     { label: "Aprobado", value: 4 },
     { label: "Observado", value: 5 }]
  end
  def self.getArrStatesNumeric
    [{ label: "Todos", value: 0 },
     { label: "Pendiente", value: 1 },
     { label: "En Edición", value: 2 },
     { label: "En Revisión", value: 3 },
     { label: "Aprobado", value: 4 },
     { label: "Observado", value: 5 }]
  end

  def getStateName(arrayStates)
    state = self.state_integer_value
    arrayStates.try(:each) do |element|
      if state == element[:value]
        return element[:label]
      end
    end
  end

  # call to  SyllabusProgram.priority_approver(params[:direction]) for send sort in controller.
  def self.order_to_approver(direction = "ASC")
    # Prevent injection by making sure the direction is either ASC or DESC
    order_for = if direction == "ASC" then "ASC" else "DESC" end
    self.custom_order_to_dashboard(order_for, SQL_ORDER_FOR_APPROVER)
      .order(:id)
  end

  def self.order_to_creator(direction = "ASC")
    order_for = if direction == "ASC" then "ASC" else "DESC" end
    self.custom_order_to_dashboard(order_for, SQL_ORDER_FOR_EDITOR)
      .order(:id)
  end

  def self.custom_order_to_dashboard(order_for, sql_editor)
    order(
      Arel.sql(
        "#{sql_editor}  #{order_for} ,#{order_alphabetic_insensitive_accent}, syllabus_programs.type_program"
      )
    )
  end

  def self.custom_order_to_list()
    order(
      Arel.sql(
        "unaccent(#{SQL_ORDER_COURSE}), syllabus_programs.type_program"
      )
    )
  end

  def self.order_alphabetic_insensitive_accent
    "unaccent(#{SQL_ORDER_COURSE})"
  end

  def get_catalog_code
    unless get_program_course.nil?
      get_program_course.catalog_code
    else
      "NO_TIENE_CODIGO_ASIGNADO"
    end
  end

  def get_program_course
    self.syllabus.course.program_courses.where(modality_id: self.modality_id).first
  end

  def code_catalog_of_syllabus
    self.get_program_course.catalog_code
  end

  def name_syllabus
    self.get_program_course.name
  end

  def permissible_states_for_current_state_plus_current(without_current)
    states = aasm.states(permitted: true).map { |state| [state.human_name, state.name.to_s] }
    states = states.unshift([aasm.human_state, state]) unless without_current
    states
  end

  def weekly_hours
    if get_program_course.modality_id == PRESENCIAL_CODE
      get_program_course.weekly_hours * (if self.syllabus.cycle.type_cycle == SUMMER_CYCLE then 2 else 1 end)
    elsif get_program_course.modality_id == VIRTUAL_CODE
      get_program_course.weekly_hours * (if self.syllabus.cycle.type_cycle == SUMMER_CYCLE && get_program_course.is_base_course then 2 else 1 end)
    else
      get_program_course.weekly_hours
    end
  end

  def credits
    get_program_course.credits
  end

  def human_type_program
    if type_program == :cgt
      CGT
    else
      PREGRADO
    end
  end

  def self.generate_pdf_collection(syllabus_program_ids, status)
    FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/pdfs/pregrado/*"))
    FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/pdfs/cgt/*"))
    Modality.all.each do |modality|
      FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/pdfs/#{modality.name.gsub(/\s+/, "_").downcase}/*"))
    end
    syllabus_program_ids.each_with_index do |id, index|
      syllabus_p = SyllabusProgram.find(id)
      syllabus_p.delay.store_pdf
    end
  end

  def has_footnote?
    syllabus.cycle.modality_cycles.where(modality_id: modality_id).present?
  end

  def get_footnote
    if has_footnote?
      syllabus.cycle.modality_cycles.where(modality_id: modality_id).first.footnote
    else
      nil
    end
  end

  def self.format_name_pdf_sillabus(syllabus_program)
    ApplicationController.helpers.format_name_pdf_sillabus(syllabus_program)
  end

  def self.make_zip(status, cycle)
    input_directory = "#{Rails.root}/public/pdfs"
    FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/*.zip"))
    input_filenames = Dir["#{input_directory}/**/**"].select { |f| !File.directory? f }

    zipfile_name = "silabos-#{Time.zone.now.strftime("%Y-%m-%d-%H-%M-%S")}.zip"
    zipfile_path = "#{Rails.root}/public/#{zipfile_name}"

    Zip::File.open(zipfile_path, Zip::File::CREATE) do |zipfile|
      input_filenames.each do |filename|
        zipfile.add(filename.sub(input_directory + "/", ""), filename)
      end
    end
    # send_file(File.join("#{Rails.root}/public/", 'myfirstzipfile.zip'), :type => 'application/zip', :filename => "#{Time.now.to_date}.zip")
    # Remove content from ‘my_pdfs’ folder if you want
    public_zip_url = set_s3_object(zipfile_name, zipfile_path, cycle)
    FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/pdfs/pregrado/*"))
    FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/pdfs/cgt/*"))
    Modality.all.each do |modality|
      FileUtils.rm_rf(Dir.glob("#{Rails.root}/public/pdfs/#{modality.name.gsub(/\s+/, "_").downcase}/*"))
    end
    status.finish_process(public_zip_url)
  end

  def store_pdf(format_download = "complete")
    av = ActionView::Base.new()
    av.view_paths = ActionController::Base.view_paths
    av.class_eval do
      include Rails.application.routes.url_helpers
      include ApplicationHelper
      include SyllabusesHelper
      include Pdf::SyllabusPdfHelper
    end
    @syllabus_program = self
    nombre_reporte = SyllabusProgram.format_name_pdf_sillabus(@syllabus_program)
    orientacion = "Portrait"
    @syllabus = @syllabus_program.syllabus
    @information_sources_basic = @syllabus.course.information_sources.basic.all_relations_included.order_by_type_source
    @information_sources_complementary = @syllabus.course.information_sources.complementary.all_relations_included.order_by_type_source

    @course = @syllabus_program.syllabus.course
    @careers_competition = @syllabus_program.get_program_course.careers.pluck("name")
    if format_download == "complete"
      pdf = av.render pdf: nombre_reporte, template: "/syllabuses/pdf/template.pdf", :locals => { :syllabus => @syllabus, :syllabus_program => @syllabus_program, :course => @course, :information_sources_basic => @information_sources_basic, :information_sources_complementary => @information_sources_complementary, :careers_competition => @careers_competition }
    elsif format_download == "summary"
      pdf = av.render pdf: nombre_reporte, template: "/syllabuses/pdf/summarized_template.pdf", :locals => { :syllabus => @syllabus, :syllabus_program => @syllabus_program, :course => @course, :information_sources_basic => @information_sources_basic, :information_sources_complementary => @information_sources_complementary, :careers_competition => @careers_competition }
    end
    doc_pdf = WickedPdf.new.pdf_from_string(pdf, encoding: "UTF-8", page_size: "A4", orientation: "Portrait", margin: { top: 20, bottom: 17, left: 18, right: 20 }, footer: { spacing: 30 })
    # program_type = @syllabus_program.type_program == TYPE_PROGRAM_PREGRADO ? "pregrado" : "cgt"
    save_path = Rails.root.join("public", "pdfs", "#{@syllabus_program.modality.name.gsub(/\s+/, "_").downcase}", "#{nombre_reporte}.pdf")
    File.open(save_path, "wb") do |file|
      file << doc_pdf
    end
    ActiveRecord::Base.transaction do
      last_job = JobStatus.last
      last_job.lock!
      last_job.increase_progress
    end
  end

  def store_pdf_s3(format_download = "complete", parent_folder = "pdfs", draft = false)
    av = ActionView::Base.new()
    av.view_paths = ActionController::Base.view_paths
    av.class_eval do
      include Rails.application.routes.url_helpers
      include ApplicationHelper
      include SyllabusesHelper
      include Pdf::SyllabusPdfHelper
    end
    begin
      @syllabus_program = self
      nombre_reporte = SyllabusProgram.format_name_pdf_sillabus(@syllabus_program)
      orientacion = "Portrait"
      @syllabus = @syllabus_program.syllabus
      @information_sources_basic = @syllabus.syllabus_information_sources.basic.all_relations_included.order_by_type_source
      @information_sources_complementary = @syllabus.syllabus_information_sources.complementary.all_relations_included.order_by_type_source
      @course = @syllabus_program.syllabus.course
      @careers_competition = @syllabus_program.get_program_course.careers.pluck("name")
      @program_course = @syllabus_program.get_program_course
      draft = false if @syllabus_program.approved?
      if format_download == "complete"
        pdf = av.render pdf: nombre_reporte, template: "/syllabuses/pdf/template.pdf", :locals => { :draft => draft, :syllabus => @syllabus, :syllabus_program => @syllabus_program, :course => @course, :information_sources_basic => @information_sources_basic, :information_sources_complementary => @information_sources_complementary, :careers_competition => @careers_competition, :program_course => @program_course }
      elsif format_download == "summary"
        pdf = av.render pdf: nombre_reporte, template: "/syllabuses/pdf/summarized_template.pdf", :locals => { :draft => draft, :syllabus => @syllabus, :syllabus_program => @syllabus_program, :course => @course, :information_sources_basic => @information_sources_basic, :information_sources_complementary => @information_sources_complementary, :careers_competition => @careers_competition, :program_course => @program_course }
      end

      doc_pdf = WickedPdf.new.pdf_from_string(
        pdf,
        encoding: "UTF-8",
        page_size: "A4",
        orientation: "Portrait",
        margin: { top: 30, bottom: 30, left: 20, right: 20 },
        dpi: 100,
      )
      # program_type = @syllabus_program.type_program == TYPE_PROGRAM_PREGRADO ? "pregrado" : "cgt"

      save_path = Rails.root.join("public", "pdfs", "#{@syllabus_program.modality.name.gsub(/\s+/, "_").downcase}", "#{nombre_reporte}.pdf")
      File.open(save_path, "wb") do |file|
        file << doc_pdf
      end
      cycle = @syllabus_program.syllabus.cycle.name
    rescue => exception
      return { error: "error al generar silabo", filename: nil, filename_error: nombre_reporte, :error => exception }
    end

    begin
      s3 = Aws::S3::Resource.new(region: ENV.fetch("AWS_REGION"),
                                 access_key_id: ENV.fetch("AWS_ACCESS_KEY_ID"),
                                 secret_access_key: ENV.fetch("AWS_SECRET_ACCESS_KEY"))
      bucket_name = ENV.fetch("S3_ASSET_DIRECTORY")
      @s3_obj = s3.bucket(bucket_name).object("#{parent_folder}/#{format_download}/#{cycle}/#{@syllabus_program.modality.name.gsub(/\s+/, "_").downcase}/#{nombre_reporte}.pdf")
      progress = Proc.new do |bytes, totals|
        p bytes.map.with_index { |b, i| "Part #{i + 1}: #{b} / #{totals[i]}" }.join(" ") + "Total: #{100.0 * bytes.sum / totals.sum} o/o"
      end
      #TODO: ADD EXPIRATION FOR TEMP FILE
      options_file = {
        :progress_callback => progress,
        :acl => "public-read",
      }
      options_file[:expires] = (DateTime.now + (1 / 24.0)) if draft

      @s3_obj.upload_file(
        save_path, options_file
      )
      url = @s3_obj.public_url
      File.unlink(save_path)
      return { filename: "#{nombre_reporte}.pdf", url: url, options_file: options_file }
    rescue Seahorse::Client::Plugins::RaiseResponseErrors
      return { error: "error al subir a S3", filename: nil, filename_error: nombre_reporte }
    end
  end

  def current_observations
    SyllabusMessage.select("id, syllabus_program_id, message, state, section, revision_number").where(syllabus_program_id: id, revision_number: revision_number)
  end

  def preview_observations_news
    SyllabusMessage.select("id, syllabus_program_id, message, state, section, revision_number").where(syllabus_program_id: id, revision_number: revision_number - 1)
  end

  def current_editions
    syllabus_editions.where(revision_number: revision_number)
  end

  def last_revision_observations
    syllabus_messages.where(revision_number: revision_number - 1)
  end

  def download_ability_message
    if authorized && !is_inactive
      "Sílabo Homologado"
    elsif !authorized && is_inactive
      "Sílabo inactivo y no homolgado"
    elsif authorized && is_inactive
      "Sílabo inactivo"
    else
      "Sílabo no homolgado"
    end
  end

  def self.set_s3_object(filename, path, cycle)
    s3 = Aws::S3::Resource.new(region: ENV.fetch("AWS_REGION"),
                               access_key_id: ENV.fetch("AWS_ACCESS_KEY_ID"),
                               secret_access_key: ENV.fetch("AWS_SECRET_ACCESS_KEY"))
    bucket_name = ENV.fetch("S3_ASSET_DIRECTORY")
    @s3_obj = s3.bucket(bucket_name).object("pdfs/#{cycle}/#{filename}")
    progress = Proc.new do |bytes, totals|
      p bytes.map.with_index { |b, i| "Part #{i + 1}: #{b} / #{totals[i]}" }.join(" ") + "Total: #{100.0 * bytes.sum / totals.sum} o/o"
    end
    begin
      @s3_obj.upload_file(path, { :progress_callback => progress, :acl => "public-read" })
      @s3_obj.public_url
    rescue Aws::S3::Errors::ServiceError => e
      puts e.message
    end
  end

  def coordinator_name
    if self.coordinator_id?
      self.coordinator.name
    else
      ""
    end
  end

  def supervisor_name
    if self.supervisor_id?
      self.supervisor.name
    else
      ""
    end
  end

  def self.syllabus_name_with_folder(syllabus)
    (syllabus.modality.name.gsub(/\s+/, "_").downcase) + "/" + SyllabusProgram.format_name_pdf_sillabus(syllabus) + ".pdf"
  end

  def self.generate_pdfs_collection(user, syllabus_program_ids, status, format_download = "complete", send_email = false, temp_path = false)
    paths = []
    ids_job = []
    syllabus_program_ids.each_with_index do |id, index|
      syllabus_p = SyllabusProgram.find(id.id)
      unless syllabus_p.nil?
        paths.push(SyllabusProgram.syllabus_name_with_folder(syllabus_p))
        job = syllabus_p.delay.create_pdf(user, status.id, format_download, send_email, temp_path)
        ids_job.push(job.id)
      end
    end
    status.update(max_progress: paths.count, extra_info: { id_job: status.extra_info["id_job"], file_paths: paths, ids_job: ids_job, send_email: send_email, temp_path: temp_path, format_pdf: format_download })
  end

  def create_pdf(user, status_id, format_download = "complete", send_email = false, temp_path = false)
    @syllabus_program = self
    begin
      last_job = JobStatus.find(status_id)
      unless last_job.completed?
        path = if temp_path then "pdfs/download_" + status_id.to_s else "pdfs/approved" end
        url_s3 = self.store_pdf_s3(format_download, path)
        ActiveRecord::Base.transaction do
          last_job.lock!
          last_job.increase_progress
          if url_s3[:filename].blank?
            last_job.extra_info["warnings"] = [] unless last_job.extra_info["warnings"].present?
            last_job.extra_info["warnings"].push({ "id": @syllabus_program.id, "state": @syllabus_program.state, "catalog_code": @syllabus_program.code_catalog_of_syllabus, "name": @syllabus_program.name_syllabus, "error": url_s3[:error] })
            last_job.extra_info["excluded_paths"] = [] unless last_job.extra_info["excluded_paths"].present?
            last_job.extra_info["excluded_paths"].push(SyllabusProgram.syllabus_name_with_folder(@syllabus_program))
            last_job.update({ extra_info: last_job.extra_info })
          end
          if last_job.progress == last_job.max_progress
            job = SyllabusProgram.delay.make_zip_s3(user, status_id, Cycle.active.name, format_download, temp_path, send_email)
            last_job.extra_info["id_job_zip"] = job.id
            last_job.update({ extra_info: last_job.extra_info })
          end
        end
      end
    end
  end

  def self.make_zip_s3(user, status_id, cycle, format_download, temp_path, send_email)
    status = JobStatus.find(status_id)
    begin
      unless status.completed?
        path = if temp_path then "pdfs/download_" + status_id.to_s else "pdfs/approved" end
        s3 = Aws::S3::Resource.new(region: ENV.fetch("AWS_REGION"),
                                   access_key_id: ENV.fetch("AWS_ACCESS_KEY_ID"),
                                   secret_access_key: ENV.fetch("AWS_SECRET_ACCESS_KEY"))
        bucket_name = ENV.fetch("S3_ASSET_DIRECTORY")
        bucket = s3.bucket(bucket_name)

        files = status.extra_info["file_paths"]
        unless status.extra_info["excluded_paths"].nil?
          excluded_paths = status.extra_info["excluded_paths"]
          files = files.reject { |w| excluded_paths.include? w }
        end

        files_zip = []
        folder = "#{path}/#{format_download}/#{cycle}"
        download_folder = "public/zip/download_#{status_id.to_s}"
        if File.directory?("#{Rails.root}/#{download_folder}")
          FileUtils.rm_rf(Dir.glob("#{Rails.root}/#{download_folder}"))
        end
        FileUtils.mkdir(["#{Rails.root}/#{download_folder}", "#{Rails.root}/#{download_folder}/presencial", "#{Rails.root}/#{download_folder}/semi_presencial", "#{Rails.root}/#{download_folder}/virtual"])

        # Download the files from S3 to a local folder
        files.each do |file_name|
          begin
            # Get the file object
            file_obj = bucket.object("#{folder}/#{file_name}")
            # Save it on disk
            file_obj.get(response_target: "#{download_folder}/#{file_name}")
            files_zip.push(file_name)
          rescue Exception => e
            status.update(error_message: file_name + ": " + e.message)
          end
        end
        zipfile_name = "silabos-#{Time.zone.now.strftime("%Y-%m-%d-%H-%M-%S")}.zip"
        zipfile_path = "#{Rails.root}/public/#{zipfile_name}"
        Zip::File.open(zipfile_path, Zip::File::CREATE) do |zipfile|
          files_zip.each do |filename|
            # Add the file to the zip
            zipfile.add(filename, "#{download_folder}/#{filename}")
          end
        end
        FileUtils.rm_rf(Dir.glob("#{Rails.root}/#{download_folder}"))
        public_zip_url = set_s3_object(zipfile_name, zipfile_path, cycle)
        File.unlink(zipfile_path)

        status.finish_process(public_zip_url)
        if send_email
          new_mail = Email::Message.new()
          new_mail.send_message_download_syllabus(user, public_zip_url)
        end
      end
    rescue Exception => e
      status.update(error: true, error_message: e.str)
    end
  end

  def all_user_authorized
    return syllabus.all_user_authorized
  end

  def base_syllabus_program
    return syllabus.base_syllabus_program
  end
end
