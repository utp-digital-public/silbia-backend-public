class AuthenticateUtpService
  attr_accessor :username, :password, :message_response

  def initialize(**attributes)
    attributes.each do |k,v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
  end

  def is_valid_authentication?
    encrypt_class = Util::EncryptDecryptAes.new
    password_encrypted = encrypt_class.aes128_encrypt(SECRET_KEY_UTP_LOGIN, @password)

    login_utp = Authentication::UtpLogin.new(
        username: @username,
        password: password_encrypted)
    if login_utp.authenticate?
      response = login_utp.get_response_login()
      create_or_update_user(response["user"], @password)
      return true
    else 
      @message_response = login_utp.get_response_login()
      return false  
    end
  end

 private 
  def create_or_update_user(user_hash, password)
    email = user_hash["email"]
    first_name = user_hash["first_name"]
    name = first_name + " " + user_hash["last_name"]
    
    User.where(email: email).update_or_create(name: name, first_name: first_name, password: password)

  end  
end  