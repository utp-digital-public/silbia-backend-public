json.status "success"
json.data do
  json.active do
    json.partial! "api/v1/cycles/cycle", cycle: @active_cycle
  end
  json.inactive do
    json.array! @inactive_cycles, partial: "api/v1/cycles/cycle", as: :cycle
  end
  json.total_courses @total_courses
end
