class Api::V1::SyllabusProgramsController < Api::ApiV1Controller
  require "base64"
  before_action :set_syllabus_program,
                only: [:show, :update, :update_text, :update_flags, :send_to_approve,
                       :request_edit, :create_observation, :observe, :approve, :request_edition, :export_pdf]
  before_action :set_syllabus, only: [:update_syllabus]
  before_action :set_syllabus_message, only: [:update_observation, :destroy_observation]

  def show
    @can_edit = policy(@syllabus_program).edit?
    @can_approve = policy(@syllabus_program).approve?
    authorize @syllabus_program
  end

  def update
    authorize @syllabus_program
    @can_edit = policy(@syllabus_program).edit?
    @can_approve = policy(@syllabus_program).approve?
    ApplicationRecord.transaction do
      begin
        if @syllabus_program.present?
          @syllabus_program.assign_attributes(syllabus_program_params.except(:sessions))
          if @syllabus_program.invalid?
            render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @syllabus_programs.errors) and return
          end
          @syllabus_program.save!
          if syllabus_program_params.has_key?(:sessions)
            new_session_ids = syllabus_program_params[:sessions].map { |x| x[:id] }
            @syllabus_program.sessions.select { |x| !new_session_ids.include? x.id }.each do |del|
              del.destroy
            end
            syllabus_program_params[:sessions].each do |s|
              session = @syllabus_program.sessions.find { |x| x.id == s[:id] }

              if session.blank?
                session = Session.new(s.except(:session_topics))
                session.syllabus_program_id = @syllabus_program.id
              else
                session.assign_attributes(s.except(:session_topics, :syllabus_program_id))
              end
              if session.invalid?
                render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", session.errors) and return
              end
              session.save!
              if s.has_key?(:session_topics)
                new_session_topic_ids = s[:session_topics].map { |x| x[:id] }
                session.session_topics.select { |x| !new_session_topic_ids.include? x.id }.each do |del|
                  del.destroy
                end
                s[:session_topics].each do |st|
                  session_topic = session.session_topics.find { |x| x.id == st[:id] }
                  if session_topic.blank?
                    session_topic = SessionTopic.new(st)
                    session_topic.session_id = session.id
                  else
                    session_topic.assign_attributes(st.except(:session_id))
                  end
                  if session_topic.invalid?
                    render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", session_topic.errors) and return
                  end
                  session_topic.save!
                end
              end
            end
          end
          syllabus_service = SyllabusProgramService.new(
            syllabus_program: @syllabus_program,
            current_user: current_user,
          )
          if syllabus_program_params.except(:flags).keys.length > 0
            result = syllabus_service.update_status("edit_progress") if @syllabus_program.pending?
          end
        end
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  def update_syllabus
    authorize @syllabus
    ApplicationRecord.transaction do
      begin
        if @syllabus.present?
          @syllabus.assign_attributes(syllabus_params.except(:units, :basic_book_ids, :complementary_book_ids))
          if @syllabus.invalid?
            render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @syllabus.errors) and return
          end
          @syllabus.save!
          if syllabus_params.has_key?(:units)
            new_unit_ids = syllabus_params[:units].map { |u| u[:id] }
            @syllabus.units.select { |u| !new_unit_ids.include? u.id }.each do |du|
              du.destroy
            end
            syllabus_params[:units].each do |u|
              unit = @syllabus.units.find { |x| x.id == u[:id] }
              if unit.blank?
                unit = Unit.new(u.except(:topics, :unit_information_sources))
                unit.syllabus_id = @syllabus.id
              else
                unit.assign_attributes(u.except(:topics, :syllabus_id, :unit_information_sources))
              end

              if unit.invalid?
                render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", unit.errors) and return
              end

              unit.save!
              if u.has_key?(:topics)
                new_topic_ids = u[:topics].map { |t| t[:id] }
                unit.topics.select { |t| !new_topic_ids.include? t.id }.each do |dt|
                  dt.destroy
                end
                u[:topics].each do |t|
                  topic = unit.topics.find { |x| x.id == t[:id] }
                  if topic.blank?
                    topic = Topic.new(t)
                    topic.unit = unit
                  else
                    topic.assign_attributes(t.except(:topic_id))
                  end

                  if topic.invalid?
                    render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", topic.errors) and return
                  end

                  topic.save!
                end
              end
              if u.has_key?(:unit_information_sources)
                new_information_sources_ids = u[:unit_information_sources].map { |sis| sis[:syllabus_information_source_id] }
                unit.unit_information_sources.select { |is| !new_information_sources_ids.include? is.syllabus_information_source_id }.each do |dis|
                  dis.destroy
                end

                u[:unit_information_sources].each do |sis|
                  unit_information_source = unit.unit_information_sources.find { |x| x.syllabus_information_source_id == sis[:syllabus_information_source_id] }
                  if unit_information_source.blank?
                    unit_information_source = UnitInformationSource.new(sis.except(:id))
                    unit_information_source.unit = unit
                  else
                    unit_information_source.assign_attributes(sis.except(:id))
                  end
                  if unit_information_source.invalid?
                    render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", unit_information_source.errors) and return
                  end
                  unit_information_source.save!
                end
              end
            end
          end
          if syllabus_params.has_key?(:basic_book_ids)
            new_book_basic_ids = syllabus_params[:basic_book_ids] || []
            basic_book_ids_to_delete = @syllabus.course.basic_book_ids - new_book_basic_ids.map(&:to_i)
            @syllabus.course.information_sources.where(book_id: basic_book_ids_to_delete).destroy_all
          end
          if syllabus_params.has_key?(:complementary_book_ids)
            new_book_complementary_ids = syllabus_params[:complementary_book_ids] || []
            complementary_book_ids_to_delete = @syllabus.course.complementary_book_ids - new_book_complementary_ids.map(&:to_i)
            complementary_book_ids_to_add = new_book_complementary_ids.map(&:to_i) - @syllabus.course.complementary_book_ids
            @syllabus.course.information_sources.where(book_id: complementary_book_ids_to_delete).destroy_all
            @syllabus.course.information_sources.build(complementary_book_ids_to_add.map { |id| { :book_id => id, :type_source => "Complementaria" } })
            @syllabus.course.save if complementary_book_ids_to_add.length > 0 or complementary_book_ids_to_delete.length > 0
          end
        end
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  def update_text
    authorize @syllabus_program, :approve?
    ApplicationRecord.transaction do
      begin
        if ["fundamentación", "sumilla", "logro general de aprendizaje"].include? text_params[:title]
          @syllabus_program.syllabus.syllabus_content["texts"].select { |text| text["title"] == text_params[:title] }.each do |text|
            if text_params[:content].length.between? 1, text["max_words"]
              text["content"] = text_params[:content]
              @syllabus_program.syllabus.save
              render_ok(text)
            else
              render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "El contenido no cumple con la longitud") and return
            end
          end
        elsif text_params[:title] == "metodología"
          @syllabus_program.program_json["texts"].select { |text| text["title"] == text_params[:title] }.each do |text|
            if text_params[:content].length.between? 1, text["max_words"]
              text["content"] = text_params[:content]
              @syllabus_program.save
              render_ok(text)
            else
              render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "El contenido no cumple con la longitud") and return
            end
          end
        else
          render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "El título del texto es inválido") and return
        end
      end
    end
  end

  def update_flags
    authorize @syllabus_program
    ApplicationRecord.transaction do
      begin
        if @syllabus_program.present?
          @syllabus_program.assign_attributes(flag_params)
          if @syllabus_program.invalid?
            render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @syllabus_programs.errors) and return
          end
          @syllabus_program.save!
        end
      rescue => e
        render_internal_server_error(e.message)
      end
    end
  end

  def send_to_approve
    authorize @syllabus_program
    begin
      errors = validate_syllabus(@syllabus_program.syllabus)
      errors_sp = validate_syllabus_program(@syllabus_program)
      if errors_sp.count > 0 || errors.count > 0
        render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", errors.merge!(errors_sp)) and return
      end
      syllabus_service = SyllabusProgramService.new(
        syllabus_program: @syllabus_program,
        current_user: current_user,
      )

      detect_changes_in_syllabus_sections()
      @editions.try(:each) do |edition|
        edit = SyllabusEdition.new(
          section_id: edition[:id_section],
          revision_number: @syllabus_program.revision_number + 1,
          syllabus_program_id: @syllabus_program.id,
        )
        edit.save!
      end
      result = syllabus_service.update_status("send_to_approve")
      @syllabus_program.update_attribute(:revision_number, @syllabus_program.revision_number + 1)
      if @syllabus_program.revision_number > 1
        @syllabus_program.flags["show_modal_changes"] = true
        @syllabus_program.update_attribute(:flags, @syllabus_program.flags)
      end

      send_email("send_to_approve")
      json_response({ can_edit: policy(@syllabus_program).edit?, can_approve: policy(@syllabus_program).approve?, human_syllabus_state: @syllabus_program.aasm.human_state, state: @syllabus_program.state, result: result[:message] })
    rescue AASM::InvalidTransition
      msg = {
        message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}",
        status: 422,
      }
      return json_response(msg, 422)
    rescue Exception => e
      msg = { message_error: "Error al procesar información: #{e.message}", status: 422 }
      return json_response(msg, 422)
    end
  end

  def request_edit
    if policy(@syllabus_program).show? && @syllabus_program.approved? && !params[:message].blank?
      @message = params[:message]
      send_email("request_edit")
      json_response({ status: "success" })
    else
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON)
    end
  end

  def create_observation
    authorize @syllabus_program, :approve?
    @syllabus_message = @observation_syllabus.find { |o| o.section == message_params[:section] }
    if !@syllabus_message.present?
      @syllabus_message = SyllabusMessage.new(message: message_params[:message], section: message_params[:section], revision_number: @syllabus_program.revision_number, syllabus_program_id: @syllabus_program.id)
    else
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "Ya existe una observación en esta sección") and return
    end
    if @syllabus_message.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @syllabus_message.errors) and return
    else
      @syllabus_message.save!
      render_ok(@syllabus_message)
    end
  end

  def update_observation
    authorize @syllabus_message.syllabus_program, :approve?
    if (@syllabus_message.revision_number != @syllabus_message.syllabus_program.revision_number)
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", "La observación no pertenece a la revisión actual del silabo") and return
    end
    @syllabus_message.message = message_params[:message]
    if @syllabus_message.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @syllabus_message.errors) and return
    else
      @syllabus_message.save!
    end
    render_ok(@syllabus_message)
  end

  def destroy_observation
    authorize @syllabus_message.syllabus_program, :approve?
    if (@syllabus_message.revision_number != @syllabus_message.syllabus_program.revision_number)
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", "La observación no pertenece a la revisión actual del silabo") and return
    end
    @syllabus_message.destroy if @syllabus_message.present?
  end

  def approve
    authorize @syllabus_program
    begin
      if !@syllabus_program.current_observations.empty?
        render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", "El silabo tiene observaciones") and return
      end
      syllabus_service = SyllabusProgramService.new(syllabus_program: @syllabus_program, current_user: current_user)
      @result = syllabus_service.update_status("approve_silabus")
      send_email("approve")
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def observe
    authorize @syllabus_program
    begin
      if @syllabus_program.current_observations.empty? && !@syllabus_program.flags["request_edition"].present?
        render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", "El silabo no tiene observaciones") and return
      end
      syllabus_service = SyllabusProgramService.new(syllabus_program: @syllabus_program, current_user: current_user)
      begin
        @result = syllabus_service.update_status("observe")
        if @syllabus_program.flags["request_edition"].present?
          @syllabus_program.flags.delete("request_edition")
          @syllabus_program.update_attribute(:flags, @syllabus_program.flags)
        end
        send_email("send_to_observate")
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def request_edition
    authorize @syllabus_program
    if !@syllabus_program.flags["request_edition"].present?
      @syllabus_program.flags["request_edition"] = true
      @syllabus_program.update_attribute(:flags, @syllabus_program.flags)
      send_email("request_edition")
      render_ok(@syllabus_program.flags["request_edition"])
    else
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", "Ya tiene una solicitud pendiente") and return
    end
  end

  def export_pdf
    authorize @syllabus_program, :show?
    begin
      if @syllabus_program.present?
        pdf_url = @syllabus_program.store_pdf_s3("complete", "preview", true)
        return render_ok(pdf_url)
      else
        render_bad_request("El silabo no existe")
      end
    rescue => e
      render_internal_server_error(e.message)
    end
  end

  private

  def validate_syllabus_program(syllabus_program)
    syllabus_program.program_json["texts"].each do |text|
      unless text["content"].length.between?(1, text["max_words"])
        syllabus_program.errors.add(:syllabus_program, "#{text["title"]} no cumple con longitud")
      end
    end
    unless syllabus_program.evaluation_system_json["evaluation_system"].length > 0
      syllabus_program.errors.add(:syllabus_program, "Debes agregar uno o más elementos de evaluación")
    end
    sum = syllabus_program.evaluation_system_json["evaluation_system"].reduce(0) do |sum, eva|
      sum += Integer(eva["weight"])
    end
    unless sum == 100
      syllabus_program.errors.add(:syllabus_program, "La sumatoria total de los pesos debe llegar a 100%")
    end
    unless syllabus_program.topics.length > 0
      syllabus_program.errors.add(:syllabus_program, "No hay temas asociados")
    end
    link_eva = []
    syllabus_program.sessions.order(:session_number).each do |s|
      s.session_topics.order(:id).each do |st|
        list_activities = st.activities.select { |x| !x["type_activity"].blank? }.pluck("type_activity")
        link_eva += list_activities.map(&:upcase) unless list_activities.empty?
      end
    end
    unless link_eva == syllabus_program.evaluation_system_json["evaluation_system"].pluck("type")
      syllabus_program.errors.add(:syllabus_program, "Diferencias")
    end
    unless syllabus_program.sessions.length > 0
      syllabus_program.errors.add(:syllabus_program, "No hay sesiones en el cronograma")
    end
    syllabus_program.sessions.each do |s|
      unless s.session_topics.length > 0
        syllabus_program.errors.add(:syllabus_program, "La sesión no tiene temas")
      end
    end

    return syllabus_program.errors
  end

  def validate_syllabus(syllabus)
    syllabus.syllabus_content["texts"].each do |text|
      unless text["content"].length.between?(1, text["max_words"])
        syllabus.errors.add(:syllabus, "#{text["title"]} no cumple con longitud")
      end
    end
    syllabus.units.each do |unit|
      unless unit.title.length.between?(1, 180)
        syllabus.errors.add(:syllabus, "Unidad #{unit[:unit_number]} no cumple con longitud")
      end
      unless unit.title.length.between?(1, 4000)
        syllabus.errors.add(:syllabus, "Logro de unidad #{unit[:unit_number]} no cumple con longitud")
      end
      unless unit.topics.length > 0
        syllabus.errors.add(:syllabus, "La unidad #{unit[:unit_number]} no tiene temas")
      end
      unit.topics.each do |topic|
        unless topic.topic.length.between?(1, 4000)
          syllabus.errors.add(:syllabus, "El tema de la unidad #{unit[:unit_number]} no cumple con longitud")
        end
      end
    end
    return syllabus.errors
  end

  def send_email(type)
    new_mail = Email::Message.new(syllabus_program: @syllabus_program, url_base: request.origin, url_logo: ActionController::Base.helpers.asset_path("mail/silbia-logo.png"))
    case type
    when "send_to_approve"
      new_mail.send_message_to_approve_syllabus
    when "send_to_observate"
      new_mail.send_message_to_observate_syllabus
    when "approve"
      new_mail.send_message_approved_syllabus
    when "request_edit"
      new_mail.send_message_request_edit(@current_user, @message)
    when "request_edition"
      new_mail.send_message_request_edition
    else
      return
    end
  end

  def set_syllabus_program
    @syllabus_program = SyllabusProgram.find(params[:id])
    unless @syllabus_program.flags.has_key?("show_modal_units")
      @syllabus_program.flags["show_modal_units"] = true
    end
    load_observation_syllabus()
    load_preview_observation_syllabus()
    load_edition_syllabus()
    @course = @syllabus_program.syllabus.course
  end

  def load_observation_syllabus
    @observation_syllabus = @syllabus_program.current_observations if policy(@syllabus_program).show_observations?
  end

  def load_edition_syllabus
    @edition_syllabus = @syllabus_program.current_editions if policy(@syllabus_program).show_observations? && @syllabus_program.to_be_approved?
  end

  def load_preview_observation_syllabus
    @observation_preview_syllabus = @syllabus_program.preview_observations_news if policy(@syllabus_program).observe?
  end

  def detect_changes_in_syllabus_sections
    detect = SyllabusChange::DetectChangeSection.new(@syllabus_program, @syllabus_program.get_program_course.is_base_course)
    detect.calculate
    @editions = detect.array_changes_in_syllabus
  end

  def set_syllabus
    @syllabus = Syllabus.find(params[:id])
  end

  def set_syllabus_message
    @syllabus_message = SyllabusMessage.find(params[:id])
  end

  def syllabus_program_params
    params.require(:syllabus_program).permit(
      {
        program_json: {
          texts: [:type, :title, :content, :max_words, :edit_only_pregrade, :section_template_id],
          units: [
            :id,
            :title,
            weeks: [
              :id,
              :number_week,
              sessions: [
                :id, :subject, :laboratory, :number_session, :type_laboratory,
                activities: [:modality, :description, :type_activity, :label_description],
              ],
            ],
          ],
        },
      },
      { evaluation_system_json: {
        evaluation_system: [:type, :weight, :description, :recoverable, :observations],
      } },
      { evaluation_rules_json: { rules: [:name, :fixed] } },
      sessions: [
        :id, :syllabus_program_id, :week_number, :session_number, :laboratory_id, :unit_id, :modality_id,
        session_topics: [:id, :session_id, :topic_id, activities: [:modality, :description, :type_activity, :label_description]],
      ],
      flags: {},
    )
  end

  def syllabus_params
    params.require(:syllabus).permit(
      {
        syllabus_content: { texts: [:type, :title, :content, :max_words, :edit_only_pregrade, :section_template_id] },
      },
      units: [
        :id, :unit_number, :title, :achievement,
        topics: [
          :id, :unit_id, :topic,
        ],
        unit_information_sources: [
          :id, :syllabus_information_source_id, :from_page, :to_page,
        ],
      ],
      :basic_book_ids => [],
      :complementary_book_ids => [],
    )
  end

  def text_params
    params.permit(:title, :content)
  end

  def message_params
    params.permit(:message, :section, :delete)
  end

  def flag_params
    params.permit(flags: {})
  end
end
