class CreateProgramCourseCareers < ActiveRecord::Migration[5.2]
  def change
    create_table :program_course_careers do |t|
			t.references :program_course, foreign_key: true
			t.references :career, foreign_key: true
      t.timestamps
    end
  end
end
