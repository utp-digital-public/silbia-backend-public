Trestle.resource(:program_courses) do
  menu do
    item :program_courses, icon: "fa fa-star", label: "Cursos"
  end

  scope :in_person, -> { model.where(type: "InPerson") }, label: "Pregrado"
  scope :blended, -> { model.where(type: "Blended") }, label: "CGT"
  scope :in_person_without_sibling, -> { model.where(course_id: Course.joins(:program_courses).group("courses.id").having("count(course_id) = 1").map { |c| c.id }) }, label: "Presencial sin CGT"
  table do
    column :course_name, ->(course) { course.name }, header: "Nombre"
    column :catalog_code, header: "Código", align: :center
    column :type, ->(program_course) { program_course.human_type }, header: "Modalidad", align: :center
    #actions
    #actions do |toolbar, instance, admin|
    #toolbar.link "Crear curso CGT", "/program_courses/#{instance.id}/cgt_course", target: "_blank", data: { behavior: "dialog" }, class: "btn btn-link btn-pdf" if instance.is_a?(InPerson) && instance.course.program_courses.where(type: "Blended").empty?
    #end
  end

  search do |query, params|
    if query
      collection.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{query}%", "%#{query}%")
    else
      collection
    end
  end

  # form do |program_course|
  #   text_field :catalog_code, label: "Código"
  #   text_field :name, label: "Nombre"
  #   static_field :modalidad, program_course.human_type
  #   text_field :weekly_hours, label: "Horas semanales"
  #   text_field :credits, label: "Créditos"
  #   select :coordinator_id, User.active.with_role(ROLE_CREATOR.to_sym).map { |user| [user.name, user.id]  }
  #   select :supervisor_id, User.active.with_role(ROLE_APPROVER.to_sym).map { |user| [user.name, user.id]  }
  #   # select :book_ids, Book.all, { label: "Libros Básicos" }, multiple: true
  #   # select :book_ids, Book.all, { label: "Libros Básicos" }, multiple: true
  #   #select_tag :book_basic_ids,Book.where(id: program_course.course.information_sources.basic).map { |book| [book.title, book.id] },multiple: true
  #   tag_select :book_basic_ids, Book.where(id: program_course.course.information_sources.basic).map { |book| book.id }
  #   #select :book_complementary_ids, @book_complementary_ids,{label: "Libros complementarios" }, multiple: true
  #   row do
  #     col(xs: 12) { collection_select :book_basic_ids, Book.where(id: program_course.course.information_sources.basic), :id, :title, {multiple: true} }
  #   end
  # end

  controller do
    def books
      if params[:q]
        # Request your data here and return a collection of id / text items, which will be used by the select2 component
        render json: Book.where("title ILIKE ?", "%#{params[:q]}%").limit(30).collect { |book| { id: book.id, text: book.label_for_tag } }
      else
        render json: []
      end
    end

    def cgt_course
      if request.post?
        @cgt_course = Blended.new(blended_params)
        in_person = @cgt_course.course.in_person_courses.first
        @cgt_course.program_course_careers.build(in_person.program_course_careers.map { |pcc| { career_id: pcc.career_id } })
        if @cgt_course.save
          s = @cgt_course.course.syllabuses.where(cycle_id: Cycle.active.id).first

          syllabus_param_hash = {
            program_json: s.syllabus_programs.first.program_json,
            type_program: TYPE_PROGRAM_CGT,
            state: NO_READ_STATE,
            coordinator_id: @cgt_course.coordinator_id,
            supervisor_id: @cgt_course.supervisor_id,
            modality_id: SEMI_PRESENCIAL_CODE,
            evaluation_system_json: { evaluation_system: [] },
            evaluation_rules_json: { rules: [] },
          }
          s.syllabus_programs.build(syllabus_param_hash)
          s.save!
          flash[:message] = flash_message("success", message: "Se creó correctamente el curso CGT #{@cgt_course.name}.", title: "Curso creado")
          redirect_to program_courses_admin_index_path
        else
          redirect_to program_courses_admin_index_path, flash: {
                                                          cgt_error: @cgt_course.errors.full_messages.first,
                                                          show_dialog: true, in_person_id: in_person.id,
                                                          cgt_name: @cgt_course.name,
                                                          cgt_coordinator: @cgt_course.coordinator_id,
                                                          cgt_supervisor: @cgt_course.supervisor_id,
                                                          cgt_weekly_hours: @cgt_course.weekly_hours,
                                                          cgt_credits: @cgt_course.credits,
                                                          cgt_catalog_code: @cgt_course.catalog_code,
                                                        }
        end
      else
        preg_course = InPerson.find(params[:id])
        @cgt_course = Blended.new()
        @cgt_course.course = preg_course.course
        @cgt_course.name = params[:cgt_name].blank? ? preg_course.name : params[:cgt_name]
        @cgt_course.coordinator_id = params[:cgt_coordinator].blank? ? preg_course.coordinator_id : params[:cgt_coordinator]
        @cgt_course.supervisor_id = params[:cgt_supervisor].blank? ? preg_course.supervisor_id : params[:cgt_supervisor]
        @cgt_course.weekly_hours = params[:cgt_weekly_hours].blank? ? preg_course.weekly_hours : params[:cgt_weekly_hours]
        @cgt_course.credits = params[:cgt_credits].blank? ? preg_course.credits : params[:cgt_credits]
        @cgt_course.catalog_code = params[:cgt_catalog_code] unless params[:cgt_catalog_code].blank?
        unless params[:cgt_error].blank?
          @error_cgt_hash =
            {
              "title" => "Error en la creación de curso CGT. Se presentó el siguiente error:",
              "message" => "<br/> #{params[:cgt_error]}",
            }
        end
      end
    end

    def show
      @program_course = ProgramCourse.find(params[:id])
      @similar_course = (@program_course.is_a?(Blended) ? @program_course.course.in_person_courses.first : @program_course.course.blended_courses.first)
      @book_basic_ids = Book.where(id: @program_course.course.information_sources.basic.pluck(:book_id))
      @book_complementary_ids = Book.where(id: @program_course.course.information_sources.complementary.pluck(:book_id))
    end

    def update
      pc = ProgramCourse.find(params[:id])
      program_course_params = pc.type == "Blended" ? blended_params : in_person_params
      if pc.update_attributes(program_course_params)
        basic_book_ids_to_delete = []
        basic_book_ids_to_add = []
        new_book_basic_ids = params[:book_basic_ids] || []
        basic_book_ids_to_delete = pc.course.basic_book_ids - new_book_basic_ids.map(&:to_i)
        basic_book_ids_to_add = new_book_basic_ids.map(&:to_i) - pc.course.basic_book_ids

        complementary_book_ids_to_delete = []
        complementary_book_ids_to_add = []
        new_book_complementary_ids = params[:book_complementary_ids] || []
        complementary_book_ids_to_delete = pc.course.complementary_book_ids - new_book_complementary_ids.map(&:to_i)
        complementary_book_ids_to_add = new_book_complementary_ids.map(&:to_i) - pc.course.complementary_book_ids

        pc.course.information_sources.where(book_id: basic_book_ids_to_delete).destroy_all
        pc.course.information_sources.where(book_id: complementary_book_ids_to_delete).destroy_all
        pc.course.information_sources.build(basic_book_ids_to_add.map { |id| { :book_id => id, :type_source => "Básica" } })
        pc.course.information_sources.build(complementary_book_ids_to_add.map { |id| { :book_id => id, :type_source => "Complementaria" } })
        if pc.course.save
          flash[:message] = flash_message("success", message: "Se actualizó correctamente el curso #{pc.name}.", title: "Curso actualizado")
          redirect_to program_courses_admin_path(pc)
        else
          redirect_to program_courses_admin_path(pc), flash: { error: pc.course.errors.full_messages.first }
        end
      else
        redirect_to program_courses_admin_path(pc), flash: { error: pc.errors.full_messages.first }
      end
    end

    def import_courses
      result = Course.import(params[:file])
      if result[:errors].empty?
        flash[:result_hash] = {
          title: "Importación exitosa",
          message: "Se registraron #{result[:courses_count]} nuevos cursos y sílabos.",
        }
        redirect_to program_courses_admin_index_path
      else
        flash[:error_hash] =
          {
            title: result[:error_type] == "invalid_document" ? "Error en el documento" : "Error en la importación. #{result[:courses_count] == 1 ? "Se registró 1 nuevo curso" : "Se registraron #{result[:courses_count]} nuevos cursos"} y #{result[:errors].count == 1 ? "1 fila presenta error" : "#{result[:errors].count} filas presentan errores:"}",
            message: "<br/> #{result[:errors].uniq.join("<br/>")}",
          }
        redirect_to program_courses_admin_index_path
      end
    end

    def blended_params
      params.require(:blended).permit(:coordinator_id, :supervisor_id, :name, :weekly_hours, :credits, :catalog_code, :course_id, :authorized)
    end

    def in_person_params
      params.require(:in_person).permit(:coordinator_id, :supervisor_id, :name, :weekly_hours, :credits, :catalog_code, :course_id, :authorized)
    end
  end

  # Customize the form fields shown on the new/edit views.
  #
  # form do |program_course|
  #   text_field :name
  #
  #   row do
  #     col(xs: 6) { datetime_field :updated_at }
  #     col(xs: 6) { datetime_field :created_at }
  #   end
  # end

  # By default, all parameters passed to the update and create actions will be
  # permitted. If you do not have full trust in your users, you should explicitly
  # define the list of permitted parameters.
  #
  # For further information, see the Rails documentation on Strong Parameters:
  #   http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters
  #
  # params do |params|
  #   params.require(:program_course).permit(:name, ...)
  # end
  routes do
    post :import_courses, on: :collection
    get :books, on: :collection
    match :cgt_course, on: :member, via: [:get, :post]
  end

  params do |params|
    params.require(:blended).permit(:coordinator_id, :supervisor_id, :name, :weekly_hours, :credits, :catalog_code, :authorized)
  end
end
