class CreateTopics < ActiveRecord::Migration[5.2]
  def change
    create_table :topics do |t|
      t.references :unit, foreign_key: true
      t.text :topic, null: false
      t.timestamps
    end
  end
end
