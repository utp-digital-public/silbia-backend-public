class AddTypeofPracticeColumnToPractices < ActiveRecord::Migration[5.2]
  def change
    add_reference :practices, :type_of_practice, foreign_key: true
  end
end
