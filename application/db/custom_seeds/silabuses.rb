

new_data = Import::ConvertToJson.new("syllabus_migrate.csv",1)
#Create first silabus 
new_data.convert_csv_to_json

#Create new Silabus

# Add new silabus II
load "#{Rails.root}/db/custom_seeds/second_silabuses_migration.rb"

# Add new silabus III
load "#{Rails.root}/db/custom_seeds/SP_10_100000M45F_syllabo_new.rb"


# Add new silabus IV
load "#{Rails.root}/db/sprint-12/import_silabus_12_sprint.rb"

