FactoryBot.define do
  factory :tracking_status_syllabus do
    syllabus_program { nil }
    state { 1 }
    user_id_track { 1 }
  end
end
