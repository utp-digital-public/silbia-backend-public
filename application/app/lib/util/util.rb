module Util
  module Util

    def remove_accents(string)
      string = string.downcase
      string = string.gsub(/[àáâãäå]/,'a')
      string = string.gsub(/æ/,'ae')
      string = string.gsub(/ç/, 'c')
      string = string.gsub(/[èéêë]/,'e')
      string = string.gsub(/[ìíîï]/,'i')
      string = string.gsub(/[ýÿ]/,'y')
      string = string.gsub(/[òóôõö]/,'o')
      string = string.gsub(/[ùúûü]/,'u')
      string = string.gsub(/[ñÑ]/,'n')
      string = string.gsub(/\/|\\/,"")
      string = string.gsub(/[,]/,"")
      string = string.gsub(/[*]/,"")
      return string 
    end
  
    
    def convertLastCharacterInRomanNumeral(string)
      final_string =""
      arr_string = string.split(" ")
      arr_string.try(:each_with_index) do |item, index|
        if arr_string.size-1 == index
          roman_object = Roman.new
          roman_to_number = roman_object.romanToDecimal(item).to_s 
          final_string = final_string + " " + roman_to_number
        else
          final_string =  final_string + " "+ item
        end
      end
      return final_string.titleize  
    end

    def removeHtmlTagsToText(text_value)
      sanitize text_value, tags: %w(ul li br )
    end
    
    def self.array_to_string_comas(array)
      array.map {|str| "\'#{str}\'"}.join(',')
    end  

  end  
end    