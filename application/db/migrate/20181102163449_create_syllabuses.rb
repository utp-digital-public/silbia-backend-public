class CreateSyllabuses < ActiveRecord::Migration[5.2]
  def change
    create_table :syllabuses do |t|
      t.string :course_catalog_code, null:false
      t.integer :year, null: false
      t.string :cycle_academic, null: false
      t.string :modality, null: false
      t.jsonb :syllabus_content, null: false, default: '{}'
      t.references :doc_template, foreign_key: true
      t.references :user, foreign_key: true
      t.references :curriculum_course, foreign_key: true
      t.timestamps
    end

    add_index  :syllabuses, :course_catalog_code
    add_index  :syllabuses, :year
    add_index  :syllabuses, :cycle_academic
    add_index  :syllabuses, :modality
  end
end
