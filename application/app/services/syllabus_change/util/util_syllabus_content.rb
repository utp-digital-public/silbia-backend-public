module SyllabusChange
  module Util
    module UtilSyllabusContent
      
      def get_last_value_of_audit(v)
        if v.kind_of?(Array)
          last_value_audit = v[1]
          value_audit = last_value_audit
        else
          value_audit = v
        end   
        value_audit
      end
      
      def get_value_before_the_last_change(v)
        if v.kind_of?(Array)
          value_before_the_last_change = v[0]
          value_audit = value_before_the_last_change
        else
          value_audit = v
        end   
        value_audit
      end  

    end 
  end  
end      