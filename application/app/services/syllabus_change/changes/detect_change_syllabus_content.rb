module SyllabusChange
  module Changes
    module DetectChangeSyllabusContent
      include SyllabusChange::Changes::UtilDetectCronogram
      include SyllabusChange::Util::UtilSyllabusContent

      def evaluate_if_has_changes_in_syllabus_content_to(key_text_value, current_value, auditable_objects)
        auditable_objects.try(:each) do |audit_object|
          audit_object.audited_changes.try(:each) do |key, value|
            syllabus_content_json = get_value_before_the_last_change(value) if key == "syllabus_content"
            value_to_evaluate = SyllabusChange::Changes::ExtractContentText.extract_to(
              syllabus_content_json,
              key_text_value
            )
            puts "LA #{key_text_value.upcase} TRACK ES: #{value_to_evaluate}  ---- ----- LA #{key_text_value.upcase} ACTUAL ES #{current_value}"
            return true if value_to_evaluate != current_value
          end
        end
        false
      end

      def set_hash_values_when_have_less_than_one_state(text_key_to_compare, objects_audit)
        objects_audit.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            if key == text_key_to_compare
              before_value = get_value_before_the_last_change(value)
              value_audit = get_last_value_of_audit(value)
              return before_value
            end
          end
        end
        ""
      end
    end
  end
end
