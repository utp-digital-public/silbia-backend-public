class AddGeneratedZipPathToJobStatus < ActiveRecord::Migration[5.2]
  def change
    add_column :job_statuses, :generated_zip_path, :string, :default => ""
  end
end
