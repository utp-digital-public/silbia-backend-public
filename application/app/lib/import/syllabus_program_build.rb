module Import
  class SyllabusProgramBuild
    attr_accessor :program_json, :type_program, :state, 
      :coordinator_id, :supervisor_id, :modality_id, :evaluation_system_json
      
    def initialize(**attributes)
      attributes.each do |k,v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end
  end  
end  