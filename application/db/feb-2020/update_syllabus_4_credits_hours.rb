require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'feb-2020-syllabus-credits-hours.csv'))
puts "UPDATE APPROBER AND EDITOR TO SYLLABUSES) "
csv = CSV.parse(csv_text.scrub, headers: true)

customPrompt = Import::CustomPrompt.new
cycle_id = customPrompt.choice_cycle

import_obj = Import::ConvertToJson.new
cant_rows = 0
csv.each_with_index do |row, index|
  cant_rows = cant_rows+1
  catalog_code = row["catalog_code"]
  course = import_obj.search_course_by_catalog_code(catalog_code)
  if course.present?
  	current_course = course.id
	  weekly_hours = row["weekly_hours"]
	  credits = row["credits"]
	  syllabus = Syllabus.where(course_id: course.id).where(cycle_id: cycle_id).first
	  syllabus.update_attributes!(weekly_hours: weekly_hours, credits: credits)
	  puts  "NroFilaExcel: #{index+1} | SilabusProgramId: #{syllabus.id} | Credits: #{credits} | Weekly hours: #{weekly_hours}"
  else
  	puts "Curso con código #{catalog_code} no está registrado"
  end
end
puts "Update #{cant_rows} records"