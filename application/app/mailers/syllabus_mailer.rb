class SyllabusMailer < ApplicationMailer
  def send_email_approve_syllabus(options = {})
    @name = options[:name]
    @email = options[:email]
    @message = options[:message]
    @title = options[:title]
    @course_name = options[:course_name]
    @syllabus_program = options[:syllabus_program]
    @url_access = options[:url_access]
    #@email = "joel@lacafetalab.pe"
    mail(:to => @email, :subject => "Tienes un Sílabo pendiente de aprobación")
  end

  def send_email_observate_syllabus(options = {})
    @name = options[:name]
    @email = options[:email]
    @message = options[:message]
    @title = options[:title]
    @course_name = options[:course_name]
    @syllabus_program = options[:syllabus_program]
    @url_access = options[:url_access]
    #@email = "joel@lacafetalab.pe"
    mail(:to => @email, :subject => @title)
  end

  def send_email_download_syllabus(options = {})
    @name = options[:name]
    @email = options[:email]
    @url_access = options[:url_access]
    mail(:to => @email, :subject => "La descarga de Sílabos se ha completado")
  end

  def send_email_to_approve_syllabus(options = {})
    @user_name = options[:user_name]
    @title = options[:title]
    @email = options[:email]
    @syllabus_program = options[:syllabus_program]
    @url_access = options[:url_access]
    @url_silbia = options[:url_silbia]
    @url_logo = options[:url_logo]
    mail(:to => @email, :subject => @title)
  end

  def send_message_request_edit(options = {})
    @email = ENV["EMAIL_CALEDU"]
    @user_name = options[:user_name]
    @title = options[:title]
    @syllabus_program = options[:syllabus_program]
    @request_user_name = options[:request_user_name]
    @reason = options[:reason]
    @url_logo = options[:url_logo]
    @url_silbia_admin = "https://silbia-admin.utp.edu.pe/"
    mail(:to => @email, :subject => @title)
  end

  def send_email_to_observe_syllabus(options = {})
    @email = options[:email]
    @user_name = options[:user_name]
    @title = options[:title]
    @syllabus_program = options[:syllabus_program]
    @url_access = options[:url_access]
    @url_silbia = options[:url_silbia]
    @url_logo = options[:url_logo]
    mail(:to => @email, :subject => @title)
  end

  def send_email_approved_syllabus(options = {})
    @email = options[:email]
    @user_name = options[:user_name]
    @title = options[:title]
    @syllabus_program = options[:syllabus_program]
    @url_access = options[:url_access]
    @url_silbia = options[:url_silbia]
    @url_logo = options[:url_logo]
    mail(:to => @email, :subject => @title)
  end

  def send_message_request_edition(options = {})
    @email = options[:email]
    @title = options[:title]
    @syllabus_program = options[:syllabus_program]
    @user_name = options[:user_name]
    @url_logo = options[:url_logo]
    @url_access = options[:url_access]
    @url_silbia = options[:url_silbia]
    mail(:to => @email, :subject => @title)
  end
end
