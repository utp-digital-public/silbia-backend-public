module SyllabusChange
  module Changes
    module ExtractContentText
      
      def self.extract_to(syllabus_content, key)
        return "" unless syllabus_content.present?
        arr_texts = syllabus_content["texts"]
        arr_texts.try(:each) do |text|
          return text["content"] if text["title"]==key
        end
        return ""  
      end  

    end 
  end  
end      