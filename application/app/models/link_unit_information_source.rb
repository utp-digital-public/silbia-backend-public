class LinkUnitInformationSource < ApplicationRecord
  audited associated_with: :information_source
  belongs_to :information_source
end
