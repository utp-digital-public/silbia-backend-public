class AddCycleStartEndToCycle < ActiveRecord::Migration[5.2]
  def change
    add_column :cycles, :cycle_start, :datetime
    add_column :cycles, :cycle_end, :datetime
  end
end
