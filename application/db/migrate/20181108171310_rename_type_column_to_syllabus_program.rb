class RenameTypeColumnToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    rename_column :syllabus_programs, :type, :type_program
  end
end
