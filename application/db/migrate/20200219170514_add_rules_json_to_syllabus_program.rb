class AddRulesJsonToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
  	add_column :syllabus_programs, :evaluation_rules_json, :jsonb, default: "{}"
  end
end
