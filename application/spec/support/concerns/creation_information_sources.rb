module CreationInformationSourcesConcern
  extend ActiveSupport::Concern
  included do
    let(:book) {FactoryBot.create(:book)}
    let(:course_estrategia) {FactoryBot.create(:estrategia)}
    let(:math_course) { FactoryBot.create(:math_coursej)}
    let(:information_source_basic) { FactoryBot.create(:information_source_basic, :information_source_with_link_units)}
    let(:information_source_complementary) { FactoryBot.create(:information_source_complementary)}
  end  
end
