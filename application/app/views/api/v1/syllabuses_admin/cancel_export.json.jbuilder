json.status "success"
json.data do
    if @job_status.present?
        json.partial! "api/v1/job_statuses/job_status", job_status: @job_status
    else
        json.null!
    end
end