class AddTitleToUnits < ActiveRecord::Migration[5.2]
  def change
    add_column :units, :title, :string, :default => ""
  end
end
