json.id syllabus.id
json.course do
  json.partial! "api/v1/syllabuses/course", course: syllabus.course
end
json.weekly_hours syllabus.weekly_hours
json.credits syllabus.credits
json.cycle syllabus.cycle
json.syllabus_content syllabus.syllabus_content
json.year syllabus.year
json.modalities do
  json.array! syllabus.modalities, partial: "api/v1/syllabus_programs/syllabus_modality", as: :modality
end
json.basic_books do
  json.array! syllabus.syllabus_information_sources.basic, partial: "api/v1/syllabus_programs/syllabus_information_source", as: :syllabus_information_source
end
json.complementary_books do
  json.array! syllabus.syllabus_information_sources.complementary, partial: "api/v1/syllabus_programs/syllabus_information_source", as: :syllabus_information_source
end
json.units do
  json.array! syllabus.units.order(:unit_number) do |unit|
    json.id unit.id
    json.syllabus_id unit.syllabus_id
    json.title unit.title
    json.achievement unit.achievement
    json.unit_number unit.unit_number
    json.created_at unit.created_at
    json.update_at unit.updated_at
    json.topics unit.topics.order(:id)
    json.unit_information_sources unit.unit_information_sources
  end
end
