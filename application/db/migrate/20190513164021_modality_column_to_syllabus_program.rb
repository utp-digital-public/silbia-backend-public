class ModalityColumnToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_reference  :syllabus_programs, :modality, foreign_key: true
  end
end
