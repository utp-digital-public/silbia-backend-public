class AddTypeCycleToCycles < ActiveRecord::Migration[5.2]
  def change
    add_column :cycles, :type_cycle, :string, default: REGULAR_CYCLE, null: false
  end
end
