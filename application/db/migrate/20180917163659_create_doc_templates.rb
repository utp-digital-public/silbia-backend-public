class CreateDocTemplates < ActiveRecord::Migration[5.2]
  def change
    create_table :doc_templates do |t|
      t.string :name, null: false
      t.integer :type_of_template, null: false, default: 1
      t.boolean :is_active, default: false
      t.timestamps
    end
  end
end
