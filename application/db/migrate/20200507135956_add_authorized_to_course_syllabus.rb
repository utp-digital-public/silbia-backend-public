class AddAuthorizedToCourseSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_column :program_courses, :authorized, :boolean, :default => true, null: false
    add_column :syllabus_programs, :authorized, :boolean, :default => true, null: false
  end
end
