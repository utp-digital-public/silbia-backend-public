json.id syllabus.id
json.weekly_hours syllabus.weekly_hours
json.credits syllabus.credits
json.code_catalog_of_syllabus syllabus.code_catalog_of_syllabus
json.name_syllabus syllabus.name_syllabus
json.coordinator syllabus.coordinator
json.modality_id syllabus.modality_id
json.state syllabus.state
json.type_program syllabus.type_program
json.human_type_program syllabus.human_type_program
json.show_modal syllabus.show_modal
json.supervisor_id syllabus.supervisor_id
json.modality syllabus.modality
