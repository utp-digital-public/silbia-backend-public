json.status "success"
json.data do
    json.partial! 'api/v1/user_admins/user_admin', user_admin: @user_admin
end