json.status "success"
json.data do
  json.partial! "api/v1/shared/pagination", collection: @careers
  json.careers do
    json.array! @careers, partial: "api/v1/careers/career", as: :career
  end
end
