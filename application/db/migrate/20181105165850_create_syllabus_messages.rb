class CreateSyllabusMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :syllabus_messages do |t|
      t.references :syllabus_program, foreign_key: true
      t.text :message, null: false
      t.integer :to_user_id, null: false
      t.integer :state, null: false

      t.timestamps
    end
  end
end
