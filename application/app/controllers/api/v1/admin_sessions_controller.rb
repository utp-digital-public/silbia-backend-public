class Api::V1::AdminSessionsController < Api::ApiV1AdminController
    skip_before_action :verify_auth_token, only: [:create]
    
    def create
      params.require(:username)
      params.require(:password)
      begin
        @user_admin = UserAdmin.find_for_database_authentication(email: params[:username])
        if @user_admin.present? 
          if @user_admin.valid_password?(params[:password])     
            @time = Time.now + token_duration
            @payload = {user_id: @user_admin.id, email: @user_admin.email}
            @token = Util::JsonWebToken.encode(@payload,@time)
          else 
            render_unauthorized("No se pudo iniciar sesión", "Correo y/o Contraseña incorrecta.", "Revise sus credenciales")
          end
        else
          render_unauthorized("No se pudo iniciar sesión", "Correo y/o Contraseña incorrecta", "Revise sus credenciales")
        end 
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
    def me
      @user_admin = @current_user
    end
    def refresh
      @time = Time.now + token_duration
      @payload = @decoded
      @token = Util::JsonWebToken.encode(@payload,@time)
      @user_admin = @current_user
    end
    private
      def token_duration
        token_duration = 60.to_i 
        if ENV['JWT_EXPIRE_MINUTES']
          token_duration = ENV['JWT_EXPIRE_MINUTES'].to_i
        end
        return token_duration.minutes.to_i
      end
  end