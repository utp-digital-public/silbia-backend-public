// This file may be used for providing additional customizations to the Trestle
// admin. It will be automatically included within all admin pages.
//
// For organizational purposes, you may wish to define your customizations
// within individual partials and `require` them here.
//
//  e.g. //= require "trestle/custom/my_custom_js"
//= require "progressbar"
//= require toastr/toastr
//= require "trestle/copy_syllabus/copy_syllabus"
//= require moment
//= require tempusdominus-bootstrap-4



$(function() {
	var copy_button = document.getElementById("cp_btn")
	if (copy_button) {
		copy_button.addEventListener("click", copy_password);
		$('#cp_btn').tooltip({
			title: 'Copiado!',
			trigger: 'manual',
			placement: 'bottom',
		});
		function copy_password() {
			$('#cp_btn').tooltip("show");
			var copyText = document.getElementById("suggested_password_label");
			var textArea = document.createElement("textarea");
			textArea.value = copyText.textContent;
			document.body.appendChild(textArea);
			textArea.select();
			document.execCommand("Copy");
			textArea.remove();
			setTimeout(function(){ $('#cp_btn').tooltip("hide");; }, 2000);
		}
	} 
	
	$("#blended_coordinator_id").select2({theme: 'bootstrap'});
	$("#in_person_coordinator_id").select2({theme: 'bootstrap'});
	$("#blended_supervisor_id").select2({theme: 'bootstrap'});
	$("#in_person_supervisor_id").select2({theme: 'bootstrap'});

  $("#book_basic_ids").select2({
    theme: 'bootstrap',
    allowClear: true,
    minimumInputLength: 4,
    maximumSelectionLength: 3,
    containerCssClass: ':all:',
    dataType: 'json',
    language: {
	    noResults: function() {

	      return "No hay resultado";        
	    },
	    searching: function() {

	      return "Buscando..";
	    },
	    inputTooShort: function (args) {
	      var remainingChars = args.minimum - args.input.length;

	      var message = 'Por favor, introduzca ' + remainingChars + ' car';

	      if (remainingChars == 1) {
	        message += 'ácter';
	      } else {
	        message += 'acteres';
	      }

	      return message;
	    },
	    maximumSelected: function (args) {
	      var message = 'Sólo puede seleccionar ' + args.maximum + ' libro';

	      if (args.maximum != 1) {
	        message += 's';
	      }

	      return message;
	    }
	  },
    ajax: {
      url: '/program_courses/books',
      delay: 250,
      data: function(params) {
        return { q: params.term }
      },
      processResults: function (data, params) {
        return {
          results: $.map(data, function(value, index) {
            return { id: value.id, text: value.text };
          })
        };
      }
    }
  });

  $("#book_complementary_ids").select2({
    theme: 'bootstrap',
    allowClear: true,
    minimumInputLength: 4,
    containerCssClass: ':all:',
    dataType: 'json',
    language: {
	    noResults: function() {

	      return "No hay resultado";        
	    },
	    inputTooShort: function (args) {
	      var remainingChars = args.minimum - args.input.length;

	      var message = 'Por favor, introduzca ' + remainingChars + ' car';

	      if (remainingChars == 1) {
	        message += 'ácter';
	      } else {
	        message += 'acteres';
	      }

	      return message;
	    },
	    searching: function() {

	      return "Buscando..";
	    }
	  },
    ajax: {
      url: '/program_courses/books',
      delay: 250,
      data: function(params) {
        return { q: params.term }
      },
      processResults: function (data, params) {
        return {
          results: $.map(data, function(value, index) {
            return { id: value.id, text: value.text };
          })
        };
      }
    }
  });

  var select2Instance = $("#book_basic_ids").data('select2');
	select2Instance.on('results:message', function(params){
	  this.dropdown._resizeDropdown();
	  this.dropdown._positionDropdown();
	});

	$("#book_basic_ids").on("select2:selecting", function (e) {
		var selected_complementary = $('#book_complementary_ids').select2('data');
		const found = selected_complementary.some(comp => parseInt(comp.id) === e.params.args.data.id);
		if (found) {
			toastr.error("Ya seleccionó el libro " + e.params.args.data.text);
			$('#book_basic_ids').select2('close');
			e.preventDefault();
		}
	})

	$("#book_complementary_ids").on("select2:selecting", function (e) {
		var selected_basic = $('#book_basic_ids').select2('data');
		const found = selected_basic.some(comp => parseInt(comp.id) === e.params.args.data.id);
		if (found) {
			toastr.error("Ya seleccionó el libro " + e.params.args.data.text);
			$('#book_complementary_ids').select2('close');
			e.preventDefault();
		}
	})

	var select2Instance = $("#book_complementary_ids").data('select2');
	select2Instance.on('results:message', function(params){
	  this.dropdown._resizeDropdown();
	  this.dropdown._positionDropdown();
	});

  var select2Instance = $("#blended_coordinator_id").data('select2');
	select2Instance.on('results:message', function(params){
	  this.dropdown._resizeDropdown();
	  this.dropdown._positionDropdown();
	});

	var select2Instance = $("#in_person_coordinator_id").data('select2');
	select2Instance.on('results:message', function(params){
	  this.dropdown._resizeDropdown();
	  this.dropdown._positionDropdown();
	});

	var select2Instance = $("#blended_supervisor_id").data('select2');
	select2Instance.on('results:message', function(params){
	  this.dropdown._resizeDropdown();
	  this.dropdown._positionDropdown();
	});

	var select2Instance = $("#in_person_supervisor_id").data('select2');
	select2Instance.on('results:message', function(params){
	  this.dropdown._resizeDropdown();
	  this.dropdown._positionDropdown();
	});


});