class SyllabusMessage < ApplicationRecord
  belongs_to :syllabus_program
  validates :message, :state, :section, presence: { message: "es requerido" }

  def type
    ([6, 7, 9].include? section) ? "unique" : "shared"
  end
end
