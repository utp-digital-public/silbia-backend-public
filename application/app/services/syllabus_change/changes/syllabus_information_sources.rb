module SyllabusChange
  module Changes
    class SyllabusInformationSources < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::UtilDetectCronogram

      def initialize(**attribute)
        super(**attribute)
        attribute.each do |k, v|
          instance_variable_set("@#{k}", v) unless v.nil?
        end
        @title = "Fuentes de Información"
        @id_section = "section-fuentes"
        @changes_detected = 0
        @is_edited = false
        @syllabus = @syllabus_program.syllabus
      end

      def detect()
        @is_edited = detect_changes_in_link_and_information_sources?(@last_date)
      end

      def get_array_id_information_source_by_course(syllabus)
        list_information = SyllabusInformationSource.information_sources_by_course(course.id)
        list_information.map { |information_source| information_source.id }
      end

      def detect_changes_in_link_and_information_sources?(date_to_filter)
        changes_detected_information = information_source_have_changes?(date_to_filter)
        return true if changes_detected_information
        changes_link_units = link_units_have_changes?(date_to_filter)
        return true if changes_link_units
        return false
      end

      def information_source_have_changes?(date_to_filter)
        audits =
          Audited.audit_class.where("audited_changes @> ?", { syllabus_id: @syllabus.id }.to_json)
            .where("auditable_type=?", "SyllabusInformationSource")
            .where(user_id: @syllabus_program.coordinator_id).reorder("created_at DESC")
        audits = audits.where("created_at>=?", date_to_filter)
        return true if audits.present?
        false
      end

      def link_units_have_changes?(date_to_filter)
        array_id_informations = @syllabus.syllabus_information_sources.pluck(:id)
        audits = get_list_audits(array_id_informations, date_to_filter)
        audits.try(:each) do |audit|
          audit.audited_changes.each do |key, value|
            return true if compare_if_changes_ocurred?(key, value)
          end
        end
        return false
      end

      def get_list_audits(array_id_informations, date_to_filter)
        audits = Audited.audit_class.where("associated_id IN (?)", array_id_informations)
        audits = audits.where(user_id: @syllabus_program.coordinator_id)
        audits = audits.where("associated_type=?", "SyllabusInformationSource")
        audits = audits.where("auditable_type=?", "UnitInformationSource").reorder("created_at DESC")
        audits = audits.where("created_at>=?", date_to_filter)
        audits
      end

      def compare_if_changes_ocurred?(key, value)
        keys_to_compare = ["from_page", "to_page"]
        if keys_to_compare.include? key
          puts "hasta aqui entro comparar"
          value_before_change = get_value_before_the_last_change(value)
          value_after_change = get_last_value_of_audit(value)
          puts " Su key es #{key} El valor anterior es: #{get_value_before_the_last_change(value)} El valor nuevo es : #{get_last_value_of_audit(value)} El ultimo : #{value}"
          if (value_before_change != value_after_change)
            return true
          end
        end
      end

      # This method overrite
      def set_hash_value(key, value)
        @hash_values[:information_source] = get_value_before_the_last_change(value) if key == "program_json"
      end
    end
  end
end
