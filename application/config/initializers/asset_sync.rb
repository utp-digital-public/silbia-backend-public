AssetSync.configure do |config|
    config.gzip_compression = true
    config.manifest = true
    config.existing_remote_files = 'keep'
    config.fog_provider = 'AWS'
    config.aws_access_key_id = ENV.fetch('AWS_ACCESS_KEY_ID')
    config.aws_secret_access_key = ENV.fetch('AWS_SECRET_ACCESS_KEY')
    config.fog_directory = ENV.fetch('S3_ASSET_DIRECTORY')
    config.fog_region = ENV.fetch('AWS_REGION') #”eu-west-1"
    # Change host option in fog (only if you need to)
    config.fog_host = ENV.fetch('FOG_HOST') #'s3.amazonaws.com'
    config.fog_path_style = true
    config.run_on_precompile = true # https://github.com/AssetSync/asset_sync#rake-task
    # The block should return an array of file paths
    config.add_local_file_paths do
        # Any code that returns paths of local asset files to be uploaded
        public_root = Rails.root.join("public")
        Dir.chdir(public_root) do
            Dir[File.join('api-docs','v1','swagger.yaml')]
        end
    end
    if !Rails.env.development?
        config.enabled = ENV.fetch('ASSET_SYNC_ENABLED') == '1' ? true:false
    else
        config.enabled
    end
end
