FactoryBot.define do
  factory :link_unit_information_source do
    number_unit { 1 }
    from_to_page { 1 }
    page_unit { 1 }
    is_checked { false }
    delete_logic { false }
    information_source 
  end
end
