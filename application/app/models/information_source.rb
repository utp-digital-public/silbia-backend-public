class InformationSource < ApplicationRecord
  audited
  belongs_to :course, :inverse_of => :information_sources
  belongs_to :book
  has_many :link_units, class_name: "LinkUnitInformationSource", :dependent => :delete_all
  after_create :after_create_syllabus
  after_destroy :after_destroy_syllabus
  scope :order_by_type_source, -> { order("type_source ASC") }
  scope :with_course_and_type_source, ->(id_course, type_source_param) {
          where(course_id: id_course)
            .where(type_source: type_source_param)
        }
  scope :collection_by_ids, ->(arr_ids) {
          where("id IN (?)", arr_ids).includes(:link_units)
        }

  scope :all_relations_included, -> {
          includes(:book).includes(:course).includes(:link_units)
        }

  scope :information_sources_by_course, ->(course_id) {
          where(course_id: course_id)
        }

  def self.basic
    where(type_source: "Básica")
  end

  def self.complementary
    where(type_source: "Complementaria")
  end

  def after_create_syllabus
    syllabus_information_source = SyllabusInformationSource.new
    syllabus_information_source.syllabus = course.current_syllabus
    syllabus_information_source.book_id = book_id
    syllabus_information_source.type_source = type_source
    syllabus_information_source.observation = observation
    syllabus_information_source.save
  end

  def after_destroy_syllabus
    SyllabusInformationSource.where(syllabus_id: course.current_syllabus.id, book_id: book_id).destroy_all
  end
end
