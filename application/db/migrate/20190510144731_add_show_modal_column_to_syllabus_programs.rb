class AddShowModalColumnToSyllabusPrograms < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :show_modal, :boolean, default: true
  end
end
