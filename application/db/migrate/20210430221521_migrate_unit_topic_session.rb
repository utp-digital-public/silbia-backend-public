class MigrateUnitTopicSession < ActiveRecord::Migration[5.2]
  def change
    begin
      execute "truncate table session_topics RESTART IDENTITY;"
      execute "truncate table sessions RESTART IDENTITY CASCADE;"
      execute "truncate table topics RESTART IDENTITY CASCADE;"
      execute "truncate table units RESTART IDENTITY CASCADE;"
      execute "
      insert into migration_units( syllabus_program_id, syllabus_id, modality_id, base_syllabus, catalog_code, course_name,
            course_deleted, course_authorized, unit_title, number_unit, unit_achievement,
            number_week, number_session, session_topic, session_laboratory, session_type_laboratory,
            activities, idx_unit, idx_week, idx_session)
      select sp.id                         syllabus_program_id,
      s.id                          syllabus_id,
      sp.modality_id,
      case
      when sp.modality_id = 3 then
      case when pcd.id is not null then true else false end
      when sp.modality_id = 1 and sp.created_at < (
      select min(created_at)
      from syllabus_programs
      where modality_id = 3
      ) then true
      else false
      end                       base_syllabus,
      pc.catalog_code,
      pc.name                       course_name,
      pc.deleted                    course_deleted,
      pc.authorized                 course_authorized,
      unit ->> 'title'              unit_title,
      unit ->> 'number_unit'        number_unit,
      unit ->> 'achievement'        unit_achievement,
      week ->> 'number_week'        number_week,
      session ->> 'number_session'  number_session,
      session ->> 'subject'         session_topic,
      session ->> 'laboratory'      session_laboratory,
      session ->> 'type_laboratory' session_type_laboratory,
      session -> 'activities'       activities,
      idx_unit,
      idx_week,
      idx_session

      from syllabus_programs sp
      join syllabuses s on sp.syllabus_id = s.id
      join cycles c on s.cycle_id = c.id
      join program_courses pc
      on s.course_id = pc.course_id and sp.modality_id = pc.modality_id
      left join program_courses pcd on pcd.course_id = pc.course_id and
                          pcd.catalog_code = pc.catalog_code and pcd.deleted = true
      ,
      jsonb_array_elements(sp.program_json -> 'units') with ordinality as t(unit, idx_unit),
      jsonb_array_elements(unit -> 'weeks') with ordinality as r(week, idx_week),
      jsonb_array_elements(week -> 'sessions') with ordinality as q(session, idx_session)

      order by sp.id, idx_unit, cast(week ->> 'number_week' as int),
      cast(session ->> 'number_session' as int)
      ;"
      execute "insert into units(syllabus_id, unit_number, title, achievement, created_at, updated_at)
      select datos.syllabus_id,
             idx_unit,
             max(datos.unit_title)       title,
             max(coalesce(datos.unit_achievement,'')) achievement,
             now(),
             now()
      from migration_units datos
               left join units on datos.syllabus_id = units.syllabus_id and datos.idx_unit = units.unit_number
      where units.id is null
        and datos.base_syllabus = true
      group by datos.syllabus_id, datos.idx_unit
      order by 1, 2;"
      execute "insert
      into topics (unit_id, topic, created_at, updated_at)
      select id, session_topic, now(), now()
      from (
               select units.id, migration_units.session_topic
               from units
                        join migration_units
                             on units.syllabus_id = migration_units.syllabus_id and
                                units.unit_number = migration_units.idx_unit
                        left join topics t on units.id = t.unit_id and migration_units.session_topic = t.topic
               where t.id is null
                 and migration_units.session_topic != ''
                 and migration_units.base_syllabus = true
               ORDER BY cast(migration_units.number_session as int)
           ) t
      group by id, session_topic
      ;"
      execute "insert into sessions(syllabus_program_id, laboratory_id, unit_id, week_number, session_number, modality_id,
                    created_at, updated_at)
              select t.syllabus_program_id,
              cast(case session_laboratory when '' then '0' else session_laboratory end as int) laboratory,
              u.id,
              cast(t.number_week as int)                                        week_number,
              cast(t.number_session as int)                                     session_number,
              cast(session_type_laboratory as int)                                      modality_id,
              now(),
              now()
              from migration_units t
              join units u on t.syllabus_id = u.syllabus_id and t.idx_unit = u.unit_number
              left join sessions s on u.id = s.unit_id and cast(t.number_session as int) = s.session_number
              where s.id is null
              and t.session_topic != ''

              order by t.syllabus_program_id,cast(t.number_week as int) ,cast(t.number_session as int);"
      execute "insert into session_topics(session_id,topic_id,activities,created_at,updated_at)
      select s.id, tt.id, t.activities, now(), now()
      from migration_units t
               join units u on t.syllabus_id = u.syllabus_id and t.idx_unit = u.unit_number
               join topics tt on u.id = tt.unit_id and tt.topic = t.session_topic
               join sessions s on u.id = s.unit_id and cast(t.number_session as int) = s.session_number and t.syllabus_program_id = s.syllabus_program_id
               left join session_topics st on s.id = st.session_id and tt.id = st.topic_id
      where st.id is null"
    end
  end
end
