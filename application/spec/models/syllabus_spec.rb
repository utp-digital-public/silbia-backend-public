require 'rails_helper'

RSpec.describe Syllabus, type: :model do
  describe "Relations model" do 
    it { should belong_to(:doc_template) }
    it { should belong_to(:user) }
    it { should belong_to(:course)}
    it { should belong_to(:cycle) }
    it { should belong_to(:curriculum_course) }
  end
  it { should validate_presence_of(:year).with_message("es requerido") }
  it { should validate_numericality_of(:year).only_integer.with_message("debe ser un numero entero") }
  it { should validate_presence_of(:cycle).with_message("es requerido") }
  it { should validate_presence_of(:cycle_academic).with_message("es requerido") }
  #it { should validate_presence_of(:modality).with_message("es requerido") }
end
