class Topic < ApplicationRecord
  audited associated_with: :unit
  belongs_to :unit
  has_many :session_topic, :dependent => :destroy
end
