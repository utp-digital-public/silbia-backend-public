module SyllabusChange
  module Changes
    class ScheduleActivityChange < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::UtilDetectCronogram
      def initialize(**attribute)
        super(**attribute)
        @title = "Cronograma de actividades"
        @id_section = "section-cronograma"
        @changes_detected = 0
        @is_edited = false
      end
       
      def detect()
        iterate_audit_and_set_hash_values_syllabus()
        units = @syllabus_program.program_json["units"]
        @is_edited = true if detect_change_units?(units)
      end

      def detect_change_units?(units)
        return false unless @hash_values[:cronogram].present?  
        with_changes = false
        last_change_unit = @hash_values[:cronogram]["units"]
        detect_changes_in_the_number_of_units(units, last_change_unit)
        units.try(:each_with_index) do |unit, index|
          iterate_fields_and_count_changes(
            array_fields: ["title","achievement"],
            object: unit,
            last_change_object: last_change_unit,
            index: index
          )
          last_weeks = last_change_unit[index]["weeks"] if last_change_unit[index]!=nil
          detect_change_in_weeks(unit["weeks"], last_weeks)
        end
        @changes_detected =
        with_changes = true  if @changes_detected > 0
        with_changes
      end

      def detect_change_in_weeks(weeks, last_change_weeks)
        last_change_weeks ||= []
        detect_changes_in_the_number_of_weeks(weeks, last_change_weeks)
        weeks.try(:each_with_index) do |week, index|
          iterate_fields_and_count_changes(
            array_fields: ["number_week"],
            object: week,
            last_change_object: last_change_weeks,
            index: index
          )
          last_sessions = last_change_weeks[index]["sessions"] if last_change_weeks[index]!=nil
          detect_change_in_sessions(week["sessions"], last_sessions )
        end  
      end

      def detect_change_in_sessions(sessions, last_change_sessions)
        last_change_sessions ||= []
        detect_changes_in_the_number_of_sessions(sessions, last_change_sessions)
        sessions.try(:each_with_index) do |session, index|
          iterate_fields_and_count_changes(
            array_fields: ["subject", "laboratory", "number_session", "type_laboratory"],
            object: session,
            last_change_object: last_change_sessions,
            index: index
          )
          last_activities = last_change_sessions[index]["activities"] if last_change_sessions[index]!=nil
          detect_change_in_activities(session["activities"], last_activities)
        end  
      end
      
      def detect_change_in_activities(activities, last_change_activities)
        last_change_activities ||= []
        detect_changes_in_the_number_of_activities(activities, last_change_activities)
        activities.try(:each_with_index) do |activity, index|
          iterate_fields_and_count_changes(
            array_fields: ["modality", "description", "type_activity", "label_description"],
            object: activity,
            last_change_object: last_change_activities,
            index: index
          )
        end 
      end

      def detect_changes_in_the_number_of_units(units, last_change_unit)
        last_change_unit ||= []
        size_of_array_are_diferent?(units, last_change_unit)
      end
      
      def detect_changes_in_the_number_of_weeks(weeks, last_change_weeks)
        size_of_array_are_diferent?(weeks, last_change_weeks)
      end

      def detect_changes_in_the_number_of_sessions(sessions, last_change_sessions)
        size_of_array_are_diferent?(sessions, last_change_sessions)
      end

      def detect_changes_in_the_number_of_activities(activities, last_change_activities)
        size_of_array_are_diferent?(activities, last_change_activities)
      end
      
      
        
       # This method overrite
      def set_hash_value(key, value)
        @hash_values[:cronogram] = get_value_before_the_last_change(value) if key == "program_json"
      end  
      
    end  
  end
end  