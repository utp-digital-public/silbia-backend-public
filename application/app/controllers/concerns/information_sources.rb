module InformationSources
  extend ActiveSupport::Concern
  included do    
  end
  
  def load_information_sources
    id_course = params[:id_course]
    type_source = params[:type_source]
    information_source_service = InformationSourceService.new(id_course: id_course)
    @information_sources = information_source_service.get_information_sources_by_type(type_source)
    render json: @information_sources, :include => [ 
      { :book=>{} },
      { :link_units=>{} } 
    ] 
  end  
  
  def search_information_sources
    text = params[:text_filter]
    id_course = params[:id_course]
    type_source_param = params[:type_source]
    information_sources_books = InformationSource.with_course_and_type_source(id_course, type_source_param)
    books_not_include = information_sources_books.map { |element| element.book_id }
    @list_books = Book.books_with_title_like_and_without_list_books(text, books_not_include).limit(10)
    #render json: @list_books
    json_response(@list_books)
  end
  
  def add_information_source
    id_book = params[:id_book]
    id_course = params[:id_course]
    type_source = params[:type_source]
    new_information_source = InformationSource.new
    new_information_source.book_id = id_book
    new_information_source.course_id = id_course
    new_information_source.type_source = if type_source == "Básica" then "Básica" else "Complementaria" end
    new_information_source.save!
    information_source_service = InformationSourceService.new(id_course: id_course)
    @information_sources = information_source_service.get_information_sources_by_type(type_source)
    render json: @information_sources, :include => {:book=>{} }
  end
  
  def remove_information_source
    id_information_source = params[:id_information_source]
    id_course = params[:id_course]
    type_source = params[:type_source]
    obj_information_source = InformationSource.find(id_information_source)
    obj_information_source.destroy
    information_source_service = InformationSourceService.new(id_course: id_course)
    @information_sources = information_source_service.get_information_sources_by_type(type_source)
    render json: @information_sources, :include => {:book=>{} }
  end
  
  def update_observation_information_source
    id_information_source = params[:id_information_source]
    text_observation = params[:observation]  
    obj_information_source = InformationSource.find(id_information_source)
    obj_information_source.observation = text_observation
    obj_information_source.update_column(:observation, text_observation)
    render json: "ok"
  end  

end 