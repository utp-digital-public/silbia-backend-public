class FixMigrateSessionTopics < ActiveRecord::Migration[5.2]
  def change
    execute "TRUNCATE TABLE session_topics RESTART IDENTITY;"
    execute "TRUNCATE TABLE sessions RESTART IDENTITY CASCADE;"
    execute "drop table if exists tmp_homologado;"
    execute "with semipresencial as (
        select syllabus_program_id,
              syllabus_id,
              catalog_code,
              course_name,
              course_authorized,
              count(1)                    count_session_topics,
              count(distinct idx_unit)    count_units,
              count(distinct number_week) count_weeks
        from migration_units t
        where modality_id = 2
        group by syllabus_program_id, syllabus_id, catalog_code, course_name, course_authorized),
        presencial as (
            select syllabus_program_id,
                    syllabus_id,
                    catalog_code,
                    course_name,
                    course_authorized,
                    count(1)                    count_session_topics,
                    count(distinct idx_unit)    count_units,
                    count(distinct number_week) count_weeks
            from migration_units t
            where base_syllabus = true
            group by syllabus_program_id, syllabus_id, catalog_code, course_name, course_authorized
        )

    select semipresencial.syllabus_id, semipresencial.syllabus_program_id
    into tmp_homologado
    from semipresencial
            join presencial on semipresencial.syllabus_id = presencial.syllabus_id
    where semipresencial.count_session_topics = 2 * presencial.count_session_topics
      and presencial.count_units = semipresencial.count_units;"

    execute "insert into sessions(syllabus_program_id, laboratory_id, unit_id, week_number, session_number, modality_id,
                        created_at, updated_at)
    select t.syllabus_program_id,
          cast(case t.session_laboratory when '' then '0' else t.session_laboratory end as int) laboratory_id,
          u.id                                                                                  unit_id,
          cast(t.number_week as int)                                                            week_number,
          cast(t.number_session as int)                                                         session_number,
          cast(t.session_type_laboratory as int)                                                modality_id,
          now(),
          now()
    from (select t.*,
                case
                    when th.syllabus_program_id is not null then (number_session + 1) / 2
                    else number_session end new_number_session
          from migration_units t
                  left join tmp_homologado th on t.syllabus_program_id = th.syllabus_program_id
        ) t
            left join migration_units tb
                      on t.syllabus_id = tb.syllabus_id and tb.base_syllabus = true and
                          tb.number_session = t.new_number_session
            join units u on t.syllabus_id = u.syllabus_id and tb.idx_unit = u.unit_number
            left join sessions s on u.id = s.unit_id and t.number_session = s.session_number
    where s.id is null
      and t.session_topic != ''
    order by t.syllabus_program_id, t.number_week, t.number_session;"

    execute "insert into session_topics(session_id, topic_id, activities, created_at, updated_at)
    select s.id session_id, tt.id topic_id, t.activities, now(), now()
    from (select t.*,
                case
                    when th.syllabus_program_id is not null then (number_session + 1) / 2
                    else number_session end new_number_session
          from migration_units t
                  left join tmp_homologado th on t.syllabus_program_id = th.syllabus_program_id
        ) t
            join migration_units tb
                  on t.syllabus_id = tb.syllabus_id and tb.base_syllabus = true and tb.number_session = t.new_number_session
            join units u on t.syllabus_id = u.syllabus_id and tb.idx_unit = u.unit_number
            join topics tt on u.id = tt.unit_id and tt.topic = tb.session_topic
            join sessions s on u.id = s.unit_id and t.number_session = s.session_number and
                                t.syllabus_program_id = s.syllabus_program_id
            left join session_topics st on s.id = st.session_id and tt.id = st.topic_id
    where st.id is null
    order by t.syllabus_program_id, t.number_session;"
  end
end
