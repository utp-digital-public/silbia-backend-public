module SyllabusChange
  class DetectChange
    attr_accessor :syllabus_program, :date_change_to_be_approved, 
      :array_changes_in_syllabus, :hash_values,:list_auditable_objects_syllabus

    def  initialize(syllabus_program)
      @syllabus_program = syllabus_program
      @hash_values = Hash.new
      @section_detect_array = 
      [
        "general_data",
        "fundamentation",
        "sumilla",
        "general_achievement",
        "evaluation_system",
        "methodology",
        "cronogram",
        "information_source"
      ]
      @array_changes_in_syllabus = Array.new
    end  

    def call
      last_two_dates = get_last_two_dates_wich_state_was_to_be_approve()
      puts "LAS ULTIMAS DOS FECHAS FUERON: #{last_two_dates.second}"      
      if has_more_than_one_state_by?(TO_BE_APPROVED_STRING)
        object_track_with_last_date_to_be_approved = get_last_date_change_to_be_approved
        last_date = object_track_with_last_date_to_be_approved.created_at.to_date
        #last_date_in_date_time = object_track_with_last_date_to_be_approved.created_at
        preview_date_in_date_time = last_two_dates.second
        last_date_in_date_time = last_two_dates.first
        has_more_than_one_state = true
        objects_audit = 
          @syllabus_program.audits.where("created_at >=?", last_date).reorder("version DESC")     
          
          date_tracking = SyllabusChange::DateTrackingSyllabus.new(
            syllabus_program: @syllabus_program,
            has_more_than_one_state: has_more_than_one_state
          )
          last_date_in_date_time_ev = date_tracking.get_first_date_create()
          #puts "FECHA LAST ANTES DATE EV VERO: #{last_date_in_date_time_ev}"   
          #puts "FECHA LAST AHORA DATE EV VERO: #{last_date_in_date_time}"  
          #puts "FECHA VERO: #{last_date}"  
          #has_more_than_one_state = false 
          objects_audit_ev =  @syllabus_program.audits
          #p "OBJETO SISTEMA VERO ES #{objects_audit_ev.inspect}"
          #audits_evaluation_system = objects_audit_ev.where("created_at >=?", last_date_in_date_time)
          audits_evaluation_system = objects_audit_ev.where("created_at >=?", last_date).reorder("version DESC") 
          #p "AUDITORIA SISTEMA VERO ES #{audits_evaluation_system.inspect}"
      else
        date_tracking = SyllabusChange::DateTrackingSyllabus.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: has_more_than_one_state
        )
        last_date_in_date_time = date_tracking.get_first_date_create()
        has_more_than_one_state = false 
        objects_audit =  @syllabus_program.audits
        audits_evaluation_system = objects_audit.where("created_at >=?", last_date_in_date_time)
      end
      
      if preview_date_in_date_time.present?
        obtain_changes_in_syllabus_last_preview(last_date_in_date_time, preview_date_in_date_time, has_more_than_one_state)
      else
        obtain_changes_in_syllabus(last_date_in_date_time, has_more_than_one_state)
      end
      @section_detect_array.each do |section_detect|
        population_array_changes(section_detect, objects_audit, has_more_than_one_state , audits_evaluation_system)
      end  
    end

    def obtain_changes_in_syllabus_last_preview(last_date_in_date_time,preview_date_in_date_time, has_more_than_one_state)
      auditable_object = SyllabusChange::AuditSyllableObject.new(syllabus_program: @syllabus_program)
      @list_auditable_objects_syllabus = 
          auditable_object.get_audits_sylabus_with_date_previews_and_last(last_date_in_date_time, preview_date_in_date_time)
    end  

    def obtain_changes_in_syllabus(last_date_in_date_time, has_more_than_one_state)
      auditable_object = SyllabusChange::AuditSyllableObject.new(syllabus_program: @syllabus_program)
      @list_auditable_objects_syllabus = 
          auditable_object.get_audits_sylabus_with_date_greather_than(last_date_in_date_time)
 
      # if has_more_than_one_state.present?
      #   @list_auditable_objects_syllabus = 
      #     auditable_object.get_audits_sylabus_with_date_greather_than(last_date_in_date_time)
      # else
      #   @list_auditable_objects_syllabus = 
      #   #auditable_object.get_audits_sylabus_with_date_are_equals(last_date_in_date_time)
      #   auditable_object.get_audits_sylabus_with_date_greather_than(last_date_in_date_time)
      # end      
    end  

    def get_last_date_change_to_be_approved
      last_date_to_be_approved = 
      TrackingStatusSyllabus.where(syllabus_program_id: @syllabus_program.id)
        .where(state: TO_BE_APPROVED).order("created_at DESC").first
    end

    def get_last_two_dates_wich_state_was_to_be_approve()
      list_track = 
      TrackingStatusSyllabus.where(syllabus_program_id: @syllabus_program.id)
          .where(state: TO_BE_APPROVED)
          .order("created_at DESC").limit(2)
      list_track.map{ |track| track.created_at }
    end  

    def population_array_changes(section_detect, objects_audit, has_more_than_one_state, audits_evaluation_system)
      case section_detect
      when "general_data"
        @array_changes_in_syllabus.push({
          title: "Datos Generales",
          is_edited: false,
          id_section: "section-datosgenerales"
        })
      when "fundamentation"
        foundation_audit = SyllabusChange::Changes::Foundation.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state 
        )
        foundation_audit.call
        @array_changes_in_syllabus.push(foundation_audit.to_hash)
      when "sumilla"
        sumilla_audit = SyllabusChange::Changes::Sumilla.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state 
        )
        sumilla_audit.call
        @array_changes_in_syllabus.push(sumilla_audit.to_hash)
      when "general_achievement"
        achievement_audit = SyllabusChange::Changes::GeneralAchievement.new(
          syllabus: @syllabus_program.syllabus,
          auditable_objects: @list_auditable_objects_syllabus,
          has_more_than_one_state: has_more_than_one_state 
        )
        achievement_audit.call
        @array_changes_in_syllabus.push(achievement_audit.to_hash)
        
      when "evaluation_system"
        evaluation_system = SyllabusChange::Changes::EvaluationSystem.new(
          syllabus_program: @syllabus_program,
          objects_audit: audits_evaluation_system, 
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state
        )
        evaluation_system.call()

        rules = SyllabusChange::Changes::Ruler.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit, 
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state
        )
        rules.call()
        p "RULES ES #{rules.is_edited}"
        p "EVALUATION SYSTEMA ES: #{evaluation_system.is_edited}"
        evaluation_system.is_edited = true if rules.is_edited.present?
        @array_changes_in_syllabus.push(evaluation_system.to_hash)
      when "methodology"
        methodology = SyllabusChange::Changes::Methodology.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit, 
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state
        )
        methodology.call()
        @array_changes_in_syllabus.push(methodology.to_hash)                    
      when  "cronogram"
        cronogram = SyllabusChange::Changes::ScheduleActivityChange.new(
          syllabus_program: @syllabus_program,
          objects_audit: objects_audit, 
          hash_values: @hash_values,
          has_more_than_one_state: has_more_than_one_state
        )
        cronogram.detect()
        @array_changes_in_syllabus.push(cronogram.to_hash)
      when "information_source"
        information_sources = SyllabusChange::Changes::InformationSources.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: has_more_than_one_state
        )
        information_sources.detect()
        @array_changes_in_syllabus.push(information_sources.to_hash)
      else
      end
    end
  
    def has_more_than_one_state_by?(state)
      number_of_found_states = 0
      @syllabus_program.audits_desc.try(:each) do |audit|
        audit.audited_changes.try(:each) do |key,value|
          number_of_found_states+=1  if ( ( key == "state" ) &&( value[1] == state) )
          return true if number_of_found_states> 1    
        end  
      end
      return false
    end  
    
    
    
  end
end  