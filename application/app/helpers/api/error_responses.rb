module Api::ErrorResponses

    def response_error params={}
      render json: {
        status: 'error',
        error: {
            description: params[:description],
          reason: params[:reason],
          suggestion: params[:suggestion]
        }
      }, status: params[:status] || :ok
    end
  
  end
  