class CreateCycles < ActiveRecord::Migration[5.2]
  def change
    create_table :cycles do |t|
      t.string :name,null:false
      t.boolean :enabled, default:false
      t.timestamps
    end
  end
end
