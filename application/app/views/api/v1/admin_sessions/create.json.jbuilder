json.status "success"
json.data do
    json.token        @token
    json.exp          @time
    json.user_admin         
    json.user_admin do
        json.partial! "api/v1/user_admins/user_admin", user_admin: @user_admin
    end
end