class CreateGeneralCompetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :general_competitions do |t|
      t.string :competition, null: false
      t.string :description, null: false, default: ""
      t.timestamps
    end
  end
end
