require 'rails_helper'
# TODO: complete test
RSpec.describe WelcomeController, type: :controller do

  let!(:user){ FactoryBot.create(:user, :approver) }
  before { sign_in user }
  
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(200)
    end
  end

end
