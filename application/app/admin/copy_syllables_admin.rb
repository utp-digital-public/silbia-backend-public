Trestle.admin(:copy_syllables) do
  menu do
    item :copy_syllables, icon: "fa fa-tachometer"
  end

  controller do
    def index
      @nro_syllabus_to_copy = 0
      @inactive_cycles = Cycle.inactive
      @active_cycle = Cycle.active
      unless @active_cycle.present?
        flash[:error_hash] = 
          { 
            title: "Advertencia!",
            message:"Actualmente no encontramos ningún ciclo académico habilitado. Te recomendamos revisar la sección de “Ciclos académicos”"
          }
      else
        @nro_syllabus_to_copy = SyllabusProgram.number_of_syllabuses(@active_cycle.id)
        flash[:error_hash] = {
          title: "Aviso!",
          message:"El ciclo #{@active_cycle.name.upcase} ya cuenta con sílabos creados y activos,
           por lo que no es posible realizar una copia.
          Para realizar una copia deberá habilitarse un ciclo académico nuevo que no contenga ningún sílabo copiado"
        }  if @nro_syllabus_to_copy > 0            
      end
    end
    
    def change_from_copy
      cycle_id_from = params[:cycle_id_from]
      cycle_id_to = params[:cycle_id_to]
      @nro_syllabus = SyllabusProgram.number_of_syllabuses(cycle_id_from)
      respond_to do |format|
        format.js 
      end
    end  

    def execute_copy
      script_execute = "$('.spinner').hide();"
      id_cycle_from_copy = params[:copy_syllabus][:cycle_from_copy]
      id_cycle_to_copy = params[:copy_syllabus_cycle_to_copy]
      copy_syllabus_service =  Admin::CopySyllabusService.new(
          id_cycle_from_copy: id_cycle_from_copy,
          id_cycle_to_copy: id_cycle_to_copy 
          )  
      unless copy_syllabus_service.valid_cycle_have_not_syllabus_assigned?
        return respond_to do |format|
          format.js { 
            render :js=>"toastr.error('#{copy_syllabus_service.msg_error}','Error');#{script_execute}"
          }
        end
      end  
    
      copy_syllabus_service.copy_syllabus_masive
      msj = copy_syllabus_service.msg_error
      if msj.present?
        return respond_to do |format|
          format.js { render :js=> "toastr.error('#{msj}', 'Error');#{script_execute}" }
        end
      end
    end

  end

  routes do
    post :execute_copy
    get :change_from_copy
  end
end