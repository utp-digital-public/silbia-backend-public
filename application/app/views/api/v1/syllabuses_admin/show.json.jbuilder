json.status "success"
json.data do
    json.partial! "api/v1/syllabuses_admin/syllabus_program", syllabus_program: @syllabus_program
end