class CreateUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :units do |t|
      t.references :syllabus, foreign_key: true
      t.integer :unit_number, null: false, default: 1
      t.text :achievement, null: false
      t.timestamps
    end
  end
end
