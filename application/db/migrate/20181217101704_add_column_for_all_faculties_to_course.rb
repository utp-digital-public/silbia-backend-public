class AddColumnForAllFacultiesToCourse < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :for_all_faculties, :boolean, default: false
  end
end
