
module SpecTestHelper
  def login_admin
    administrator = FactoryBot.create(:administrator)
    #session[:user_id] = administrator.id
    post "/login", params: { email: administrator.email, password: administrator.password }
  end

  def login_approver
    approver = FactoryBot.create(:user, :approver, email: "approver@email.com")
    #session[:user_id] = approver.id
    post "/users/sign_in", params: {user:{ email: approver.email, password: approver.password } }
    return approver
  end
  
  def login_creator
    creator = FactoryBot.create(:user, :creator, email: "creator@email.com")
    #session[:user_id] = creator.id
    post "/users/sign_in", params: { user: { email: creator.email, password: creator.password } }
    return creator
  end  
end
  