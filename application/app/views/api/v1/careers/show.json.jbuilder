json.status "success"
json.data do
  json.name @career.name
  json.code @career.code
  json.modality @career.modality
  json.specific_competitions @specific_competitions
  json.general_competitions @general_competitions
  json.total_credits @career.program_courses.active.reduce(0) { |sum, num| sum + num.credits }
  json.total_courses @career.program_courses.active.count
end
