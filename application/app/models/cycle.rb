class Cycle < ApplicationRecord
  has_many :syllabuses, :dependent => :destroy
  has_many :modality_cycles, :dependent => :destroy
  has_many :modalities, :through => :modality_cycles
  validates :name, :cycle_start, :cycle_end, :type_cycle, presence: { message: "es requerido" }
  validates :enabled, inclusion: { in: [true, false] }
  validates :name, uniqueness: { case_sensitive: false, message: "Ciclo con el mismo nombre ya existe" }
  accepts_nested_attributes_for :modality_cycles

  scope :active, -> { where(:enabled => true).empty? ? last : where(:enabled => true).first }

  scope :inactive, -> { where(:enabled => false) }

  validate :end_date_after_start_date
  extend Enumerize
  enumerize :type_cycle, in: { :regular => REGULAR_CYCLE, :verano => SUMMER_CYCLE }

  after_save :update_enable_cycles

  def can_delete
    self.syllabuses.count == 0 && !self.enabled ? true : false
  end

  def human_type_cycle
    case type_cycle
    when REGULAR_CYCLE
      "Ciclo Regular"
    when SUMMER_CYCLE
      "Ciclo de Verano"
    else nil
    end
  end

  def update_enable_cycles
    Cycle.where(enabled: true).where.not(id: self.id).update_all(enabled: false) if self.enabled
  end

  def can_disable
    self.enabled ? false : true
  end

  def total_syllabus
    SyllabusProgram.number_of_syllabuses(self.id)
  end

  private

  def end_date_after_start_date
    return if cycle_end.blank? || cycle_start.blank?

    if cycle_end < cycle_start
      errors.add(:cycle_end, "debe ser posterior a la fecha de inicio")
    end
  end

  def enabled_cycle_with_end_date
    return if cycle_end.blank? || cycle_start.blank?
    if ((cycle_end.to_date < Time.now.to_date) && (enabled.present?))
      errors.add(:cycle_end, "debe ser mayor o igual al dia de hoy, si va habilitar el ciclo")
    end
  end
end
