class SyllabusEdition < ApplicationRecord
  belongs_to :syllabus_program
  validates :syllabus_program, :section_id, presence: { message: "es requerido" }

  def type
    ([6, 7, 9].include? section_id) ? "unique" : "shared"
  end
end
