module SyllabusChange
  class AuditSyllableObject
    attr_accessor :syllabus_program

    def initialize(**attributes)
      attributes.each do |k,v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end  
    end

    def get_audits_sylabus_with_date_greather_than(last_date_in_date_time)
      audit_objects = Audited::Audit.all
        .where(auditable_type: "Syllabus")
        .where(auditable_id: @syllabus_program.syllabus.id)
        .order("created_at DESC")
      if (last_date_in_date_time.present?)
        date_in_utc_final = date_time_subtract_second(last_date_in_date_time)
        audit_objects = audit_objects.where("created_at >= ?", date_in_utc_final)  
      end  
      audit_objects
    end

    def get_audits_sylabus_with_date_previews_and_last(last_date_in_date_time, preview_date_in_date_time)
      audit_objects = Audited::Audit.all
        .where(auditable_type: "Syllabus")
        .where(auditable_id: @syllabus_program.syllabus.id)
        .order("created_at DESC")
      if (last_date_in_date_time.present?)
        audit_objects = audit_objects.where("created_at > ?", preview_date_in_date_time)  
                                     .where("created_at <= ?", last_date_in_date_time)  
      end  
      audit_objects
    end

    def get_audits_sylabus_with_date_are_equals(last_date_in_date_time)
      audit_objects = Audited::Audit.all
        .where(auditable_type: "Syllabus")
        .where(auditable_id: @syllabus_program.syllabus.id)
        .order("created_at DESC")
      if (last_date_in_date_time.present?)
        date_in_utc_final = date_time_subtract_second(last_date_in_date_time)
        audit_objects = audit_objects.where("created_at = ?", date_in_utc_final)  
      end  
      audit_objects
    end

    def date_time_subtract_second(date_time_value)
      (date_time_value- 40.second)
    end  
    
  end  
end  