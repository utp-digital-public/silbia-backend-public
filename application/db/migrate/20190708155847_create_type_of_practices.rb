class CreateTypeOfPractices < ActiveRecord::Migration[5.2]
  def change
    create_table :type_of_practices do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
