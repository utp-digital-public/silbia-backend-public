class CreateCurriculumCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :curriculum_courses do |t|
      t.integer :year, null:false
      t.string :modality_program, null: false
      t.string :desc_grade , null: false
      t.string :faculty_code , null: false
      t.string :faculty_name, null: false
      t.string :program_academic, null: false
      t.string :academic_plan_code, null: false
      t.string :career_code, null: false
      t.string :career_name, null: false
      t.string :cycle, null: false
      t.string :course_catalog_code, null: false
      t.string :course_short_name
      t.string :course_name, null: false
      t.string :course_catalog_cgt_code
      t.boolean :with_equivalence, default: false
      t.integer :credits, null: false
      t.integer :weekly_hours, null: false
      t.string :face_to_face_course_mode
      t.string :virtual_course_mode

      t.timestamps
    end
  end
end
