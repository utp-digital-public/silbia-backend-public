json.id course.id
json.information_sources_ordered_asc course.information_sources_ordered_asc
json.for_all_career course.for_all_career
json.faculties course.faculties

json.basic_books do
  json.array! course.information_sources.basic, partial: "api/v1/syllabuses/information_source", as: :information_source
end
json.complementary_books do
  json.array! course.information_sources.complementary, partial: "api/v1/syllabuses/information_source", as: :information_source
end
