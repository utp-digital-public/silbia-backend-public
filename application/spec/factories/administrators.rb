FactoryBot.define do
  factory :administrator do 
    email { "admin@silbia.com" }
    password { "12345678" }
    first_name { "Administrador" }
    last_name { "de Silbia" }
    remember_token { "MyString" }
    remember_token_expires_at { "2019-03-07 10:48:03" }
  end
end
