json.status "success"
json.data do
	json.syllabuses do
        json.array! @syllabus_programs, partial: 'api/v1/syllabuses/syllabus_for_list', as: :syllabus
  	end
end