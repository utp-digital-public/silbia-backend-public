json.status "success"
json.data do
  json.syllabus_program do
    json.partial! "api/v1/syllabus_programs/syllabus_program", syllabus_program: @syllabus_program
  end
  json.syllabus do
    json.partial! "api/v1/syllabus_programs/syllabus", syllabus: @syllabus_program.syllabus
  end
  json.competitions do
    json.partial! "api/v1/syllabus_programs/competitions", program_course: @syllabus_program.get_program_course
  end
  json.syllabus_observations @observation_syllabus || []
  json.syllabus_editions @edition_syllabus || []
  json.syllabus_previews_observations @observation_preview_syllabus || []
  json.can_edit @can_edit
  json.can_approve @can_approve
  json.base_syllabus_approved @syllabus_program.syllabus.modalities.first[:state] == APPROVED_STATE_STRING
  json.flags @syllabus_program.flags
end
