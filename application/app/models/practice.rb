class Practice < ApplicationRecord
  validates :description, :code, presence: { message: "es requerido" }
  validates :code, uniqueness: { case_sensitive: false, message: "El código ya está siendo usado." }
  belongs_to :type_of_practice, optional: true
end
