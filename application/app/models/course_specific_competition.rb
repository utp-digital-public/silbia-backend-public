class CourseSpecificCompetition < ApplicationRecord
  belongs_to :program_course_career
  belongs_to :specific_competition
  has_one :program_course, :through => :program_course_career
  validates :specific_competition, :program_course_career, presence: { message: "es requerido" }
  validates :specific_competition, uniqueness: { message: "ya existe", scope: :program_course_career_id }
end
