module DownloadSyllabus
  extend ActiveSupport::Concern
  included do 
    #include Util::Util
    before_action :load_data_basic_report, only: [:download_total_syllabus, :download_syllabus_evaluation_system]
  end

  def download_total_syllabus
    @syllabus_programs = SyllabusProgram.whit_cycle_active.match_course_coordinator.actives
    name_file ="Syllabus_total"
    setHeaderExportExcel(name_file) 
    respond_to do |format|
      format.excel { render template: '/syllabuses/excel/total_syllabus', layout: 'report/excel/template'}
    end
  end

  def  download_syllabus_evaluation_system
    @syllabus_programs = SyllabusProgram.whit_cycle_active.match_course_coordinator.actives.order(:id)
   
    name_file = "Syllabus_evalution_system"
    setHeaderExportExcel(name_file) 
    respond_to do |format|
      format.excel { render template: '/syllabuses/excel/syllabus_evaluation_system', layout: 'report/excel/template', status: :ok}
    end
  end
  
  def load_data_basic_report
    @practices = Practice.all
    @laboratories = Laboratory.all
  end  

end 