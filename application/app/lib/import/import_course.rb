module Import
  class ImportCourse < Import::Entity
    def import_course_if_not_exist
      import_obj = Import::ConvertToJson.new
      total_records_create = 0
      @csv.each_with_index do |row, index|
        code_pregrade = row['catalog_code']
        code_cgt = row['catalog_code_cgt']
        course_pregrade = import_obj.search_course_by_catalog_code(  code_pregrade )
        unless course_pregrade.present?
          course = build_course(row)
          course.save!
          total_records_create = total_records_create+1
          puts "#{course.name}, #{course.catalog_code} saved"
        else
          puts "YA EXISTE Y ES "+   row['catalog_code']
        end
      end
      puts "New records was  #{total_records_create} created"
      puts "There are Total  #{Course.count} rows in the transactions table"
    end

    def build_course(row)
      course = Course.new
      course.catalog_code = row['catalog_code']
      course.name = row['name']
      course.catalog_code_cgt = row['catalog_code_cgt']
      course.for_all_career = if row['for_all_career'].present? then true else false end
      course.for_all_faculties = if row['for_all_faculties'].present? then true else false end 
      course 
    end  
    
  end  
end  