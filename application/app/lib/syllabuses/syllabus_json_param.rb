module Syllabuses
  class SyllabusJsonParam
    attr_accessor :json_syllabus, :program_json, :information_sources_json, :evaluation_system_json, :evaluation_rules_json
    def initialize(**attributes)
      attributes.each do |k,v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end
  end  
end