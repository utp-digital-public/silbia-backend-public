require 'csv'
### Migrate BOOKS from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'books.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  book = Book.new
  book.code = row['code']
  book.title = if row['title'].nil?  then "No aplica" else row['title'] end
  book.author = row['autor']
  book.support = row['support']
  book.year = if row['year'].present? then row['year'].to_i else 0 end
  book.number_editorial = row['number_editorial']
  book.editorial = row['editorial']
  is_acquired = row['is_acquired']
  if is_acquired.present?
    book.is_acquired = if is_acquired == "SI" then true else false end
  else
    book.is_acquired = nil    
  end  
  book.save!
  puts "#{book.code}, #{book.title} saved"
end
puts "There are now #{Book.count} rows in the transactions table"
