class AlterTablesSessions < ActiveRecord::Migration[5.2]
  def change
    drop_table :session_topics
    drop_table :sessions

    create_table :sessions do |t|
      t.references :syllabus_program, foreign_key: true
      t.references :laboratory, foreign_key: true
      t.references :unit, foreign_key: true
      t.integer :week_number, null: false, default: 1
      t.integer :session_number, null: false, default: 1
      t.integer :modality_id, null: false, default: 1
      t.timestamps
    end
    create_table :session_topics do |t|
      t.references :session, foreign_key: true
      t.references :topic, foreign_key: true
      t.jsonb :activities, null: false, default: "{}"
      t.timestamps
    end
  end
end
