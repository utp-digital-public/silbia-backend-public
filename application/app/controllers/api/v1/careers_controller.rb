class Api::V1::CareersController < Api::ApiV1SharedController
  before_action :set_career, only: [:import_competences, :courses, :remove_competences, :show, :update, :update_courses]

  def index
    @careers = Career.all
    @careers = @careers.where(manager_id: params[:manager_id].to_i) unless params[:manager_id].blank?
    @careers = @careers.where("unaccent(name) ILIKE unaccent(?)", "%#{params[:query]}%") unless params[:query].blank?
    @careers = @careers.where("id IN (?)", ProgramCourseCareer.pluck(:career_id))
    @careers = @careers.order(:name)
    @careers = @careers.page(params[:page])
  end

  def show
    @general_competitions = GeneralCompetition.all.order(:name)
    @specific_competitions = @career.specific_competitions.order(:name)
  end

  def courses
    filter_params = params.slice(:type, :specific_competitions, :general_competitions)

    if filter_params.blank?
      @program_courses = @career.program_course_careers.includes(:program_course).where(:program_courses => { :deleted => false }).order(:cycle_number, "program_courses.name")
      return
    end
    type = params[:type]
    specific_competitions = []
    general_competitions = []
    ids = []
    case type
    when "specific"
      specific_competitions = @career.specific_competitions.pluck("id")
    when "general"
      general_competitions = GeneralCompetition.all.pluck("id")
    end
    specific_competitions = params[:specific_competitions] if params[:specific_competitions].present?
    general_competitions = params[:general_competitions] if params[:general_competitions].present?
    ids = ids | @career.course_general_competitions.where(general_competition_id: general_competitions).pluck("program_courses.id") unless general_competitions.blank?
    ids = ids | @career.course_specific_competitions.where(specific_competition_id: specific_competitions).includes(:program_course).pluck("program_courses.id") unless specific_competitions.blank?
    @program_courses = @career.program_course_careers.includes(:program_course).where(:program_courses => { :deleted => false }).where(program_course_id: ids).order(:cycle_number, "program_courses.name")
  end

  def update
    authorize :user, :is_admin?
    @career.assign_attributes(career_params)
    if (@career.invalid?)
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @career.errors) and return
    end
    ApplicationRecord.transaction do
      begin
        @career.save!()
        render_ok(@career)
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  def import_competences
    authorize :user, :is_admin?
    @msg_error = nil
    specific_competitions = []
    program_course_careers = @career.program_course_careers.joins(:program_course).where(:program_courses => { :deleted => false }).select(:id, :catalog_code)
    all_courses = Hash[program_course_careers.map { |x| [x[:catalog_code], x[:id]] }]
    all_competitions = Hash[@career.specific_competitions.map { |x| [x[:code], x[:id]] }]

    import_array = import_params.map { |i| CourseSpecificCompetition.new(:level => i[:level], :specific_competition_id => all_competitions[i[:competition_code]], :program_course_career_id => all_courses[i[:catalog_code]]) }
    begin
      specific_competitions = CourseSpecificCompetition.import import_array, returning: [:id], validate_uniqueness: true, track_validation_failures: true
      render_ok(specific_competitions)
    rescue => e
      render_internal_server_error(e.message)
    end
  end

  def remove_competences
    authorize :user, :is_admin?
    @msg_error = nil
    specific_competitions = []
    program_course_careers = @career.program_course_careers.joins(:program_course).where(:program_courses => { :deleted => false }).select(:id, :catalog_code)
    all_courses = Hash[program_course_careers.map { |x| [x[:catalog_code], x[:id]] }]
    all_competitions = Hash[@career.specific_competitions.map { |x| [x[:code], x[:id]] }]

    #import_array = import_params.map { |i| CourseSpecificCompetition.new(:level => i[:level], :specific_competition_id => all_competitions[i[:competition_code]], :program_course_career_id => all_courses[i[:catalog_code]]) }
    begin
      import_params.each do |i|
        CourseSpecificCompetition.where(:program_course_career_id => all_courses[i[:catalog_code]], :specific_competition_id => all_competitions[i[:competition_code]]).destroy_all
      end
      render_ok(specific_competitions)
    rescue => e
      render_internal_server_error(e.message)
    end
  end

  def update_courses
    authorize :user, :is_admin?
    all_courses = Hash[ProgramCourse.active.all.map { |x| [x[:catalog_code], x[:id]] }]
    courses_to_add = params[:courses_to_add] || []
    @career.program_course_careers.build(courses_to_add.map { |i| { program_course_id: all_courses[i[:catalog_code]], cycle_number: i[:cycle_number] || 0 } })
    @career.save
    courses_to_delete = params[:courses_to_delete] || []
    @career.program_course_careers.where(program_course_id: courses_to_delete.map { |i| all_courses[i] }).destroy_all
    courses_to_update = params[:courses_to_update] || []
    courses_to_update.each do |i|
      if all_courses[i[:catalog_code]]
        ProgramCourseCareer.find(all_courses[i[:catalog_code]]).update_attributes(i.except(:catalog_code))
      end
    end
    render_ok(@career.program_courses)
  end

  private

  def import_params
    params.permit(data: [
                    :competition_code,
                    :catalog_code,
                    :level,
                  ])
      .require(:data)
  end

  def career_params
    params.permit(:manager_id)
  end

  def set_career
    @career = Career.find_by code: params[:id]
    if @career.nil?
      render json: {}, status: :not_found and return
    end
  end
end
