module Pdf
  module SyllabusPdfHelper
    def print_link_unit_source_information_by_unit(information_sources, unit)
      list_result = ""
      list_result = "<ul>"
      information_sources.try(:each) do |information_source|
        link_unit_line = ""
        if Pdf::Syllabus::InformationSourceService.contains_units_actives?(information_source, unit)
          list_result += "<li>"
          line_info =
            Pdf::Syllabus::InformationSourceService.get_formated_line_source_information(information_source)
          list_result += line_info
          link_unit_line = Pdf::Syllabus::LinkUnitService.get_line_link_unit_by(information_source, unit)
          list_result += "<br>  #{link_unit_line}"
          list_result += "</li>"
        end
      end
      list_result += "</ul>"
      list_result.html_safe
    end

    def show_link_units?(information_sources, unit)
      information_sources.try(:each) do |information_source|
        if Pdf::Syllabus::InformationSourceService.contains_units_actives?(information_source, unit)
          return true
        end
      end
      false
    end

    def show_type_session(syllabus_program, session)
      key_type_session = session["type_laboratory"]
      key_virtual_type = SESSION_TYPE_HASH.index("Virtual")
      value_string_hash = SESSION_TYPE_HASH["#{key_type_session}"]
      if ((syllabus_program["type_program"] == TYPE_PROGRAM_CGT) &&
          (key_type_session.to_i == key_virtual_type.to_i))
        value_to_return = "Sesión #{value_string_hash} "
      end
      value_to_return ||= ""
    end

    def show_type_session_description(syllabus_program, session)
      key_type_session = session.modality_id
      key_virtual_type = SESSION_TYPE_HASH.index("Virtual")
      value_string_hash = SESSION_TYPE_HASH["#{key_type_session}"]
      if ((syllabus_program.modality_id != PRESENCIAL_CODE) &&
          (key_type_session.to_i == key_virtual_type.to_i))
        value_to_return = "Sesión #{value_string_hash} "
      end
      value_to_return ||= ""
    end

    def print_unit_information_source_by_unit(unit)
      list_result = ""
      list_result = "<ul>"
      unit.unit_information_sources.try(:each) do |unit_information_source|
        link_unit_line = ""
        list_result += "<li>"
        line_info =
          Pdf::Syllabus::InformationSourceService.get_formated_line_source_information(unit_information_source.syllabus_information_source)
        list_result += line_info
        if unit_information_source.from_page.present?
          link_unit_line = "<span > Desde la pag.#{unit_information_source.from_page} hasta la pag #{unit_information_source.to_page} </span>"
          list_result += "<br>  #{link_unit_line}"
        end
        list_result += "</li>"
      end
      list_result += "</ul>"
      list_result.html_safe
    end
  end
end
