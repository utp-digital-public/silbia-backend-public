json.id modality[:syllabus_program].id
json.modality modality[:syllabus_program].modality
json.order modality[:order]
json.state modality[:syllabus_program].state
if modality[:syllabus_program].observed?
  json.observations modality[:observations]
else
  json.set! :observations, {}
end

if policy(modality[:syllabus_program]).observe?
  json.prev_observations modality[:prev_observations]
else
  json.set! :prev_observations, {}
end

if policy(modality[:syllabus_program]).show_editions?
  json.editions modality[:editions]
else
  json.set! :editions, {}
end
json.is_owner policy(modality[:syllabus_program]).owner?
