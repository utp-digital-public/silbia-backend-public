json.status "success"
json.data do
    json.array! @user_admins, partial: 'api/v1/user_admins/user_admin', as: :user_admin
end
json.partial! "api/v1/shared/pagination", collection: @user_admins