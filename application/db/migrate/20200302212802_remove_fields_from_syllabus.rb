class RemoveFieldsFromSyllabus < ActiveRecord::Migration[5.2]
  def change
  	remove_column :syllabuses, :modality, :string
  	remove_column :syllabuses, :user_id, :bigint
  end
end
