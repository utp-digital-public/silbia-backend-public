json.status "success"
json.data do
	json.states_syllabus do
        json.array! @arr_states
    end
    json.states_grouped do
        json.array! @states_grouped
    end
    json.quantity_list_syllabus @quantity_list_syllabus
    json.syllabus_programs do
        json.array! @syllabus_programs, partial: 'api/v1/syllabuses/syllabus_for_list', as: :syllabus
    end
end
json.partial! "api/v1/shared/pagination", collection: @syllabus_programs