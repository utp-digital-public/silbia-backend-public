module SyllabusChange
  module Changes
    class Ruler < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::DetectChangeSyllabusContent
      include SyllabusChange::Changes::UtilDetectCronogram
      attr_accessor :syllabus, :auditable_objects

      def initialize(**attribute)
        super(**attribute)
        attribute.each do |k, v|
          instance_variable_set("@#{k}", v) unless v.nil?
        end
        @changes_detected = 0
        @is_edited = false
      end

      def call
        key_to_extract = "rules"

        current_rules = @syllabus_program.evaluation_rules_json["rules"]

        if @has_more_than_one_state.present?
          @is_edited = detec_change_syll_cont?(current_rules)
        else
          rules_before = obtain_rules_when_not_have_more_one_state()

          @is_edited = true if current_rules != rules_before
        end
      end

      def obtain_rules_when_not_have_more_one_state()
        content_first = set_hash_values_when_have_less_than_one_state("evaluation_rules_json", @objects_audit)
        if content_first.present?
          return content_first["rules"]
        end
        []
      end

      def detec_change_syll_cont?(rules)
        date_track = SyllabusChange::DateTrackingSyllabus.new(
          syllabus_program: @syllabus_program,
          has_more_than_one_state: @has_more_than_one_state,
        )
        date_to_filter = @last_date
        puts "RCONR"
        with_changes = false
        puts "PASO AUDITABLES"
        filter_objects_audit = @objects_audit.where("created_at>=?", date_to_filter)
        last_units_audit = get_last_rules_from_audit(filter_objects_audit)

        return false unless last_units_audit.present?
        if last_units_audit.size != rules.size
          return true
        end

        rules.try(:each_with_index) do |ruler, index|
          iterate_fields_and_count_changes(
            array_fields: ["name"],
            object: ruler,
            last_change_object: last_units_audit,
            index: index,
          )
        end
        with_changes = true if @changes_detected > 0
        with_changes
      end

      def get_last_rules_from_audit(objects_audit)
        last_rules = []

        objects_audit.try(:each) do |objects_audi|
          objects_audi.audited_changes.try(:each) do |key, value|
            evaluation_rules_json_a = get_value_before_the_last_change(value) if key == "evaluation_rules_json"

            if evaluation_rules_json_a.present?
              last_rules = evaluation_rules_json_a["rules"]

              return last_rules
            end
          end
        end
        []
      end
    end
  end
end
