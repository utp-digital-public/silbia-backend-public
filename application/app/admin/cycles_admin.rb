Trestle.resource(:cycles) do
  menu do
    item :cycles, icon: "fa fa-star", label: "Ciclos"
  end

  # Customize the table columns shown on the index view.
  #
  table do
    column :name, header: "Nombre"
    column :cycle_start, ->(cycle) {cycle.cycle_start.strftime("%d-%m-%Y")}, header: "Inicio "
    column :cycle_end,->(cycle) {cycle.cycle_end.strftime("%d-%m-%Y")}, header: "Fin "
    column :enabled, align: :center, header: "¿Habilitado?"
    actions
  end

  # Customize the form fields shown on the new/edit views.
  #
  form do |cycle|
    text_field :name, label: "Nombre"
    date_field :cycle_start, label: "Inicio"
    date_field :cycle_end, label: "Fin"
    check_box :enabled, label: "¿Habilitado?"
  end

  controller do

    def update
      cycle = instance
      a = Cycle.new
      a= cycle
      if a.valid?
        puts "ES VALIDO"
      else
        puts "Es invalido"  
      end  
      enable_cycle = params[:cycle][:enabled]
      Cycle.update_all(enabled: false) if enable_cycle == "1"    
      super
    end
    
    def create
      enable_cycle = params[:cycle][:enabled]
      Cycle.update_all(enabled: false) if enable_cycle == "1"    
      super
    end  

    def destroy
      instance = admin.find_instance(params)
      id_cycle = instance.id
     # save the parent so we can redirect there instead of the index for this resource
     
     # ActiveRecord::InvalidForeignKey

     begin
      cycle = instance
      success = admin.delete_instance(cycle)
      respond_to do |format|
        format.html do
          if success
            flash[:message] = flash_message("success.destroy", message: "The %{model_name} was successfully deleted.", title: "Success!")
            # here's the custom redirect
            redirect_to admin.path
          else
            flash[:error] = flash_message("failure.destroy", message: "Could not delete %{model_name}.", title: "Error")
            redirect_to admin.path(:edit, id: id_cycle)
          end
        end
        format.json { head :no_content }
        format.js
      end
     rescue ActiveRecord::InvalidForeignKey 
      flash[:error] = flash_message("failure.destroy", message: "El ciclo #{cycle.name}  no puede ser eliminado, ya que esta siendo usado en algunos silabos. Comuniquese con el area de sistemas.", title: "Advertencia!")
      redirect_to admin.path
     end

    end
  end

  # By default, all parameters passed to the update and create actions will be
  # permitted. If you do not have full trust in your users, you should explicitly
  # define the list of permitted parameters.
  #
  # For further information, see the Rails documentation on Strong Parameters:
  #   http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters
  #
  # params do |params|
  #   params.require(:cycle).permit(:name, ...)
  # end
end
