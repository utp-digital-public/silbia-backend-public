class CourseGeneralCompetition < ApplicationRecord
  belongs_to :program_course
  belongs_to :general_competition
  validates :general_competition, uniqueness: { message: "ya existe", scope: :program_course_id }
end
