class GeneralCompetition < ApplicationRecord
  has_many :course_general_competitions, :dependent => :destroy, :inverse_of => :general_competition
end
