class CapitalizeModalities < ActiveRecord::Migration[5.2]
  def change
    Modality.all.each do |m|
      m.name = m.name.capitalize()
      m.save
    end
  end
end
