module SyllabusChange
  module Changes
    class EvaluationSystem < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::UtilDetectCronogram
      include SyllabusChange::Changes::DetectChangeSyllabusContent

      def initialize(**attribute)
        super(**attribute)
        attribute.each do |k, v|
          instance_variable_set("@#{k}", v) unless v.nil?
        end
        @title = "Sistema de evaluación"
        @id_section = "section-evaluation-system"
        @changes_detected = 0
        @is_edited = false
      end

      def call
        filter_audits()
        #iterate_audit_and_set_hash_values_syllabus()
        array_formulas = @syllabus_program.evaluation_system_json["evaluation_system"]
        @is_edited = true if detect_change_in_formulas?(array_formulas)
      end

      def filter_audits
        audits_evaluation_system = @objects_audit.where("audited_changes?'evaluation_system_json'")
        set_hash_value("evaluation_system_json", audits_evaluation_system.last.audited_changes["evaluation_system_json"]) unless audits_evaluation_system.empty?
      end

      def detect_change_in_formulas?(array_formulas)
        return false unless @hash_values.has_key?(:formulas)
        last_change_formulas = []
        with_changes = false
        if @has_more_than_one_state.present?
          last_change_formulas = @hash_values[:formulas]["evaluation_system"] unless @hash_values[:formulas].blank?
        else
          last_change_formulas ||= []
          hash_formula = set_hash_values_when_have_less_than_one_state("evaluation_system_json", @objects_audit)
          last_change_formulas = hash_formula["evaluation_system"] if hash_formula.present?
          return true if last_change_formulas != array_formulas
        end

        return true if size_of_array_are_diferent?(array_formulas, last_change_formulas)
        detect_changes_in_formulas(array_formulas, last_change_formulas)
        array_formulas.try(:each_with_index) do |formula, index|
          last_change_formulas[index]["type"] = last_change_formulas[index]["type"].upcase unless last_change_formulas[index].nil?
          iterate_fields_and_count_changes(
            array_fields: ["type", "weight", "description", "recoverable", "observations"],
            object: formula,
            last_change_object: last_change_formulas,
            index: index,
          )
        end
        with_changes = true if @changes_detected > 0
        with_changes
      end

      def detect_changes_in_rules?(array_rules)
        return false unless @hash_values[:rules].present?
      end

      def detect_changes_in_formulas(formulas, last_change_formula)
        last_change_formula ||= []
        size_of_array_are_diferent?(formulas, last_change_formula)
      end

      def detect_changes_in_indication_formulas(indication_formulas, last_change_indication_formula)
        last_change_indication_formula ||= []
        size_of_array_are_diferent?(indication_formulas, last_change_indication_formula)
      end

      def set_hash_value(key, value)
        @hash_values[:formulas] = get_value_before_the_last_change(value) if key == "evaluation_system_json"
      end
    end
  end
end
