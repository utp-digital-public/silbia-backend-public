class AddColumnDeleteLogicToLinkUnitsInformationSources < ActiveRecord::Migration[5.2]
  def change
    add_column :link_unit_information_sources, :delete_logic, :boolean, default: false
  end
end
