# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_30_171750) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "administrators", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_name"
    t.string "remember_token"
    t.datetime "remember_token_expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.jsonb "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "books", force: :cascade do |t|
    t.string "code"
    t.string "title", null: false
    t.string "author", default: "Sin autor"
    t.string "support"
    t.integer "year", default: 0
    t.string "number_editorial"
    t.string "editorial"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_acquired"
  end

  create_table "careers", force: :cascade do |t|
    t.string "code", null: false
    t.integer "modality", default: 0, null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "competitions", force: :cascade do |t|
    t.string "career", default: ""
    t.string "competition_description", default: ""
    t.string "criterion", default: ""
    t.string "achievement_level", default: ""
    t.bigint "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_competitions_on_course_id"
  end

  create_table "components", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "consume_data_servicio", default: false
    t.string "foto"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "parent_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "course_id_requeriment"
    t.boolean "for_all_career", default: false
    t.boolean "for_all_faculties", default: false
    t.index ["course_id_requeriment"], name: "index_courses_on_course_id_requeriment"
  end

  create_table "courses_faculties", id: false, force: :cascade do |t|
    t.bigint "course_id", null: false
    t.bigint "faculty_id", null: false
    t.index ["course_id", "faculty_id"], name: "index_courses_faculties_on_course_id_and_faculty_id"
    t.index ["faculty_id", "course_id"], name: "index_courses_faculties_on_faculty_id_and_course_id"
  end

  create_table "curriculum_courses", force: :cascade do |t|
    t.integer "year", null: false
    t.string "modality_program", null: false
    t.string "desc_grade", null: false
    t.string "faculty_code", null: false
    t.string "faculty_name", null: false
    t.string "program_academic", null: false
    t.string "academic_plan_code", null: false
    t.string "career_code", null: false
    t.string "career_name", null: false
    t.string "cycle", null: false
    t.string "course_catalog_code", null: false
    t.string "course_short_name"
    t.string "course_name", null: false
    t.string "course_catalog_cgt_code"
    t.boolean "with_equivalence", default: false
    t.integer "credits", null: false
    t.integer "weekly_hours", null: false
    t.string "face_to_face_course_mode"
    t.string "virtual_course_mode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_catalog_cgt_code"], name: "index_curriculum_courses_on_course_catalog_cgt_code"
    t.index ["course_catalog_code"], name: "index_curriculum_courses_on_course_catalog_code"
    t.index ["modality_program"], name: "index_curriculum_courses_on_modality_program"
  end

  create_table "cycles", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "enabled", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "cycle_start"
    t.datetime "cycle_end"
    t.string "type_cycle", default: "1", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "doc_templates", force: :cascade do |t|
    t.string "name", null: false
    t.integer "type_of_template", default: 1, null: false
    t.boolean "is_active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faculties", force: :cascade do |t|
    t.string "code", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "information_sources", force: :cascade do |t|
    t.bigint "course_id"
    t.bigint "book_id"
    t.text "observation"
    t.string "type_source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["book_id"], name: "index_information_sources_on_book_id"
    t.index ["course_id"], name: "index_information_sources_on_course_id"
  end

  create_table "job_statuses", force: :cascade do |t|
    t.integer "progress", default: 0
    t.integer "max_progress"
    t.boolean "completed", default: false, null: false
    t.boolean "error", default: false, null: false
    t.text "error_message"
    t.text "message", default: ""
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "generated_zip_path", default: ""
    t.jsonb "extra_info"
    t.string "job_name", default: ""
  end

  create_table "laboratories", force: :cascade do |t|
    t.string "name", null: false
    t.integer "type_laboratory", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "link_unit_information_sources", force: :cascade do |t|
    t.integer "number_unit"
    t.integer "from_to_page"
    t.integer "page_unit"
    t.boolean "is_checked", default: false
    t.bigint "information_source_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "delete_logic", default: false
    t.index ["information_source_id"], name: "index_link_unit_information_sources_on_information_source_id"
  end

  create_table "login_activities", force: :cascade do |t|
    t.string "scope"
    t.string "strategy"
    t.string "identity"
    t.boolean "success"
    t.string "failure_reason"
    t.string "user_type"
    t.bigint "user_id"
    t.string "context"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "city"
    t.string "region"
    t.string "country"
    t.datetime "created_at"
    t.index ["identity"], name: "index_login_activities_on_identity"
    t.index ["ip"], name: "index_login_activities_on_ip"
    t.index ["user_type", "user_id"], name: "index_login_activities_on_user_type_and_user_id"
  end

  create_table "modalities", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "modality_cycles", force: :cascade do |t|
    t.bigint "cycle_id"
    t.bigint "modality_id"
    t.text "footnote"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cycle_id"], name: "index_modality_cycles_on_cycle_id"
    t.index ["modality_id"], name: "index_modality_cycles_on_modality_id"
  end

  create_table "practices", force: :cascade do |t|
    t.string "description", default: "", null: false
    t.string "code", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "type_of_practice_id"
    t.index ["type_of_practice_id"], name: "index_practices_on_type_of_practice_id"
  end

  create_table "program_course_careers", force: :cascade do |t|
    t.bigint "program_course_id"
    t.bigint "career_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["career_id"], name: "index_program_course_careers_on_career_id"
    t.index ["program_course_id"], name: "index_program_course_careers_on_program_course_id"
  end

  create_table "program_courses", force: :cascade do |t|
    t.integer "weekly_hours", default: 0
    t.integer "credits", default: 0
    t.string "catalog_code", null: false
    t.string "name", null: false
    t.string "type", null: false
    t.bigint "course_id"
    t.bigint "coordinator_id"
    t.bigint "supervisor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "authorized", default: true, null: false
    t.bigint "modality_id"
    t.boolean "deleted", default: false
    t.index ["catalog_code"], name: "index_program_courses_on_catalog_code"
    t.index ["coordinator_id"], name: "index_program_courses_on_coordinator_id"
    t.index ["course_id", "modality_id"], name: "index_program_courses_on_course_id_and_modality_id", unique: true
    t.index ["course_id"], name: "index_program_courses_on_course_id"
    t.index ["modality_id"], name: "index_program_courses_on_modality_id"
    t.index ["supervisor_id"], name: "index_program_courses_on_supervisor_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "sections_templates", force: :cascade do |t|
    t.bigint "doc_template_id"
    t.bigint "component_id"
    t.string "title", default: ""
    t.integer "sort", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["component_id"], name: "index_sections_templates_on_component_id"
    t.index ["doc_template_id"], name: "index_sections_templates_on_doc_template_id"
  end

  create_table "syllabus_messages", force: :cascade do |t|
    t.bigint "syllabus_program_id"
    t.text "message", null: false
    t.integer "state", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "section", default: 0, null: false
    t.integer "revision_number", default: 0, null: false
    t.index ["syllabus_program_id"], name: "index_syllabus_messages_on_syllabus_program_id"
  end

  create_table "syllabus_programs", force: :cascade do |t|
    t.jsonb "program_json", default: "{}", null: false
    t.integer "type_program"
    t.bigint "syllabus_id", null: false
    t.integer "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "coordinator_id"
    t.integer "supervisor_id"
    t.boolean "enable_edit_formulas", default: false
    t.boolean "is_inactive", default: false
    t.boolean "show_modal", default: true
    t.bigint "modality_id"
    t.jsonb "evaluation_system_json", default: "{}"
    t.jsonb "evaluation_rules_json", default: "{}"
    t.integer "revision_number", default: 0, null: false
    t.boolean "authorized", default: true, null: false
    t.index ["coordinator_id"], name: "index_syllabus_programs_on_coordinator_id"
    t.index ["modality_id"], name: "index_syllabus_programs_on_modality_id"
    t.index ["supervisor_id"], name: "index_syllabus_programs_on_supervisor_id"
    t.index ["syllabus_id"], name: "index_syllabus_programs_on_syllabus_id"
  end

  create_table "syllabuses", force: :cascade do |t|
    t.integer "year", null: false
    t.string "cycle_academic", null: false
    t.jsonb "syllabus_content", default: "{}", null: false
    t.bigint "doc_template_id"
    t.bigint "curriculum_course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cycle_id"
    t.bigint "course_id"
    t.integer "credits", default: 0
    t.integer "weekly_hours", default: 0
    t.index ["course_id"], name: "index_syllabuses_on_course_id"
    t.index ["curriculum_course_id"], name: "index_syllabuses_on_curriculum_course_id"
    t.index ["cycle_academic", "course_id", "cycle_id"], name: "index_syllabuses_on_cycle_academic_and_course_id_and_cycle_id", unique: true
    t.index ["cycle_academic"], name: "index_syllabuses_on_cycle_academic"
    t.index ["cycle_id"], name: "index_syllabuses_on_cycle_id"
    t.index ["doc_template_id"], name: "index_syllabuses_on_doc_template_id"
    t.index ["year"], name: "index_syllabuses_on_year"
  end

  create_table "tracking_status_syllabuses", force: :cascade do |t|
    t.bigint "syllabus_program_id", null: false
    t.integer "state", null: false
    t.integer "user_id_track", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["syllabus_program_id"], name: "index_tracking_status_syllabuses_on_syllabus_program_id"
  end

  create_table "type_of_practices", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "last_name", default: ""
    t.index ["email"], name: "index_user_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_user_admins_on_reset_password_token", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "user"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "code_user"
    t.integer "state", default: 0, null: false
    t.boolean "show_modal_sidebar", default: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "competitions", "courses"
  add_foreign_key "information_sources", "books"
  add_foreign_key "information_sources", "courses"
  add_foreign_key "link_unit_information_sources", "information_sources"
  add_foreign_key "modality_cycles", "cycles"
  add_foreign_key "modality_cycles", "modalities"
  add_foreign_key "practices", "type_of_practices"
  add_foreign_key "program_course_careers", "careers"
  add_foreign_key "program_course_careers", "program_courses"
  add_foreign_key "program_courses", "courses"
  add_foreign_key "program_courses", "modalities"
  add_foreign_key "program_courses", "users", column: "coordinator_id"
  add_foreign_key "program_courses", "users", column: "supervisor_id"
  add_foreign_key "sections_templates", "components"
  add_foreign_key "sections_templates", "doc_templates"
  add_foreign_key "syllabus_messages", "syllabus_programs"
  add_foreign_key "syllabus_programs", "modalities"
  add_foreign_key "syllabus_programs", "syllabuses"
  add_foreign_key "syllabuses", "courses"
  add_foreign_key "syllabuses", "curriculum_courses"
  add_foreign_key "syllabuses", "cycles"
  add_foreign_key "syllabuses", "doc_templates"
  add_foreign_key "tracking_status_syllabuses", "syllabus_programs"
end
