class Api::V1::BooksController < Api::ApiV1SharedController
  before_action :set_book, only: [:show, :update]

  def index
    text = params[:text_filter]
    @books = Book.books_with_title_like_and_without_list_books(text, nil).limit(10)
  end

  def show
  end

  def update
    authorize @book
    @book.assign_attributes(books_params.except(:id))
    if @book.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @book.errors) and return
    end
    ApplicationRecord.transaction do
      begin
        @book.save!()
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  def create
    @book = Book.new(books_params)
    authorize @book
    if @book.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @book.errors) and return
    else
      ApplicationRecord.transaction do
        begin
          @book.save!()
        rescue Exception => e
          render_internal_server_error(e.message)
        end
      end
    end
  end

  private

  def books_params
    params.permit(
      :id, :code, :title, :author, :support, :year, :number_editorial, :editorial, :is_acquired
    )
  end

  def set_book
    @book = Book.find(params[:id])
  end
end
