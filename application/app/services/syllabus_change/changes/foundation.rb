module SyllabusChange
  module Changes
    class Foundation < SyllabusChange::Changes::GeneralChange
      include SyllabusChange::Changes::DetectChangeSyllabusContent
      attr_accessor :syllabus, :auditable_objects
      def initialize(**attribute)
        super(**attribute)
        attribute.each do |k,v|
          instance_variable_set("@#{k}", v) unless v.nil?
        end  
        @title = "Fundamentación"
        @id_section = "section-fundamentacion"
        @changes_detected = 0
        @is_edited = false
      end

      def call
        syllabus_content = SyllabusChange::Changes::SyllabusContentChange.new(
          @has_more_than_one_state,
          @syllabus
        )
        @is_edited = syllabus_content.exist_change_in_syllabus_content_to?("fundamentación", @auditable_objects)
      end
      
    end 
  end  
end      