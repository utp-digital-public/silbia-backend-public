class Api::ApiV1AdminController < ApiAdminController
    include ResponseJson
    include ExceptionHandlerJson

    def append_info_to_payload(payload)
      super
      payload[:host] = request.host
      payload[:remote_ip] = request.remote_ip
      payload[:ip] = request.ip
    end
end