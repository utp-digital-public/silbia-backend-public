class CycleProgramCourseCareer < ActiveRecord::Migration[5.2]
  def change
    load "#{Rails.root}/db/custom_seeds/program_course_careers_cycle.rb"
  end
end
