class AddCodeToCompetitions < ActiveRecord::Migration[5.2]
  def change
    rename_column :specific_competitions, :competition, :name
    rename_column :general_competitions, :competition, :name
    add_column :specific_competitions, :code, :string, :null => false, :default => ""
    add_index :specific_competitions, [:code, :career_id], unique: true
    add_column :general_competitions, :code, :string, :null => false, :default => ""
    add_index :general_competitions, [:code], unique: true
    add_index :course_specific_competitions, [:specific_competition_id, :program_course_career_id], unique: true, name: "ix_unique_course_specific_competition"
    add_index :course_general_competitions, [:general_competition_id, :program_course_id], unique: true, name: "ix_unique_course_general_competition"
  end
end
