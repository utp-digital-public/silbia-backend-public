json.status "success"
json.data do
  json.id @practice.id
  json.description @practice.description
  json.code @practice.code
  json.type_of_practice_id @practice.type_of_practice_id
end
