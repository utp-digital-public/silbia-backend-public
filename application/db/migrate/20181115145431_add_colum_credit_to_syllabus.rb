class AddColumCreditToSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabuses, :credits, :integer, default: 0
    add_column :syllabuses, :weekly_hours, :integer, default: 0
  end
end
