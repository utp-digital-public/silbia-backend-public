class AddUniqueIndexToProgramCourses < ActiveRecord::Migration[5.2]
  def change
    add_index :program_courses, [:course_id, :modality_id], unique: true
  end
end
