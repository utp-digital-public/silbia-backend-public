FactoryBot.define do
  factory :component do
    name { 'uncomponent' }
    consume_data_servicio { false }
    foto { 'silabus' }
  end
  factory :cabecera_silabus, class: Component do
    id { 1 }
    name { 'cabecera_silabus' }
    consume_data_servicio { true }
    foto { 'un/cabecera.jpg' }
  end
  factory :texto, class: Component do
    id { 2 }
    name { 'texto' }
    consume_data_servicio { false }
    foto { 'un/texto.jpg' }
  end
  factory :fuentes_informacion, class: Component do
    id { 3 }
    name { 'fuentes_informacion' }
    consume_data_servicio { false }
    foto { 'un/fuentes_informacion.jpg' }
  end
  factory :cronograma_actividades, class: Component do
    id { 4 }
    name { 'cronograma_actividades' }
    consume_data_servicio { false }
    foto { 'un/cronograma_actividades.jpg' }
  end
  factory :formula, class: Component do
    id { 5 }
    name { 'formulas' }
    consume_data_servicio { false }
    foto { 'un/formulas.jpg' }
    parent_id { 4 }
  end  
end
