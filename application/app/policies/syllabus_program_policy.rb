#TODO: complete test policy
class SyllabusProgramPolicy < ApplicationPolicy
  def initialize(user, record)
    @user = user
    @record = record
  end

  def approve?
    return approve_or_observe?
  end

  def observe?
    return approve_or_observe?
  end

  def send_to_approve?
    return edit?
  end

  def refuse?
    true if (@user.is_approver?)
  end

  def show?
    return @record.all_user_authorized.include? @user.id
  end

  def edit?
    if (@record.to_be_approved? || @record.approved?)
      return false
    end
    if (@user.is_creator?)
      return (@record.coordinator_id == @user.id)
    end
    return false
  end

  def update?
    return edit?
  end

  def update_flags?
    if (@user.is_approver?)
      return (@record.supervisor_id == @user.id) && @record.to_be_approved?
    elsif (@user.is_creator?)
      return edit?
    end
    return false
  end

  def owner?
    if (@user.is_approver?)
      return (@record.supervisor_id == @user.id)
    elsif (@user.is_creator?)
      return (@record.coordinator_id == @user.id)
    end
    return false
  end

  def show_observations?
    if (@user.is_approver?)
      return (@record.to_be_approved? || @record.observed?)
    elsif (@user.is_creator?)
      return (@record.observed?)
    end
    return false
  end

  def show_editions?
    return owner? && @record.to_be_approved?
  end

  def approve_or_observe?
    return @user.is_approver? && (@record.supervisor_id == @user.id) && @record.to_be_approved?
  end

  def request_edition?
    return @user.is_creator? && (@record.coordinator_id == @user.id) && @record.to_be_approved?
  end

  # def approve?
  #  true if ( @user.is_approver? && @record.may_approve? )
  # end

  # def send_to_approve?
  #   true if ( @user.is_creator? && @record.may_send_to_approve? )
  # end

  # def refuse?
  #  true if ( @user.is_approver? && @record.may_observe? )
  # end

  # def show?
  #  true if ( @user.is_approver? || @user.is_creator? )
  # end

  # def update?
  #   @user.is_creator?
  # end
end
