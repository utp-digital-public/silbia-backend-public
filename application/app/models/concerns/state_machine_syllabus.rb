module StateMachineSyllabus
  extend ActiveSupport::Concern
  included do
    include AASM
    aasm column: "state", enum: true do
      state :pending, initial: true
      state :in_progress, :to_be_approved, :approved, :observed

      # Events
      event :edit_progress do
        transitions from: [:pending], to: :in_progress
      end

      event :send_to_approve do
        transitions from: [:in_progress, :observed], to: :to_be_approved
      end

      event :approve_silabus do
        transitions from: [:to_be_approved], to: :approved
      end

      event :observe do
        transitions from: [:to_be_approved, :approved], to: :observed
      end

      event :reset_state do
        transitions from: [:approved], to: :pending
      end

      # event :send_to_approve_from_observed do
      #   transitions from: :observed, to: :to_be_approved
      # end

    end
  end
end
