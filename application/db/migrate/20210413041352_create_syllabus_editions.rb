class CreateSyllabusEditions < ActiveRecord::Migration[5.2]
  def change
    create_table :syllabus_editions do |t|
      t.references :syllabus_program, foreign_key: true
      t.integer :section_id, null: false
      t.integer :revision_number, null: false
      t.jsonb :detail, null: false, default: "{}"
      t.timestamps
    end
  end
end
