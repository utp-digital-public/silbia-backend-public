class ApplicationMailer < ActionMailer::Base
  default from: 'silbia@utp.edu.pe'
  layout 'mailer'
end
