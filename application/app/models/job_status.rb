class JobStatus < ApplicationRecord
  def start_process
    update!(
      message: "Obteniendo tus archivos...",
    )
  end

  def increase_progress
    new_progress = progress + 1
    update!(
      progress: new_progress,
      message: "Obteniendo tus archivos (#{(new_progress * 100.0 / max_progress).round(2)}%)...",
    )
  end

  def setting_zip
    update!(
      message: "Generando comprimido de sílabos",
    )
  end

  def error_in_process(error)
    update!(
      message: "Error en el procesamiento:",
      error_message: "#{error}",
      error: true,
    )
  end

  def finish_process(zip_path)
    update!(
      generated_zip_path: zip_path,
      message: "Procesamiento exitoso",
      completed: true,
      error: false,
    )
  end
end
