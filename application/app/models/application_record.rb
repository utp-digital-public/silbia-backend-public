class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def human_enum(enum_name)
    I18n.t("activerecord.attributes.#{model_name.i18n_key}.#{enum_name.to_s.pluralize}.#{send(enum_name)}")
  end

end
