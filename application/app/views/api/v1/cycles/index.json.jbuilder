json.status "success"
json.data do
  json.array! @cycles, partial: "api/v1/cycles/cycle", as: :cycle
end
