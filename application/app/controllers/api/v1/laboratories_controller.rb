class Api::V1::LaboratoriesController < Api::ApiV1Controller
  before_action :set_order_params, only: [:index]

  def index
    @laboratories = Laboratory.all
    @laboratories = @laboratories.order(@sort) unless @sort.blank?
  end

  def set_order_params
    order = if params[:order].present? then params[:order] else "asc" end
    @sort = if params[:sort].present? then params[:sort] + " " + order else "" end
  end
end
