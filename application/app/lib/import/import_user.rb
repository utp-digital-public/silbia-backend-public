module Import
  class ImportUser < Import::Entity 

    def build_row_user_to_update(row, email_user)
      user = User.new
      user.email = email_user
      user.name = row["name"].titleize
      user.first_name = row["first_name"].titleize
      user.password = row["password"]
      user.code_user = row["code"]
      user
    end
    
    def assign_role(row, user)
      user_created = User.where(email: user.email).first
      if row['role'].include? "1"
        user_created.add_role ROLE_CREATOR.to_sym
      end  
      if row['role'].include? "2"  
        user_created.add_role ROLE_APPROVER.to_sym
      end  
    end  
    
    def import_user_if_not_exist
      User.transaction do
        begin
          load_and_save_users()
        rescue => exception
          puts "an error ocurred #{exception}"
        end  
      end
    end

    def load_and_save_users()
      row_updated = 0
      #csv = CSV.parse(@csv_text.scrub, headers: true)
      puts "start to update table users"
      @csv.each do |row|
        # Find user with email first
        email_user = row["email"].delete(' ').downcase
        user_to_find = User.where('lower(email) = ?', email_user.downcase).first 
        unless user_to_find.present?
          user = build_row_user_to_update(row, email_user)
          user.save!
          row_updated +=1 
          assign_role(row, user)
          puts "#{user.name}, #{user.email} saved"
        else
          puts "The user #{user_to_find.name} with email: #{user_to_find.email} Exist"
        end
      end
      puts "Updated in total  #{row_updated} rows"  
    end  

  end
end
