class BookPolicy < ApplicationPolicy
  def initialize(user, record)
    @user = user
    @record = record
  end

  def create?
    return @user.is_a? UserAdmin
  end

  def update?
    return @user.is_a? UserAdmin
  end
end
