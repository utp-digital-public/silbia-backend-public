class RemoveAasmtColumnToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    remove_column :syllabus_programs, :aasm_state
  end
end
