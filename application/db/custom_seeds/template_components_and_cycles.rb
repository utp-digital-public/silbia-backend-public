Component.create!([
  {name: "cabecera_silabus", consume_data_servicio: true, foto: "https://s3.amazonaws.com/images.seroundtable.com/google-jobs-1493638048.jpg", parent_id: nil},
  {name: "texto", consume_data_servicio: false, foto: "https://www.emberjs.com/images/brand/ember_Tomster-Lockup-3a0f7b78.png", parent_id: nil},
  {name: "texto_doble", consume_data_servicio: false, foto: "https://s3.amazonaws.com/images.seroundtable.com/google-jobs-1493638048.jpg", parent_id: nil},
  {name: "cronograma_actividades", consume_data_servicio: false, foto: "https://s3.amazonaws.com/images.seroundtable.com/google-jobs-1494243864.jpg", parent_id: nil}
])

DocTemplate.create!([
  {name: "plantilla 2019", type_of_template: "silabus", is_active: true}
])
SectionsTemplate.create!([
  {doc_template_id: 1, component_id: 1, title: "", sort: 1},
  {doc_template_id: 1, component_id: 2, title: "Fundamentación", sort: 2},
  {doc_template_id: 1, component_id: 2, title: "Sumilla", sort: 3},
  {doc_template_id: 1, component_id: 2, title: "Logro General de aprendizaje", sort: 4},
  {doc_template_id: 1, component_id: 2, title: "Metodologia", sort: 5},
  {doc_template_id: 1, component_id: 3, title: "Fuentes de Información", sort: 6}
])

Cycle.create!([{ id:1, name:"2019 - Ciclo 1 Marzo", enabled: true, cycle_start: Time.now, cycle_end: DateTime.now+1 }])
puts "termino"