class Api::ApiV1Controller < ApiController
    include ResponseJson

    def append_info_to_payload(payload)
      super
      payload[:remote_ip] = request.remote_ip
      payload[:ip] = request.ip
    end

end