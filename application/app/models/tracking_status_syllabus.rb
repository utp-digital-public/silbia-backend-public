class TrackingStatusSyllabus < ApplicationRecord
  belongs_to :syllabus_program
  validates :syllabus_program_id, :state, presence: {message: "es requerido"}
end
