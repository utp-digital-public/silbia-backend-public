FactoryBot.define do
  factory :syllabus do
    year  { 2019 } 
    cycle_academic { "1" }
    modality { "presencial" }
    syllabus_content { "{}" }
    association :doc_template 
    association :user 
    association :curriculum_course
    association :cycle 
    association :course
    credits { 4 }
    weekly_hours { 5 }

    trait :syllabus_estrategia do
      course { FactoryBot.build_stubbed(:estrategia) }
    end

    trait :syllabus_ingles1 do
      course { FactoryBot.build_stubbed(:ingles1) }
    end

    trait :syllabus_ingles2 do
      course { FactoryBot.build_stubbed(:ingles2) }
    end

    trait :syllabus_mathematics do
      course { FactoryBot.build_stubbed(:math_course) }
    end

    trait :language do
      course { FactoryBot.build_stubbed(:language) }
    end

  end
end
