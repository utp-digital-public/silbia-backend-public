require 'csv'
### Migrate COMPETITIONS from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'feb-2020-new-competitions-careers.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  catalog_code = row["Codigo"]
  import_obj = Import::ConvertToJson.new
  course = import_obj.search_course_by_catalog_code(catalog_code)
  if course.present?
    career = row["Carrera"].upcase
    if Competition.where('lower(career) = ?', row["Carrera"].downcase).where(course_id: course.id).empty?
      comp = Competition.new
      comp.career = career
      comp.competition_description = "Sin competencias"
      comp.criterion = "Sin criterio"
      comp.achievement_level = "Sin nivel: Sin descripción del nivel"
      comp.course_id = course.id
      comp.save!
      puts "#{comp.competition_description}, #{catalog_code} saved"
    else
      puts "Carrera #{career} ya registrada en curso #{course.name}"
    end
  end  

end
puts "There are now #{Competition.count} rows in the transactions table"
#####################################################################################
Competition.where(course_id: 752, career: "ADMINISTRACIÓN Y MARKETING",competition_description: "Sin competencias").empty?