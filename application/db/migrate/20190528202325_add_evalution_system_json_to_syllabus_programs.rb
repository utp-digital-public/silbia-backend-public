class AddEvalutionSystemJsonToSyllabusPrograms < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :evaluation_system_json, :jsonb, default: "{}"
  end
end
