class RemoveColumnsFromCourses < ActiveRecord::Migration[5.2]
  def change
  	remove_column :courses, :catalog_code
  	remove_column :courses, :name
  	remove_column :courses, :catalog_code_cgt
  	remove_column :courses, :name_cgt
  end
end
