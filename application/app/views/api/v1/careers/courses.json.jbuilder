json.status "success"
json.data do
  json.array! @program_courses do |program_course_career|
    json.id program_course_career.id
    json.program_course_id program_course_career.program_course_id
    json.catalog_code program_course_career.program_course.catalog_code
    json.name program_course_career.program_course.name
    json.credits program_course_career.program_course.credits
    json.weekly_hours program_course_career.program_course.weekly_hours
    json.cycle_number program_course_career.cycle_number
  end
end
