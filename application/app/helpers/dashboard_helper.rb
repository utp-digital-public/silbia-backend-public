module DashboardHelper
  
  def th_coordinator
    "<th> Editor</th>".html_safe if current_user.is_approver?
  end  

  def tag_select(id)
    id ||= ""
    text =""

    SyllabusProgram.getArrStates.try(:each) do |element|
      text = element[:label]  if id == element[:value]
    end  
    
    "<span class='selected-tag'>#{text} </span> <input type='search' autocomplete='off' readonly='readonly' role='combobox' aria-label='Search for option' class='form-control'>".html_safe
  end  
end
