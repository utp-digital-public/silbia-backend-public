module Import
  require 'csv'
  class Entity
    attr_accessor :csv
    def initialize(file_name)
      @csv_text = build_csv_text(file_name)
      @csv = get_csv
    end

    def build_csv_text(file_name)
      @csv_text = File.read(Rails.root.join('lib', 'seeds_csv', file_name))
    end

    def get_csv
      CSV.parse(@csv_text.scrub, headers: true)
    end
    
    def remove_space(string_value)
      return "" unless string_value.present?
      string_value.delete(' ') 
    end  
  end  
  
end