class Career < ApplicationRecord
  enum modality: [:in_person, :blended]
  has_many :program_course_careers
  has_many :specific_competitions, :dependent => :destroy, :inverse_of => :career
  has_many :course_specific_competitions, :through => :program_course_careers
  has_many :program_courses, :through => :program_course_careers
  has_many :course_general_competitions, :through => :program_courses
  belongs_to :manager, class_name: "User", foreign_key: "manager_id"
end
