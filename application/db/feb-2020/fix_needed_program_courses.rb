
objs = {2361 => {:name => "DESARROLLO DE HABILIDADES PERSONALES Y VIDA PROFESIONAL", :catalog_code => "100000CF20", :catalog_code_cgt => "100000U08F", :name_cgt => ""},2494 => {:name => "OPERACIÓN DE ALIMENTOS Y BEBIDAS", :catalog_code => "100000H06F", :catalog_code_cgt => "", :name_cgt => ""},2495 => {:name => "TURISMO SOSTENIBLE", :catalog_code => "100000H07F", :catalog_code_cgt => "", :name_cgt => ""},2496 => {:name => "RECEPCION Y HOUSEKEEPING", :catalog_code => "100000H19F", :catalog_code_cgt => "", :name_cgt => ""},2497 => {:name => "ADMINISTRACIÓN DE CLUBES Y CASINOS", :catalog_code => "100000H21F", :catalog_code_cgt => "", :name_cgt => ""},2498 => {:name => "TQM TOTAL QUALITY MANAGEMENT EN TURISMO Y HOTELERA", :catalog_code => "100000H22F", :catalog_code_cgt => "", :name_cgt => ""},2499 => {:name => "ADMINISTRACIÓN DE PERSONAL", :catalog_code => "100000I44F", :catalog_code_cgt => "", :name_cgt => ""},2500 => {:name => "LOGÍSTICA EN EL SECTOR PÚBLICO", :catalog_code => "100000I52F", :catalog_code_cgt => "", :name_cgt => ""}}

objs.keys.each do |course_id|
  syllabus = Syllabus.where(course_id: course_id).last
  if syllabus.present?
    course_names = objs[course_id]
    syllabus_in_person = syllabus.syllabus_programs.where(type_program: 1).first
    syllabus_cgt = syllabus.syllabus_programs.where(type_program: 2).first
    begin
      pc_preg = ProgramCourse.create!(weekly_hours: 8, credits: 4, name: course_names[:name], catalog_code: course_names[:catalog_code], type: 'InPerson', course_id: course_id, coordinator_id: syllabus_in_person.coordinator_id, supervisor_id: syllabus_in_person.supervisor_id)
      pc_cgt = ProgramCourse.create!(weekly_hours: 8, credits: 4, name: course_names[:name_cgt].blank? ? course_names[:name] : course_names[:name_cgt], catalog_code: course_names[3], type: 'Blended', course_id: course_id, coordinator_id: syllabus_cgt.coordinator_id, supervisor_id: syllabus_cgt.supervisor_id) unless course_names[:catalog_code_cgt].blank? || syllabus_cgt.nil?
    rescue Exception => e
      p "#{e.message}"
    end
  end
end