class AddManagerToCareer < ActiveRecord::Migration[5.2]
  def change
    add_reference :careers, :manager, foreign_key: { to_table: :users }
  end
end
