class CreateSpecificCompetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :specific_competitions do |t|
      t.references :career, foreign_key: true
      t.string :competition, null: false
      t.string :description, null: false, default: ""
      t.timestamps
    end
  end
end
