class Course < ApplicationRecord
  has_many :information_sources, :dependent => :destroy, :inverse_of => :course
  has_many :books, :through => :information_sources
  has_many :competitions

  belongs_to :course_dependent, class_name: "Course", foreign_key: "course_id_requeriment", optional: true
  has_many :requirements, class_name: "Course", foreign_key: "course_id_requeriment"

  has_and_belongs_to_many :faculties
  has_many :program_courses
  has_many :syllabuses

  def information_sources_ordered_asc
    information_sources.order_by_type_source.includes(:book)
  end

  def basic_book_ids
    information_sources.basic.pluck(:book_id)
  end

  def complementary_book_ids
    information_sources.complementary.pluck(:book_id)
  end

  def current_syllabus
    current_syllabus = syllabuses.active
  end

  def modalities
    program_courses.pluck(:modality_id)
  end

  def self.import(file)
    spreadsheet = Roo::Spreadsheet.open(file)
    errors = []
    courses_count = 0
    return { errors: ["Documento vacío. Debe ingresar un archivo con la estructura correcta"], courses_count: 0, error_type: "invalid_document" } if spreadsheet.first_row.nil?
    all_headers = spreadsheet.row(6)
    unless all_headers.size == 17
      return { errors: ["No se han considerado todas las columnas. Debe ingresar un archivo con la estructura correcta"], courses_count: 0, error_type: "invalid_document" }
    end
    (7..spreadsheet.last_row).each do |i|
      course = Course.new
      course.for_all_career = spreadsheet.row(i)[4].present? ? I18n.transliterate(spreadsheet.row(i)[4].to_s).downcase.strip.eql?("si") : false
      course.for_all_faculties = spreadsheet.row(i)[5].present? ? I18n.transliterate(spreadsheet.row(i)[5].to_s).downcase.strip.eql?("si") : false
      code_in_person = spreadsheet.row(i)[0].to_s
      code_in_person.strip! if code_in_person.present?
      credits_in_person = spreadsheet.row(i)[13]
      w_hours_in_person = spreadsheet.row(i)[15]
      supervisor = User.search_user_by_email(spreadsheet.row(i)[6].to_s)
      if supervisor.nil?
        errors << "Error en la Fila #{i}: Ingrese un email de aprobador existente"
        next
      end
      coordinator_preg = User.search_user_by_email(spreadsheet.row(i)[7].to_s)
      if coordinator_preg.nil?
        errors << "Error en la Fila #{i}: Ingrese un email de editor PREG existente"
        next
      end
      name_in_person = spreadsheet.row(i)[2].to_s
      name_in_person.strip! if name_in_person.present?
      course.program_courses.build(catalog_code: code_in_person, type: "InPerson", name: name_in_person, credits: credits_in_person, weekly_hours: w_hours_in_person, coordinator_id: coordinator_preg.id, supervisor_id: supervisor.id)
      code_blended = spreadsheet.row(i)[1].to_s
      unless code_blended.blank?
        code_blended.strip!
        coordinator_cgt = User.search_user_by_email(spreadsheet.row(i)[8].to_s)
        if coordinator_cgt.nil?
          errors << "Error en la Fila #{i}: Ingrese un email de editor CGT existente"
          next
        end
        name_blended = spreadsheet.row(i)[3].blank? ? name_in_person : spreadsheet.row(i)[3].to_s
        name_blended.strip!
        credits_blended = spreadsheet.row(i)[14]
        w_hours_blended = spreadsheet.row(i)[16]
        course.program_courses.build(catalog_code: code_blended, type: "Blended", name: name_blended, credits: credits_blended, weekly_hours: w_hours_blended, coordinator_id: coordinator_cgt.id, supervisor_id: supervisor.id)
      end
      s = nil
      begin
        if course.save!
          s = Syllabus.new
          s.cycle_id = Cycle.active.id
          s.cycle_academic = REGULAR_CYCLE
          s.year = 2020
          s.doc_template_id = 1
          s.curriculum_course_id = 1
          s.course = course
          fundamentation = spreadsheet.row(i)[9].to_s[0..1349] unless spreadsheet.row(i)[9].blank?
          summary = spreadsheet.row(i)[10].to_s[0..999] unless spreadsheet.row(i)[10].blank?
          achievement = spreadsheet.row(i)[11].to_s[0..999] unless spreadsheet.row(i)[11].blank?
          methodology = spreadsheet.row(i)[12].to_s[0..1799] unless spreadsheet.row(i)[12].blank?
          s.syllabus_content = Syllabus.set_content_json_silabus(fundamentation, summary, achievement)
          json_units = { units: [], texts: [Syllabus.populate_methodology_json_text(methodology)] }.to_json

          course.program_courses.each do |p_course|
            syllabus_param_hash = {
              program_json: JSON.parse(json_units),
              type_program: p_course.is_a?(InPerson) ? TYPE_PROGRAM_PREGRADO : TYPE_PROGRAM_CGT,
              state: NO_READ_STATE,
              coordinator_id: p_course.is_a?(InPerson) ? coordinator_preg.id : coordinator_cgt.id,
              supervisor_id: supervisor.id,
              modality_id: p_course.is_a?(InPerson) ? PRESENCIAL_CODE : SEMI_PRESENCIAL_CODE,
              evaluation_system_json: { evaluation_system: [] },
              evaluation_rules_json: { rules: [] },
            }
            s.syllabus_programs.build(syllabus_param_hash)
            s.save!
            courses_count += 1
          end
        end
      rescue ActiveRecord::RecordInvalid => invalid
        error_obj = course.program_courses.first.errors.full_messages.empty? ? course.program_courses.last : course.program_courses.first
        errors << "Error en la Fila #{i}: #{error_obj.errors.full_messages.first}"
        next
      rescue Exception => e
        errors << "Error en la Fila #{i}: #{e.message}"
        next
      end

      # course_pregrade = ProgramCourse.search_course_by_catalog_code(code_pregrade)
      # unless course_pregrade.present?
      # else
      # errors << ""
      # end

    end
    { errors: errors, courses_count: courses_count }
  end
end
