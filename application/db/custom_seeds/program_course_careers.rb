require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'program_course_careers.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
	modality = row[0]=="Presencial" ? 0 : 1
	career_name = row["career"]
	career = Career.where("modality = ? and unaccent(name) ILIKE unaccent(?)", modality, career_name).first
	unless career.nil?
		program_course = ProgramCourse.find_by_catalog_code(row["course_code"])
		unless program_course.nil?
			theoric_hours = row["theoric_hours"].to_i
			practical_hours = row["practical_hours"].to_i
			credits = row["credits"]
			hours = theoric_hours + practical_hours
		  program_course.weekly_hours = hours
		  program_course.credits = credits
		  program_course.program_course_careers.build(career_id: career.id)
		  begin
		  	program_course.save!
		  	puts "Se asoció el curso #{program_course.name} con la carrera #{career.name}"
		  rescue Exception => e
		  	p "Error: #{program_course.errors.full_messages.first}"
		  end
		else
			puts "No hay curso con el código #{row["course_code"]}"
		end
	else
		puts "No hay carrera con el nombre #{career_name} y modalidad #{modality}"
	end
end
puts "There are now #{ProgramCourseCareer.count} rows in the transactions table"
