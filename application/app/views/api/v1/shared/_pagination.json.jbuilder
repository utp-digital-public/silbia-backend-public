json.pagination do
    json.current  collection.current_page
    json.previous collection.prev_page
    json.next     collection.next_page
    json.per_page collection.limit_value
    json.pages    collection.total_pages
    json.count    collection.total_count
end