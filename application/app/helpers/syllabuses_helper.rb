#TODO :refactor and separate in class this Helper
module SyllabusesHelper
  def activities_li_helper(activity, evaluation_systems)
    custom_message = ""
    if ((activity["description"].present?) && (activity["type_activity"].empty?))
      return "<li> #{activity["description"].camelize} </li>".html_safe
    elsif activity["type_activity"].present?
      evaluation_systems.try(:each) do |evaluation_system|
        if ((evaluation_system["type"].upcase) == (activity["type_activity"].upcase))
          observation = evaluation_system["observations"]
          custom_message = "#{activity["label_description"]} "
          #custom_message = "#{custom_message}  (#{observation.titleize})" if observation.present?
        end
      end
    end
    #return "<li class='no_list'> #{custom_message} </li>".html_safe
    return "<li > #{custom_message} </li>".html_safe if custom_message.present?
  end

  def show_session_subject_or_type_activity(session)
    html = ""
    # session["activities"].try(:each) do |activity|
    #   # if  (activity["type_activity"].present? ) && (activity["type_activity"]!= "Sin Evaluación")
    #   #   #html = " #{activity['label_description']} ".html_safe
    #   # else
    #   #   html = " #{ show_text_in_html_format( session['subject'] ) } ".html_safe if session["subject"].present?
    #   # end
    # end
    session["subject"].split("\n").try(:each) do |row|
      html += "#{row} <br/>" if row.present?
    end
    html = removeHtmlTagsToText(html)
    html.html_safe
  end

  def sumary_cells_weeks_h(units)
    list_td = ""
    units.try(:each) do |unit|
      unit["weeks"].try(:each) do |week|
        list_td += "<td>  #{week["number_week"]}  </td>"
      end
    end
    list_td.html_safe
  end

  def sumary_cells_units_h(units)
    list_td = ""
    units.try(:each_with_index) do |unit, index_unit|
      unit["weeks"].try(:each) do |week|
        list_td += "<td>  #{index_unit + 1}  </td>"
      end
    end
    list_td.html_safe
  end

  def sumary_cells_practices_h(units)
    list_td = ""
    units.try(:each_with_index) do |unit, index_unit|
      unit["weeks"].try(:each) do |week|
        class_check = ""
        week["sessions"].try(:each) do |session|
          session["activities"].try(:each) do |activity|
            class_check = "check" if activity["type_activity"].present?
            break
          end
        end
        list_td += "<td class='#{class_check}'>  </td>"
      end
    end
    list_td.html_safe
  end

  def show_weeks_unit(unit, sessions)
    list_weeks = ""
    session_weeks = sessions.where(unit_id: unit.id).order(:week_number).pluck(:week_number).uniq
    session_weeks.try(:each_with_index) do |week, index_week|
      if session_weeks.last == week
        list_weeks += " y " if session_weeks.length > 1
        list_weeks += week.to_s
      else
        coma = ""
        coma = "," unless session_weeks[index_week + 2] == nil
        list_weeks += week.to_s + coma
      end
    end
    list_weeks.html_safe
  end

  def show_weeks_specific_achievement_format(unit)
    list_weeks = ""
    unit["weeks"].try(:each_with_index) do |week, index_week|
      if unit["weeks"].last == week
        list_weeks += " y " if unit["weeks"].length > 1
        list_weeks += week["number_week"].to_s
      else
        coma = ""
        coma = "," unless unit["weeks"][index_week + 2] == nil
        list_weeks += week["number_week"].to_s + coma
      end
    end
    list_weeks.html_safe
  end

  def show_weeks_for_a_practice(units, cod_practice)
    list_weeks = []
    arr_numbers = []
    units.try(:each) do |unit|
      unit["weeks"].try(:each_with_index) do |week, index_week|
        week["sessions"].try(:each) do |session|
          session["activities"].try(:each) do |activity|
            if activity["type_activity"].upcase == cod_practice.upcase
              list_weeks.push(week["number_week"].to_s)
            end
          end
        end
      end
    end
    final_text = remove_duplicate_and_coma(list_weeks)
    final_text.html_safe
  end

  def show_weeks_for_an_activity(sessions, cod_practice)
    list_weeks = []
    arr_numbers = []
    sessions.try(:each) do |session|
      session.session_topics.try(:each_with_index) do |session_topic, index_session_topic|
        session_topic.activities.try(:each) do |activity|
          if activity["type_activity"].upcase == cod_practice.upcase
            list_weeks.push(session.week_number.to_s)
          end
        end
      end
    end
    final_text = remove_duplicate_and_coma(list_weeks)
    final_text.html_safe
  end

  def show_unit_topics(unit)
    list_topics = ""
    unit.topics.order(:id).try(:each) do |topic|
      list_topics += "<li>#{topic.topic}</li>" if topic.present?
    end
    list_subjects = removeHtmlTagsToText(list_topics)
    list_topics.html_safe
  end

  def show_subject_session_specific_achievement(unit)
    list_subjects = ""
    unit["weeks"].try(:each) do |week|
      week["sessions"].try(:each) do |session|
        list_subjects += "<li> " if session["subject"].present?
        session["subject"].split("\n").try(:each) do |row|
          list_subjects += " #{row} <br> " if row.present?
        end
        #session["subject"].split("\n").try(:each) do |row|
        #list_subjects+= "<li>  #{row}  </li>" if row.present?
        #list_subjects+= "#{row} <br>" if row.present?
        list_subjects += "</li>"
      end
    end
    list_subjects = removeHtmlTagsToText(list_subjects)
    list_subjects.html_safe
  end

  def show_information_source_texts(information_sources)
    return "No hay bibliografía ".html_safe unless information_sources.present?
    list_inf_sources = ""
    list_inf_sources = Pdf::Syllabus::InformationSourceService.get_format_collection(information_sources, "html")
    list_inf_sources.html_safe
  end

  def show_text_in_html_format(text)
    return removeHtmlTagsToText(text.gsub("\n", "<br>")) if text.present?
    ""
  end

  def show_career_competition(careers, course)
    text = ""
    unless course.for_all_career.present?
      careers.try(:each) do |career|
        text += career + "<br>" unless career.nil?
      end
    else
      return "Todas las carreras "
    end
    text.html_safe
  end

  def show_faculties(faculties, course)
    text = ""
    return "Todas las facultades" if course.for_all_faculties.present?
    faculties.try(:each) do |faculty|
      text += "#{faculty.name}  <br>"
    end
    text.html_safe
  end

  private

  def build_coma(variable, is_year = false)
    coma = ""
    if is_year.present?
      unless variable.present?
        return ""
      else
        coma = if variable.to_i > 0 then "," else "" end
        return coma
      end
    end

    if ((variable.present?) && (variable.blank? == false))
      return ","
    end
    ""
  end

  def remove_duplicate_and_coma(array)
    arr_without_repeat = array.uniq
    final_text = ""
    arr_without_repeat.try(:each) do |element|
      unless arr_without_repeat.last == element
        coma = ","
      else coma = ""       end
      final_text += element + coma
    end
    return final_text
  end

  def show_competitions(competitions)
    text = "<ul>"
    competitions.try(:each) do |competition|
      text += "<li>" + competition.name + "</li>" unless competition.nil?
    end
    text += "</ul>"
    text.html_safe
  end
end
