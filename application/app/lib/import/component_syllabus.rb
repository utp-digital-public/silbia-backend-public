module Import
	class ComponentSyllabus
	  attr_accessor :section_template_id
	  def initialize(**attributes)
	    attributes.each do |k,v|
	      instance_variable_set("@#{k}", v) unless v.nil?
	    end
	  end

	  def to_json(*options)
	    as_json(*options).to_json(*options)
	  end
	end

end