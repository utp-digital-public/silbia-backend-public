FactoryBot.define do
  factory :syllabus_message do
    syllabus_program nil
    message "MyText"
    to_user_id 1
    state 1
  end
end
