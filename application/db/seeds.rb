require 'csv'

puts "---- Cargar Malla curricular-----"
load "#{Rails.root}/db/custom_seeds/curriculum.rb"

puts "------ Carga dato basico ------------"
load "#{Rails.root}/db/custom_seeds/template_components_and_cycles.rb"

puts ".-------- Carga de Facultades ------------"
load "#{Rails.root}/db/custom_seeds/faculties.rb"

puts "------- CARGA DE CURSOS -------"
load "#{Rails.root}/db/custom_seeds/courses.rb"

puts "------- CARGA DE CURSOS FACULTADES -------"
load "#{Rails.root}/db/custom_seeds/faculties_courses.rb"

puts "------- CARGA DE LIBROS ------"
load "#{Rails.root}/db/custom_seeds/books.rb"

puts "-------- CARGA DE FUENTES DE INFORMACION ------"
load "#{Rails.root}/db/custom_seeds/information_sources.rb"

puts "---------- Carga competencias ----------."
load "#{Rails.root}/db/custom_seeds/competitions.rb"

puts "---------Carga de Roles  ------------------"
load "#{Rails.root}/db/custom_seeds/roles.rb"

puts "----------- Carga de Usuarios -----------"
load "#{Rails.root}/db/custom_seeds/users.rb"

puts ".-------- Carga de Practicas ------------"
load "#{Rails.root}/db/custom_seeds/practices.rb"

puts ".-------- Carga de Laboratories ------------"
load "#{Rails.root}/db/custom_seeds/laboratories.rb"

puts ".-------- Carga de Modalidades ------------"
load "#{Rails.root}/db/custom_seeds/insert_modalities.rb"

puts ".-------- Carga de Silabus ------------"
load "#{Rails.root}/db/custom_seeds/silabuses.rb"

puts ".--------  Second Load data of Silabus ------------"
load "#{Rails.root}/db/custom_seeds/second_silabuses_migration.rb"

puts ".-------- Update Approver and Coordinator of Syllabus ------------"
load "#{Rails.root}/db/custom_seeds/update_aprober_editor_silabus.rb"

puts ".-------- Update Code for exist user ------------"
load "#{Rails.root}/db/custom_seeds/update_code_to_exist_user.rb"


puts ".-------- Create Admin User Dashboard------------"
load "#{Rails.root}/db/custom_seeds/admin_create_user.rb"


puts ".--------   Execute custom scripts to Sprint-10 ------------"
load "#{Rails.root}/db/custom_seeds/SP_10_100000M45F_syllabo_new.rb"
load "#{Rails.root}/db/custom_seeds/SP_10_update_credits_hours.rb"
puts ".....Execute custom scripts to Sprint-12..........."
load "#{Rails.root}/db/sprint-12/import_user.rb"

puts ".-------- Carga de Usuarios Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/import_user.rb"

puts ".-------- Actualización de Usuarios Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/update_users.rb"

puts ".-------- Carga de libros Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/new_books.rb"

puts ".-------- Carga de libros Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/import_new_courses.rb"

puts ".-------- Actualización de Fuentes de información Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/information_sources.rb"

puts ".-------- Actualización de Fuentes de información para eliminar Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/information_sources_to_delete.rb"

puts ".-------- Carga de sílabos nuevos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/import_new_syllabuses.rb"

puts ".-------- Actualización de encargados de sílabos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/update_syllabus.rb"

puts ".-------- 2da Actualización de encargados de sílabos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/update_syllabus_2.rb"

puts ".-------- Carga de sílabos nuevos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/import_new_syllabuses_2.rb"

puts ".-------- 3ra Actualización de encargados de sílabos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/update_syllabus_3.rb"

puts ".-------- 3ra carga de sílabos nuevos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/import_new_syllabuses_3.rb"

puts ".-------- Copiar reglas de evaluaciones a syllabus_programs ------------"
load "#{Rails.root}/db/feb-2020/copy_rules_to_syllabus_programs.rb"

puts ".-------- Importar competencias para tener carreras en cursos ------------"
load "#{Rails.root}/db/feb-2020/import_new_competitions_for_careers.rb"

puts ".-------- 5ra Actualización de créditos y horas semanales de sílabos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/update_syllabus_4_credits_hours.rb"

puts ".-------- 5ra Actualización de encargados de sílabos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/update_syllabus_5.rb"

puts ".-------- 5ra Actualización de encargados de sílabos Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/careers_to_delete.rb"

puts ".-------- 2da carga de libros Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/new_books_2.rb"

puts ".-------- 2da actualización de fuentes de información para eliminar Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/information_sources_to_delete_2.rb"

puts ".-------- Actualización de fuentes de información Feb 2020 ------------"
load "#{Rails.root}/db/feb-2020/information_sources_2.rb"

puts ".-------- Carga de carreras ------------"
load "#{Rails.root}/db/custom_seeds/create_career_data.rb"

puts ".-------- Carga de program_course_careers ------------"
load "#{Rails.root}/db/custom_seeds/program_course_careers.rb"

puts ".-------- 2da Carga de program_course_careers ------------"
load "#{Rails.root}/db/custom_seeds/program_course_careers_2.rb"

puts ".-------- Agregar program courses faltantes ------------"
load "#{Rails.root}/db/feb-2020/fix_needed_program_courses.rb"

puts ".-------- 3ra Carga de program_course_careers ------------"
load "#{Rails.root}/db/custom_seeds/program_course_careers_3.rb"

puts ".--------  Carga de program course presencial ------------"
load "#{Rails.root}/db/custom_seeds/in_person_pc.rb"
