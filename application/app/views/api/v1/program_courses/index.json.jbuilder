json.status "success"
json.data do
  json.program_courses do
    json.array! @program_courses, partial: "api/v1/program_courses/list", as: :program_course
  end
  json.types do
    json.array! @arr_types
  end
  json.modalities do
    json.array! @modalities
  end
end
json.partial! "api/v1/shared/pagination", collection: @program_courses
