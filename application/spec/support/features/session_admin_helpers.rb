module Features
  module SessionAdminHelpers
    def login_de_usuario(email, password)
      visit "/login"
      within('form')  do 
        fill_in 'email', with: email
        fill_in 'password', with: password
      end
      click_button 'Login'
    end
  end  
end  