FactoryBot.define do
  factory :user do
    name { "Name Default"}
    email {"email_default@utp.edu.pe"}
    password { "12345678" }

    trait :creator do
      name {"Halley Rios"}
      email { "halley@lacafetalab.pe" }
      after(:create) {|user| user.add_role(:creator)}
    end
    trait :creator_2 do
      name {"Luis  Valdez"}
      email { "lvaldez@lacafetalab.pe" }
      after(:create) {|user| user.add_role(:creator)}
    end
    
    trait :approver do
      name {"Raul Haya de la Torre"}
      email { "raul@utp.edu.pe" }
      after(:create) {|user| user.add_role(:approver)}
    end

    trait :approver_2 do
      name {"Carlos Cespedes"}
      email { "cespedes@utp.edu.pe" }
      after(:create) {|user| user.add_role(:approver)}
    end

  end
end
