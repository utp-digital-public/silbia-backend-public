module RolesUserConcern
  extend ActiveSupport::Concern
  included do
    let(:approver) {FactoryBot.create(:user, :approver)}
    let(:creator) {FactoryBot.create(:user, :creator)}
  end  
end