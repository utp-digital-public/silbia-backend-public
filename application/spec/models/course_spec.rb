require 'rails_helper'

RSpec.describe Course, type: :model do
  describe "Relations" do
    it { should belong_to(:course_dependent).class_name("Course").with_foreign_key(:course_id_requeriment)  }
    it { should have_many(:requirements).class_name("Course").with_foreign_key(:course_id_requeriment)  }
  end  
  it { should validate_presence_of(:catalog_code).with_message("es requerido") }
  it { should validate_presence_of(:name).with_message("es requerido") }
  it { should have_many(:books).through(:information_sources)}

end
