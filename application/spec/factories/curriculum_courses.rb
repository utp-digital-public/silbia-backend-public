FactoryBot.define do
  factory :curriculum_course do
    modality_program "MyString"
    year { 2018 }
    desc_grade "MyString"
    faculty_code "MyString"
    faculty_name "MyString"
    program_academic "MyString"
    academic_plan_code "MyString"
    career_code "MyString"
    career_name "MyString"
    cycle "MyString"
    course_catalog_code "MyString"
    course_short_name "MyString"
    course_name "MyString"
    course_catalog_cgt_code "MyString"
    with_equivalence false
    credits 1
    weekly_hours 1
    face_to_face_course_mode "MyString"
    virtual_course_mode "MyString"
  end
end
