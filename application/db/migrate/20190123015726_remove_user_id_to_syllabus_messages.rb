class RemoveUserIdToSyllabusMessages < ActiveRecord::Migration[5.2]
  def change
    remove_column :syllabus_messages, :to_user_id
  end
end
