class RemoveColumCourseCatalogCodeToSyllabuses < ActiveRecord::Migration[5.2]
  def change
    remove_column :syllabuses, :course_catalog_code
  end
end
