require 'rails_helper'

RSpec.describe CurriculumCourse, type: :model do
  it { should validate_presence_of(:modality_program).with_message('es requerido') }
  it { should validate_presence_of(:desc_grade).with_message('es requerido') }
  it { should validate_presence_of(:faculty_code).with_message('es requerido') }

  it { should validate_presence_of(:program_academic).with_message('es requerido') }
  it { should validate_presence_of(:academic_plan_code).with_message('es requerido') }
  it { should validate_presence_of(:career_code).with_message('es requerido') }
  it { should validate_presence_of(:career_name).with_message('es requerido') }
  it { should validate_presence_of(:cycle).with_message('es requerido') }
  it { should validate_presence_of(:course_catalog_code).with_message('es requerido') }
  it { should validate_presence_of(:course_name).with_message('es requerido') }
  it { should validate_presence_of(:course_catalog_cgt_code).with_message('es requerido') }
  it { should validate_presence_of(:credits).with_message('es requerido') }
  it { should validate_presence_of(:weekly_hours).with_message('es requerido') }

end
