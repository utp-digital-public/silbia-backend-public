class AddAsmStateToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :aasm_state, :string
  end
end
