json.status "success"
json.data do
    json.token        @token
    json.exp          @time
    json.user         
    json.user do
        json.partial! "api/v1/users/user", user: @user
    end
end