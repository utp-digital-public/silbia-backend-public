require 'csv'
### Migrate COMPETITIONS from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'feb-2020-delete-careers.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  catalog_code = row["Codigo"]
  import_obj = Import::ConvertToJson.new
  course = import_obj.search_course_by_catalog_code(catalog_code)
  if course.present?
    comp = Competition.where('lower(career) = ?', row["Carrera"].downcase).where(course_id: course.id)
    unless comp.empty?
      comp.destroy_all
      puts "#{row["Carrera"]} eliminada del curso #{catalog_code} saved"
    else
      puts "Carrera #{row["Carrera"]} no está relacionada a curso #{catalog_code}"
    end
  end  

end
puts "There are now #{Competition.count} rows in the transactions table"
#####################################################################################