class AddIndexCoordintorToSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_index :syllabus_programs, :coordinator_id
  end
end
