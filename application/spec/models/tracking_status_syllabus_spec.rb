require 'rails_helper'

RSpec.describe TrackingStatusSyllabus, type: :model do
  describe "relations" do 
    it { should belong_to(:syllabus_program) }
  end
  it { should validate_presence_of(:syllabus_program_id).with_message("es requerido") }
end
