#TODO: complete test policy
class SyllabusPolicy < ApplicationPolicy
  def initialize(user, record)
    @user = user
    @record = record
  end

  def update_syllabus?
    if (@user.is_creator?)
      return (@record.base_syllabus_program.coordinator_id == @user.id)
    end
    return false
  end
end
