class SyllabusProgramService
  attr_reader :syllabus_program, :current_user

  def initialize(**attributes)
    attributes.each do |k, v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
  end

  def update_status(name_method)
    name_method = name_method + "!"
    if syllabus_program.method(name_method.to_sym).call
      object = { message: "Enviado a aprobación" }
      save_track_state_syllabus()
      return object
    end
  end

  def save_observations(syllabus_observations)
    syllabus_program.syllabus_messages_attributes = syllabus_observations.map { |obs| obs.to_unsafe_h }
    syllabus_program.save
  end

  def list_syllabus_for_roles(current_user, filters = {})
    return SyllabusProgram.where(id: nil) if current_user.has_not_role_asigned?
    syllabus_programs = SyllabusProgram.match_course_coordinator
    syllabus_programs = syllabus_programs.joins(:syllabus => :course)
    syllabus_programs = syllabus_programs.actives.whit_cycle_active
    if current_user.is_creator?
      syllabus_programs = syllabus_programs.where(coordinator_id: current_user.id)
      syllabus_programs = syllabus_programs.order_to_creator
    end
    if current_user.is_approver?
      syllabus_programs = syllabus_programs.where(supervisor_id: current_user.id)
      syllabus_programs = syllabus_programs.order_to_approver
    end
    #Filter by state or query
    syllabus_programs = syllabus_programs.where(state: filters[:state]) if filters[:state].present?
    syllabus_programs = syllabus_programs.where(id: ProgramCourse.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{filters[:query]}%", "%#{filters[:query]}%").map { |ff| ff.get_current_syllabus_program }.compact.map { |s| s.id }) if filters[:query].present?
    syllabus_programs
  end

  def save_syllabus(syllabus_param)
    if syllabus_program.type_program == :pregrado
      syllabus = syllabus_program.syllabus
      syllabus.update_attributes!(syllabus_content: syllabus_param.json_syllabus)
    end
    syllabus_program.update_attributes!(
      program_json: syllabus_param.program_json,
      evaluation_system_json: syllabus_param.evaluation_system_json,
      evaluation_rules_json: syllabus_param.evaluation_rules_json,
    )
    serviceInformationSources =
      InformationSources::LinkUnits::Builder.call(syllabus_param.information_sources_json)
  end

  def save_track_state_syllabus()
    syllabus_program_to_update = SyllabusProgram.find(syllabus_program.id)
    track_object = TrackingStatusSyllabus.new
    track_object.syllabus_program = syllabus_program
    track_object.state = syllabus_program_to_update.state_integer_value
    track_object.user_id_track = current_user.id
    track_object.save!
  end

  def copy_syllabus_masive(id_cycle_from_copy, id_cycle_to_copy, status_id)
    job_status = JobStatus.find_by(id: status_id)
    @msg_error = nil
    nro_syllabus_from_copy = SyllabusProgram.number_of_syllabuses(id_cycle_from_copy)
    syllabus_from_copy = Syllabus.where(cycle_id: id_cycle_from_copy).includes(:syllabus_programs => [:coordinator, :supervisor])
    current_course_id = nil
    arr_syl = []
    # syllabus_from_copy.try(:each_with_index) do |syllabo, index|
    #   syllabo_clone = create_syllabus_clone(syllabo, id_cycle_to_copy)
    #   arr_syl << syllabo_clone
    # end
    last_cycle_id = SyllabusProgram.order("id").last.syllabus.cycle_id
    pending_syllabus = list_pending_syllabus_courses(id_cycle_from_copy)
    pending_syllabus.try(:each) do |pending|
      syllabo_clone = create_syllabus_clone(pending, id_cycle_to_copy, (last_cycle_id == id_cycle_to_copy ? true : false))
      arr_syl << syllabo_clone
    end
    begin
      ApplicationRecord.transaction do
        Syllabus.import arr_syl, batch_size: 3, recursive: true
        imported_syllabuses = Syllabus.where(cycle_id: id_cycle_to_copy).select(:id)
        arr_id_syllabus = imported_syllabuses.map { |syllabus| syllabus.id }
        audit_service_syllabus = AuditService.new(
          imported_syllabuses,
          "Syllabus",
          "Proceso de duplicación generado desde el admin"
        )
        audit_service_syllabus.save_masive()
        imported_syllabus_programs = SyllabusProgram.where("syllabus_id IN (?)", arr_id_syllabus)
        audit_service_program = AuditService.new(
          imported_syllabus_programs,
          "SyllabusProgram",
          "Proceso de duplicación generado desde el admin"
        )
        audit_service_program.save_masive()
      end
    rescue ActiveRecord::RecordNotSaved => e
      @msg_error = "Ocurrio un error al guardar los datos de el silabo cuyo ID de Curso es : #{current_course_id}"
    rescue ActiveRecord::RecordInvalid => e
      @msg_error = "Registro invalido en el silabo cuyo ID de Curso es : #{current_course_id}"
    rescue ActiveRecord::RecordNotUnique => e
      @msg_error = "Error registro duplicado en silabus cuyo id de curso es: #{arr_syl.count} "
    rescue CustomExceptions::CycleZeroSyllabusCopy => e
      @msg_error = e.message
    rescue => e
      @msg_error = "Ocurrio Excepcion al copiar silabus"
      puts = e.to_s
      raise ActiveRecord::Rollback
    ensure
      if job_status.present?
        job_status.update(completed: true, progress: arr_syl.count, error: @msg_error.present?, error_message: @msg_error)
      end
    end
  end

  def create_syllabus_clone(syllabo, id_cycle_to_copy, copy_from_last_cycle)
    syllabo_clone = syllabo.deep_clone include: { syllabus_programs: { unless: lambda { |syl_p| syl_p.get_program_course.nil? || syl_p.get_program_course.deleted } } }, validate: false
    syllabo_clone.cycle_id = id_cycle_to_copy
    syllabo_clone.year = Time.current.year
    syllabo_clone.syllabus_programs.try(:each) do |syllabus_program|
      p_course = syllabus_program.get_program_course
      unless p_course.nil?
        syllabus_program.coordinator_id = p_course.coordinator_id
        syllabus_program.supervisor_id = p_course.supervisor_id
        syllabus_program.authorized = p_course.authorized
      end
      unless copy_from_last_cycle
        last_syllabus_program = SyllabusProgram.joins(:syllabus)
        last_syllabus_program = last_syllabus_program.where("syllabuses.course_id = ?", syllabo_clone.course_id)
        last_syllabus_program = last_syllabus_program.where(modality_id: syllabus_program.modality_id).order("id").last
        syllabus_program.program_json["texts"] = last_syllabus_program.program_json["texts"]
      end

      syllabus_program.state = APPROVED_STATE
    end
    if syllabo.course.program_courses.count > syllabo.syllabus_programs.count
      modalities = syllabo.syllabus_programs.map { |syl_p| syl_p.modality_id }
      syllabo.course.syllabuses.last.syllabus_programs.each do |syl_p|
        unless modalities.include?(syl_p.modality_id)
          syllabo_clone.syllabus_programs.build(
            syllabus_id: syllabo_clone.id,
            program_json: syl_p.program_json,
            modality_id: syl_p.modality_id,
            supervisor_id: syl_p.supervisor_id,
            coordinator_id: syl_p.coordinator_id,
            type_program: syl_p.type_program,
            authorized: syl_p.authorized,
            state: APPROVED_STATE,
            evaluation_system_json: syl_p.evaluation_system_json,
            evaluation_rules_json: syl_p.evaluation_rules_json,
          )
        end
      end
    end
    current_course_id = syllabo_clone.course_id
    return syllabo_clone
  end

  def list_pending_syllabus_courses(id_cycle_from_copy)
    courses = Course.includes(:syllabuses)
    syllabus_from_copy = []
    courses.try(:each) do |course|
      syllabuses = course.syllabuses.order("id")
      syllabus_from_cycle = syllabuses.select { |syllabus| syllabus.cycle_id == id_cycle_from_copy }
      if (syllabuses.count > 0 && syllabus_from_cycle.count == 0)
        syllabus_from_copy << syllabuses.last
      else
        syllabus_copy = syllabus_from_cycle.first
        syllabus_copy.syllabus_content = syllabuses.last.syllabus_content
        syllabus_from_copy << syllabus_copy
      end
    end
    return syllabus_from_copy
  end
end
