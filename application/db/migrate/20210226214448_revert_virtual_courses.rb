class RevertVirtualCourses < ActiveRecord::Migration[5.2]
  def change
    execute "UPDATE program_courses
          SET deleted= false
          WHERE catalog_code IN ('100000AN83','100000I13N','100000D04H','100000E21P','100000C21V','100000A11R')
            AND modality_id = 1;"
    execute "DELETE
          FROM program_course_careers
              USING program_courses
          WHERE program_course_careers.program_course_id = program_courses.id
            AND catalog_code IN ('100000AN83','100000I13N','100000D04H','100000E21P','100000C21V','100000A11R')
            AND modality_id = 3;"
    execute "DELETE
          FROM program_courses
          WHERE catalog_code IN ('100000AN83','100000I13N','100000D04H','100000E21P','100000C21V','100000A11R')
            AND modality_id = 3;"
    execute "UPDATE syllabus_programs AS SP
          SET modality_id = 1
          FROM syllabuses AS S
                  JOIN cycles as c ON S.cycle_id = C.id AND C.enabled = true
                  JOIN program_courses PC ON S.course_id = PC.course_id AND PC.modality_id != 2
          WHERE SP.syllabus_id = S.id
            AND PC.modality_id = 1
            AND SP.modality_id = 3
            AND PC.catalog_code IN ('100000AN83','100000I13N','100000D04H','100000E21P','100000C21V','100000A11R');"
  end
end
