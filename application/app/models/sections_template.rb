# Clase donde se almacenan las secciones de una plantilla
class SectionsTemplate < ApplicationRecord
  belongs_to :doc_template
  belongs_to :component
end
