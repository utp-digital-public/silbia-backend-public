module UtilHelper
  def getPracticeNamebyCode(code, practices)
    code = code.strip
    practices.try(:each) do |practice|
      if practice.code.upcase == code.upcase
        return practice.description.upcase
      end  
    end 
    return "" 
  end

  #
  # @param type (sumilla, fundamentacion, logro general aprendizaje)
  # @param texts ( array de text )
  #
  def getTextAreaValueofSyllabus(type, texts)
    texts.try(:each) do |text|
      if type == text["title"]
        return text["content"]
      end  
    end 
    return "" 
  end
  
  def getLaboratoryName(code_laboratory, laboratories)
    laboratories.try(:each) do |laboratory|
      if code_laboratory == laboratory.id 
        return laboratory.name
      end 
      return "Sin Laboratorio" 
    end   
  end  


end   