class Api::V1::SyllabusesController < Api::ApiV1Controller
  include ResponseJson
  include SyllabusMixins
  include InformationSources
  include DownloadSyllabus
  before_action :set_syllabus_program,
                only: [:edit, :show, :approve, :send_to_approve,
                       :refuse, :set_obs_new, :update, :export, :export_and_save]
  before_action :eval_is_approver, only: [:show, :edit]

  #valid pemision only method show and edit with Pundit
  # after_action :verify_authorized, unless: :devise_controller? , only: [:show,:edit]

  def edit
    authorize @syllabus_program
    reset_user_redirect_after_login
  end

  def show
    authorize @syllabus_program
    reset_user_redirect_after_login
  end

  def approve
    begin
      syllabus_service = SyllabusProgramService.new(syllabus_program: @syllabus_program, current_user: current_user)
      begin
        result = syllabus_service.update_status("approve_silabus")
      rescue AASM::InvalidTransition
        msg = { message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}", status: 422 }
        return json_response(msg, 422)
      end
      json_response(result)
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def send_to_approve
    begin
      ruta = "/syllabuses/email_message_success"
      syllabus_param = build_syllabus_params()
      @syllabus_program.revision_number += 1
      syllabus_service = SyllabusProgramService.new(syllabus_program: @syllabus_program, current_user: current_user)
      begin
        syllabus_service.save_syllabus(syllabus_param)
        if params[:syllabus_observations_json]
          syllabus_service.save_observations(params[:syllabus_observations_json])
        end
        result = syllabus_service.update_status("send_to_approve")
        send_email("send_to_approve")
      rescue AASM::InvalidTransition
        msg = {
          message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}",
          status: 422,
        }
        return json_response(msg, 422)
      rescue Exception => e
        msg = { message_error: "Error al procesar información: #{e.message}", status: 422 }
        return json_response(msg, 422)
      end
      json_response(result)
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def update
    begin
      syllabus_param = build_syllabus_params()
      ApplicationRecord.transaction do
        begin
          syllabus_service = SyllabusProgramService.new(
            syllabus_program: @syllabus_program,
            current_user: current_user,
          )
          syllabus_service.save_syllabus(syllabus_param)
          syllabus_service.save_observations(params[:syllabus_observations_json]) if params[:syllabus_observations_json]
          result = syllabus_service.update_status("edit_progress") if @syllabus_program.pending?
          json_response({ message: "Cambios Guardados" })
        rescue AASM::InvalidTransition
          msg = { message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}", status: 422 }
          return json_response(msg, 422)
        end
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def export_and_save
    begin
      syllabus_param = build_syllabus_params()
      ApplicationRecord.transaction do
        begin
          syllabus_service = SyllabusProgramService.new(syllabus_program: @syllabus_program, current_user: current_user)
          syllabus_service.save_syllabus(syllabus_param)
          syllabus_service.update_status("edit_progress") if @syllabus_program.pending?
          syllabus_service.save_observations(params[:syllabus_observations_json]) if params[:syllabus_observations_json]
          json_response({ message: "Cambios Guardados" })
        rescue AASM::InvalidTransition
          msg = { message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}", status: 422 }
          return json_response(msg, 422)
        end
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def refuse
    begin
      syllabus_service = SyllabusProgramService.new(syllabus_program: @syllabus_program, current_user: current_user)
      begin
        result = syllabus_service.update_status("observe")
        send_email("send_to_observate")
      rescue AASM::InvalidTransition
        msg = { message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}", status: 422 }
        return json_response(msg, 422)
      end
      return json_response(result)
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def set_obs_new
    begin
      observation_text = params[:observation]
      id_section = params[:id_section]
      begin
        syllabus_message = SyllabusMessage.new(message: observation_text, section: id_section, revision_number: @syllabus_program.revision_number, syllabus_program_id: @syllabus_program.id)
        syllabus_message.save!
        json_response({ message: "Cambios Guardados", syllabus_message_id: syllabus_message.id, syllabus_program_id: @syllabus_program.id, revision_number: @syllabus_program.revision_number })
      rescue AASM::InvalidTransition
        msg = { message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}", status: 422 }
        return json_response(msg, 422)
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def remove_message_obs
    begin
      id_message = params[:id_message]
      begin
        syllabus_message = SyllabusMessage.find(id_message)
        syllabus_message.destroy
        json_response({ message: "Cambios Guardados" })
      rescue AASM::InvalidTransition
        msg = { message_error: "Transición invalida en el estado de silabus, status silabus: #{@syllabus_program.state}", status: 422 }
        return json_response(msg, 422)
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def email_message_success
  end

  def export
    begin
      nombre_reporte = helpers.format_name_pdf_sillabus(@syllabus_program)
      orientacion = "Portrait"
      @syllabus = @syllabus_program.syllabus
      @information_sources_basic = @syllabus.course.information_sources.basic.all_relations_included.order_by_type_source
      @information_sources_complementary = @syllabus.course.information_sources.complementary.all_relations_included.order_by_type_source
      respond_to do |format|
        format.html { render template: "/syllabuses/html/template", :layout => false }
        format.pdf {
          render template: "/syllabuses/pdf/template",
            page_size: "A4",
            pdf: nombre_reporte,
            locals: { syllabus: @syllabus, syllabus_program: @syllabus_program, course: @course, information_sources_basic: @information_sources_basic, information_sources_complementary: @information_sources_complementary, careers_competition: @careers_competition },
            orientation: orientacion,
            # outline: {
            #   outline: true,
            #   outline_depth: 50 },
            margin: {
              top: 20,
              bottom: 17,
              left: 18,
              right: 20,
            },
            footer: {
              spacing: 30,
            # html: {
            #    template: '/syllabuses/pdf/pdf-footer.html'
            # }
            }
        }
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  private

  # TODO: refactoring json response includes
  def set_syllabus_program
    @show_modal_sidebar = current_user.show_modal_sidebar && !current_user.is_approver?
    @syllabus_program = SyllabusProgram.find(params[:id])
    load_observation_syllabus()
    load_preview_observation_syllabus()
    @course = @syllabus_program.syllabus.course
    @practice_list = Practice.all
    @careers_competition = @syllabus_program.get_program_course.careers.pluck("name")  #Competition.where(course_id: @course.id).distinct("career").select(:career).group(:career)
    @syllabus_rules = @syllabus_program.evaluation_rules_json.to_json
    @laboratories = Laboratory.all
    set_permision_button_json()
    detect_changes_in_syllabus()
  end

  def detect_changes_in_syllabus
    detect = SyllabusChange::DetectChange.new(@syllabus_program)
    detect.call
    @change_detect_syllabus = detect.array_changes_in_syllabus
    @auditados = @syllabus_program.audits_desc
  end

  def load_observation_syllabus
    @approver_observation_syllabus = nil
    @observation_syllabus = @syllabus_program.current_observations
    if ((@observation_syllabus.present?) && (@syllabus_program.may_edit_progress? || @syllabus_program.may_send_to_approve?))
      @approver_observation_syllabus = @syllabus_program.supervisor
    end
  end

  def load_preview_observation_syllabus
    @observation_preview_syllabus = @syllabus_program.preview_observations_news
  end

  def eval_is_approver
    @is_approver = if current_user.is_approver? then true else false end
  end

  def set_permision_button_json()
    approve_flag = save_progress_flag = send_approve_flag = observe_flag ||= false
    permision_hash = Hash.new
    approve_flag = @syllabus_program.may_approve_silabus? && policy(@syllabus_program).approve?
    save_progress_flag = policy(@syllabus_program).update?
    send_approve_flag = ((@syllabus_program.may_send_to_approve?) && (policy(@syllabus_program).send_to_approve?))
    observe_flag = ((@syllabus_program.may_observe?) && (policy(@syllabus_program).refuse?))
    permision_hash =
      {
        may_approve: approve_flag,
        may_send_to_approve: send_approve_flag,
        edit_progress: save_progress_flag,
        observe: observe_flag,
      }
    @permsions_json = permision_hash.to_json
  end

  def send_email(type)
    new_mail = Email::Message.new(syllabus_program: @syllabus_program, url_base: request.origin)
    case type
    when "send_to_approve"
      new_mail.send_message_to_approve
    when "send_to_observate"
      new_mail.send_message_to_observate
    end
  end

  def build_syllabus_params
    json_syllabus = params[:sections_template]
    program_json = params[:programs_json]
    source_information_json = params[:source_information_json]
    evaluation_system_json = params[:evaluation_system_json]
    rules_json = params[:evaluation_rules_json]
    syllabus_params = Syllabuses::SyllabusJsonParam.new(
      json_syllabus: json_syllabus,
      program_json: program_json,
      information_sources_json: source_information_json,
      evaluation_system_json: { evaluation_system: evaluation_system_json },
      evaluation_rules_json: { rules: rules_json.has_key?(:rules) ? rules_json[:rules] : [] },
    )
    syllabus_params
  end

  # Reset url redirect after login if the request came from a petition by email
  def reset_user_redirect_after_login
    session["user_return_to"] = nil if session["user_return_to"].present?
  end
end
