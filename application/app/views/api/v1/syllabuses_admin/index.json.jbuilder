json.status "success"
json.data do
  json.syllabuses do
    json.array! @syllabus_programs, partial: "api/v1/syllabuses_admin/syllabus_program_list", as: :syllabus_program
  end
  json.count_approved_syllabus @count_approved_syllabus
  json.count_inactive @count_inactive
  json.count_no_authorized @count_no_authorized
  json.states do
    json.array! @arr_states
  end
end
json.partial! "api/v1/shared/pagination", collection: @syllabus_programs
