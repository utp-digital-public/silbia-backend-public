class CreateJobStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :job_statuses do |t|
      t.integer :progress, default: 0
      t.integer :max_progress
      t.boolean :completed, default: false, null: false
      t.boolean :error, default: false, null: false
      t.text :error_message
      t.text :message, default: ""
      t.string :name, null: false
      t.timestamps
    end
  end
end
