json.status "success"
json.data do
    json.partial! "api/v1/job_statuses/job_status", job_status: @job_status    
end