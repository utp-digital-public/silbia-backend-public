FactoryBot.define do
  factory :practice do
    description { "MyString" }
    code { "MyString" }
    type_of_practice
  end
end
