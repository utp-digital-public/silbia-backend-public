require 'rails_helper'
#TODO: add test controller
RSpec.describe SyllabusesController, type: :controller do
  let!(:user){ FactoryBot.create(:user, :approver) }
  include CreationSyllabusConcern 
  include CreationInformationSourcesConcern
  before { sign_in user }
  let(:user_creator_obj) { FactoryBot.create(:user,:creator) }
  let(:user_supervisor_obj) { FactoryBot.create(:user,:approver, email: "ap@email.com") }
  let(:syllabus_master) { FactoryBot.create(:syllabus)  } 
  let(:syllabus_approved) { 
    FactoryBot.create(:syllabus_program,:sp_mathematics_pregrade,
      syllabus_id: syllabus_master.id,
      coordinator_id: user_creator_obj.id,
      supervisor_id: user_supervisor_obj.id,
      state: :approved
      )
  }
  let(:syllabus_to_be_approve) { 
    FactoryBot.create(:syllabus_program,:sp_ingles2_pregrade,
        syllabus_id: syllabus_master.id,
        coordinator_id: user_creator_obj.id,
        supervisor_id: user_supervisor_obj.id,
        state: :to_be_approved
        )
  }
  let(:syllabus_in_progress) { 
    FactoryBot.create(:syllabus_program,:sp_ingles2_pregrade,
        syllabus_id: syllabus_master.id,
        coordinator_id: user_creator_obj.id,
        supervisor_id: user_supervisor_obj.id,
        state: :in_progress
        )
  }
  let(:syllabus_in_pending) { 
    FactoryBot.create(:syllabus_program,:sp_ingles2_pregrade,
        syllabus_id: syllabus_master.id,
        coordinator_id: user_creator_obj.id,
        supervisor_id: user_supervisor_obj.id,
        state: :pending
        )
  }
  def build_parameters_to_with_link_units
    course = syllabus_with_relations.syllabus.course
    information_sources = FactoryBot.create(
      :information_source,
      :information_source_basic_with_link_units,
      course_id: course.id
    )
    information_sources_json = InformationSource.all.to_json(:include => {:link_units=>{} })
    information_sources_json = JSON.parse(information_sources_json)
    nro_link_units = LinkUnitInformationSource.all.count
    #Add 2 new links
    new_json_units =[
      {id: nil, number_unit:3,from_to_page: 23,page_unit: 34,is_checked: false, information_source_id:information_sources.id, delete_logic: false},
      {id: nil, number_unit:4,from_to_page: 11,page_unit: 22,is_checked: true, information_source_id:information_sources.id, delete_logic: false},
    ]
    information_sources_json.try(:each) do |information_source|
      information_source["link_units"].try(:each) do |link_unit|
        information_source["link_units"] = new_json_units
      end 
    end
    information_sources_json
  end 
  
  def execute_update_syllabus(id_syllabus, method_controller, section_template, 
     programs_json, information_sources_json)
    put method_controller, 
    params:{
      id: id_syllabus,
      sections_template: section_template,
      programs_json: programs_json,
      source_information_json: JSON.parse("#{information_sources_json.to_json}")
    } 
  end
  
  def valid_syllabus_refuse(syllabus_to_use)
    put "refuse", params:{ 
      id: syllabus_to_use.id,
      observation: "Una observación"
    }
    syllabus_changed = SyllabusProgram.find(syllabus_to_use.id)
    expect(syllabus_changed.state).to eq('observed')
  end
  
  def valid_syllabus_aprove(syllabus_to_use)
    put "approve", params:{ 
      id: syllabus_to_use.id
    }
    syllabus_changed = SyllabusProgram.find(syllabus_to_use.id)
    expect(syllabus_changed.state).to eq('approved')
  end  

  describe "syllabus_program#update " do
    context "as an authorized " do
      before do 

      end  
    end  
  end
  
  describe "GET#download_syllabus_excel" do
    it "return succes response" do
      get :download_total_syllabus,  format: :excel
      expect(response).to  be_successful
    end
  end
  
  describe "GET#download_syllabus_evaluation_system" do
    it "return succes response" do
      get :download_syllabus_evaluation_system,  format: :excel
      expect(response).to  be_successful
    end
  end
  describe "PUTS#update" do
    context "when user is loggin" do
      it "update syllabus and syllabus program" do
        
      end
    end
    context "when syllabus have new units to link" do
      before do
        information_sources_json = build_parameters_to_with_link_units()
        execute_update_syllabus(syllabus_with_relations.id, "update", "{}","{}",information_sources_json) 
      end
      it "save link units to syllabus" do
        expect(LinkUnitInformationSource.all.count).to eq(5)
      end
    end  
    context "when syllabus is cgt" do
      it "only modify cronogram" 
    end
    context "when syllabus is cgt but one child" do
      it "edit syllabus and syllabus program" 
    end  
  end

  describe "PUTS#export_and_save" do
    before do
      information_sources_json = build_parameters_to_with_link_units()
      execute_update_syllabus(syllabus_with_relations.id, "export_and_save", "{}","{}",information_sources_json)
    end
    context "when syllabus have unit to links" do
      it "save syllabus with unit links" do
        expect(LinkUnitInformationSource.all.count).to eq(5)
      end
    end  
  end
  
  describe "GET#EDIT" do
    context "When user is creator" do
      before(:each) do
        sign_in user_creator_obj 
        get "edit", params:{ 
          id: syllabus_approved.id
        }
      end  
      it "return http status 200" do 
        expect(response).to have_http_status(200)
      end  
    end
    context "when user is supervisor" do
      before(:each) do
        sign_in user_supervisor_obj 
        get "edit", params:{ 
          id: syllabus_approved.id
        }
      end  
      it "return http status 302 redirect" do 
        expect(response).to have_http_status(302)
      end 
    end
  end

  describe "GET#SHOW" do
    context "When user is creator" do
      before(:each) do
        sign_in user_creator_obj 
        get "show", params:{ 
          id: syllabus_approved.id
        }
      end  
      it "return http status 200" do 
        expect(response).to have_http_status(200)
      end  
    end
    context "when user is supervisor" do
      before(:each) do
        sign_in user_supervisor_obj 
        get "edit", params:{ 
          id: syllabus_approved.id
        }
      end  
      it "return http status 302 redirect" do 
        expect(response).to have_http_status(302)
      end 
    end
  end

  describe "POST#REFUSE" do
    context "when syllabus state is to be approved" do
      it "refuse syllabus succesfull" do
        valid_syllabus_refuse(syllabus_approved)
      end  
    end
    context "when syllabus state is approved" do
      it "refuse syllabus succesfull" do
        valid_syllabus_refuse(syllabus_approved)
      end 
    end
    context "when syllabus state is in progress " do
      before(:each) do
        put "refuse", params:{ 
          id: syllabus_in_progress.id,
          observation: "Una observación"
        }
      end
      it "not refuse syllabus" do
        syllabus_not_change = SyllabusProgram.find(syllabus_in_progress.id)
        expect(syllabus_not_change.state).to eq("in_progress")
      end 
      it "show message error transition state" do
        expect(response).to have_http_status(422)
        body_json =  JSON.parse(response.body)
        expect(body_json["message_error"]).to include("Transición invalida en el estado de silabus")
      end  
    end
  end

  describe "POST#APROVE" do
    context "when syllabus state is to be approved" do
      it "approve syllabus succesfull" do
        valid_syllabus_aprove(syllabus_to_be_approve)
      end  
    end
    context "when syllabus state is approved" do
      it "approve syllabus succesfull" do
        valid_syllabus_aprove(syllabus_to_be_approve)
      end 
    end
    context "when syllabus state is in progress " do
      before(:each) do
        put "approve", params:{ 
          id: syllabus_in_progress.id
        }
      end
      it "not approve syllabus" do
        syllabus_not_change = SyllabusProgram.find(syllabus_in_progress.id)
        expect(syllabus_not_change.state).to eq("in_progress")
      end 
      it "show message error transition state" do
        expect(response).to have_http_status(422)
        body_json =  JSON.parse(response.body)
        expect(body_json["message_error"]).to include("Transición invalida en el estado de silabus")
      end  
    end
  end

  describe "PUT#disable_modal_to_syllabus" do
    context "when user is loggin" do
        before do
          coordinator = FactoryBot.create(:user, :creator)
          supervisor = FactoryBot.create(:user, :approver, email: "pe@hf.com")
          syllabus = FactoryBot.create(:syllabus)
          @syllabus_program = FactoryBot.create(
            :syllabus_program, 
            syllabus_id: syllabus.id,
            coordinator_id: coordinator.id,
            supervisor_id: supervisor.id,
            show_modal: true,
            show_modal_sidebar: true
            )
           put :disable_modal_to_syllabus, params: { id: @syllabus_program.id }
        end  
        it "return success response" do
          expect(response).to be_successful
        end
        it "disable modal sucessfull" do
          syllabus_program_changed = SyllabusProgram.find(@syllabus_program.id)
          expect(syllabus_program_changed.show_modal).to eq(false)
        end  
    end  
  end  
end
