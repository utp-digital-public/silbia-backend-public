class AlterEnabledFieldToCycles < ActiveRecord::Migration[5.2]
  def change
    change_column :cycles, :enabled, :boolean, default: false, null: false
  end
end
