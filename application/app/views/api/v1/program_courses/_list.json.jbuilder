json.id program_course.id
json.weekly_hours program_course.weekly_hours
json.credits program_course.credits
json.catalog_code program_course.catalog_code
json.name program_course.name
json.course_id program_course.course_id
json.supervisor_id program_course.supervisor_id
json.type program_course.type
json.authorized program_course.authorized
json.coordinator_id program_course.coordinator_id
json.human_type program_course.human_type
json.modality program_course.modality
json.updated_at program_course.updated_at
json.course_modalities program_course.modalities
if program_course.modality_id == SEMI_PRESENCIAL_CODE
  json.can_create_modality false
elsif program_course.modality_id == VIRTUAL_CODE && program_course.modalities.count < 3 && program_course.modalities.select { |c| c["deleted"] == true && c["modality_id"] == PRESENCIAL_CODE }.count > 0
  json.can_create_modality true
elsif program_course.modality_id == PRESENCIAL_CODE && program_course.modalities.count < 3
  json.can_create_modality true
else
  json.can_create_modality false
end
