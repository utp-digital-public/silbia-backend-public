class CreateUnitInformationSources < ActiveRecord::Migration[5.2]
  def change
    create_table :unit_information_sources do |t|
      t.references :syllabus_information_source, foreign_key: true, index: { name: "ix_unit_information_source" }
      t.references :unit, foreign_key: true
      t.integer :from_page
      t.integer :to_page
      t.timestamps
    end
  end
end
