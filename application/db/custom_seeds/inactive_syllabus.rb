require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'inactive_syllabus.csv'))
puts "INACTIVE OR ACTIVE SILABUSES "
csv = CSV.parse(csv_text.scrub, headers: true)

import_obj = Import::ConvertToJson.new
cant_rows = 0
csv.each_with_index do |row, index|
  cycle_id = row["cycle"]
  type_program = row["type_program"]

  if row["inactive"].present?
    cant_rows = cant_rows+1
    catalog_code = row["catalog_code"] 
    course = import_obj.search_course_by_catalog_code(catalog_code)
    syllabus_program = import_obj.get_syllabus_program_by_course_cycle_type_program(course.id,cycle_id, type_program)
    syllabus_program.update_attributes!(is_inactive: true)
    
    puts  "NroFilaExcel: #{index+1} | SilabusProgramId: #{syllabus_program.id} | Course: #{course.name} Updated "
  end
end
puts "Update #{cant_rows} records"