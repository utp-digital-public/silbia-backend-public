class AlterMigrationUnits < ActiveRecord::Migration[5.2]
  def change
    change_column :migration_units, :number_unit, "integer USING number_unit::int", :default => 0
    change_column :migration_units, :number_week, "integer USING number_week::int", :default => 0
    change_column :migration_units, :number_session, "integer USING number_session::int", :default => 0
  end
end
