require 'rails_helper'

RSpec.describe Modality, type: :model do
  it { should validate_presence_of(:name).with_message("es requerido")}
  describe "Relations" do  
    it { should have_many(:syllabus_programs) }
  end  
end
