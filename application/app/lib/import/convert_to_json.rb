# TODO: refactor in separate clases
require 'csv'
module Import
  class ConvertToJson
    attr_accessor :cantidad, :file_csv, :cycle_id

    def initialize(file_csv= "syllabus_migrate.csv", cycle_id=nil, show_prompt=false)
        @file_csv = file_csv
        @cantidad =0
        if show_prompt.present?
          customPrompt = Import::CustomPrompt.new
          @cycle_id = customPrompt.choice_cycle if show_prompt #if ENV["show_prompt"].present?
        else
          @cycle_id = cycle_id  
        end  
    end  

    def convert_csv_to_json()
      cantidad_sin_curso =0
      code_course = ""
      unidad =0
      first_iteracion = true
      syllabus = Syllabus.new
      arr_textos = Array.new
      csv_text = File.read(Rails.root.join('lib', 'seeds_csv', @file_csv))
      csv = CSV.parse(csv_text.scrub, headers: true)
      csv.each_with_index do |row, index|
        if code_course != row["catalog_code"]
         unidad+1
         puts "----ES UN NUEVO CURSO "
         puts "FILA #{index} " + row["email_coordinator"] + " CURSO: " + row["catalog_code"]  
          ApplicationRecord.transaction do 
            begin
              syllabus = set_and_save_new_syllabus(row)
            rescue ActiveRecord::RecordNotUnique => e
              puts "Ocurrio una excepcion, Registro duplicado " + e
            end
          end  
        else
          fist_iteracion = false 
        end

        if first_iteracion == false
         puts "UNIDAD "+ unidad.to_s
        end
        code_course = row['catalog_code']
      end
      puts "Se ingresaron #{@cantidad} Silabus nuevos"
      puts "La cantidad Silabus en total son de: #{Syllabus.count}"
    end

    def search_user_by_email(email)
      email = email.delete(' ')
      user = User.where("LOWER(email) = LOWER(?)", email).first
      return nil unless user.present?
      user
    end

    def search_course_by_catalog_code(catalog_code)
      #puts "el codigo es " + catalog_code
      catalog_code = catalog_code.strip
      course = Course.where("catalog_code =? or  catalog_code_cgt =?", catalog_code, catalog_code).first
      #puts "el curso es " + course.name if course.present?
      return nil unless course.present?
      course
    end

    def search_book_by_code(code)
      return Book.where(code: code).first
    end
    
    def exist_information_source?(book, course)
      return false if ((book== nil) || (course == nil) )
      information_find = InformationSource.where(book_id: book.id, course_id: course.id)
      return true if information_find.present?
      false
    end  
    
    def get_syllabus_program_by_course_cycle_type_program(course_id, cycle_id, type_program)
      syllabus = Syllabus.where(course_id: course_id).where(cycle_academic: cycle_id).first
      syllabus_program = SyllabusProgram.where(syllabus_id: syllabus.id).where(type_program: type_program).first
      syllabus_program
    end  
    
    def set_content_json_silabus(row)
      fundamentation = row["fundamentation"] || ""
      sumilla = row["sumilla"] || ""
      specific_achievement_of_learning = row["logro_de_aprendizaje"] || ""

      obj_fundamentation = TextComponent.new(
        title: "fundamentación",
        content: fundamentation, 
        max_words: 1350,
        edit_only_pregrade: true ,
        section_template_id: 1
        )
      obj_sumilla = TextComponent.new(
        title: "sumilla",
        content: sumilla, 
        max_words: 1000,
        edit_only_pregrade: true ,
        section_template_id: 2
       )
      obj_specific_achievement_of_learning = TextComponent.new(
        title: "logro general de aprendizaje",
        content: specific_achievement_of_learning, 
        max_words: 1000,
        edit_only_pregrade: true ,
        section_template_id: 3
       )
      title = "", content = "", max_words = 200, edit_only_pregrade = true, section_template_id = 1
      result ={
        texts:[
          obj_fundamentation.as_json,
          obj_sumilla.as_json,
          obj_specific_achievement_of_learning.as_json
        ],
        #evaluation_system: set_evalution_system_silabus(row["formulas_evaluacion"],6),
        rules: set_rules_silabus( row["reglas"] )
      }.to_json

      result = JSON.parse(result)
    end

    def set_evalution_system_silabus(row_data_excel, section_id)
      arr = Array.new
      if row_data_excel.present?
        line = row_data_excel.split("\n")
        line.try(:each)  do |item|
          columns = item.split(",")
          evaluation = EvaluationSystem.new( 
            type: columns[0], 
            description: columns[2], 
            weight: columns[1], 
            observations: columns[2], 
            recoverable: false, 
            section_template_id: section_id
            )
          arr.push(evaluation)
        end  
      end
      arr  
    end

    def set_rules_silabus(rules)
      arr = Array.new
      if rules.present?
        line = rules.split("\n")
        line.try(:each) do |item|
          rule = Rule.new
          rule.name = item
          rule.editable = false
          arr.push(rule)
        end  
      end   
      arr
    end

    def populate_methodology_json_text(row)
      text_methodology = TextComponent.new(
        title: "metodología",
        content: row["metodologia"] || "" , 
        max_words: 2300,
        edit_only_pregrade: false ,
        section_template_id: 3
      )
      text_methodology
    end

    def build_units_json(course_code)
      arr_units = Array.new
      methodology_text = TextComponent.new

      csv_text = File.read(Rails.root.join('lib', 'seeds_csv', @file_csv))
      csv = CSV.parse(csv_text.scrub, headers: true)
      csv.each_with_index do |row, index|
        if course_code == row["catalog_code"]
          weeks_unit = build_weeks_for_unit(row)
          obj_unit = Unit.new(title: row["titulo_unidad"].to_s, achievement: row["logro_especifico_aprendizaje"].to_s, weeks: weeks_unit )
          arr_units.push(obj_unit)
          methodology_text = populate_methodology_json_text(row)
        end 
      end
      json_unit_hash ={units: arr_units, texts:[methodology_text] }.to_json
    end

    def build_weeks_for_unit(row)
      arr_week = Array.new
      weeks_row = row['nro_semanas_por_unidad']
      split_weeks = weeks_row.split(",") if weeks_row.present?
      split_weeks.try(:each_with_index) do |number_week, index|
        sessions_for_week = build_sessions_for__week(row, number_week, index) 
        week = Week.new(number_week: number_week, sessions: sessions_for_week )
        arr_week.push(week)
      end
      arr_week  
    end

    def get_default_session(custom_subject="")
      activity_default = get_default_activity
      session = Session.new(number_session: 1, subject: "", activities: [activity_default] )
      if custom_subject.present?
        session.subject = custom_subject
      end
      session  
    end
    
    def get_default_activity
      activity_default = Activity.new(modality:"" ,type_activity:"", label_description: "", description:"")
    end
    
    def set_evaluation_session_match(row, number_week)
     cell_csv =  row["evaluaciones_semana"] 
      if cell_csv.present?
        lines = cell_csv.split("\n")
        lines.try(:each) do |line|
          evaluation_line = line.split(":")
          number_week_line = evaluation_line[0]
          return line if number_week_line.to_i == number_week.to_i
        end  
      end
      return "" 
    end  
    
    def build_sessions_for__week( row, number_week, index)
      arr_session =[]
      # if index == 0
      #   session_cell = ""
      #   session_cell = row["tema_sesion"].to_s
      #   #session_cell = session_cell + "\n  -------------------------------------"
      #   session_cell = session_cell + row["actividades_descripcion"].to_s 
      #   arr_session = Array.new
      #   activity_default = get_default_activity
      #   session = get_default_session(session_cell)
      #   arr_session.push(session)
      # else
        #custom_subject = set_evaluation_session_match(row, number_week)  
        custom_subject = ""
        session = get_default_session(custom_subject)
        arr_session.push(session)
      # end
      # put practice in session with wee match
      arr_session
    end
    
    def set_and_save_new_syllabus(row)
      course_object = search_course_by_catalog_code(row["catalog_code"])
      puts "---buscamos coordinador---"
      user = search_user_by_email(row["email_coordinator"])
      puts "coordinador es "+ user.name
      puts "-----buscamos supervisor----"
      supervisor =  search_user_by_email(row["email_supervisor"])
      puts "supervisor es "+ supervisor.name
      syllabus = Syllabus.new
      syllabus.year = 2018
      syllabus.cycle_academic = REGULAR_CYCLE
      syllabus.doc_template_id = 1
      syllabus.curriculum_course_id =1 
      syllabus.cycle_id = @cycle_id
      syllabus.user = user
      syllabus.course_id = course_object.id
      syllabus.credits = row["credits"]
      syllabus.weekly_hours = row["hora_semana"]
      syllabus.modality = if row["modalidad"].present? then row["modalidad"].downcase else "" end
      syllabus.syllabus_content =  set_content_json_silabus( row )
      #####
      # Construimos json unidades
      json_units = build_units_json(row["catalog_code"])
      
      puts "paso construir "
      puts "LOS PARAMETORS DE SYLLABUS PROGRAM SON:"
      puts "
      json_units: #{json_units} 
      type_program: #{TYPE_PROGRAM_PREGRADO}
      state: #{NO_READ_STATE}
      "
      syllabus_param_hash = {
        program_json: JSON.parse(json_units) ,
        type_program: TYPE_PROGRAM_PREGRADO, 
        state: NO_READ_STATE, 
        coordinator_id: user.id,
        supervisor_id: supervisor.id,
        modality_id: row["modality_id"],
        evaluation_system_json:  evaluation_system_hash(row)
      }
      ####
      pregrade_syllabus = Import::SyllabusProgramPregrade.new( syllabus_param_hash)
      build_new_syllabus_program(syllabus,pregrade_syllabus)
      if course_object.catalog_code_cgt.present?
        cgt_syllabus = Import::SyllabusProgramCgt.new( syllabus_param_hash )
        build_new_syllabus_program(syllabus,cgt_syllabus)
      end   
      
      if syllabus.course_id.present? 
        puts "ENTRO AL IF"
        syllabus.save!
        puts "SE PASO AL GUARDAR"
        @cantidad +=1
        puts "Silabus saved"
      else
        puts "************ Error not found course : #{row['catalog_code']} ********"   
      end  
      syllabus
    end

    def build_new_syllabus_program(syllabus,syllabus_param_hash)
      syllabus.syllabus_programs.build(
        program_json: syllabus_param_hash.program_json ,
        type_program: syllabus_param_hash.type_program, 
        state: syllabus_param_hash.state, 
        coordinator_id: syllabus_param_hash.coordinator_id,
        supervisor_id: syllabus_param_hash.supervisor_id,
        modality_id: syllabus_param_hash.modality_id,
        evaluation_system_json:  syllabus_param_hash.evaluation_system_json
      )
    end  

    def evaluation_system_hash(row)
      { evaluation_system: set_evalution_system_silabus(row["formulas_evaluacion"],6) }
    end  
    
    def updated_catalog_cgt_to_course( course_pregrade, name_equivalent_cgt, row_code_cgt, total_records_updated)
      course_pregrade.update_columns(
        name_cgt: name_equivalent_cgt,
        catalog_code_cgt: row_code_cgt
      )
      total_records_updated = total_records_updated+1
      puts "#{course_pregrade.name}, #{course_pregrade.catalog_code} updated"
      total_records_updated
    end

  end

  

  # class ComponentSyllabus
  #   attr_accessor :section_template_id
  #   def initialize(**attributes)
  #     attributes.each do |k,v|
  #       instance_variable_set("@#{k}", v) unless v.nil?
  #     end
  #   end

  #   def to_json(*options)
  #     as_json(*options).to_json(*options)
  #   end
  # end  

  # class TextComponent < ComponentSyllabus
  #   attr_accessor :title,:content, :max_words, :edit_only_pregrade
  #   # def initialize(title = "", content = "", max_words = 200, edit_only_pregrade = true, section_template_id = 1)
  #   #   @title = title
  #   #   @content = content
  #   #   @max_words = max_words
  #   #   @edit_only_pregrade = edit_only_pregrade
  #   #   @section_template_id = section_template_id
  #   # end

  #   def as_json(options={})
  #     {
  #       type: "text",
  #       title: @title,
  #       content: @content,
  #       max_words: @max_words,
  #       edit_only_pregrade: @edit_only_pregrade,
  #       section_template_id: 1
  #     }
  #   end
  # end

  # class EvaluationSystem < ComponentSyllabus
  #   attr_accessor :type, :description, :recoverable, :weight, :observations, :rules
  
  #   def as_json(options={})
  #     {
  #       type: @type,
  #       description: @description,
  #       weight: @weight,
  #       observations: @observations,
  #       recoverable: @recoverable,
  #       section_template_id: @section_template_id
  #     }
  #   end

  # end
  
  # class Rule < ComponentSyllabus
  #   attr_accessor :name, :editable
  #   def as_json(options={})
  #     {
  #       name: @name,
  #       editable: @editable,
  #       section_template_id: @section_template_id
  #     }
  #   end
  # end
  
  # class Unit < ComponentSyllabus
  #   attr_accessor :title, :achievement, :weeks, :section_template_id
  #   def as_json(options={})
  #     {
  #       title: @title,
  #       achievement: @achievement,
  #       weeks: @weeks,
  #       section_template_id: @section_template_id
  #     }
  #   end
  # end
  
  class Week 
    attr_accessor :number_week, :sessions

    def initialize(**attributes)
      attributes.each do |k,v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end

    def as_json(options={})
      {
        number_week: @number_week,
        sessions: @sessions
      }
    end 
  end
  
  class Session
    attr_accessor :number_session, :subject, :activities, :type_laboratory, :laboratory
    
    def initialize(**attributes)
      attributes.each do |k,v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end

    def as_json(options={})
      {
        number_session: @number_session,
        subject: @subject,
        type_laboratory: @type_laboratory,
        laboratory: @laboratory,
        activities: activities
      }
    end 
  end
  
  class Activity
    attr_accessor :modality, :type_activity, :label_description,  :description

    def initialize(**attributes)
      attributes.each do |k,v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end

    def as_json(options={})
      {
        modality: @modality,
        type_activity: @type_activity,
        label_description: @label_description,
        description: @description
      }
    end 
  end  


end  