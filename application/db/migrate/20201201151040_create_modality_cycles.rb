class CreateModalityCycles < ActiveRecord::Migration[5.2]
  def change
    create_table :modality_cycles do |t|
      t.references :cycle, foreign_key: true
      t.references :modality, foreign_key: true
      t.text :footnote
      t.timestamps
    end
  end
end
