require 'csv'
### Migrate USER from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'update_credits_hours_syllabus.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
import_obj = Import::ConvertToJson.new
cant_rows = 0
puts "Se actualizara horas y creditos de los siguientes silabus"
csv.each do |row|
  # Find user with email first
  catalog_code = row["catalog_code"]
  course = import_obj.search_course_by_catalog_code(catalog_code)
  cycle_id = row["cycle_id"].strip
  hours = row["hours"].strip
  credits = row["credits"].strip
  cycle_academic = row["cycle_academic"].strip

  syllabus = Syllabus.where(course_id: course.id)
    .where(cycle_id: cycle_id)
    .where(cycle_academic: cycle_academic).first

  if syllabus.present?
    syllabus.update_attributes!(weekly_hours: hours, credits: credits)
    cant_rows = cant_rows+1
  end  
  
end
puts "Se actualizarón  #{cant_rows} registros"
#####################################################################################
