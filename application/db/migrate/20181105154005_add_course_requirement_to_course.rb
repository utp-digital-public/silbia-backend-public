class AddCourseRequirementToCourse < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :course_id_requeriment, :integer
    add_index :courses, :course_id_requeriment
  end
end
