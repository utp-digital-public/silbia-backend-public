require "csv"
### Migrate Practices from csv file  ################################################
begin
  csv_text = File.read(Rails.root.join("lib", "seeds_csv", "update_general_achievement.csv"))
  puts "UPDATE TEXTS TO SYLLABUSES) "
  cycle_id = Cycle.active.id
  csv = CSV.parse(csv_text.scrub, headers: true)

  import_obj = Import::ConvertToJson.new
  cant_rows = 0
  csv.each_with_index do |row, index|
    if row[0].present?
      catalog_code = row[0]
      cant_rows = cant_rows + 1
      general_achievement = row[1]
      course = ProgramCourse.search_course_by_catalog_code(catalog_code)
      if course.present?
        syllabus = Syllabus.where(course_id: course.course_id).where(cycle_id: cycle_id).first
        if syllabus.present? && general_achievement.present?
          puts "NroFilaExcel: #{index + 1} | Syllabus Id: #{syllabus.id} | Logro: #{syllabus.syllabus_content["texts"][2]["content"]}"
          if general_achievement.length.between?(1, 4000)
            syllabus.syllabus_content["texts"][2]["content"] = general_achievement
            syllabus.save
          else
            puts ""
          end
          puts "NroFilaExcel: #{index + 1} | Syllabus Id: #{syllabus.id} | Logro: #{syllabus.syllabus_content["texts"][2]["content"]}"
        end
      end
    end
  end
  puts "Update #{cant_rows} records"
rescue Exception => e
  puts "Exception #{e.message}"
end
