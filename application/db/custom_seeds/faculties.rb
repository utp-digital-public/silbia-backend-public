require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'faculties.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  obj = Faculty.new
  obj.code = row["code"]
  obj.name = row["name"]
  obj.save!
  puts "#{obj.code}, #{obj.name} saved"
end
puts "There are now #{Faculty.count} rows in the transactions table"