require 'csv'
### Migrate COURSE from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'information_sources.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  code_course = row['catalog_code'].strip
  code_catalog_book = row['item_number'].strip
  util = Import::Util.new
  
  course = Course.where(catalog_code: code_course).first
  book = Book.where(code: code_catalog_book).first
  
  
  if book.present? && course.present?
    unless InformationSource.where(book_id: book.id, course_id: course.id).empty?
      puts "Ya existe fuente de información con id de libro #{book.id} y id de curso #{course.id}"
    else  
      source_inf = InformationSource.new
      source_inf.book_id = book.id
      source_inf.course_id = course.id
      source_inf.type_source = row['type_source']
      source_inf.observation = row['observation']
      source_inf.save!
      puts "#{code_course}, #{code_catalog_book} saved"
    end
  end  
end
puts "There are now #{InformationSource.count} rows in the transactions table"
#####################################################################################

