require 'rails_helper'

RSpec.describe InformationSource, type: :model do
 describe "Relations" do 
  it { should belong_to(:course) }
  it { should belong_to(:book) }
  it { should have_many(:link_units) }
 end
  
end
