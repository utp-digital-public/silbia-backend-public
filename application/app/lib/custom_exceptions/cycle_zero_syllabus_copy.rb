module CustomExceptions
  class CycleZeroSyllabusCopy < StandardError 
    def initialize( cycle_name= "")
      message = "El Ciclo #{cycle_name.upcase}  no tiene ningún Silabo asignado. Elija un ciclo con Silabos a copiar"
      super(message)
    end  
  end  
  
end

