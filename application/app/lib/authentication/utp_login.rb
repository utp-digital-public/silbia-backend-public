module Authentication
  class UtpLogin
    attr_accessor :username, :password, :application_id, :ip, :url
    attr_accessor :data_response

    def initialize(**attributes)
      attributes.each do |k,v|
        instance_variable_set("@#{k}", v.delete(' ')) unless v.nil?
      end
      @url = URL_UTP_LOGIN
      @application_id = APPLICATION_ID_UTP_LOGIN
      @ip = IP_UTP_LOGIN
    end

    def authenticate?
      the_body = self.as_json
      response = HTTParty.post(@url, headers: { "Content-Type" => "application/json" }, body: the_body)
      @data_response = response
      return response["token"].present?
    end

    def get_response_login()
      @data_response
    end
    
    def as_json(options={})
      {
        username: @username,
        password: @password,
        applicationId: @application_id,
        ip: @ip
      }.to_json
    end 
    
    
  end

end  