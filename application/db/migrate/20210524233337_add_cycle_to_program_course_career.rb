class AddCycleToProgramCourseCareer < ActiveRecord::Migration[5.2]
  def change
    add_column :program_course_careers, :cycle_number, :integer, default: 0
  end
end
