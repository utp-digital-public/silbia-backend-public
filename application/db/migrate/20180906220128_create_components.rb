class CreateComponents < ActiveRecord::Migration[5.2]
  def change
    create_table :components do |t|
      t.string :name, null: false
      t.boolean :consume_data_servicio, default: false
      t.string :foto

      t.timestamps
    end
  end
end
