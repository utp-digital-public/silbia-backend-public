FactoryBot.define do
  factory :sections_template do
    doc_template
    component { nil }
    title { 'Fundamentación' }
    sort { 1 }
  end
end
