json.id syllabus_information_source.id
json.syllabus_id syllabus_information_source.syllabus_id
json.observation syllabus_information_source.observation
json.type_source syllabus_information_source.type_source
json.book syllabus_information_source.book
json.unit_information_sources do
  json.array! syllabus_information_source.unit_information_sources.sort_by { |o| o.unit.unit_number } do |uis|
    json.unit_id uis.unit.id
    json.unit_number uis.unit.unit_number
    json.from_page uis.from_page
    json.to_page uis.to_page
  end
end if syllabus_information_source.type_source == "Básica"