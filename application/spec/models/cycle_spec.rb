require 'rails_helper'

RSpec.describe Cycle, type: :model do
  it { should have_many(:syllabuses) }
  it { should validate_presence_of(:name).with_message("es requerido") }
  it { should validate_presence_of(:cycle_start).with_message("es requerido") }
  it { should validate_presence_of(:cycle_end).with_message("es requerido") }

  describe "validate unique name cycle" do
    subject { Cycle.create(name: "2019 March 1",enabled: false, cycle_start: Time.now, cycle_end: Time.now) }
     it {should validate_uniqueness_of(:name).case_insensitive.with_message("Ciclo con el mismo nombre ya existe")}
  end
  
  it "validate start date is not great that end date " do
    ## TODO 
    #pending "add some examples to (or delete) #{__FILE__}"
  end    
end
