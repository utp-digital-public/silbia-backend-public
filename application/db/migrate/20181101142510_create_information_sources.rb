class CreateInformationSources < ActiveRecord::Migration[5.2]
  def change
    create_table :information_sources do |t|
      t.references :course, foreign_key: true
      t.references :book, foreign_key: true
      t.text :observation
      t.string :type

      t.timestamps
    end
  end
end
