class ProgramCourseCareer < ApplicationRecord
  belongs_to :career
  belongs_to :program_course
  has_many :course_specific_competitions, :dependent => :destroy, :inverse_of => :program_course_career
  has_many :specific_competitions, through: :course_specific_competitions
  validates :program_course, uniqueness: { message: "ya existe", scope: :career_id }
end
