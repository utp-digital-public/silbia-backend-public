document.addEventListener("DOMContentLoaded",() =>{
    const btnDropdown = document.getElementById("btnMenu");
    const menuDropdown = document.getElementById("menuDropdown");
    if(btnDropdown && menuDropdown){
        btnDropdown.addEventListener("click" , () =>{
            return menuDropdown.classList.toggle("is-show");
        })
        
        window.addEventListener("click", (ev) => {
            let targetElement = ev.target;
            if (!btnDropdown.contains(targetElement) && !menuDropdown.contains(targetElement) ){
                return menuDropdown.classList.remove("is-show");
            }
        });
    }
  })
  