module CreationSyllabusConcern
  extend ActiveSupport::Concern
  included do
    let(:cycle_from) {FactoryBot.create(:cycle)}
    let(:syllabus_obj) {FactoryBot.create(:syllabus)}
    let(:syllabus_program) { FactoryBot.create(:syllabus_program, syllabus: syllabus_obj)}
    let(:syllabus_with_relations) { FactoryBot.create(:syllabus_program, :syllabus_with_relations) }
  end  
end
