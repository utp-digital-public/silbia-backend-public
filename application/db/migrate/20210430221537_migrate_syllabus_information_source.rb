class MigrateSyllabusInformationSource < ActiveRecord::Migration[5.2]
  def change
    begin
      execute "truncate table unit_information_sources RESTART IDENTITY;"
      execute "truncate table syllabus_information_sources RESTART IDENTITY CASCADE;"
      execute "insert into syllabus_information_sources (syllabus_id, book_id, observation, type_source, created_at, updated_at)
      select s.id, i.book_id, i.observation, i.type_source, i.created_at, i.updated_at
      from information_sources i
               join courses c on i.course_id = c.id
               join syllabuses s on c.id = s.course_id
               left join syllabus_information_sources sis
                         on s.id = sis.syllabus_id and i.book_id = sis.book_id and i.type_source = sis.type_source
      where sis.id is null ;"
      execute "insert into unit_information_sources(syllabus_information_source_id, unit_id, from_page, to_page, created_at,updated_at)
      select sis.id, u.id, luis.from_to_page, luis.page_unit, luis.created_at, luis.updated_at
      from link_unit_information_sources luis
               join information_sources i on luis.information_source_id = i.id
               join courses c on i.course_id = c.id
               join syllabuses s on c.id = s.course_id
               join syllabus_information_sources sis
                    on s.id = sis.syllabus_id and i.book_id = sis.book_id and i.type_source = sis.type_source
               join units u on s.id = u.syllabus_id and luis.number_unit = u.unit_number
               left join unit_information_sources uis on uis.unit_id = u.id and uis.syllabus_information_source_id = sis.id
      where uis.id is null"
    end
  end
end
