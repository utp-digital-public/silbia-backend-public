FactoryBot.define do
  factory :role do
   
    trait :creator do
      id { 1 }
      name { "creator" }
    end 
    trait :approver do
      id { 2 }
      name { "approver" }
    end 
  end
end
