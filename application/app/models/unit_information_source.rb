class UnitInformationSource < ApplicationRecord
  audited associated_with: :syllabus_information_source
  belongs_to :unit
  belongs_to :syllabus_information_source
end
