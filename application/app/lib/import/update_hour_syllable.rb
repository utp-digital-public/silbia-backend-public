module Import
  class UpdateHourSyllable < Import::Entity
    attr_accessor :cycle_id, :nro_not_found_syllable, :total_records_update,:arr_course_not_found

    def initialize(file_name)
      super(file_name)
      @nro_not_found_course = 0
      @nro_not_found_syllable = 0
      @total_records_update = 0
      @arr_course_not_found = []
      initilize_cycle_from_prompt()
    end
    
    def execute_update 
      total_records_update = 0
      import_obj = Import::ConvertToJson.new
      ApplicationRecord.transaction do 
        begin
          @csv.each_with_index do |row, index|
            number_row = index+1
            code_catalog = remove_space( row[0] )
            week_hour =  row[1] 
            course = import_obj.search_course_by_catalog_code( code_catalog )
            unless course.present?
              @nro_not_found_course+=1
              @arr_course_not_found << code_catalog
              mensaje =  "ERROR No se encontro el curso con catalogo #{code_catalog}, Nro Fila #{number_row}"
              Rails.logger.info mensaje
              raise Exception.new mensaje
            else
              syllable = Syllabus.where(course_id: course.id).where(cycle_id: @cycle_id).first
              update_transaction({
                syllable: syllable,
                course: course,
                week_hour: week_hour,
                code_catalog: code_catalog,
                number_row: number_row,
              })  
            end
            
          end
          
        rescue ActiveRecord::RecordInvalid => rexception
          Rails.logger.error ".........ERROR!!:........"
          Rails.logger.error "- Ocurrio una excepcion en base de datos #{rexception}"
        rescue => exception
          Rails.logger.error ".........ERROR!!:........"
          Rails.logger.error "ERROR: Ocurrio una excepcion al actualizar horas #{exception}"   
        end
      ensure
        ActiveRecord :: Rollback if ( (@nro_not_found_course>0) || (@nro_not_found_syllable>0) )
        print_results()
      end  
    end

    def print_results()
      puts "....... Resultados de Actualización........."
      if ( (@nro_not_found_course> 0) || (@nro_not_found_syllable >0) )
        puts "... Errores encontrados #{@nro_not_found_course} "
        Rails.logger.error "CURSOS NO ENCONTRADOS #{@nro_not_found_course}"
        Rails.logger.error "Silabos que no tiene un curso asignado #{@nro_not_found_syllable}"  
        puts "#{@arr_course_not_found}"
      else
        puts "Se actualizaron #{@total_records_update} registros" 
      end
       
    end  

    def update_transaction(**params)
      syllable = params[:syllable]
      if syllable.present?
        syllable.update_columns(
          weekly_hours: params[:week_hour]
        )
        @total_records_update+=1
        Rails.logger.info "SILABO #{params[:course].name}, #{params[:course].catalog_code} saved"
      else
        @nro_not_found_syllable+=1
        Rails.logger.error "NO EXISTE EL SILABO CON CODIGO #{params[:code_catalog]} Nro Fila #{params[:number_row]} "
      end
      
    end  

    def initilize_cycle_from_prompt
      customPrompt = Import::CustomPrompt.new
      if ENV["show_prompt"].present?
        @cycle_id = customPrompt.choice_cycle
      end
    end  
    
  end  
end  