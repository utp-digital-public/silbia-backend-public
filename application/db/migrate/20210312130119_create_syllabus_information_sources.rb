class CreateSyllabusInformationSources < ActiveRecord::Migration[5.2]
  def change
    create_table :syllabus_information_sources do |t|
      t.references :syllabus, foreign_key: true
      t.references :book, foreign_key: true
      t.text :observation
      t.string :type_source
      t.timestamps
    end
  end
end
