require 'rails_helper'
require 'support/features/session_admin_helpers'

RSpec.feature "Cycles", type: :feature do
  let(:cycle_one) {  FactoryBot.build_stubbed(:cycle)} 
  let(:cycle_two) {  FactoryBot.build_stubbed(:cycle)} 
  before(:all) do
    @admin= FactoryBot.create(:administrator)
    login_de_usuario(@admin.email, @admin.password)
  end
  

  context "List Cycles" do
    scenario "List all cycles" do
      FactoryBot.create_list(:cycle, 3)
      cycles_count = Cycle.all.count
      visit "/cycles"
      expect(page).to have_content "Listing Cycles"
      expect(cycles_count).to eq(3) 
      puts "paso"  
    end
  end

  context "Create new Cycle" do
    before(:each) do 
      
      login_de_usuario(@admin.email, @admin.password)
      visit "/cycles/new" 
      within("form#new_cycle") do
        fill_in "cycle[name]",	with: "nuevo"
        fill_in "cycle[cycle_start]", with: DateTime.now
        fill_in "cycle[cycle_end]", with: DateTime.now
      end
    end
    scenario "should be sucessful" do
      click_button "Save Cycle"
      expect(page).to have_content "The cycle was successfully created."  
    end
    
    scenario "Should fail without name of cycle" do
      fill_in "cycle[name]",	with: "" 
      fill_in "cycle[cycle_start]", with: DateTime.now
      fill_in "cycle[cycle_end]", with: DateTime.now
      click_button "Save Cycle"
      expect(page).to have_content "Please correct the errors below"
      expect(page).to have_content "es requerido"
    end

    scenario "should be create cycle enabled" do
      check "cycle[enabled]"
      click_button "Save Cycle"
      expect(page).to have_content "The cycle was successfully created."
      expect(page.find("#cycle_enabled")).to be_checked
    end    
  end
  
  context "Update Cycle" do
    before(:each) do 
      @obj_cycle = FactoryBot.create(:cycle)
      login_de_usuario(@admin.email, @admin.password)
      visit "/cycles/#{@obj_cycle.id}/edit" 
      within("form") do
        fill_in "cycle[name]",	with: "Otro nombre"
        fill_in "cycle[cycle_start]", with: DateTime.now
        fill_in "cycle[cycle_end]", with: DateTime.now
      end
    end

    scenario "should update correct" do
      click_button "Save Cycle"
      expect(page).to have_content "The cycle was successfully updated."
      cycle_updated = Cycle.find(@obj_cycle.id)
    end
  end 
  
  context "Remove Cycle" do
    before(:each) do 
      @obj_cycle = FactoryBot.create(:cycle)
      login_de_usuario(@admin.email, @admin.password)
    end
    scenario "remove succesfull" do 
      visit "/cycles/"
      expect { click_link 'Delete Cycle' }.to change(Cycle, :count).by(-1)
    end  
   
  end  
  
end
