#include Rails.application.routes.url_helpers
module Email
  class Message
    attr_accessor :syllabus_program, :to, :message, :date_finish, :url_base, :url_logo

    def initialize(**attributes)
      attributes.each do |k, v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end

    def send_message_to_approve
      @supervisor = @syllabus_program.supervisor
      @coordinator = @syllabus_program.coordinator
      @date_finish = "28/12/2018"
      url_access = "#{@url_base}/go_to_silabus/#{@syllabus_program.id}"

      url_path = "#{@url_base}/syllabuses/syllabus_programs/#{@syllabus_program.id}"
      title = "El Sílabo de <b style='color: black;'>#{@syllabus_program.name_syllabus}</b> está listo para su aprobación "
      description = "Revisa el Sílabos desde el 
      <a href='#{@url_base}' style='color: #24A3AB'>link</a> o búscalo en el dashboard del portal 
       <a href='#{url_access}}' style='color: #24A3AB'>Silbia</a>"
      # raise
      url_go_to = "#{@url_base}/go_to_silabus/#{@syllabus_program.id}/#{@supervisor.id}"
      SyllabusMailer.send_email_approve_syllabus(
        name: @supervisor.name,
        email: @supervisor.email,
        title: title,
        syllabus_program: @syllabus_program,
        url_access: url_access,
        message: description,
      ).deliver_later(wait: 15.seconds)
    end

    def send_message_to_observate
      @supervisor = @syllabus_program.supervisor
      @coordinator = @syllabus_program.coordinator

      url_access = "#{@url_base}/go_to_silabus/#{@syllabus_program.id}"

      url_path = "#{@url_base}/syllabuses/syllabus_programs/#{@syllabus_program.id}"
      title = "Tu Sílabo de <b style='color: black;'>#{@syllabus_program.name_syllabus} </b style='color:#ff8389'> <span style='color:#ff8389'>no fue aprobado <span> "
      description = ""
      @syllabus_program.syllabus_messages.last.message.split("\n").try(:each) do |line|
        description += "<p>#{line} </p>"
      end

      url_go_to = "#{@url_base}/go_to_silabus/#{@syllabus_program.id}"

      SyllabusMailer.send_email_observate_syllabus(
        name: @coordinator.name,
        email: @coordinator.email,
        title: title,
        syllabus_program: @syllabus_program,
        url_access: url_access,
        message: description,
      ).deliver_later(wait: 15.seconds)
    end

    def send_message_download_syllabus(user, url)
      SyllabusMailer.send_email_download_syllabus(
        name: user.name,
        email: user.email,
        url_access: url,
      ).deliver_later(wait: 15.seconds)
    end

    def send_message_to_approve_syllabus
      @supervisor = @syllabus_program.supervisor
      @coordinator = @syllabus_program.coordinator
      @url_base = "http://localhost" if @url_base.blank?
      url_access = "#{@url_base}/syllabus/#{@syllabus_program.id}"

      SyllabusMailer.send_email_to_approve_syllabus(
        user_name: @supervisor.name,
        email: @supervisor.email,
        syllabus_program: @syllabus_program,
        url_access: url_access,
        url_silbia: @url_base,
        url_logo: @url_logo,
        title: "Tienes nuevos sílabos para Revisar",
      ).deliver_later(wait: 15.seconds)
    end

    def send_message_request_edit(user, reason)
      SyllabusMailer.send_message_request_edit(
        user_name: "Equipo CALEDU",
        request_user_name: user.name,
        syllabus_program: @syllabus_program,
        reason: reason,
        title: "Tienes un solicitud de habilitación por revisar",
        url_logo: @url_logo,
      ).deliver_later(wait: 15.seconds)
    end

    def send_message_to_observate_syllabus
      @supervisor = @syllabus_program.supervisor
      @coordinator = @syllabus_program.coordinator
      @url_base = "http://localhost" if @url_base.blank?
      url_access = "#{@url_base}/syllabus/#{@syllabus_program.id}"

      SyllabusMailer.send_email_to_observe_syllabus(
        user_name: @coordinator.name,
        email: @coordinator.email,
        syllabus_program: @syllabus_program,
        url_access: url_access,
        url_silbia: @url_base,
        url_logo: @url_logo,
        title: "Tienes un sílabo en estado observado",
      ).deliver_later(wait: 15.seconds)
    end

    def send_message_request_edition
      @url_base = "http://localhost" if @url_base.blank?
      url_access = "#{@url_base}/syllabus/#{@syllabus_program.id}"
      SyllabusMailer.send_message_request_edition(
        user_name: @syllabus_program.supervisor.name,
        email: @syllabus_program.supervisor.email,
        syllabus_program: @syllabus_program,
        title: "Tienes un solicitud de edición por revisar",
        url_access: url_access,
        url_silbia: @url_base,
        url_logo: @url_logo,
      ).deliver_later(wait: 15.seconds)
    end

    def send_message_approved_syllabus
      @coordinator = @syllabus_program.coordinator
      @url_base = "http://localhost" if @url_base.blank?
      url_access = "#{@url_base}/syllabus/#{@syllabus_program.id}"

      SyllabusMailer.send_email_approved_syllabus(
        user_name: @coordinator.name,
        email: @coordinator.email,
        syllabus_program: @syllabus_program,
        url_access: url_access,
        url_silbia: @url_base,
        url_logo: @url_logo,
        title: "Tu sílabo fue aprobado",
      ).deliver_later(wait: 15.seconds)
    end
  end
end
