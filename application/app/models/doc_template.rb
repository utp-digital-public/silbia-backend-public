# Clase DocTemplate que son las plantillas de los silabus a editar
class DocTemplate < ApplicationRecord
  validates :name, :type_of_template, presence: { message: 'es requerido' }
  has_many :sections_templates, -> { order(:sort).all }
  extend Enumerize
  enumerize :type_of_template, in: { silabus: 1, examen: 2 }
end
