class Api::V1::SpecificCompetitionsController < Api::ApiV1AdminController
  def import
    @msg_error = nil
    specific_competitions = []
    careers = Career.all
    all_careers = Hash[careers.map { |x| [x[:code], x[:id]] }]
    import_array = import_params.map { |i| SpecificCompetition.new(:code => i[:code], :name => i[:name], :description => i[:description], :career_id => all_careers[i[:career_code]]) }
    begin
      specific_competitions = SpecificCompetition.import import_array, returning: [:id, :code], validate_uniqueness: true, track_validation_failures: true
      render_ok(specific_competitions)
    rescue => e
      render_internal_server_error(e.message)
    end
  end

  private

  def import_params
    params.permit(data: [
                    :code,
                    :career_code,
                    :name,
                    :description,
                  ])
      .require(:data)
  end
end
