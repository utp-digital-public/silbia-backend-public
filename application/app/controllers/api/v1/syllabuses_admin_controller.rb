class Api::V1::SyllabusesAdminController < Api::ApiV1AdminController
  before_action :set_syllabus_program, only: [:show, :update]

  def index
    query = params[:query]
    @syllabus_programs = SyllabusProgram.whit_cycle_active.joins(:syllabus => :course).includes(:coordinator).includes(:supervisor)
    @syllabus_programs = @syllabus_programs.where(id: ProgramCourse.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{query}%", "%#{query}%").map { |ff| ff.get_current_syllabus_program }.compact.map { |s| s.id }) unless query.blank?
    @syllabus_programs = @syllabus_programs.where(supervisor_id: params[:supervisor_id].to_i) unless params[:supervisor_id].blank?
    unless params[:updated_at].blank?
      from_datetime = Time.zone.strptime(params[:updated_at], "%d/%m/%Y %H:%M %p")
      @syllabus_programs = @syllabus_programs.where(updated_at: from_datetime..Time.zone.now)
    end
    @syllabus_total = @syllabus_programs
    count_all = @syllabus_total.count
    sql = @syllabus_total.group("state").select("state,COUNT(*) AS quantity").to_sql
    @states_grouped = ActiveRecord::Base.connection.execute(sql)
    @syllabus_programs = @syllabus_programs.where(state: params[:state].to_i) unless params[:state].blank? || params[:state].to_i == 0
    @count_approved_syllabus = @syllabus_programs.where(state: 4, is_inactive: false, authorized: true).count
    @count_inactive = @syllabus_programs.where(is_inactive: true).count
    @count_no_authorized = @syllabus_programs.where(authorized: false).count
    @all_states = Hash[@states_grouped.map { |s| [s["state"], s["quantity"]] }]
    @arr_states = SyllabusProgram.getArrStatesNumeric.try(:each) do |element|
      element[:quantity] = @all_states[element[:value]] || 0 unless element[:value].blank?
      element[:quantity] = count_all if element[:value] == 0
      if element[:value] != 0
        s = SyllabusProgram.new
        s.state = element[:value]
        element[:permissible_states] = s.permissible_states_for_current_state_plus_current(true)
        s = nil
      end
    end
    @syllabus_programs = @syllabus_programs.custom_order_to_list()
    @syllabus_programs = @syllabus_programs.page(params[:page])
  end

  def export_pdf
    begin
      id = params[:id]
      format_pdf = params[:format_pdf]
      @syllabus_program = SyllabusProgram.find(id)
      if @syllabus_program.present?
        pdf_url = @syllabus_program.store_pdf_s3(format_pdf)
        return render_ok(pdf_url)
      else
        render_bad_request("El silabo no existe")
      end
    rescue => e
      render_internal_server_error(e.message)
    end
  end

  # GET /syllabus_program/:id
  def show
  end

  # PUT /syllabus_program/:id
  def update
    ApplicationRecord.transaction do
      begin
        @syllabus_program.update(syllabus_program_params)
      rescue AASM::InvalidTransition
        render_bad_request("No actualizado", "Transición invalida de estado del sílabo.")
      end
    end
  end

  def export_pdf_collection
    query = params[:query]
    format_pdf = "complete"
    format_pdf = params[:format_pdf] unless params[:format_pdf].blank?
    send_email = if params[:send_email] then true else false end

    current_job_status = JobStatus.where(:job_name => "export_pdf_collection", :name => "export_pdf_#{@current_user.id.to_s}", :completed => false)
    if current_job_status.present?
      render_bad_request("Hay un proceso de descarga de silabos en curso") and return
    end

    syllabus_program_ids = SyllabusProgram.whit_cycle_active
    temp_path = false
    if params[:selected].present?
      syllabus_program_ids = syllabus_program_ids.where(id: params[:selected])
      if syllabus_program_ids.where.not(state: 4).count > 0
        temp_path = true
      end
      if syllabus_program_ids.count > 30
        render_bad_request("No puede exportar más de 30 silabos") and return
      end
    else
      syllabus_program_ids = syllabus_program_ids.where(state: 4, is_inactive: false, authorized: true)
      syllabus_program_ids = syllabus_program_ids.where(id: ProgramCourse.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{query}%", "%#{query}%").map { |ff| ff.get_current_syllabus_program }.compact.map { |s| s.id }) unless query.blank?
      syllabus_program_ids = syllabus_program_ids.where(supervisor_id: params[:supervisor_id].to_i) unless params[:supervisor_id].blank?
      unless params[:updated_at].blank?
        from_datetime = Time.zone.strptime(params[:updated_at], "%d/%m/%Y %H:%M %p")
        syllabus_program_ids = syllabus_program_ids.where(updated_at: from_datetime..Time.zone.now)
      end
    end
    syllabus_program_ids = syllabus_program_ids.select("id")
    if syllabus_program_ids.count > 1
      status = JobStatus.create!(job_name: "export_pdf_collection", name: "export_pdf_#{current_user.id.to_s}", max_progress: syllabus_program_ids.size, extra_info: { send_email: send_email })
      @job = SyllabusProgram.delay.generate_pdfs_collection(@current_user, syllabus_program_ids, status, format_pdf, send_email, temp_path)
      status.update!(
        message: "Obteniendo tus archivos...", extra_info: { id_job: @job.id, send_email: send_email, format_pdf: format_pdf, temp_path: temp_path },
      )
      @job_status = status
    else
      render_bad_request("Debe exportar como mínimo 2 silabos") and return
    end
  end

  def job_status
    @job_status = JobStatus.where(:job_name => "export_pdf_collection", :name => "export_pdf_#{@current_user.id.to_s}").last
  end

  def cancel_export
    @job_status = JobStatus.where(:job_name => "export_pdf_collection", :name => "export_pdf_#{@current_user.id.to_s}").last
    if @job_status.present?
      if @job_status.completed?
        render_bad_request("La descarga de silabos ya está completa") and return
      else
        if !@job_status.extra_info["id_job_zip"].blank?
          job = Delayed::Job.where(id: @job_status.extra_info["id_job_zip"]).first
          if job.nil?
            render_bad_request("El job ya ha terminado") and return
          elsif job.locked_at.nil? || !job.last_error.nil?
            job.delete
            @job_status.update(completed: true)
            render_ok("eliminando id_job") and return
          else
            render_bad_request("El job está en proceso") and return
          end
          render_bad_request("La descarga de silabos ya está en la etapa final") and return
        elsif @job_status.extra_info["ids_job"].present?
          jobs = Delayed::Job.where(id: @job_status.extra_info["ids_job"])
          if jobs.count > 0
            jobs.delete_all
            @job_status.update(completed: true)
            render_ok("eliminando ids_job") and return
          else
            render_bad_request("No hay silabos pendientes por generar") and return
          end
        elsif @job_status.extra_info["id_job"].present?
          job = Delayed::Job.where(id: @job_status.extra_info["id_job"]).first
          if job.nil?
            render_bad_request("Intente nuevamente") and return
          elsif job.locked_at.nil? || !job.last_error.nil?
            job.delete
            @job_status.update(completed: true)
            render_ok("eliminando id_job") and return
          else
            render_bad_request("Generación masiva de silabos en curso, intente luego") and return
          end
        end
      end
    else
      render_bad_request("No hay una descarga previa")
    end
  end

  def retry_export
    @job_status = JobStatus.where(:job_name => "export_pdf_collection", :name => "export_pdf_#{@current_user.id.to_s}").last
    if @job_status.present?
      if @job_status.completed?
        render_bad_request("La descarga de silabos ya está completa") and return
      elsif ((@job_status.progress == @job_status.max_progress) && @job_status.error)
        job = Delayed::Job.where(id: @job_status.extra_info["id_job_zip"]).first
        if job.nil?
          job_id = SyllabusProgram.delay.make_zip_s3(@current_user, @job_status.id, Cycle.active.name, @job_status.extra_info["format_pdf"], @job_status.extra_info["temp_path"], @job_status.extra_info["send_email"])
          @job_status.extra_info["id_job_zip"] = job_id.id
          @job_status.update({ extra_info: @job_status.extra_info, error: false })
        elsif !job.last_error.nil?
          job.delete
          job_id = SyllabusProgram.delay.make_zip_s3(@current_user, @job_status.id, Cycle.active.name, @job_status.extra_info["format_pdf"], @job_status.extra_info["temp_path"], @job_status.extra_info["send_email"])
          @job_status.extra_info["id_job_zip"] = job_id.id
          @job_status.update({ extra_info: @job_status.extra_info, error: false })
        else
          render_bad_request("El job no tiene errores") and return
        end
      else
        render_bad_request("No se puede reintentar") and return
      end
    else
      render_bad_request("No hay una descarga previa") and return
    end
  end

  def update_states
    if params[:state].blank?
      render_bad_request("Debe indicar el estado") and return
    end
    syllabus_program_ids = SyllabusProgram.whit_cycle_active.where(state: params[:state])
    if params[:selected].present?
      syllabus_program_ids = syllabus_program_ids.where(id: params[:selected])
    else
      query = params[:query]
      syllabus_program_ids = syllabus_program_ids.where(id: ProgramCourse.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{query}%", "%#{query}%").map { |ff| ff.get_current_syllabus_program }.compact.map { |s| s.id }) unless query.blank?
      syllabus_program_ids = syllabus_program_ids.where(supervisor_id: params[:supervisor_id].to_i) unless params[:supervisor_id].blank?
      unless params[:updated_at].blank?
        from_datetime = Time.zone.strptime(params[:updated_at], "%d/%m/%Y %H:%M %p")
        syllabus_program_ids = syllabus_program_ids.where(updated_at: from_datetime..Time.zone.now)
      end
    end
    ApplicationRecord.transaction do
      begin
        @count_update = syllabus_program_ids.update_all(state: params[:selected_state])
      rescue AASM::InvalidTransition
        render_bad_request("Transición invalida de estado de algunos sílabos.")
      rescue ArgumentError
        render_bad_request("Argumento inválido") and return
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  private

  def set_order_params
    order = if params[:order].present? then params[:order] else "asc" end
    sort = "program_courses.name"
    if params[:sort].present?
      case params[:sort]
      when "coordinator_name"
        sort = "users.name"
      when "supervisor_name"
        sort = "supervisors_syllabus_programs.name"
      else
        sort = params[:sort]
      end
    end
    @sort = if sort.present? then sort + " " + order else "" end
  end

  def syllabus_program_params
    params.permit(:enable_edit_formulas, :is_inactive, :show_modal, :state)
  end

  def set_syllabus_program
    @syllabus_program = SyllabusProgram.find(params[:id])
  end
end
