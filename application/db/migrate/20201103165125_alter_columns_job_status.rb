class AlterColumnsJobStatus < ActiveRecord::Migration[5.2]
  def change
    change_column :job_statuses, :file_paths, "jsonb USING json_build_object('file_paths',file_paths)::jsonb", default: nil, null: true
    rename_column :job_statuses, :file_paths, :extra_info
  end
end
