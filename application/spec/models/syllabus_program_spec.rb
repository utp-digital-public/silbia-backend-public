require 'rails_helper'

RSpec.describe SyllabusProgram, type: :model do
  it { should validate_presence_of(:program_json).with_message("es requerido") }
  it { should validate_presence_of(:modality_id).with_message("es requerido") }
  it "return correct array of states to syllabus progran" do
    syllabus_program_new = SyllabusProgram.new
    collection_expected =  
    [ {label:"Todos", value: ""},
      {label:"Pendiente", value:1},
      {label:"En Edición",value:2},
      {label:"En Revisión",value: 3},
      {label:"Aprobado",value:4},
      {label:"Observado", value: 5}
    ]
    expect(SyllabusProgram.getArrStates).to match_array(collection_expected)
  end
  
  describe "sort correct syllabuses for roles" do
    
    let(:syllabus_master) { FactoryBot.create(:syllabus)  } 
    let(:syllabus_pending) { FactoryBot.create(:syllabus_program, :sp_estrategia_pregrade, syllabus_id: syllabus_master.id) }
    let(:syllabus_in_progress) { FactoryBot.create(:syllabus_program, :sp_ingles1_pregrade, syllabus_id: syllabus_master.id) }
    let(:syllabus_to_be_approved) { FactoryBot.create(:syllabus_program, :sp_ingles2_pregrade, syllabus_id: syllabus_master.id) }
    let(:syllabus_approved) { FactoryBot.create(:syllabus_program, :sp_mathematics_pregrade, syllabus_id: syllabus_master.id) }
    let(:syllabus_observed) { FactoryBot.create(:syllabus_program, :sp_language_pregrade, syllabus_id: syllabus_master.id) }
    
    it "when role is approver" do 
     syllabus_programs = SyllabusProgram.all.order_to_approver
     expect(syllabus_programs).to eq([
       syllabus_to_be_approved, syllabus_in_progress,
       syllabus_pending,syllabus_approved, 
       syllabus_observed
        ])
    end
    
    it "order to creator" do 
      syllabus_programs = SyllabusProgram.all.order_to_creator
     expect(syllabus_programs).to eq([
       syllabus_pending, syllabus_in_progress,
       syllabus_to_be_approved, syllabus_approved,
       syllabus_observed
        ])
    end
  end
   
end
