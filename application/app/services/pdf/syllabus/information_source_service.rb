module Pdf
  module Syllabus
    module InformationSourceService
      def self.get_format_collection(information_sources, format="html")
        if format == "html"
          get_collection_in_html_format(information_sources)
        else
          get_collection_in_array_format(information_sources)  
        end  
      end

      def self.build_array_information_source(information_sources)
        array_information_source_formated = []
        line_item_formated = ""
        information_sources.try(:each) do |infomation_source|
          line_item_formated = get_formated_line_source_information(infomation_source) 
          array_information_source_formated.push(line_item_formated)
        end
        array_information_source_formated
      end

      def self.get_formated_line_source_information(infomation_source)
          line_item_formated = ""
          author, anio, title, editorial, observation = ""
          autor = if infomation_source.book.author.present? then "#{infomation_source.book.author.upcase}" else "" end   
          anio = "(#{infomation_source.book.year})" if infomation_source.book.year !=0 
          title = "#{infomation_source.book.title}" unless  infomation_source.book.title.blank?
          editorial = "#{infomation_source.book.editorial}" if infomation_source.book.editorial.present?
          observation = "#{infomation_source.observation}" if infomation_source.observation.present?
          line_item_formated += "#{autor}" + build_coma(anio, is_year=true)
          line_item_formated+= " #{anio}" + unless title.blank? then "" else "," end
          line_item_formated+= " #{title}" + build_coma(editorial)
          line_item_formated+= " #{editorial}" + build_coma(observation)
          line_item_formated+= " #{observation}"
          line_item_formated
      end
      
      def self.contains_units_actives?(information_source, unit)
        arr_number_units = information_source.link_units.map { |link_unit| link_unit.number_unit }
        information_source.link_units.try(:each) do |link_unit|
          if ( (link_unit.number_unit == unit["number_unit"]) &&
              (link_unit.is_checked == true) && 
              (link_unit.delete_logic == false) )
              return true
          end    
        end
        false  
      end  
      
      def self.get_collection_in_html_format(information_sources)
        array_information_sources = build_array_information_source(information_sources)
        line_item = ""
        line_item ="<ul>"
        array_information_sources.try(:each) do |element|
          line_item += "<li> #{element} </li>"
        end
        line_item +="</ul>"
        line_item  
      end
      
      def self.get_collection_in_array_format
        array_information_sources = build_array_information_source(information_sources)
      end
      
      def self.build_coma(variable, is_year=false)
        coma = ""
        if is_year.present?
          unless variable.present?
            return ""
          else 
            coma = if variable.to_i > 0 then "," else "" end 
            return coma
          end 
        end
        return "," if ((variable.present?) && (variable.blank? == false ) )
        ""  
      end 
    end  
  end  
end  
