require 'csv'
### Migrate Laboratories from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'laboratories.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
lab = Laboratory.where(id:0)
unless lab.present?
  lab_cero = Laboratory.new
  lab_cero.id = 0
  lab_cero.name= "Sin Laboratorio"
  lab_cero.type_laboratory=1
  lab_cero.save!
  puts "insert lab 0 Sin laboratorio"
end  


csv.each do |row|
  laboratory = Laboratory.new
  laboratory.name = row["name"].titleize
  laboratory.type_laboratory = row["type_laboratory"]
  laboratory.save!
  puts "#{laboratory.name}, #{laboratory.type_laboratory} saved"
end
puts "There are now #{Laboratory.count} rows in the transactions table"