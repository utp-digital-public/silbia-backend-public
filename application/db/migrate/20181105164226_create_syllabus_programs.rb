class CreateSyllabusPrograms < ActiveRecord::Migration[5.2]
  def change
    create_table :syllabus_programs do |t|
      t.jsonb :program_json, null:false , default: "{}"
      t.integer :type
      t.references :syllabus, foreign_key: true, null:false
      t.integer :state

      t.timestamps
    end
  end
end
