syllabuses = Syllabus.all.includes(:syllabus_programs)
number_row_updated = 0
syllabuses.try(:each) do |syllabus|
  rules_json = syllabus.syllabus_content["rules"]
  syllabus.syllabus_programs.try(:each) do |syllabus_program|
    syllabus_program.update!( 
      evaluation_rules_json: {rules: rules_json},
      audit_comment: "se copia el json de silabo a syllabus_program para independizar las reglas de evaluaciones"
      )
    number_row_updated += 1  
    puts "Silabo #{number_row_updated} updated!"
  end  
end  

