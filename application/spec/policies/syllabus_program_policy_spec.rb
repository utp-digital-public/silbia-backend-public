require 'rails_helper'
#TODO: edit test case
RSpec.describe SyllabusProgramPolicy do
  subject { SyllabusProgramPolicy }
  include RolesUserConcern 

  permissions :approve? do
    it "approver role can"  do
     expect(subject).to permit(approver)
    end
    it "creator role, cannot approve a silabus" do 
     expect(subject).not_to permit(creator)
    end  
  end 

  permissions :send_to_approve? do
    it "creator " do
      expect(subject).to permit(creator)
    end
    it "approver, cannot send to approve" do 
      expect(subject).not_to permit(approver)
    end    
  end
  
  permissions :refuse?  do
    it "approver " do
      expect(subject).to permit(approver)
    end
    it "creator cannot refuse silabus" do 
      expect(subject).not_to permit(creator)
    end     
  end  

  permissions :show? do
    it "approver and creator can show" do 
      expect(subject).to permit(approver)
      expect(subject).to permit(creator)
    end  
  end

  permissions :create? do
    pending "add some examples to (or delete) #{__FILE__}"
  end

  permissions :update? do
    it "Can update silabus" do 
      expect(subject).to permit(creator)
    end
    it "Can´t update silabus" do
      expect(subject).not_to permit(approver)
    end   
  end

  permissions :destroy? do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
