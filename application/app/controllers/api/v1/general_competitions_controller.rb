class Api::V1::GeneralCompetitionsController < Api::ApiV1AdminController
  before_action :set_general_competition, only: [:show, :update, :delete]

  def index
    @general_competitions = GeneralCompetition.all
  end

  def show
  end

  def update
    @general_competition.assign_attributes(general_competition_params)
    if @general_competition.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @general_competition.errors) and return
    end
    ApplicationRecord.transaction do
      begin
        @general_competition.save!()
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  def create
    @general_competition = GeneralCompetition.new(general_competition_params)
    if @general_competition.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @general_competition.errors) and return
    else
      ApplicationRecord.transaction do
        begin
          @general_competition.save!()
        rescue Exception => e
          render_internal_server_error(e.message)
        end
      end
    end
  end

  def import_courses
    @msg_error = nil
    general_competitions = []
    program_courses = ProgramCourse.active.all
    competitions = GeneralCompetition.all
    all_courses = Hash[program_courses.map { |x| [x[:catalog_code], x[:id]] }]
    all_competitions = Hash[competitions.map { |x| [x[:code], x[:id]] }]

    import_array = import_params.map { |i| CourseGeneralCompetition.new(:level => i[:level], :general_competition_id => all_competitions[i[:competition_code]], :program_course_id => all_courses[i[:catalog_code]]) }
    begin
      general_competitions = CourseGeneralCompetition.import import_array, returning: [:id], validate_uniqueness: true, track_validation_failures: true
      render_ok(general_competitions)
    rescue => e
      render_internal_server_error(e.message)
    end
  end

  private

  def general_competition_params
    params.permit(
      :code,
      :name,
      :description
    )
  end

  def import_params
    params.permit(data: [
                    :competition_code,
                    :catalog_code,
                    :level,
                  ])
      .require(:data)
  end

  def set_general_competition
    @general_competition = GeneralCompetition.find(params[:id])
  end
end
