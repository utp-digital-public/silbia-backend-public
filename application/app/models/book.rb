class Book < ApplicationRecord
  validates_numericality_of :year, message: "debe ser un numero"
  validates :code, :title, presence: { message: "es requerido" }
  validates :code, uniqueness: { message: "ya existe" }

  has_many :information_sources, :dependent => :destroy
  has_many :courses, :through => :information_sources
  has_many :syllabus_information_sources, :dependent => :destroy, :inverse_of => :book

  def label_for_tag
    "#{title}#{", (#{author})" unless author.blank?}"
  end

  def self.books_with_title_like_and_without_list_books(text, books_not_include)
    result = where("unaccent(title) ILIKE unaccent(?)", "#{text}%")
    result = result.where("id NOT IN (?)", books_not_include) if books_not_include.present?
    result
  end
end
