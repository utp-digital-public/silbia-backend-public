class CreateMigrationUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :migration_units do |t|
      t.bigint :syllabus_program_id
      t.bigint :syllabus_id
      t.integer :modality_id
      t.boolean :base_syllabus
      t.string :catalog_code
      t.string :course_name
      t.boolean :course_deleted
      t.boolean :course_authorized
      t.string :unit_title
      t.string :number_unit
      t.string :unit_achievement
      t.string :number_week
      t.string :number_session
      t.string :session_topic
      t.string :session_laboratory
      t.string :session_type_laboratory
      t.jsonb :activities
      t.bigint :idx_unit
      t.bigint :idx_week
      t.bigint :idx_session
    end
  end
end
