class FixMigrationUnits < ActiveRecord::Migration[5.2]
  def change
    execute "TRUNCATE TABLE session_topics RESTART IDENTITY;"
    execute "TRUNCATE TABLE sessions RESTART IDENTITY CASCADE;"
    execute "TRUNCATE TABLE topics RESTART IDENTITY CASCADE;"
    execute "TRUNCATE TABLE units RESTART IDENTITY CASCADE;"
    execute "TRUNCATE TABLE migration_units RESTART IDENTITY CASCADE;"
    execute "INSERT INTO migration_units(syllabus_program_id, syllabus_id, modality_id, base_syllabus, catalog_code, course_name,
                                course_deleted, course_authorized, unit_title, number_unit, unit_achievement,
                                number_week, number_session, session_topic, session_laboratory, session_type_laboratory,
                                activities, idx_unit, idx_week, idx_session)
    SELECT sp.id                                                                             syllabus_program_id,
          s.id                                                                              syllabus_id,
          sp.modality_id,
          case when sp.modality_id = base_syllabuses.base_modality then true else false end base_syllabus,
          pc.catalog_code,
          pc.name                                                                           course_name,
          pc.deleted                                                                        course_deleted,
          pc.authorized                                                                     course_authorized,
          unit ->> 'title'                                                                  unit_title,
          (unit ->> 'number_unit')::int                                                     number_unit,
          unit ->> 'achievement'                                                            unit_achievement,
          (week ->> 'number_week')::int                                                     number_week,
          (session ->> 'number_session')::int                                               number_session,
          session ->> 'subject'                                                             session_topic,
          session ->> 'laboratory'                                                          session_laboratory,
          session ->> 'type_laboratory'                                                     session_type_laboratory,
          session -> 'activities'                                                           activities,
          idx_unit,
          idx_week,
          idx_session

    from syllabus_programs sp
            join syllabuses s on sp.syllabus_id = s.id
            join cycles c on s.cycle_id = c.id
            join program_courses pc
                  on s.course_id = pc.course_id and sp.modality_id = pc.modality_id
            join (select s.id,
                          s.cycle_id,
                          case
                              when array [1,3] = array_agg(sp.modality_id::int ORDER BY sp.modality_id::int) then
                                  case when count(distinct pc.deleted) > 0 then 3 else 1 end
                              when array [1] = array_agg(sp.modality_id::int) then 1
                              when array [3] = array_agg(sp.modality_id::int) then 3
                              else 1 end base_modality
                  FROM syllabuses s
                            left join syllabus_programs sp on sp.syllabus_id = s.id and sp.modality_id != 2
                            left join program_courses pc on pc.course_id = s.course_id and pc.deleted = true

                  group by s.id, s.cycle_id) base_syllabuses
                  on s.id = base_syllabuses.id and s.cycle_id = base_syllabuses.cycle_id
            ,
        jsonb_array_elements(sp.program_json -> 'units') with ordinality as t(unit, idx_unit),
        jsonb_array_elements(unit -> 'weeks') with ordinality as r(week, idx_week),
        jsonb_array_elements(week -> 'sessions') with ordinality as q(session, idx_session)

    order by sp.id, idx_unit, (week ->> 'number_week')::int,
            (session ->> 'number_session')::int
    ;"

    execute "insert into units(syllabus_id, unit_number, title, achievement, created_at, updated_at)
    select datos.syllabus_id,
          idx_unit,
          max(datos.unit_title)                     title,
          max(coalesce(datos.unit_achievement, '')) achievement,
          now(),
          now()
    from migration_units datos
            left join units on datos.syllabus_id = units.syllabus_id and datos.idx_unit = units.unit_number
    where units.id is null
      and datos.base_syllabus = true
    group by datos.syllabus_id, datos.idx_unit
    order by 1, 2;"

    execute "insert
    into topics (unit_id, topic, created_at, updated_at)
    select id, session_topic, now(), now()
    from (
            select units.id, migration_units.session_topic
            from units
                      join migration_units
                          on units.syllabus_id = migration_units.syllabus_id and
                              units.unit_number = migration_units.idx_unit
                      left join topics t on units.id = t.unit_id and migration_units.session_topic = t.topic
            where t.id is null
              and migration_units.session_topic != ''
              and migration_units.base_syllabus = true
            ORDER BY cast(migration_units.number_session as int)
        ) t
    group by id, session_topic;"
  end
end
