class AddCoordinatorToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :coordinator_id, :integer
  end
end
