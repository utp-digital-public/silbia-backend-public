require 'rails_helper'

RSpec.describe SyllabusMessage, type: :model do
  describe "relations" do 
    it { should belong_to(:syllabus_program) }
  end 
  it { should validate_presence_of(:message).with_message("es requerido") } 
  it { should validate_presence_of(:state).with_message("es requerido") } 
end
