class CreateCareers < ActiveRecord::Migration[5.2]
  def change
    create_table :careers do |t|
    	t.string :code, null:false
    	t.integer :modality, null: false, default: 0
    	t.string :name, null:false
      t.timestamps
    end
  end
end
