#fix update associate_id reeplace into auditable_id
@audits = Audited.audit_class.where("associated_id IS NOT NULL").where(action: "create")
puts "La cantidad actualizar de audit es #{@audits.count}"
@audits.try(:each) do |audit|
  associated_id = audit.associated_id
  audit.update_columns(auditable_id: associated_id, associated_id: nil)
end  