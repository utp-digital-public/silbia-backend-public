class RollbackVirtualCourse < ActiveRecord::Migration[5.2]
  def change
    execute "UPDATE program_courses
          SET deleted= false
          WHERE catalog_code = '100000A22N'
            AND modality_id = 1;"
    execute "DELETE
          FROM program_course_careers
              USING program_courses
          WHERE program_course_careers.program_course_id = program_courses.id
            AND catalog_code = '100000A22N'
            AND modality_id = 3;"
    execute "DELETE
          FROM program_courses
          WHERE catalog_code = '100000A22N'
            AND modality_id = 3;"
    execute "UPDATE syllabus_programs AS SP
          SET modality_id = 1
          FROM syllabuses AS S
                  JOIN cycles as c ON S.cycle_id = C.id AND C.enabled = true
                  JOIN program_courses PC ON S.course_id = PC.course_id AND PC.modality_id != 2
          WHERE SP.syllabus_id = S.id
            AND PC.modality_id = 1
            AND SP.modality_id = 3
            AND PC.catalog_code = '100000A22N';"
  end
end
