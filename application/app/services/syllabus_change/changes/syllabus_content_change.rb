module SyllabusChange
  module Changes
    class SyllabusContentChange
      include SyllabusChange::Changes::DetectChangeSyllabusContent

      def initialize(has_more_than_one_state, syllabus)
        @has_more_than_one_state = has_more_than_one_state
        @is_edited = false
        @syllabus = syllabus
      end

      def exist_change_in_syllabus_content_to?(key_to_extract, auditable_objects)
        #key_to_extract = "logro general de aprendizaje"
        current_achievement = SyllabusChange::Changes::ExtractContentText.extract_to(
          @syllabus.syllabus_content,
          key_to_extract
        )
        if @has_more_than_one_state == true
          before_value = nil
          @is_edited = evaluate_if_has_changes_in_syllabus_content_to(
            key_to_extract,
            current_achievement,
            auditable_objects
          )
        else
          puts "SOLO TIENE UNO"
          content_first = set_hash_values_when_have_less_than_one_state("syllabus_content", auditable_objects)
          before_value = SyllabusChange::Changes::ExtractContentText.extract_to(
            content_first,
            key_to_extract
          )
          puts "ES #{key_to_extract.upcase}"
          puts "BEFORE #{before_value}"
          puts "ACTUAL #{current_achievement}"
          @is_edited = true if before_value != current_achievement
          return @is_edited
        end
      end
    end
  end
end
