class ApiAdminController < ActionController::API
  include Api::ErrorResponses
  include Pundit
  include SentryConf
  helper ApplicationHelper

  before_action :verify_auth_token

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def user_not_authorized(exception)
    render_forbidden
  end

  def verify_auth_token
    token = request.headers["Authorization"]
    token = token.split(" ").last if token
    unless token.blank?
      begin
        @decoded = Util::JsonWebToken.decode(token)
        puts @decoded
        @current_user = UserAdmin.find(@decoded[:user_id])
      rescue ActiveRecord::RecordNotFound => e
        render_unauthorized
      rescue JWT::DecodeError => e
        render_unauthorized
      end
    else
      render_unauthorized
    end
  end

  def render_disabled
    render json: { status: "error",
                   error: { code: 555 } }, status: :unauthorized
  end

  def render_unauthorized(description = "", reason = "", suggestion = "")
    if description != ""
      render json: { status: "error",
                    error: { description: description,
                             reason: reason,
                             suggestion: suggestion } }, status: :unauthorized
    else
      render json: { status: "error",
                    error: { description: MESSAGE_ERROR_DESCRIPTION,
                             reason: "No está autorizado para realizar esta acción.",
                             suggestion: "Revise sus credenciales" } }, status: :unauthorized
    end
  end

  def render_forbidden
    render json: { status: "error",
                  error: { description: MESSAGE_ERROR_DESCRIPTION,
                           reason: "No tiene permiso para realizar esta acción.",
                           suggestion: "Revise sus credenciales" } }, status: :forbidden
  end

  def render_bad_request(description = MESSAGE_ERROR_DESCRIPTION, reason = "No tiene permiso para realizar esta acción.", suggestion = "", details = nil)
    render json: { status: "error",
                  error: { description: description,
                           reason: reason,
                           suggestion: suggestion,
                           details: details } }, status: :bad_request
  end

  def render_internal_server_error(reason)
    render json: { status: "error",
                  error: { description: "Ocurrió un error inesperado",
                           reason: reason,
                           suggestion: "" } }, status: :internal_server_error
  end

  def render_ok(data)
    render json: { status: "success", data: data }, status: :ok
  end

  def current_user
    @current_user
  end
end
