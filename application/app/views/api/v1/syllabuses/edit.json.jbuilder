json.status "success"
json.data do
    json.syllabus_program do
        json.partial! "api/v1/syllabuses/syllabus_program", syllabus_program: @syllabus_program
    end
end