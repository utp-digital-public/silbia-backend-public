FactoryBot.define do
  factory :course do
    catalog_code "1111118998"
    name "CURSO NAME ONE"
    short_name ""
    catalog_code_cgt "C6789393939"
    for_all_career { false }
    for_all_faculties { false}
  end

  trait :course_with_information_sources do
    after(:create) { |course| create_list(:information_source, 4, course: course)}
  end
  factory :estrategia, class: Course do
    catalog_code "100000A01N"
    name "ESTRATEGIAS EN NEGOCIOS INTERNACIONALES"
    short_name ""
    catalog_code_cgt "CGT001"
    for_all_career { false }
    for_all_faculties { false }
  end

  factory :ingles1, class: Course do
    catalog_code "200000N035"
    name "INGLÉS I"
    short_name ""
    catalog_code_cgt "CGT003"
    for_all_career { false }
    for_all_faculties { true}
  end

  factory :ingles2, class: Course do
    catalog_code "100000N03I"
    name "INGLÉS II"
    short_name ""
    catalog_code_cgt "CGT002"
    for_all_career { true }
    for_all_faculties { false}
  end

  factory :math_course, class: Course do
    catalog_code "1400000023"
    name "MATEMÁTIA FINANCIERA I"
    short_name ""
    catalog_code_cgt "CGT004"
    for_all_career { false }
    for_all_faculties { false}
  end

  factory :language, class: Course do
    catalog_code "1600000043"
    name "LENGUAJE"
    short_name ""
    catalog_code_cgt "CGT0089"
    for_all_career { false }
    for_all_faculties { false}
  end
end
