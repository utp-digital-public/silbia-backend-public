require 'csv'
### Migrate COURSE from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'feb_2020_information_sources_to_delete_2.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  code_course = row['catalog_code'].strip
  code_catalog_book = row['item_number'].strip
  
  course = Course.where(catalog_code: code_course).first
  book = Book.where(code: code_catalog_book).first
  
  if book.present? && course.present?
    i_s = InformationSource.where(book_id: book.id, course_id: course.id).first
    if i_s.nil?
      puts "No existe fuente de información con id de libro #{book.id} y id de curso #{course.id} para eliminar"
    else  
      i_s.destroy
    end
  else
    puts "No existe el libro con código #{code_catalog_book}" unless book.present?
    puts "No existe el curso con código #{code_course}" unless course.present?
  end  
end
puts "There are now #{InformationSource.count} rows in the transactions table"
#####################################################################################

