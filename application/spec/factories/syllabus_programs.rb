FactoryBot.define do
  factory :syllabus_program do
    program_json { "{}" }
    type_program { 1 }
    modality
    enable_edit_formulas { false }
    coordinator { FactoryBot.build_stubbed(:user, :creator) }
    supervisor { FactoryBot.build_stubbed(:user, :approver) }
    show_modal { true }
    trait :sp_estrategia_pregrade do 
      syllabus { FactoryBot.build_stubbed(:syllabus, :syllabus_estrategia) }
      state { :pending }
    end
    # state in progress
    trait :sp_ingles1_pregrade do
      syllabus { FactoryBot.build_stubbed(:syllabus, :syllabus_ingles1)}
      state { :in_progress }
    end
    # state to be approved
    trait :sp_ingles2_pregrade do
      syllabus { FactoryBot.build_stubbed(:syllabus, :syllabus_ingles2) }
      state { :to_be_approved }
    end

    # approved
    trait :sp_mathematics_pregrade do 
      syllabus { FactoryBot.build_stubbed(:syllabus, :syllabus_mathematics)} 
      state { :approved }
    end
    
    #
    trait :sp_language_pregrade do
      syllabus { FactoryBot.build_stubbed(:syllabus, :language)}
      state { :observed }
    end
   
  end
  # Syllabus program to CGT
  factory :syllabus_program_cgt, class: SyllabusProgram do
    program_json { "{}" }
    type_program { 2 } 
    syllabus 
  end

  trait :syllabus_with_relations do
    before(:create) do |instance|
      coordinator = FactoryBot.create(:user, :creator, email: "email2@hotmail.com") 
      supervisor =  FactoryBot.create(:user, :approver, email: "ema1@hotmail.com") 
      instance.coordinator_id = coordinator.id
      instance.supervisor_id = supervisor.id
      instance.syllabus = FactoryBot.create(:syllabus)
    end
  end
end
