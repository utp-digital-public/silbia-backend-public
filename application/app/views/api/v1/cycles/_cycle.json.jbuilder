json.id cycle.id
json.name cycle.name
json.enabled cycle.enabled
json.created_at cycle.created_at
json.updated_at cycle.updated_at
json.cycle_start cycle.cycle_start
json.cycle_end cycle.cycle_end
json.can_delete cycle.can_delete
json.can_disable cycle.can_disable
json.type_cycle cycle.type_cycle
json.human_type_cycle cycle.human_type_cycle
json.syllabus_count cycle.total_syllabus
json.modality_cycles do
  json.array! cycle.modality_cycles, partial: "api/v1/cycles/modality_cycle", as: :modality_cycle
end
