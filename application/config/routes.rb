Rails.application.routes.draw do
  mount Rswag::Ui::Engine => "/api-docs"
  resources :doc_templates
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "download_syllabus_excel" => "syllabuses#download_total_syllabus", as: "download_syllabus_excel"
  get "download_syllabus_evaluation_system" => "syllabuses#download_syllabus_evaluation_system", as: "download_syllabus_evaluation_system"
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :sessions, only: :create
      resources :admin_sessions, only: :create do
        get "me" => "admin_sessions#me", as: "me", on: :collection
        post "refresh" => "admin_sessions#refresh", as: "refresh", on: :collection
      end
      resources :syllabuses, only: :index do
        collection do
          resources :syllabus_program, controller: "syllabuses", only: [:edit, :update, :show]
        end
        put "approve/:id" => "syllabuses#approve", as: "aprove_syllabus", on: :collection
        put "send_to_approve/:id" => "syllabuses#send_to_approve", as: "send_to_approve_syllabus", on: :collection
        put "refuse/:id" => "syllabuses#refuse", as: "refuse_syllabus", on: :collection
        put "set_obs_new/:id" => "syllabuses#set_obs_new", as: "set_obs_new_syllabus", on: :collection
        delete "remove_message_obs/:id_message" => "syllabuses#remove_message_obs", as: "remove_message_obs", on: :collection
        get "email_message_success" => "syllabuses#email_message_success", as: "email_message_success", on: :collection
        put "export_and_save/:id" => "syllabuses#export_and_save", as: "export_and_save", on: :collection
        get "export/:id" => "syllabuses#export", as: "export", on: :collection
        put "disable_modal/:id" => "syllabuses#disable_modal_to_syllabus", as: "disable_modal", on: :collection
        put "disable_modal_sidebar/:id" => "syllabuses#disable_modal_sidebar_to_syllabus", as: "disable_modal_sidebar", on: :collection
        ### Information Sources ########
        get "load_information_sources" => "syllabuses#load_information_sources", as: "load_information_sources", on: :collection
        get "search_information_sources" => "syllabuses#search_information_sources", as: "search_information_sources", on: :collection
        post "add_information_source" => "syllabuses#add_information_source", as: "add_information_source", on: :collection
        delete "remove_information_source/:id_information_source" => "syllabuses#remove_information_source", as: "remove_information_source", on: :collection
      end
      resources :dashboard, only: :index do
        get "filter_syllabuses" => "dashboard#filter_syllabuses", as: "filter_syllabuses", on: :collection
      end
      resources :sessions, only: :index do
        get "me" => "sessions#me", as: "me", on: :collection
        post "refresh" => "sessions#refresh", as: "refresh", on: :collection
      end
      resources :cycles do
        get "list" => "cycles#list", as: "list", on: :collection
        get "job_status" => "cycles#job_status", as: "job_status", on: :collection
        post "execute_copy" => "cycles#execute_copy", as: "execute_copy", on: :collection
      end
      resources :user_admins
      resources :syllabuses_admin do
        post "export_pdf" => "syllabuses_admin#export_pdf", as: "export_pdf", on: :collection
        post "export_pdf_collection" => "syllabuses_admin#export_pdf_collection", as: "export_pdf_collection", on: :collection
        post "update_states" => "syllabuses_admin#update_states", as: "update_states", on: :collection
        get "job_status" => "syllabuses_admin#job_status", as: "job_status", on: :collection
        post "cancel_export" => "syllabuses_admin#cancel_export", as: "cancel_export", on: :collection
        post "retry_export" => "syllabuses_admin#retry_export", as: "retry_export", on: :collection
      end
      resources :users
      resources :program_courses do
        post "cgt_course" => "program_courses#cgt_course", as: "cgt_course", on: :collection
        post "add_similar_course" => "program_courses#add_similar_course", as: "add_similar_course", on: :collection
      end
      resources :books
      resources :job_status
      resources :syllabus_programs do
        put "update_syllabus/:id" => "syllabus_programs#update_syllabus", as: "update_syllabus", on: :collection
        put "send_to_approve/:id" => "syllabus_programs#send_to_approve", as: "send_to_approve_syllabus_program", on: :collection
        put "observe/:id" => "syllabus_programs#observe", as: "observe_syllabus_program", on: :collection
        put "approve/:id" => "syllabus_programs#approve", as: "approve_syllabus_program", on: :collection
        post "request_edit/:id" => "syllabus_programs#request_edit", as: "request_edit_syllabus_program", on: :collection
        put "request_edition/:id" => "syllabus_programs#request_edition", as: "request_edition_syllabus_program", on: :collection
        put "update_text/:id" => "syllabus_programs#update_text", as: "update_text", on: :collection
        put "update_flags/:id" => "syllabus_programs#update_flags", as: "update_flags", on: :collection
        post "/:id/observations" => "syllabus_programs#create_observation", as: "create_observation_syllabus", on: :collection
        put "/observations/:id" => "syllabus_programs#update_observation", as: "update_observation_syllabus", on: :collection
        delete "/observations/:id" => "syllabus_programs#destroy_observation", as: "delete_observation_syllabus", on: :collection
        post "/:id/export_pdf" => "syllabus_programs#export_pdf", as: "export_pdf_syllabus", on: :collection
      end
      resources :practices
      resources :laboratories
      resources :general_competitions do
        post "/import_courses" => "general_competitions#import_courses", as: "general_competitions_import_courses", on: :collection
      end
      resources :specific_competitions do
        post "import" => "specific_competitions#import", as: "import_specific_competitions", on: :collection
      end
      resources :careers do
        post "/:id/import_competences" => "careers#import_competences", as: "career_import_competences", on: :collection
        post "/:id/remove_competences" => "careers#remove_competences", as: "career_remove_competences", on: :collection
        put "/:id/update_courses" => "careers#update_courses", as: "career_update_courses", on: :collection
        post "/:id/courses" => "careers#courses", as: "career_courses", on: :collection
      end
    end
  end

  mount ActionCable.server, at: "/cable"
end
