json.id syllabus_program.id
json.program_json syllabus_program.program_json
json.weekly_hours syllabus_program.weekly_hours
json.credits syllabus_program.credits
json.code_catalog_of_syllabus syllabus_program.code_catalog_of_syllabus
json.name_syllabus syllabus_program.name_syllabus
json.coordinator syllabus_program.coordinator
json.evaluation_rules_json syllabus_program.evaluation_rules_json
json.evaluation_system_json syllabus_program.evaluation_system_json
json.enable_edit_formulas syllabus_program.enable_edit_formulas
json.modality syllabus_program.modality
json.state syllabus_program.state
json.type_program syllabus_program.type_program
json.show_modal syllabus_program.show_modal
json.supervisor_id syllabus_program.supervisor_id
json.human_syllabus_state syllabus_program.aasm.human_state
json.is_base_course syllabus_program.get_program_course.is_base_course
json.is_owner policy(syllabus_program).owner?
json.revision_number syllabus_program.revision_number
#json.base_course syllabus_program.get_program_course.base_course
json.syllabus_footnote syllabus_program.get_footnote unless syllabus_program.get_footnote.nil?
json.careers syllabus_program.get_program_course.careers.pluck("name")
json.sessions do
  json.array! syllabus_program.sessions.order(:session_number) do |session|
    json.id session.id
    json.syllabus_program_id session.syllabus_program_id
    json.week_number session.week_number
    json.session_number session.session_number
    json.laboratory session.laboratory
    json.unit session.unit
    json.modality_id session.modality_id
    json.session_topics do
      json.array! session.session_topics.order(:id) do |session_topic|
        json.id session_topic.id
        json.topic session_topic.topic
        json.activities session_topic.activities
      end
    end
    json.created_at session.created_at
    json.updated_at session.updated_at
  end
end
