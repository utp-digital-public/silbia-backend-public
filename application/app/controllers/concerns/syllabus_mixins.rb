module SyllabusMixins
  extend ActiveSupport::Concern
  included do 
   
  end

  def load_course_competences
    course_id = params[:course_id]
    @competitions = Competition.where(course_id: course_id)
    #json_response(@competitions.to_json)
  end

  def disable_modal_to_syllabus
   syllabus_to_disable = get_syllabus_by_id(params[:id])
   syllabus_to_disable.update_attributes!(show_modal: false)
   render json: "ok"
  end
  
  def disable_modal_sidebar_to_syllabus
    syllabus_to_disable = get_syllabus_by_id(params[:id])
    user_to_disable = User.find(syllabus_to_disable.coordinator_id)
    user_to_disable.update_attributes!(show_modal_sidebar: false)
    render json: "ok"
  end

  def get_syllabus_by_id(id_syllabus_program)
    syllabus_to_disable = SyllabusProgram.find(id_syllabus_program)
    syllabus_to_disable
  end  

end 