require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'program_course_careers_3.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
	code = row["code"]
	career_name = row["career_name"]
	career = Career.where("unaccent(code) ILIKE unaccent(?)", code).first
	unless career.nil?
		program_course = ProgramCourse.find_by_catalog_code(row["catalog_code"])
		unless program_course.nil?
			existing_pc_career = ProgramCourseCareer.where(career_id: career.id, program_course_id: program_course.id)
			unless existing_pc_career.present?
				weekly_hours = row["weekly_hours"].to_i
				credits = row["credits"].to_i
				program_course.weekly_hours = weekly_hours
				program_course.credits = credits
				program_course.program_course_careers.build(career_id: career.id)
				begin
					program_course.save!
					puts "Se asoció el curso #{program_course.name} con la carrera #{career.name}"
				rescue Exception => e
					p "Error: #{program_course.errors.full_messages.first}"
				end
			else
				puts "Ya existe relación entre el curso #{program_course.name} y la carrera #{career.name}"
			end
		else
			puts "No hay curso con el código #{row["catalog_code"]}"
		end
	else
		puts "No hay carrera con el nombre #{career_name}"
	end
end
puts "There are now #{ProgramCourseCareer.count} rows in the transactions table"
