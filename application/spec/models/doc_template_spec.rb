require 'rails_helper'

RSpec.describe DocTemplate, type: :model do
  describe 'relaciones' do
    it { should have_many(:sections_templates) }
  end
  describe 'validaciones' do
    it { should validate_presence_of(:name).with_message('es requerido') }
    it { should validate_presence_of(:type_of_template).with_message('es requerido') }
  end
end
