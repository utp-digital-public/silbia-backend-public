class Api::V1::ProgramCoursesController < Api::ApiV1AdminController
  before_action :set_program_course, only: [:show, :update]
  before_action :set_order_params, only: [:index]

  def index
    query = params[:query]
    @program_courses = ProgramCourse.active
    @program_courses = @program_courses.where("unaccent(program_courses.name) ILIKE unaccent(?) OR unaccent(program_courses.catalog_code) ILIKE unaccent(?)", "%#{query}%", "%#{query}%") unless query.blank?
    count_all = @program_courses.count
    sql = @program_courses.group("type").select("type,COUNT(*) AS quantity").to_sql
    types_grouped = ActiveRecord::Base.connection.execute(sql)
    all_types = Hash[types_grouped.map { |s| [s["type"], s["quantity"]] }]
    @arr_types = ProgramCourse.getArrCourseTypes.try(:each) do |element|
      element[:quantity] = all_types[element[:value]] || 0 unless element[:value].blank?
      element[:quantity] = count_all if element[:value].blank?
    end
    @program_courses = @program_courses.where(type: params[:type]) unless params[:type].blank?
    @program_courses = @program_courses.where(modality_id: params[:modality_id]) unless params[:modality_id].blank?
    @program_courses = @program_courses.select("program_courses.*,(SELECT json_agg(json_build_object('id', c2.id, 'modality_id', c2.modality_id, 'deleted', c2.deleted))
    FROM program_courses c2
    where c2.course_id = program_courses.course_id) modalities")
    @program_courses = @program_courses.order(@sort) unless @sort.blank?
    @program_courses = @program_courses.page(params[:page])
    @modalities = Modality.all
  end

  # GET /program_course/:id
  def show
    if @program_course.present?
      @similar_courses = @program_course.course.program_courses.where.not(modality_id: @program_course.modality_id, deleted: true).includes(:modality)
      @book_basic_ids = Book.where(id: @program_course.course.information_sources.basic.pluck(:book_id))
      @book_complementary_ids = Book.where(id: @program_course.course.information_sources.complementary.pluck(:book_id))
      @course_modalities = @program_course.course.program_courses.select(:modality_id, :id, :deleted)
    else
      render_bad_request("El curso con id #{params[:id]} no existe")
    end
  end

  def update
    ApplicationRecord.transaction do
      begin
        if @program_course.present?
          if @program_course.update_attributes(program_course_params)
            basic_book_ids_to_delete = []
            basic_book_ids_to_add = []
            new_book_basic_ids = params[:book_basic_ids] || []
            basic_book_ids_to_delete = @program_course.course.basic_book_ids - new_book_basic_ids.map(&:to_i)
            basic_book_ids_to_add = new_book_basic_ids.map(&:to_i) - @program_course.course.basic_book_ids

            complementary_book_ids_to_delete = []
            complementary_book_ids_to_add = []
            new_book_complementary_ids = params[:book_complementary_ids] || []
            complementary_book_ids_to_delete = @program_course.course.complementary_book_ids - new_book_complementary_ids.map(&:to_i)
            complementary_book_ids_to_add = new_book_complementary_ids.map(&:to_i) - @program_course.course.complementary_book_ids

            @program_course.course.information_sources.where(book_id: basic_book_ids_to_delete).destroy_all
            @program_course.course.information_sources.where(book_id: complementary_book_ids_to_delete).destroy_all
            @program_course.course.information_sources.build(basic_book_ids_to_add.map { |id| { :book_id => id, :type_source => "Básica" } })
            @program_course.course.information_sources.build(complementary_book_ids_to_add.map { |id| { :book_id => id, :type_source => "Complementaria" } })
            unless @program_course.course.save
              render_bad_request(MESSAGE_ERROR_DESCRIPTION, "Error al actualizar", "", @program_course.errors) and return
            end
          else
            render_bad_request(MESSAGE_ERROR_DESCRIPTION, "Error al actualizar", "", @program_course.errors) and return
          end
        else
          render_bad_request("El curso con id #{params[:id]} no existe")
        end
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
  end

  def add_similar_course
    begin
      @program_course = ProgramCourse.new(program_course_modality_params)
      if params[:replace_base_course].present?
        if @program_course.modality_id == VIRTUAL_CODE
          presencial_course = ProgramCourse.where(course_id: @program_course.course_id, modality_id: PRESENCIAL_CODE).first
          presencial_course.update(deleted: true)
          if @program_course.invalid?
            render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @program_course.errors) and return
          end
          @program_course.program_course_careers.build(presencial_course.program_course_careers.map { |pcc| { career_id: pcc.career_id } })
          if @program_course.save
            s = @program_course.course.syllabuses.where(cycle_id: Cycle.active.id).first
            SyllabusProgram.where(syllabus_id: s.id, modality_id: PRESENCIAL_CODE).update(modality_id: VIRTUAL_CODE)
          else
            presencial_course.update(deleted: false)
            render_bad_request(MESSAGE_ERROR_DESCRIPTION, "Error al guardar", "", @program_course.errors) and return
          end
        else
          render_bad_request(MESSAGE_ERROR_DESCRIPTION, "Error al guardar", "", "Solo puede crearse una nueva versión de modalidad virtual") and return
        end
      else
        if @program_course.invalid?
          render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @program_course.errors) and return
        else
          base_course = ProgramCourse.where(course_id: program_course_modality_params[:course_id], deleted: false).first
          unless base_course.nil?
            @program_course.program_course_careers.build(base_course.program_course_careers.map { |pcc| { career_id: pcc.career_id } })
            if @program_course.save
              s = @program_course.course.syllabuses.where(cycle_id: Cycle.active.id).first
              syllabus_param_hash = {
                program_json: s.syllabus_programs.first.program_json,
                type_program: if @program_course.modality_id == SEMI_PRESENCIAL_CODE then TYPE_PROGRAM_CGT else TYPE_PROGRAM_PREGRADO end,
                state: NO_READ_STATE,
                coordinator_id: @program_course.coordinator_id,
                supervisor_id: @program_course.supervisor_id,
                modality_id: @program_course.modality_id,
                authorized: true,
                evaluation_system_json: { evaluation_system: [] },
                evaluation_rules_json: { rules: [] },
              }
              s.syllabus_programs.build(syllabus_param_hash)
              s.save!
            else
              render_bad_request(MESSAGE_ERROR_DESCRIPTION, "Error al guardar", "", @program_course.errors) and return
            end
          else
            render_bad_request("El curso base no existe") and return
          end
        end
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  def create
    course = Course.new(course_params.except(:program_course))
    @program_course = ProgramCourse.new(course_params[:program_course].except(:careers))
    @program_course.modality_id = PRESENCIAL_CODE
    @program_course.type = "InPerson"
    @program_course.course = course
    if course_params[:program_course][:careers].present?
      careers_id = Career.where(code: course_params[:program_course][:careers]).pluck(:id)
      @program_course.program_course_careers.build(careers_id.map { |pcc| { career_id: pcc } })
    end
    if @program_course.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @program_course.errors) and return
    end
    if course.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", course.errors) and return
    end
    begin
      if course.save!
        @program_course.save!
        s = Syllabus.new
        s.cycle_id = Cycle.active.id
        s.cycle_academic = REGULAR_CYCLE
        s.year = 2020
        s.doc_template_id = 1
        s.curriculum_course_id = 1
        s.course = course
        s.syllabus_content = Syllabus.set_content_json_silabus("", "", "")
        json_units = { units: [], texts: [Syllabus.populate_methodology_json_text("")] }.to_json

        syllabus_param_hash = {
          program_json: JSON.parse(json_units),
          type_program: TYPE_PROGRAM_PREGRADO,
          state: NO_READ_STATE,
          coordinator_id: @program_course.coordinator_id,
          supervisor_id: @program_course.supervisor_id,
          modality_id: @program_course.modality_id,
          evaluation_system_json: { evaluation_system: [] },
          evaluation_rules_json: { rules: [] },
        }
        s.syllabus_programs.build(syllabus_param_hash)
        if s.invalid?
          render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", s.errors) and return
        end
        s.save!
      end
    rescue Exception => e
      render_internal_server_error(e.message)
    end
  end

  private

  def set_order_params
    order = if params[:order].present? then params[:order] else "asc" end
    @sort = if params[:sort].present? then params[:sort] + " " + order else :name end
  end

  def program_course_params
    params.require(:program_course).permit(:catalog_code, :name, :coordinator_id, :supervisor_id, :weekly_hours, :credits, :authorized)
  end

  def program_course_modality_params
    params.require(:program_course).permit(:type, :coordinator_id, :supervisor_id, :name, :weekly_hours, :credits, :catalog_code, :course_id, :authorized, :modality_id)
  end

  def course_params
    params.require(:course).permit(:for_all_career, :for_all_faculties, program_course: [:catalog_code, :name, :coordinator_id, :supervisor_id, :weekly_hours, :credits, careers: []])
  end

  def set_program_course
    @program_course = ProgramCourse.find(params[:id])
  end
end
