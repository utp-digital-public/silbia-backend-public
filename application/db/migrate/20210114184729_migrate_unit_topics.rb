class MigrateUnitTopics < ActiveRecord::Migration[5.2]
  def change
=begin
    execute "drop table if exists tmp_migration_units;"
    execute "select sp.id,
                s.id                        syllabus_id,
                sp.modality_id,
                pc.name,
                unit ->> 'title'            title,
                unit ->> 'number_unit'      number_unit,
                unit ->> 'achievement'      achievement,
                week ->> 'number_week'       number_week,
                session ->> 'number_session' number_session,
                session ->> 'subject'       topic,
                session ->> 'laboratory' laboratory,
                session ->> 'type_laboratory' type_laboratory,
                session -> 'activities' activities,
                idx,
                idxw,
                idxs
            INTO TEMP TABLE tmp_migration_units
            from syllabus_programs sp
                  join syllabuses s on sp.syllabus_id = s.id
                  join program_courses pc
                      on s.course_id = pc.course_id and sp.modality_id = pc.modality_id
                ,
              jsonb_array_elements(sp.program_json -> 'units') with ordinality as t(unit, idx),
              jsonb_array_elements(unit -> 'weeks') with ordinality as r(week, idxw),
              jsonb_array_elements(week -> 'sessions') with ordinality as q(session, idxs)

            order by sp.id, idx, cast(week ->> 'number_week' as int)    ,
                  cast(session ->> 'number_session' as int)"
    execute "insert into units(syllabus_id, unit_number,title, achievement, created_at, updated_at)
            select datos.syllabus_id, idx, max(datos.title) title, max(datos.achievement) achievement, now(), now()
            from tmp_migration_units datos
            left join units on datos.syllabus_id  = units.syllabus_id and datos.idx = units.unit_number
            where units.id is null and datos.modality_id=1
            group by datos.syllabus_id, datos.idx
            order by 1,2"
    execute "insert into topics (unit_id, topic, created_at, updated_at)
            select units.id, tmp_migration_units.topic, now(), now()
            from units
                    join tmp_migration_units
                          on units.syllabus_id = tmp_migration_units.syllabus_id and units.unit_number = tmp_migration_units.idx
                    left join topics t on units.id = t.unit_id and tmp_migration_units.topic = t.topic
            where t.id is null and tmp_migration_units.topic != '' and tmp_migration_units.modality_id=1
            ORDER BY cast(tmp_migration_units.number_session as int)"
    execute "insert into sessions(syllabus_program_id, laboratory_id, unit_id, week_number, session_number, modality_id,created_at, updated_at)
            select t.id,
            cast(case laboratory when '' then '0' else laboratory end as int) laboratory,
            u.id,
            cast(t.number_week as int)                                        week_number,
            cast(t.number_session as int)                                     session_number,
            cast(type_laboratory as int)                                      modality_id,
            now(),
            now()
            from tmp_migration_units t
            join units u on t.syllabus_id = u.syllabus_id and t.idx = u.unit_number
            left join sessions s on u.id = s.unit_id and cast(t.number_session as int) = s.session_number
            where s.id is null
            and t.topic != ''
            order by t.id,cast(t.number_week as int) ,cast(t.number_session as int)"
    execute "insert into session_topics(session_id,topic_id,activities,created_at,updated_at)
            select s.id, tt.id, t.activities, now(), now()
            from tmp_migration_units t
                    join units u on t.syllabus_id = u.syllabus_id and t.idx = u.unit_number
                    join topics tt on u.id = tt.unit_id and tt.topic = t.topic
                    join sessions s on u.id = s.unit_id and cast(t.number_session as int) = s.session_number
                    left join session_topics st on s.id = st.session_id and u.id = st.topic_id
            where st.id is null"
  end
=end
  end
end
