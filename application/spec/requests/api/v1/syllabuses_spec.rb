require 'swagger_helper'

RSpec.describe 'api/v1/syllabuses', type: :request do

  path '/api/v1/syllabuses/syllabus_program/{id}/edit' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    get(summary: 'edit syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/syllabus_program/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    get(summary: 'show syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
    patch(summary: 'update syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
    put(summary: 'update syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/approve/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    put(summary: 'approve syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/send_to_approve/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    put(summary: 'send_to_approve syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/refuse/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    put(summary: 'refuse syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/set_obs_new/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    put(summary: 'set_obs_new syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/remove_message_obs/{id_message}' do
    # You'll want to customize the parameter types...
    parameter 'id_message', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id_message) { '123' }

    delete(summary: 'remove_message_obs syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/email_message_success' do
    get(summary: 'email_message_success syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/export_and_save/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    put(summary: 'export_and_save syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/export/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    get(summary: 'export syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/disable_modal/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    put(summary: 'disable_modal_to_syllabus syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/disable_modal_sidebar/{id}' do
    # You'll want to customize the parameter types...
    parameter 'id', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id) { '123' }

    put(summary: 'disable_modal_sidebar_to_syllabus syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/load_information_sources' do
    get(summary: 'load_information_sources syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/search_information_sources' do
    get(summary: 'search_information_sources syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/add_information_source' do
    post(summary: 'add_information_source syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses/remove_information_source/{id_information_source}' do
    # You'll want to customize the parameter types...
    parameter 'id_information_source', in: :path, type: :string
    # ...and values used to make the requests.
    let(:id_information_source) { '123' }

    delete(summary: 'remove_information_source syllabus') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end

  path '/api/v1/syllabuses' do
    get(summary: 'list syllabuses') do
      response(200, description: 'successful') do
        # You can add before/let blocks here to trigger the response code
      end
    end
  end
end
