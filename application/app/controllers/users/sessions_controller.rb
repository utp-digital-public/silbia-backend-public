# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    username = params[:user][:email].split("@")[0]
    authenticate_utp_service = AuthenticateUtpService.new( 
        username: username,
        password: params[:user][:password])
    
    # unless  authenticate_utp_service.is_valid_authentication?
    #   mensaje_failed_auth = authenticate_utp_service.message_response
    #   flash[:error] = mensaje_failed_auth["msg"]
    #   user = nil
    #   return redirect_to new_user_session_path
    # else
      user = User.find_for_database_authentication(email: params[:user][:email])
    #end  
    
    if valid_internal_autentication(user)
      super
    end  
  end


  def sign_in_with_rol
    user_id = params[:user_id]
    rol_parameter = params[:rol_parameter]
    @user = User.find(user_id)
    if rol_parameter.present?
      case rol_parameter 
      when "ap"
        session[:rol_to_use] = ROLE_APPROVER 
      when "ed"
        session[:rol_to_use] = ROLE_CREATOR  
      end
    end
    sign_in(@user)
    redirect_to "/"
    
  end
  

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end

  private 
  def valid_internal_autentication(user)
    if user.present? 
      if user.valid_password?(params[:user][:password])
        # User with two roles respond in ajax
        if ( (user.is_approver?) && (user.is_creator?) )
          @user_id = user.id
            return respond_to do |format|
            format.js
            end
        end  
        # password correct but one rol
        # super 
        return true
      else 
        #super 
        return true 
      end 
    else
      return true
    end
  end
    
end
