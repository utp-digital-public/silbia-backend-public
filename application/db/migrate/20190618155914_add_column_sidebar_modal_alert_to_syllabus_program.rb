class AddColumnSidebarModalAlertToSyllabusProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_programs, :show_modal_sidebar, :boolean, default: true
  end
end
