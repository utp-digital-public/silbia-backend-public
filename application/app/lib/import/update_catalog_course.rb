module Import
  class UpdateCatalogCourse < Import::Entity
    attr_accessor :number_row_update_cgt, :number_row_update_pregrade

    def  initialize(string_file)
      super(string_file)
      @number_row_update_cgt = 0
      @number_row_update_pregrade = 0
    end

    def update_only_catalog_code()
      import_obj = Import::ConvertToJson.new
      total_records_create = 0
      ApplicationRecord.transaction do 
        begin
          @csv.each_with_index do |row, index|
            old_catalog_code = remove_space(row['old_catalog_code'])
            new_catalog_code = remove_space(row['new_catalog_code'])
            puts "OLD CATALOG #{old_catalog_code} "
            puts "NEw catalog #{new_catalog_code}"
            course_to_update = import_obj.search_course_by_catalog_code( old_catalog_code )
            if course_to_update.present?
              course_to_update.update_columns(catalog_code: new_catalog_code)
              total_records_create+= 1
              puts "#{course_to_update.name}, #{course_to_update.catalog_code} updated"
            else
              Rails.logger.error "NO EXISTE CURSO CON CODIGO #{old_catalog_code}"
            end
          end  
          
        rescue ActiveRecord::RecordInvalid => rexception
          Rails.logger.error ".........ERROR!!:........"
          Rails.logger.error "- Ocurrio una excepcion en base de datos #{rexception}"
        rescue => exception
          Rails.logger.error ".........ERROR!!:........"
          Rails.logger.error "ERROR: Ocurrio una excepcion al actualizar horas #{exception}"   
        end
      end  
     
    end 
      
    def update_catalog_codes
      import_obj = Import::ConvertToJson.new
      total_records_create = 0
      @csv.each_with_index do |row, index|
        previous_code_pre = remove_space(row['previous_code_pre'])
        new_catalog_code_pre = remove_space(row['new_catalog_code_pre'])
        previous_catalog_code_cgt = remove_space(row['previous_catalog_code_cgt'])
        new_catalog_code_cgt = remove_space(row['new_catalog_code_cgt'])
        name_equivalent_cgt = remove_space(row['name_equivalent_cgt'])

        course_to_update = import_obj.search_course_by_catalog_code(previous_code_pre)
        if course_to_update.present?
          update_model(course_to_update, new_catalog_code_pre,new_catalog_code_cgt, name_equivalent_cgt)
          total_records_create = total_records_create+1
          puts "#{course_to_update.name}, #{course_to_update.catalog_code} saved"
        else
          puts "NO EXISTE CURSO CON CODIGO #{previous_code_pre}"
        end
      end
      puts "There are Total  #{Course.count} rows in the transactions table"
      puts "Total Records was  #{total_records_create} updated"
      puts "Of wich #{@number_row_update_cgt} in CGT was updated"
    end

    def update_model(course_to_update, new_catalog_code_pre, new_catalog_code_cgt, name_equivalent_cgt)
      if new_catalog_code_cgt.present?
        @number_row_update_pregrade+=1
        course_to_update.update_columns(
          catalog_code: new_catalog_code_pre,
          catalog_code_cgt: new_catalog_code_cgt,
          name_cgt: name_equivalent_cgt
        )
        
      else
        @number_row_update_cgt+=1
        course_to_update.update_columns(
          catalog_code: new_catalog_code_pre
        )
       
      end  
    end  

    
  end  
end  