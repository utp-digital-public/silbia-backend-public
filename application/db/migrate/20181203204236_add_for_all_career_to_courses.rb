class AddForAllCareerToCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :for_all_career, :boolean, default: false
  end
end
