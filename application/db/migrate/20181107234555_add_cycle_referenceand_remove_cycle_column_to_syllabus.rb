class AddCycleReferenceandRemoveCycleColumnToSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_reference  :syllabuses, :cycle, foreign_key: true
  end
end
