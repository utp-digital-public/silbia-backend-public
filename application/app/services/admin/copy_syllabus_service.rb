module Admin
  class CopySyllabusService
    attr_accessor :msg_error, :id_cycle_to_copy, :id_cycle_from_copy

    def initialize(**attributes)
      attributes.each do |k, v|
        instance_variable_set("@#{k}", v) unless v.nil?
      end
    end

    def valid_cycle_have_not_syllabus_assigned?
      nro_syllabus_to_copy = SyllabusProgram.number_of_syllabuses(@id_cycle_to_copy)
      if nro_syllabus_to_copy > 0
        @msg_error = "Operación cancelada, Este ciclo ya contiene silabos asignados. Elija otro ciclo"
        return false
      end
      true
    end

    def copy_syllabus_masive
      @msg_error = nil
      nro_syllabus_from_copy = SyllabusProgram.number_of_syllabuses(@id_cycle_from_copy)
      syllabus_from_copy = Syllabus.where(cycle_id: @id_cycle_from_copy).includes(:syllabus_programs => [:coordinator, :supervisor])
      current_course_id = nil
      begin
        if nro_syllabus_from_copy <= 0
          cycle = Cycle.find(@id_cycle_from_copy)
          raise CustomExceptions::CycleZeroSyllabusCopy.new(cycle.name)
        end
        ApplicationRecord.transaction do
          arr_syl = convert_syllabus_in_array(syllabus_from_copy)
          Syllabus.import arr_syl, batch_size: 3, recursive: true
          imported_syllabuses = Syllabus.where(cycle_id: @id_cycle_to_copy)
          arr_id_syllabus = imported_syllabuses.map { |syllabus| syllabus.id }
          #build_syllabo_cgt_from_pregrade(arr_id_syllabus)
          save_model_audit_to_syllabus_copied(imported_syllabuses, arr_id_syllabus)
        end
      rescue ActiveRecord::RecordNotSaved => e
        @msg_error = "Ocurrio un error al guardar los datos de el silabo cuyo ID de Curso es : #{current_course_id}"
      rescue ActiveRecord::RecordInvalid => e
        @msg_error = "Registro invalido en el silabo cuyo ID de Curso es : #{current_course_id}"
      rescue ActiveRecord::RecordNotUnique => e
        @msg_error = "Error registro duplicado en silabus cuyo id de curso es: #{current_course_id} "
      rescue CustomExceptions::CycleZeroSyllabusCopy => e
        @msg_error = e.message
      rescue => e
        puts "Ocurrio Excepcion al copiar silabus"
        puts e.to_s
        raise ActiveRecord::Rollback
      end
    end

    def build_syllabo_cgt_from_pregrade(arr_id_syllabus)
      arr_syllabus_insert = []
      syllabus_programs = SyllabusProgram.where("syllabus_id IN (?)", arr_id_syllabus).includes(:syllabus => :course)
      syllabus_programs.try(:each) do |syllabus_program|
        syllabus_cgt_to_clone = syllabus_program.deep_clone
        if syllabus_program.syllabus.course.catalog_code_cgt.present?
          syllabus_cgt_to_clone.type_program = CGT_PROGRAM_CODE
          syllabus_cgt_to_clone.modality_id = SEMI_PRESENCIAL_CODE
          arr_syllabus_insert << syllabus_cgt_to_clone
        end
      end
      SyllabusProgram.import arr_syllabus_insert
    end

    private

    def convert_syllabus_in_array(syllabus_from_copy)
      arr_syl = []
      syllabus_from_copy.try(:each_with_index) do |syllabo, index|
        syllabo_clone = syllabo.deep_clone include: :syllabus_programs, validate: false
        syllabo_clone.cycle_id = @id_cycle_to_copy
        syllabo_clone.year = Time.current.year
        syllabo_clone.syllabus_programs.try(:each) do |syllabus_program|
          p_course = syllabus_program.get_program_course
          unless p_course.nil?
            syllabus_program.coordinator_id = p_course.coordinator_id
            syllabus_program.supervisor_id = p_course.supervisor_id
          end
          syllabus_program.state = APPROVED_STATE
        end
        current_course_id = syllabo_clone.course_id
        arr_syl << syllabo_clone
      end
      arr_syl
    end

    def save_model_audit_to_syllabus_copied(imported_syllabuses, arr_id_syllabus)
      audit_service = Admin::AuditableSyllabusService.new(
        imported_syllabuses,
        "Syllabus",
        "Proceso de duplicación generado desde el admin"
      )
      audit_service.save_masive()
      imported_syllabus_programs = SyllabusProgram.where("syllabus_id IN (?)", arr_id_syllabus)
      audit_service_program = Admin::AuditableSyllabusService.new(
        imported_syllabus_programs,
        "SyllabusProgram",
        "Proceso de duplicación generado desde el admin"
      )
      audit_service_program.save_masive()
    end
  end
end
