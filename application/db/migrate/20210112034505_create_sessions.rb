class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.references :syllabus_program, foreign_key: true
      t.references :laboratory, foreign_key: true
      t.references :unit, foreign_key: true
      t.integer :week_number, null: false, default: 1
      t.integer :session_number, null: false, default: 1
      t.string :modality, null: false, default: ""
      t.string :title, null: false, default: ""
      t.timestamps
    end
  end
end
