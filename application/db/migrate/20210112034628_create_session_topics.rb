class CreateSessionTopics < ActiveRecord::Migration[5.2]
  def change
    create_table :session_topics do |t|
      t.references :session, foreign_key: true
      t.references :unit, foreign_key: true
      t.jsonb :activities, null: false, default: "{}"
      t.timestamps
    end
  end
end
