module Import
  class SyllabusProgramPregrade < Import::SyllabusProgramBuild
    def initialize(**attributes)
      super
      @type_program = TYPE_PROGRAM_PREGRADO
      @modality_id = PRESENCIAL_CODE
    end
  end  
end  