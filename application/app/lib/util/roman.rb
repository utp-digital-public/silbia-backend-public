module Util
  class Roman
    def value(r) 
      if (r == 'I') 
        return 1
      end    
      if (r == 'V') 
        return 5
      end    
      if (r == 'X') 
        return 10
      end    
      if (r == 'L') 
        return 50
      end    
      if (r == 'C') 
        return 100
      end    
      if (r == 'D') 
        return 50
      end    
      if (r == 'M') 
        return 1000
      end    
      return -1
    end

    def romanToDecimal(str) 
      res = 0
      i = 0
      # skyp if text only have 3 character
      return str if str.length>3

      while (i < str.length) 
        # Getting value of symbol s[i] 
        s1 = value(str[i]) 

        if (i+1 < str.length) 
          # Getting value of symbol s[i+1] 
          s2 = value(str[i+1]) 

          # Comparing both values 
            if (s1 >= s2) 
              # Value of current symbol is greater 
              # or equal to the next symbol 
              res = res + s1 
              i = i + 1
            else 
              # Value of current symbol is greater 
              # or equal to the next symbol 
              res = res + s2 - s1 
              i = i + 2
            end    
        else 
            res = res + s1 
            i = i + 1
        end
      end  
      return res 
    end  

  end  
end  