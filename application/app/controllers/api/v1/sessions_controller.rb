class Api::V1::SessionsController < Api::ApiV1Controller
    skip_before_action :verify_auth_token, only: [:create]
    skip_before_action :verify_enabled_user, only: [:create]
    skip_before_action :verify_user_role, only: [:create,:me, :refresh]
  
    def create
      params.require(:username)
      params.require(:password)
      begin
        @user = User.find_for_database_authentication(email: params[:username])
        if @user.present? 
          if @user.valid_password?(params[:password])     
            @time = Time.now + token_duration
            @payload = {user_id: @user.id, email: @user.email}
            @token = Util::JsonWebToken.encode(@payload,@time)
          else 
            render_unauthorized("No se pudo iniciar sesión", "Correo electrónico y/o Contraseña incorrecta.", "Revise sus credenciales")
          end
        else
          render_unauthorized("No se pudo iniciar sesión", "No reconocemos la dirección de email, comunicarse con CALEDU.", "Revise sus credenciales")
        end 
      rescue Exception => e
        render_internal_server_error(e.message)
      end
    end
    def me
      @user = @current_user
    end
    def refresh
      @time = Time.now + token_duration
      @payload = @decoded
      @token = Util::JsonWebToken.encode(@payload,@time)
      @user = @current_user
    end
    private
      def token_duration
        token_duration = 60.to_i 
        if ENV['JWT_EXPIRE_MINUTES']
          token_duration = ENV['JWT_EXPIRE_MINUTES'].to_i
        end
        return token_duration.minutes.to_i
      end
  end