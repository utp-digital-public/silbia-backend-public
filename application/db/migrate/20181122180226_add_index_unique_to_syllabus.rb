class AddIndexUniqueToSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_index :syllabuses, [:cycle_academic, :course_id, :cycle_id], unique: true
  end
end
