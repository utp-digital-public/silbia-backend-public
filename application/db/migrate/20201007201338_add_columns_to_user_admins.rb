class AddColumnsToUserAdmins < ActiveRecord::Migration[5.2]
  def change
    add_column :user_admins, :last_name, :string, :default => ""
    remove_column :user_admins, :name
    remove_column :users, :admin
  end
end
