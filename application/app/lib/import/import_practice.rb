module Import
  class ImportPractice < Import::Entity
    def update_type_of_practice_to_practices
      total_records_create = 0
      @csv.each_with_index do |row, index|
        type_of_practice_id = row["type_of_practice_id"].strip.to_i
        id_practice = row["id"].strip.to_i
        practice = Practice.find(id_practice) 
        if practice.present?
          practice.update_columns(type_of_practice_id: type_of_practice_id)
          total_records_create = total_records_create+1
          puts "#{practice.description} updated"
        else
          puts "NO EXISTE practica con ID #{practice.id}"
        end
      end
      puts "New records was  #{total_records_create} updated"
      puts "There are Total  #{Practice.count} rows in the transactions table"
    end
    
  end  
end  