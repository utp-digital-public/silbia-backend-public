Trestle.resource(:users) do
	menu do
    item :users, icon: "fa fa-star", label: "Usuarios"
  end

  scope :approvers, -> { User.with_role(:approver) }, label: "Aprobadores"
  scope :editors, -> { User.with_role(:creator) }, label: "Editores"

  search do |query, params|
    if query
      collection.where("unaccent(users.name) ILIKE unaccent(?)", "%#{query}%")
    else
      collection
    end
  end
	
	table do
    column :name, header: "Nombre completo", sort: :name
    column :email, header: "Correo electrónico"
    column :roles, ->(user) { user.roles_label }, header: "Roles"
    column :state, ->(user) { user.human_enum(:state)}, header:"Estado", sort: :state
  end

  controller do
  	def new
      @user = User.new
      @suggested_password = generate_random_string(("A".."Z").to_a) + generate_random_string(("0".."9").to_a)
    end

    def generate_random_string(chars, length=3)
      string = ""
      length.times do
        string << chars[rand(chars.length-1)]
      end
      string
    end
    

  	def show
  		@user = User.find(params[:id])
  		@user.is_approver = @user.is_approver?
  		@user.is_creator = @user.is_creator?
  	end

  	def create
  		@user = User.new(user_params)
  		if @user.save
  			@user.add_role ROLE_APPROVER.to_sym if @user.is_approver=="1"
  			@user.add_role ROLE_CREATOR.to_sym if @user.is_creator=="1"
  			flash[:message] = flash_message("success", message: "Se creó correctamente el usuario #{@user.name}.", title: "Usuario creado")
        redirect_to users_admin_path(@user)
  		else
  			flash[:error_user] = @user.errors.full_messages.first
  			render :new
  		end

  	end

  	def update
  		@user = User.find(params[:id])

  		if @user.update_attributes(user_params)
  			if @user.is_approver?
  				@user.remove_role(ROLE_APPROVER.to_sym) unless @user.is_approver=="1"
  			else
	  			@user.add_role(ROLE_APPROVER.to_sym) if @user.is_approver=="1"
  			end
  			if @user.is_creator?
	  			@user.remove_role(ROLE_CREATOR.to_sym) unless @user.is_creator=="1"
  			else
	  			@user.add_role(ROLE_CREATOR.to_sym) if @user.is_creator=="1"
  			end
  			flash[:message] = flash_message("success", message: "Se actualizó correctamente el usuario #{@user.name}.", title: "Usuario actualizado")
        redirect_to users_admin_path(@user)
  		else
  			flash[:error_user] = @user.errors.full_messages.first
  			render :show
  		end
  	end

    def import
      result = User.import(params[:file])
      if result[:errors].empty?
        flash[:result_hash] = {
          title: "Importación exitosa",
          message: "Se registraron #{result[:users_count]} nuevos usuarios."
        }
        redirect_to users_admin_index_path 
      else
        flash[:error_hash] = 
          { 
            title: result[:error_type]=="invalid_document" ? "Error en el documento" : "Error en la importación. #{result[:users_count]==1 ? "Se registró 1 nuevo usuario" : "Se registraron #{result[:users_count]} nuevos usuarios"} y #{result[:errors].count==1 ? "1 fila presenta error" : "#{result[:errors].count} filas presentan errores:"}",
            message:"<br/> #{result[:errors].uniq.join("<br/>")}"
          }
        redirect_to users_admin_index_path
      end
    end

  	def user_params
      params.require(:user).permit(:name, :first_name, :email, :password, :password_confirmation, :code_user, :is_approver, :is_creator, :show_modal_sidebar)
    end
  end

  routes do
    post :import, on: :collection
  end

end