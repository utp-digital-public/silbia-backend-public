class Unit < ApplicationRecord
  audited associated_with: :syllabus
  belongs_to :syllabus
  has_many :topics, :dependent => :destroy
  has_many :sessions, :dependent => :destroy
  has_many :unit_information_sources, :dependent => :destroy, :inverse_of => :unit
  has_many :syllabus_information_sources, :through => :unit_information_sources
end
