module Util
  require "openssl"
  require "digest"
  class EncryptDecryptAes
    def aes128_encrypt(key, data)
      key = Digest::MD5.digest(key) if(key.kind_of?(String) && 16 != key.bytesize)
      aes = OpenSSL::Cipher.new('AES-128-CBC')
      aes.encrypt
      aes.key = key
      aes.iv =key
      encrypted_text =  aes.update(data) + aes.final
      Base64.encode64(encrypted_text)
    end
   
    # TODO complete decrypt  
    # show error  wrong final block length
    def aes128_decrypt(key, data)
      key = Digest::MD5.digest(key) if(key.kind_of?(String) && 16 != key.bytesize)
      aes = OpenSSL::Cipher.new('AES-128-CBC')
      aes.decrypt
      aes.key = Digest::MD5.digest(key)
      puts "EL KEY ES " + Digest::MD5.digest(key)
      aes.iv = Digest::MD5.digest(key)
      decrypt_text = aes.update(data) + aes.final
      #Base64.decode64(decrypt_text)
    end

  end  
  
end