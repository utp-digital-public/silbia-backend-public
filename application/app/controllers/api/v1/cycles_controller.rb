class Api::V1::CyclesController < Api::ApiV1AdminController
  before_action :set_order_params, only: [:index]
  before_action :set_cycle, only: [:show, :update, :destroy]

  def index
    @cycles = Cycle
    @cycles = @cycles.order(@sort) unless @sort.blank?
  end

  def show
  end

  def update
    can_disable = @cycle.can_disable
    @cycle.assign_attributes(cycle_params)
    if @cycle.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @cycle.errors) and return
    elsif (!@cycle.enabled && !can_disable)
      @cycle.errors.add(:enabled, "No se puede deshabilitar un ciclo habilitado")
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @cycle.errors) and return
    else
      current_job_status = JobStatus.where(:job_name => "export_pdf_collection", :name => "export_pdf_#{@current_user.id.to_s}", :completed => false)
      if current_job_status.present?
        if params[:modality_cycles].present?
          render_bad_request("Aún no se podrá agregar pie de página", "Deberás esperar a que la descarga masiva que se está realizado termine")
        else
          render_bad_request("Aún no se podrá modificar el ciclo", "Deberás esperar a que la descarga masiva que se está realizado termine")
        end
      else
        ApplicationRecord.transaction do
          begin
            @cycle.save!()
            modality_ids_to_delete = []
            new_modalities = params[:modality_cycles] || []
            modality_ids_to_delete = @cycle.modality_ids - new_modalities.map { |mod| mod[:modality_id] }
            @cycle.modality_cycles.where(modality_id: modality_ids_to_delete).destroy_all
            new_modalities.each do |m_cycle|
              mc = ModalityCycle.find_or_create_by(modality_id: m_cycle[:modality_id], cycle_id: @cycle.id)
              mc.footnote = m_cycle[:footnote]
              mc.save
            end
          rescue Exception => e
            render_internal_server_error(e.message)
          end
        end
      end
    end
  end

  def create
    @cycle = Cycle.new(cycle_params)
    if @cycle.invalid?
      render_bad_request(MESSAGE_ERROR_DESCRIPTION, MESSAGE_ERROR_REASON, "", @cycle.errors) and return
    else
      ApplicationRecord.transaction do
        begin
          @cycle.save!()
          new_modalities = params[:modality_cycles] || []
          new_modalities.each do |modality_cycle|
            mc = ModalityCycle.new(modality_id: modality_cycle[:modality_id], cycle_id: @cycle.id, footnote: modality_cycle[:footnote])
            mc.save
          end
        rescue Exception => e
          render_internal_server_error(e.message)
        end
      end
    end
  end

  def destroy
    if @cycle.can_delete
      @cycle.destroy
    else
      render_bad_request("No se puede eliminar un ciclo activo o con silabos") and return
    end
  end

  def list
    @total_courses = ProgramCourse.active.count
    @inactive_cycles = Cycle.inactive
    @active_cycle = Cycle.active
    if @active_cycle.present?
      @nro_syllabus_to_copy = SyllabusProgram.number_of_syllabuses(@active_cycle.id)
    end
  end

  def execute_copy
    id_cycle_from_copy = params[:cycle_from]
    cycle_from = Cycle.where(:id => id_cycle_from_copy)
    @active_cycle = Cycle.active
    current_job_status = JobStatus.where(:name => "copy_syllabus", :completed => false)
    if current_job_status.present?
      render_bad_request("Hay un proceso de copia de silabos en curso")
    elsif @active_cycle.present?
      if SyllabusProgram.number_of_syllabuses(@active_cycle.id) <= 0
        if cycle_from.present?
          count_syllabus_copy = SyllabusProgram.number_of_syllabuses(id_cycle_from_copy)
          if count_syllabus_copy > 0
            @job_status = JobStatus.create!(name: "copy_syllabus", max_progress: count_syllabus_copy, message: "Copia de Silabos #{id_cycle_from_copy} a #{@active_cycle.id}")
            syllabus_program_service = SyllabusProgramService.new
            syllabus_program_service.delay.copy_syllabus_masive(id_cycle_from_copy, @active_cycle.id, @job_status.id)
          else
            render_bad_request("El ciclo base no tiene silabos")
          end
        else
          render_bad_request("El ciclo base no existe")
        end
      else
        render_bad_request("No se puede copiar a un ciclo con silabos existentes")
      end
    else
      render_bad_request("No hay un ciclo activo")
    end
  end

  def job_status
    @job_status = JobStatus.where(:name => "copy_syllabus").last
  end

  private

  def set_order_params
    order = if params[:order].present? then params[:order] else "asc" end
    @sort = if params[:sort].present? then params[:sort] + " " + order else "cycle_start desc" end
  end

  def set_cycle
    @cycle = Cycle.find(params[:id])
  end

  def cycle_params
    params.permit(:name, :cycle_start, :cycle_end, :enabled, :type_cycle)
  end
end
