class CreateProgramCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :program_courses do |t|
    	t.integer :weekly_hours, default: 0
    	t.integer :credits, default: 0
    	t.string :catalog_code, null:false
    	t.string :name, null: false
      t.string :type, null: false
      t.references :course, foreign_key: true
      t.references :coordinator, foreign_key: {to_table: :users}
      t.references :supervisor, foreign_key: {to_table: :users}
      t.timestamps
    end
    Course.all.each do |course|
      syllabus = Syllabus.where(course_id: course.id).last
      if syllabus.present?
        course_names = Course.where(id: course.id).pluck("name, name_cgt, catalog_code, catalog_code_cgt").first
        syllabus_in_person = syllabus.syllabus_programs.where(type_program: 1).first
        syllabus_cgt = syllabus.syllabus_programs.where(type_program: 2).first
        begin
          pc_preg = ProgramCourse.create!(weekly_hours: syllabus.weekly_hours, credits: syllabus.credits, name: course_names[0], catalog_code: course_names[2], type: 'InPerson', course_id: course.id, coordinator_id: syllabus_in_person.coordinator_id, supervisor_id: syllabus_in_person.supervisor_id)
          pc_cgt = ProgramCourse.create!(weekly_hours: syllabus.weekly_hours, credits: syllabus.credits, name: course_names[1].blank? ? course_names[0] : course_names[1], catalog_code: course_names[3], type: 'Blended', course_id: course.id, coordinator_id: syllabus_cgt.coordinator_id, supervisor_id: syllabus_cgt.supervisor_id) unless course_names[3].blank? || syllabus_cgt.nil?
        rescue Exception => e
          p "#{e.message}"
        end
      end
    end
  end
end
