class ProgramCourse < ApplicationRecord
  validates :catalog_code, :name, :weekly_hours, :credits, presence: { message: Proc.new { |prog_course, data|
                                                   "#{prog_course.code_type} es requerido."
                                                 } }
  validates :catalog_code, format: { :with => /\A[A-Z0-9]{10}\z/i, message: Proc.new { |prog_course, data|
                   "#{prog_course.code_type} debe contener 10 caracteres entre mayúsculas y números."
                 } }
  belongs_to :coordinator, class_name: "User", foreign_key: "coordinator_id"

  belongs_to :supervisor, class_name: "User", foreign_key: "supervisor_id", optional: true
  belongs_to :course
  scope :active, -> { where(deleted: false) }
  validates :catalog_code, uniqueness: { case_sensitive: false, message: "El código ya está siendo usado.", scope: :deleted }
  validates :credits, :numericality => { :only_integer => true, :greater_than => 0, :less_than_or_equal_to => 18, message: "Debe ser un entre 1 y 18." }
  validates :weekly_hours, :numericality => { :only_integer => true, :greater_than => 0, :less_than_or_equal_to => 30, message: "Debe estar entre 1 y 30" }
  validates :modality_id, uniqueness: { scope: [:course_id, :modality_id], message: "Ya existe el curso en esta modalidad" }
  after_update :update_current_syllabus_program_users
  has_many :information_sources, through: :course
  belongs_to :modality
  has_many :program_course_careers, :dependent => :destroy, :inverse_of => :program_course
  has_many :careers, through: :program_course_careers
  has_many :course_general_competitions, :dependent => :destroy, :inverse_of => :program_course
  has_many :general_competitions, through: :course_general_competitions
  has_many :course_specific_competitions, through: :program_course_careers
  has_many :specific_competitions, through: :course_specific_competitions
  attr_accessor :book_complementary_ids
  attr_accessor :book_basic_ids

  def human_type
    if type == "Blended"
      "CGT"
    else
      "Pregrado"
    end
  end

  def code_type
    if type == "Blended"
      "CGT"
    else
      "PREG"
    end
  end

  def self.getArrCourseTypes
    [
      { label: "Semi presencial", value: "Blended" },
      { label: "Presencial", value: "InPerson" },
    ]
  end

  def coordinator_name
    if self.coordinator_id?
      self.coordinator.name
    else
      ""
    end
  end

  def supervisor_name
    if self.supervisor_id?
      self.supervisor.name
    else
      ""
    end
  end

  def type_careers
    c_type = type == "Blended" ? :blended : :in_person
    careers.where(modality: c_type)
  end

  def is_base_course
    similar_courses = course.program_courses
    course_deleted = similar_courses.select { |c| c["deleted"] == true && c["modality_id"] == PRESENCIAL_CODE }.first
    if modality_id == VIRTUAL_CODE && course_deleted.present?
      true
    elsif modality_id == PRESENCIAL_CODE && !course_deleted.present?
      true
    else
      false
    end
  end

  def base_course
    similar_courses = course.program_courses
    course_deleted = similar_courses.select { |c| c["deleted"] == true && c["modality_id"] == PRESENCIAL_CODE }.first

    if (course_deleted.present?)
      return similar_courses.select { |c| c["modality_id"] == VIRTUAL_CODE }.first
    else
      return similar_courses.select { |c| c["modality_id"] == PRESENCIAL_CODE }.first
    end
  end

  # def is_base_course
  #   similar_courses = course.program_courses
  #   course_deleted = similar_courses.select { |c| c["deleted"] == true && c["modality_id"] == PRESENCIAL_CODE }.first
  #   if modality_id == VIRTUAL_CODE && similar_courses.count < 3 && course_deleted.present?
  #     true
  #   elsif modality_id == PRESENCIAL_CODE && similar_courses.count < 3
  #     true
  #   else
  #     false
  #   end
  # end

  def book_basic_ids
    Book.where(id: course.information_sources.basic.map { |ic| ic.book_id })
  end

  def self.search_course_by_catalog_code(catalog_code)
    catalog_code = catalog_code.strip
    course = ProgramCourse.where("catalog_code =?", catalog_code).first
    return nil unless course.present?
    course
  end

  def update_current_syllabus_program_users
    if self.saved_change_to_supervisor_id? or self.saved_change_to_coordinator_id? or self.saved_change_to_authorized?
      syllabus_program = get_current_syllabus_program
      unless syllabus_program.nil?
        syllabus_program.supervisor_id = self.supervisor_id
        syllabus_program.coordinator_id = self.coordinator_id
        syllabus_program.authorized = self.authorized
        syllabus_program.save
      end
    end
  end

  def get_current_syllabus_program
    current_syllabus = course.syllabuses.where(cycle_id: Cycle.active.id).first
    return nil if current_syllabus.nil?
    current_syllabus_program = current_syllabus.syllabus_programs.where(modality_id: modality_id).first
    current_syllabus_program
  end

  def have_competitions
    if general_competitions.length > 0 || specific_competitions.length > 0
      return true
    else
      return false
    end
  end

  def specific_competitions_grouped
    return specific_competitions.sort_by { |s| s.career.name }.group_by { |s| s.career.name }.transform_values { |v| v.pluck(:name).sort }.map { |k, v| { career: k, competitions: v } }
  end
end
