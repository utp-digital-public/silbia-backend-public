require 'csv'
### Migrate Practices from csv file  ################################################
csv_text = File.read(Rails.root.join('lib', 'seeds_csv', 'careers.csv'))
#csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv = CSV.parse(csv_text.scrub, headers: true)
csv.each do |row|
  obj = Career.new
  obj.code = row[0]
  obj.modality = row['modality']=="PRESENCIAL" ? 0 : 1
  p "#{obj.inspect}"
  obj.name = row['name']
  obj.save!
  puts "#{obj.code}, #{obj.name} saved"
end
puts "There are now #{Career.count} rows in the transactions table"
