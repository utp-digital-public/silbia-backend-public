class AddParentToComponent < ActiveRecord::Migration[5.2]
  def change
    add_column :components, :parent_id, :integer
  end
end
