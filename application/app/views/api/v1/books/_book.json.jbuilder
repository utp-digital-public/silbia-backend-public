json.id book.id
json.title book.title
json.author book.author
json.label book.label_for_tag
json.support book.support
json.year book.year
json.number_editorial book.number_editorial
json.editorial book.editorial
