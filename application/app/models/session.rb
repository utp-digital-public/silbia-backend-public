class Session < ApplicationRecord
  audited associated_with: :syllabus_program
  belongs_to :laboratory
  belongs_to :syllabus_program
  belongs_to :unit
  has_many :session_topics, :dependent => :destroy
  before_save :set_defaults

  def set_defaults
    self.modality_id ||= 1
  end
end
